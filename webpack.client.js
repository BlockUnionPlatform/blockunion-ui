const webpack = require('webpack');
const path = require('path');
const Dotenv = require('dotenv-webpack');

const APP_DIR = path.resolve(__dirname, 'src/client');
const PUBLIC_DIR = path.resolve(__dirname, 'dist');

const merge = require('webpack-merge');
const baseConfig = require('./webpack.base.config');

const config = {
    entry: APP_DIR + '/index.js',
    //devtool: 'eval',
    //mode: 'production',
    mode: 'development',

    devServer: {
        contentBase: PUBLIC_DIR,
        port: 3030,
        open: true,
        historyApiFallback: true
    },

    output: {
        path: PUBLIC_DIR,
        filename: 'bundle.client.js'
    },
    plugins: [
        new Dotenv(),
        /*new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        })*/
    ]
};


module.exports = merge(baseConfig, config);