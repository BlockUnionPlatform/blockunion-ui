const express = require('express');
const app = express();

app.use(express.static('dist', { index: false }));

import renderer from './renderer';
app.use('*', (req, res) => {
    const rendererInstance = renderer(req);
    console.log(req);
    if (rendererInstance.routestatus == 404) {
        res.status(404).end('Error 404. Requested resource not found!');
    } else {
        res.send(renderer(req).htmlcode);
    }
});

app.listen(3040, function() {
    console.log('Listening on port 3040');
});