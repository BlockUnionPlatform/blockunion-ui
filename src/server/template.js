export default (body, title) => {
    return `
   <!DOCTYPE html>
<html lang="en">
    <head>
        <title>Block Union - Discover, Rate, Earn.</title>
        <meta charset="UTF-8" />
        <meta name="HandheldFriendly" content="true">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <base href="/" />

        <link type="text/css" rel="stylesheet" href="assets/css/bootstrap.min.css" />

        <link type="image/x-icon" rel="shortcut icon" href="assets/rating/images/favicon.png" />
        <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css' />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link type="text/css" rel="stylesheet" href="assets/member/css/jquery.mCustomScrollbar.min.css" />
        <link type="text/css" rel="stylesheet" href="assets/member/css/ie.css" />
        <link type="text/css" rel="stylesheet" href="assets/member/css/owl.carousel.css" />
        
        <script type="text/javascript" src="assets/member/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/member/js/html5.js"></script>
        <script type="text/javascript" src="assets/member/js/respond.js"></script>
        <!-- <script type="text/javascript" src="assets/member/js/jquery.mCustomScrollbar.min.js"></script> -->
        <script type="text/javascript" src="assets/member/js/owl.carousel.js"></script>
        <script type="text/javascript" src="assets/member/js/newsletter.js"></script>
        <script type="text/javascript" src="assets/member/js/member.js"></script>

        <script type="text/javascript" src="assets/rating/js/jquery.fancybox.min.js" ></script>
        <script type="text/javascript" src="assets/rating/js/contact-us-enquiry.js" ></script>
        <script type="text/javascript" src="assets/rating/js/newsletter.js"></script>
        <script type="text/javascript" src="assets/rating/js/rating.js"></script>
    </head>

    <body class="member">
        <div id="root"></div>
        <script type="text/javascript" src="bundle.client.js"></script>
        <link type="text/css" rel="stylesheet" href="assets/css/app.css" />
    </body>
</html>
  `;
};
