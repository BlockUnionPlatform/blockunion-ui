import React from 'react';
import {renderToString} from 'react-dom/server';
import {StaticRouter as Router} from 'react-router-dom';

import FullPage from '../client/Components/common/FullPage';
import template from './template';

export default (req) => {
    let context = {};
    const content = renderToString(
        <Router location={req.path} context={context}>
            <FullPage />
        </Router>
    );

    return {
        htmlcode: template(content, 'BlockUnion'),
        routestatus: context.status
    }
};