let Repository = {
    Tokens: [
        {
            id: 1,
            name: 'BlockUnion',
            symbol: 'BKU',
            isActive: true,
            guid: 'e5890596-2e68-40a1-9d77-ba1f09f33464'
        },

        {
            id: 2,
            name: 'FortKnoxer',
            symbol: 'FKX',
            isActive: true,
            guid: 'cc2e1b71-3fa5-4683-97ec-64cff6fee232'
        },

        {
            id: 3,
            name: 'XTrade',
            symbol: 'XRBT',
            isActive: true,
            guid: 'fcb757e1-b885-48af-99b0-962cbb96bd2e'
        },

        {
            id: 4,
            name: 'HumzaKhan',
            symbol: 'HK',
            isActive: true,
            guid: '16d668fc-670c-4852-8799-e8707ad22920'
        }
    ],
    SystemLogs: [
        {
            id: 1,
            level: 'Debug',
            shortMessage: 'Initiating Backup #24',
            longMessage: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Condimentum id venenatis a condimentum vitae sapien pellentesque habitant morbi. Fermentum et sollicitudin ac orci phasellus egestas tellus rutrum tellus. Pharetra convallis posuere morbi leo urna molestie at. Euismod lacinia at quis risus sed vulputate.',
            logger: 'BackupService.cs',
            datetime: 'Sept 1, 2018 8:00 AM'
        },

        {
            id: 2,
            level: 'Error',
            shortMessage: 'Unable to find the specified user',
            longMessage: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Condimentum id venenatis a condimentum vitae sapien pellentesque habitant morbi. Fermentum et sollicitudin ac orci phasellus egestas tellus rutrum tellus. Pharetra convallis posuere morbi leo urna molestie at. Euismod lacinia at quis risus sed vulputate.',
            logger: 'UserService.cs',
            datetime: 'Sept 1, 2018 8:00 AM'
        },

        {
            id: 3,
            level: 'Warning',
            shortMessage: 'Remote storage service did not respond.',
            longMessage: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Condimentum id venenatis a condimentum vitae sapien pellentesque habitant morbi. Fermentum et sollicitudin ac orci phasellus egestas tellus rutrum tellus. Pharetra convallis posuere morbi leo urna molestie at. Euismod lacinia at quis risus sed vulputate.',
            logger: 'RemoteConnection.cs',
            datetime: 'Sept 1, 2018 8:00 AM'
        }
    ],
    ActivityLogs: [
        {
            id: '78f8204d-f127-401b-9e08-0bfe4b7532c1',
            type: 'user.login',
            user: 'Humza Khan',
            userId: 'sadasdsadsadasdasd',
            comments: 'User logged in',
            datetime: 'Sept 1, 2018 5:05 AM UTC +0'
        },

        {
            id: 'd7f29ac5-e5ba-41c3-9024-faa853a5b5d3',
            type: 'block.view',
            user: 'Humza Khan',
            userId: 'sadasdsadsadasdasd',
            comments: '',
            datetime: 'Sept 1, 2018 5:05 AM UTC +0'
        },

        {
            id: '1cb14635-e160-4f2d-9a26-19393d845251',
            type: 'profile.update',
            user: 'Humza Khan',
            userId: 'sadasdsadsadasdasd',
            comments: '',
            datetime: 'Sept 1, 2018 5:05 AM UTC +0'
        }
    ],
    Users: [
        {
            id: 1,
            name: 'User 1',
            email: 'user1@blockunion.io',
            phone: '+923008899556',
            guid: '3eae679a-edbf-42aa-a88f-e6637e0531a9',
            isActive: true
        },

        {
            id: 2,
            name: 'User 2',
            email: 'user2@blockunion.io',
            phone: '+923008899556',
            guid: '6f3b1ad9-8c99-4f74-8b19-c60855ee1412',
            isActive: true
        },

        {
            id: 3,
            name: 'User 3',
            email: 'user3@blockunion.io',
            phone: '+923008899556',
            guid: '45ef5042-8007-42cb-b669-9428b0e86859',
            isActive: true
        },

        {
            id: 4,
            name: 'User 4',
            email: 'user4@blockunion.io',
            phone: '+923008899556',
            guid: 'e2492762-2f0e-4cea-84f7-11471bb8f517',
            isActive: true
        },

        {
            id: 5,
            name: 'User 5',
            email: 'user5@blockunion.io',
            phone: '+923008899556',
            guid: '629b28ed-7542-491b-a111-c5b1baaffb49',
            isActive: true
        }
    ],
    Rewards: [
        {
            id: 1,
            value: 2.5,
            isActive: true,
            guid: 'e5890596-2e68-40a1-9d77-ba1f09f33464',
            token: {
                id: 1,
                name: 'BlockUnion',
                symbol: 'BKU',
                isActive: true,
                guid: 'e5890596-2e68-40a1-9d77-ba1f09f33464'
            }
        },

        {
            id: 2,
            value: 10,
            isActive: true,
            guid: 'cc2e1b71-3fa5-4683-97ec-64cff6fee2324',
            token: {
                id: 2,
                name: 'FortKnoxer',
                symbol: 'FKX',
                isActive: true,
                guid: 'cc2e1b71-3fa5-4683-97ec-64cff6fee232'
            }
        },

        {
            id: 3,
            value: 25,
            isActive: true,
            guid: 'fcb757e1-b885-48af-99b0-962cbb96bd2e\'',
            token: {
                id: 3,
                name: 'XTrade',
                symbol: 'XRBT',
                isActive: true,
                guid: 'fcb757e1-b885-48af-99b0-962cbb96bd2e'
            }
        }
    ],
    RewardClaims: [
        {
            id: 1,
            guid: 'b7b45ee3-d9a0-485b-a13e-c23806b4a811',
            user:  {
                id: 1,
                name: 'User 1',
                email: 'user1@blockunion.io',
                phone: '+923008899556',
                guid: '3eae679a-edbf-42aa-a88f-e6637e0531a9',
                isActive: true
            },
            reward: {
                id: 1,
                value: 2.5,
                isActive: true,
                guid: 'e5890596-2e68-40a1-9d77-ba1f09f33464',
                token: {
                    id: 1,
                    name: 'BlockUnion',
                    symbol: 'BKU',
                    isActive: true,
                    guid: 'e5890596-2e68-40a1-9d77-ba1f09f33464'
                }
            },
            comments: 'comments goes here',
            dateCreated: 'Sept 14, 2018'
        },

        {
            id: 2,
            guid: '703605db-9b21-4915-a3a0-3aa8b28b0c05',
            user:  {
                id: 2,
                name: 'User 2',
                email: 'user2@blockunion.io',
                phone: '+923008899556',
                guid: '6f3b1ad9-8c99-4f74-8b19-c60855ee1412',
                isActive: true
            },
            reward: {
                id: 2,
                value: 10,
                isActive: true,
                guid: 'cc2e1b71-3fa5-4683-97ec-64cff6fee2324',
                token: {
                    id: 2,
                    name: 'FortKnoxer',
                    symbol: 'FKX',
                    isActive: true,
                    guid: 'cc2e1b71-3fa5-4683-97ec-64cff6fee232'
                }
            },
            comments: 'comments goes here',
            dateCreated: 'Sept 15, 2018'
        }
    ],
    Payouts: [
        {
            id: 1,
            guid: 'f120a550-b2f8-40f5-89d6-e4e77a54f7b3',
            user: {
                id: 1,
                name: 'User 1',
                email: 'user1@blockunion.io',
                phone: '+923008899556',
                guid: '3eae679a-edbf-42aa-a88f-e6637e0531a9',
                isActive: true
            },

            admin: {
                id: 2,
                name: 'User 2',
                email: 'user2@blockunion.io',
                phone: '+923008899556',
                guid: '6f3b1ad9-8c99-4f74-8b19-c60855ee1412',
                isActive: true
            },

            reward: {
                id: 2,
                value: 10,
                isActive: true,
                guid: 'cc2e1b71-3fa5-4683-97ec-64cff6fee2324',
                token: {
                    id: 2,
                    name: 'FortKnoxer',
                    symbol: 'FKX',
                    isActive: true,
                    guid: 'cc2e1b71-3fa5-4683-97ec-64cff6fee232'
                }
            },

            comments: 'random comments goes here',
            dateCreated: 'Sept 10, 2018 5:05 PM'
        },
        {
            id: 2,
            guid: '66604628-4c55-4230-8d57-4b989a2014f2',
            admin: {
                id: 1,
                name: 'User 1',
                email: 'user1@blockunion.io',
                phone: '+923008899556',
                guid: '3eae679a-edbf-42aa-a88f-e6637e0531a9',
                isActive: true
            },

            user: {
                id: 2,
                name: 'User 2',
                email: 'user2@blockunion.io',
                phone: '+923008899556',
                guid: '6f3b1ad9-8c99-4f74-8b19-c60855ee1412',
                isActive: true
            },

            reward: {
                id: 1,
                value: 2.5,
                isActive: true,
                guid: 'e5890596-2e68-40a1-9d77-ba1f09f33464',
                token: {
                    id: 1,
                    name: 'BlockUnion',
                    symbol: 'BKU',
                    isActive: true,
                    guid: 'e5890596-2e68-40a1-9d77-ba1f09f33464'
                }
            },

            comments: 'random comments goes here',
            dateCreated: 'Sept 14, 2018 5:05 PM'
        }
    ],
    DailyActions: [
        {
            id: 1,
            guid: 'c253eaf2-da7f-4748-9b8d-c8ddb0281115',
            shortTitle: 'Daily Action Bonus BKU',
            longTitle: 'Daily Action Bonus BKU',
            description: 'Complete this simple task to earn the reward',
            validTill: 'Sept 20, 2018 12:00 AM',
            isActive: true,
            logo: 'assets/member/images/earn1.png',
            reward: {
                id: 1,
                value: 2.5,
                isActive: true,
                guid: 'e5890596-2e68-40a1-9d77-ba1f09f33464',
                token: {
                    id: 1,
                    name: 'BlockUnion',
                    symbol: 'BKU',
                    isActive: true,
                    guid: 'e5890596-2e68-40a1-9d77-ba1f09f33464'
                }
            }
        },

        {
            id: 2,
            guid: '716746c3-1b93-428b-86e5-042c2b1877ca',
            shortTitle: 'Daily Action Bonus BKU',
            description: 'Fill in the form to earn',
            validTill: 'Sept 18, 2018 1:00 PM',
            isActive: true,
            logo: 'assets/member/images/earn2.png',
            reward: {
                id: 2,
                value: 10,
                isActive: true,
                guid: 'cc2e1b71-3fa5-4683-97ec-64cff6fee2324',
                token: {
                    id: 2,
                    name: 'FortKnoxer',
                    symbol: 'FKX',
                    isActive: true,
                    guid: 'cc2e1b71-3fa5-4683-97ec-64cff6fee232'
                }
            }
        },

        {
            id: 3,
            guid: 'af56d768-3dc2-4b62-9b33-0bfca17319f3',
            shortTitle: 'Daily Action Bonus BKU',
            description: 'Register and join the Slack channel for rewards.',
            validTill: 'Sept 15, 2018 3:30 PM',
            isActive: true,
            logo: 'assets/member/images/earn3.png',
            reward: {
                id: 3,
                value: 25,
                isActive: true,
                guid: 'fcb757e1-b885-48af-99b0-962cbb96bd2e\'',
                token: {
                    id: 3,
                    name: 'XTrade',
                    symbol: 'XRBT',
                    isActive: true,
                    guid: 'fcb757e1-b885-48af-99b0-962cbb96bd2e'
                }
            }
        }
    ],
    Referrals: [
        {
            id: '81c13a3d-a6ab-48b7-9ab0-1f7ae95c4610',
            referral: {
                url: 'ZWTGXLXNMX',
                user: {
                    name: 'User 2',
                    email: 'user2@blockunion.io',
                    phone: '+923008899556',
                    id: '6f3b1ad9-8c99-4f74-8b19-c60855ee1412',
                    isActive: true
                }
            },
            referred: {
                name: 'User 1',
                email: 'user1@blockunion.io',
                phone: '+923008899556',
                id: '3eae679a-edbf-42aa-a88f-e6637e0531a9',
                isActive: true
            },
            dateCreatedUtc: '9/20/2018 3:42:59 AM'
        },
        {
            id: '39edaabe-fd14-4c1d-9765-e73dd603e2fb',
            referral: {
                url: 'NRGGJGUZQQ',
                user: {
                    name: 'User 1',
                    email: 'user1@blockunion.io',
                    phone: '+923008899556',
                    id: '3eae679a-edbf-42aa-a88f-e6637e0531a9',
                    isActive: true
                }
            },
            referred: {
                name: 'User 2',
                email: 'user2@blockunion.io',
                phone: '+923008899556',
                id: '6f3b1ad9-8c99-4f74-8b19-c60855ee1412',
                isActive: true
            },
            dateCreatedUtc: '9/20/2018 3:42:59 AM'
        },
        {
            id: 'b521c757-bc43-41ec-b43b-a4c7dc9880a9',
            referral: {
                url: 'YEROZUWYTP',
                user: {
                    name: 'User 1',
                    email: 'user1@blockunion.io',
                    phone: '+923008899556',
                    gd: '3eae679a-edbf-42aa-a88f-e6637e0531a9',
                    isActive: true
                }
            },
            referred: {
                name: 'User 3',
                email: 'user3@blockunion.io',
                phone: '+923008899556',
                id: '45ef5042-8007-42cb-b669-9428b0e86859',
                isActive: true
            },
            dateCreatedUtc: '9/20/2018 3:42:59 AM'
        }
    ],
    Packages: [
        {
            id: 'd6ff3e64-d02f-4ce5-bef5-e1e882466e62',
            title: 'Default',
            description: 'Default Package',
            token: {
                id: 1,
                name: 'BlockUnion',
                symbol: 'BKU',
                isActive: true,
                guid: 'e5890596-2e68-40a1-9d77-ba1f09f33464'
            },
            recurring: 'Monthly',
            value: 0,
            color: '#1f6565',
            logo: '',
            isActive: true
        },
        {
            id: '52f54086-246a-42bd-ad17-2a2130f6e344',
            title: 'Gold',
            description: 'Gold Package',
            token: {
                id: 1,
                name: 'BlockUnion',
                symbol: 'BKU',
                isActive: true,
                guid: 'e5890596-2e68-40a1-9d77-ba1f09f33464'
            },
            recurring: 'Monthly',
            value: 100,
            color: '#f7d460',
            logo: '',
            isActive: true
        },

        {
            id: '631e9424-3d56-47a8-bf4a-8aac40266045',
            title: 'Platinum',
            description: 'Platinum Package',
            token: {
                id: 1,
                name: 'BlockUnion',
                symbol: 'BKU',
                isActive: true,
                guid: 'e5890596-2e68-40a1-9d77-ba1f09f33464'
            },
            recurring: 'Monthly',
            value: 100,
            color: '#414141',
            logo: '',
            isActive: true
        },

        {
            id: 'cf3a1aab-a69d-4c04-a14a-52ef4368f127',
            title: 'Silver',
            description: 'Silver Package',
            token: {
                id: 1,
                name: 'BlockUnion',
                symbol: 'BKU',
                isActive: true,
                guid: 'e5890596-2e68-40a1-9d77-ba1f09f33464'
            },
            recurring: 'Monthly',
            value: 100,
            color: '#e0dfde',
            logo: '',
            isActive: true
        }
    ],
    PackageTypes: [
        'Monthly',
        'Weekly',
        'Quarterly',
        'Yearly'
    ],
    BlockAccounts: [
        {
            id: 'a14022e8-7fe2-4a87-9afe-f65be4c8d7d2',
            title: 'Default HK Account',
            description: 'The default account to hold tokens for HK token listing.',
            token: {
                id: '16d668fc-670c-4852-8799-e8707ad22920',
                name: 'HumzaKhan',
                symbol: 'HK',
                isActive: true
            },
            isActive: true,
            balance: 15250.00,
            dateCreated: '9/22/2018 3:08:18 PM'
        },

        {
            id: '12aba424-ccbb-497f-9f20-1b153641b568',
            title: 'BKU Core Holdings',
            description: 'Main BKU token holdings account',
            token: {
                id: 'e5890596-2e68-40a1-9d77-ba1f09f33464',
                name: 'BlockUnion',
                symbol: 'BKU',
                isActive: true
            },
            isActive: true,
            balance: 75000.00,
            dateCreated: '9/22/2018 3:08:18 PM'
        },

        {
            id: 'ad5e4935-f836-443d-8189-435ad8669001',
            title: 'FKX Main Account',
            description: 'The main account to record FKX transactions',
            token: {
                id: 'cc2e1b71-3fa5-4683-97ec-64cff6fee232',
                name: 'FortKnoxer',
                symbol: 'FKX',
                isActive: true
            },
            isActive: true,
            balance: 23150.00,
            dateCreated: '9/22/2018 3:08:18 PM'
        }
    ],
    Transactions: [
        {
            id: 'd9fa044a-1eab-43bc-96a3-413ff043ee31',
            title: 'Payout to user #589',
            description: '',
            amount: 10,
            user: {
                id: '6f3b1ad9-8c99-4f74-8b19-c60855ee1412',
                name: 'User 2',
                email: 'user2@blockunion.io',
                phone: '+923008899556',
                isActive: true
            },
            admin: {
                id: '629b28ed-7542-491b-a111-c5b1baaffb49',
                name: 'User 5',
                email: 'user5@blockunion.io',
                phone: '+923008899556',
                isActive: true
            },
            blockAccount: {
                id: 'ad5e4935-f836-443d-8189-435ad8669001',
                title: 'FKX Main Account',
                description: 'The main account to record FKX transactions',
                token: {
                    id: 'cc2e1b71-3fa5-4683-97ec-64cff6fee232',
                    name: 'FortKnoxer',
                    symbol: 'FKX',
                    isActive: true
                },
                isActive: true,
                balance: 23150.00,
                dateCreated: '9/22/2018 3:08:18 PM'
            },
            type: 'Payout',
            dateCreatedUtc: '9/22/2018 7:47:40 PM'
        },

        {
            id: '9b0b1d20-974a-4bd2-8af3-73fe60926368',
            title: 'Tokens received for listing',
            description: '',
            amount: 5000,
            user: null,
            admin: {
                id: '629b28ed-7542-491b-a111-c5b1baaffb49',
                name: 'User 5',
                email: 'user5@blockunion.io',
                phone: '+923008899556',
                isActive: true
            },
            blockAccount: {
                id: 'a14022e8-7fe2-4a87-9afe-f65be4c8d7d2',
                title: 'Default HK Account',
                description: 'The default account to hold tokens for HK token listing.',
                token: {
                    id: '16d668fc-670c-4852-8799-e8707ad22920',
                    name: 'HumzaKhan',
                    symbol: 'HK',
                    isActive: true
                },
                isActive: true,
                balance: 15250.00,
                dateCreated: '9/22/2018 3:08:18 PM'
            },
            type: 'Deposit',
            dateCreatedUtc: '9/22/2018 7:47:40 PM'
        }
    ],
    TransactionTypes: [
        'Withdrawl',
        'Deposit',
        'Payout'
    ],
    BlockClaims: [
        {
            id: 'adf03f9b-a45c-405a-bc49-a6893c12f938',
            notes: '',
            dateCreatedUtc: '9/22/2018 3:08:18 PM',
            isProcessed: false,
            user: {
                id: '629b28ed-7542-491b-a111-c5b1baaffb49',
                name: 'User 5',
                email: 'user5@blockunion.io',
                phone: '+923008899556',
                isActive: true
            },
            admin: null,
            block: {
                id: '7642941e-3f2e-4615-82dd-d05a9f0684f1',
                name: 'Business Name'
            }
        },

        {
            id: '3b9273cd-760a-4dfb-a5da-b27c10739498',
            notes: '',
            dateCreatedUtc: '9/22/2018 3:08:18 PM',
            isProcessed: true,
            user: {
                id: '629b28ed-7542-491b-a111-c5b1baaffb49',
                name: 'User 5',
                email: 'user5@blockunion.io',
                phone: '+923008899556',
                isActive: true
            },
            admin: {
                id: '45ef5042-8007-42cb-b669-9428b0e86859',
                name: 'User 3',
                email: 'user3@blockunion.io',
                phone: '+923008899556',
                isActive: true
            },
            block: {
                id: 'e7658221-92c4-48d3-9702-eea6dbe5e0dd',
                name: 'Business Name'
            }
        }
    ],
    Blocks: [
        {
            id: 'e9ed740d-57a4-42ba-8b22-fc50e00b9e44',
            businessName: 'STAYGE',
            isApproved: true,
            admin: {
                id: '3eae679a-edbf-42aa-a88f-e6637e0531a9',
                name: 'User 1',
                email: 'user1@blockunion.io',
                phone: '+923008899556',
                isActive: true
            },
            websiteUrl: 'https://stayge.io/',
            emailAddress: 'info@stayge.io',
            authorName: 'Humza Khan',
            isAuthorInvolved: false,
            category: 'Airdrops',
            hasToken: true,
            tokenSymbol: 'STG',
            aboutBusiness: 'STAYGE is a vertical blockchain platform specially designed and developed for the entertainment industry. STAYGE rewards fans’ contribution with cryptocurrency (tokens) based on the incentivized fan community platform.\n' +
                '\n' +
                'STAYGE is airdropping 3800 STG tokens to their community members. Join their Telegram group, follow them on Twitter, like their Facebook page, subscribe to their mailing list and submit your details to the Telegram bot to receive the tokens. Also get 200 STG for every referral.',
            legalName: 'STAYGE LLC',
            legalAddress: 'FastFive #510, 161, Yanghwa-ro,\n' +
                'Mapo-gu, Seoul, Republic of Korea',
            originCountry: 'Republic of Korea',
            whitepaper: 'https://airdrops.io/visit/n051/',
            logo: 'https://airdrops.io/wp-content/uploads/2018/09/STAYGE-logo.jpg',
            logoToken: 'https://airdrops.io/wp-content/uploads/2018/09/STAYGE-logo.jpg',
            package: {
                id: 'd6ff3e64-d02f-4ce5-bef5-e1e882466e62',
                title: 'Default',
                description: 'Default Package',
                token: {
                    id: 1,
                    name: 'BlockUnion',
                    symbol: 'BKU',
                    isActive: true,
                    guid: 'e5890596-2e68-40a1-9d77-ba1f09f33464'
                },
                recurring: 'Monthly',
                value: 50,
                color: '#1f6565',
                logo: '',
                isActive: true
            },
            social: {
                id: 'e1c93f8e-6792-4f0a-9d6a-abc153041a57',
                facebook: 'https://fb.me/',
                twitter: 'https://t.co/',
                telegram: 'https://telegram.com/',
                medium: 'https://medium.com/',
                bitcoinTalkForum: '',
                instagram: 'https://instagram.com'
            },
            ico: {
                id: 'ec33432c-1da1-4a65-ad55-2f5b92a6f6fb',
                privateSaleStart: '',
                privateSaleEnd: '',
                preSaleIcoStart: '',
                preSaleIcoEnd: '',
                publicIcoStart: '',
                publicIcoEnd: '',
                platformTokenType: 'ETH',
                pricePerToken: '$1.52',
            },
            votes: {}
        },
        {
            id: 'abe81b43-5c2b-41f5-ba02-8e3ac7a21c80',
            businessName: 'AIVON',
            isApproved: false,
            admin: null,
            websiteUrl: 'https://aivon.io/',
            emailAddress: 'info@aivon.io',
            authorName: 'Humza Khan',
            isAuthorInvolved: true,
            category: 'Airdrops',
            hasToken: true,
            tokenSymbol: 'AVO',
            aboutBusiness: 'AIVON is a decentralized blockchain platform and protocol built on an Artificial Intelligence (AI) network and a community of human experts working together to generate normalized and enhanced metadata for video content.\n' +
                '\n' +
                'AIVON is airdropping 44 AVO (~ $7) to max 10,000 participants. Participate in our exclusive airdrop and complete all required tasks to receive the tokens. Also get 31 AVO (~ $5) for every referral. You can invite unlimited friends.',
            legalName: 'AIVON LLC',
            legalAddress: '',
            originCountry: 'United Kingdom',
            whitepaper: 'https://aivon.io/whitepaper/',
            logo: 'https://airdrops.io/wp-content/uploads/2018/09/AIVON-logo.jpg',
            logoToken: 'https://airdrops.io/wp-content/uploads/2018/09/AIVON-logo.jpg',
            package: {
                id: '52f54086-246a-42bd-ad17-2a2130f6e344',
                title: 'Gold',
                description: 'Gold Package',
                token: {
                    id: 1,
                    name: 'BlockUnion',
                    symbol: 'BKU',
                    isActive: true,
                    guid: 'e5890596-2e68-40a1-9d77-ba1f09f33464'
                },
                recurring: 'Monthly',
                value: 100,
                color: '#f7d460',
                logo: '',
                isActive: true
            },
            social: {
                id: 'eab7d893-369a-4636-94b5-39bb67676cf6',
                facebook: 'https://fb.me/',
                twitter: 'https://t.co/',
                telegram: 'https://telegram.com/',
                medium: 'https://medium.com/',
                bitcoinTalkForum: '',
                instagram: 'https://instagram.com'
            },
            ico: {
                id: 'a9dea232-df7a-46e5-b3c6-b2c71d223627',
                privateSaleStart: '2018-09-24',
                privateSaleEnd: '2018-09-24',
                preSaleIcoStart: '2018-09-24',
                preSaleIcoEnd: '2018-09-24',
                publicIcoStart: '2018-09-24',
                publicIcoEnd: '2018-09-24',
                platformTokenType: 'ETH',
                pricePerToken: '$0.16',
            },
            votes: {}
        }
    ],
    BlockCategories: [
        'Dapp',
        'Exchange',
        'Marijuana',
        'Airdrops',
        'IcoRankBusiness',
        'CryptoApparel',
        'Otc',
        'CryptoMixer'
    ],

    GappingBlocks: [
        {
            logo: 'xdack',
            name: 'xDAC Airdrop',
            text: 'Claim $200 For Easy<br> Social Tasks.'
        },

        {
            logo: 'energi',
            name: 'energitoken Airdrop',
            text: 'Get 1,000-3,000 ETK Per Claim And Win<br> Up To $20,000!'
        },

        {
            logo: 'binance',
            name: 'BINANCE Airdrop',
            text: '50% Trading Fee Discount For<br> BINANCE Users.'
        },

        {
            logo: 'telegram',
            name: 'Airdrops via Telegram',
            text: 'Join Our Telegram Group &<br> Never Miss An Airdrop Again'
        },

        {
            logo: 'xdack',
            name: 'xDAC Airdrop',
            text: 'Claim $200 For Easy<br> Social Tasks.'
        },

        {
            logo: 'energi',
            name: 'energitoken Airdrop',
            text: 'Get 1,000-3,000 ETK Per Claim And Win<br> Up To $20,000!'
        },

        {
            logo: 'binance',
            name: 'BINANCE Airdrop',
            text: '50% Trading Fee Discount For<br> BINANCE Users.'
        },

        {
            logo: 'telegram',
            name: 'Airdrops via Telegram',
            text: 'Join Our Telegram Group &<br> Never Miss An Airdrop Again'
        }
    ]
};

export { Repository };