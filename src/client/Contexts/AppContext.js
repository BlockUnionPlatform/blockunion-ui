import React from 'react';

const AppContext = React.createContext({
    isLoggedIn: true,
    user: {
        "id": "5fb822cf-6351-4673-84bc-7aa0a7fbcc73",
        "firstName": "Humza",
        "lastName": "Khan",
        "emailAddress": "humzakhan@outlook.com",
        "password": "wl7GKdXdNWBgiPdd3pJRuA==",
        "salt": "ZNY5K1eUfiWml1cHl0QJuLhlxoIVF3oFcA==",
        "phoneNumber": "+923348881997",
        "accessLevel": 10,
        "isBlocked": false,
        "dateCreatedUtc": "2018-09-16T15:55:31.338692",
        "dateModifiedUtc": "2018-09-16T15:55:31.338798",
        "dateLastLoginUtc": "0001-01-01T00:00:00"
    }
});

function isUserLoggedIn() {
    let loggedIn = false;

    <AppContext.Consumer>
        {(context) => (loggedIn = context.isLoggedIn)}
    </AppContext.Consumer>

    return loggedIn;
}

function getActiveUser() {
    let _user = null;

    <AppContext.Consumer>
        {(context) => (_user = context.user)}
    </AppContext.Consumer>

    return _user;
}

function setContext(props) {
    <AppContext.Provider value={props}>
    </AppContext.Provider>
}

export {AppContext, isUserLoggedIn, getActiveUser, setContext};