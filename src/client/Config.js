const Config = {
    Environment: 'Production',
    SiteName: 'Block Union',
    MinAdminLevel: 3,
    Formats: {
        DateTime: 'MMM DD, YYYY hh:mm a',
        Date: 'MMM DD, YYYY',
        DateNumeric: 'YYYY-MM-DD',
        Time: 'hh:mm:ss a'
    },
    RouteUrls: {
        Auth: {
            ForgotPassword: '/auth/password/forgot',
            Login: '/auth/login',
            ResetPassword: '/auth/password/reset',
            Logout: '/auth/logout',
            Register: '/auth/register'
        },

        Member: {
            Home: '/member',
            Misc: {
                Terms: '/member/terms',
                Privacy: '/member/privacy'
            },
            Blocks: {
                Home: '/member/blocks',
                View: '/blocks',
                Edit: '/member/blocks/edit',
                Add: '/member/blocks/add',
                Payouts: '/member/blocks/payouts',
                Claims: '/member/blocks/claims'
            },
            Referrals: '/member/referrals',
            Payments: '/member/payments',
            DailyActionPayouts: '/member/dailyactions/payouts',
            DailyActionClaims: '/member/dailyactions/claims',
            DailyActions: {
                Home: '/member/dailyactions'
            },
            Chat: '/member/chat',
            Pro: '/member/pro',
            Discover: '/member/discover',
            Search: '/member/search'
        },

        Profile: {
            Admin: '/admin/profile',
            Member: '/member/profile'
        },

        Admin: {
            Home: '/admin',
            Blocks: {
                Home: '/admin/blocks',
                Add: '/admin/blocks/add',
                Edit: '/admin/blocks/edit',
                View: '/admin/blocks/view',
                Status: '/admin/blocks/status'
            },

            Referrals: '/admin/referrals',

            DailyActions: {
                Home: '/admin/dailyactions',
                Add: '/admin/dailyactions/add',
                Edit: '/admin/dailyactions/edit',
                View: '/admin/dailyactions/view'
            },

            Rewards: {
                Home: '/admin/rewards',
                Claims: {
                    Home: '/admin/rewards/claims',
                    Process: '/admin/rewards/claims/process',
                    View: '/admin/rewards/claims/view'
                },
                Add: '/admin/rewards/add',
                View: '/admin/rewards/view',
                Edit: '/admin/rewards/edit'
            },
            Payouts: {
                Home: '/admin/payouts',
                View: '/admin/payouts/view'
            },

            Tokens: {
                Home: '/admin/tokens',
                Add: '/admin/tokens/add',
                ViewWithId: '/admin/tokens/view/:id',
                View: '/admin/tokens/view',
                EditWithId: '/admin/tokens/edit/:id',
                Edit: '/admin/tokens/edit',
            },

            Logs: {
                Activity: {
                    Home: '/admin/logs/activity',
                    View: '/admin/logs/activity/view',
                    ViewWithId: '/admin/logs/activity/view/:id'
                },
                System: {
                    Home: '/admin/logs/system',
                    View: '/admin/logs/system/view',
                    ViewWithId: '/admin/logs/system/view/:id'
                }
            },
            Settings: {
                Home: '/admin/settings'
            },

            Users: {
                Home: '/admin/users',
                Add: '/admin/users/add',
                View: '/admin/users/view',
                Edit: '/admin/users/edit'
            },

            Packages: {
                Home: '/admin/packages',
                Add: '/admin/packages/add',
                Edit: '/admin/packages/edit',
                View: '/admin/packages/view'
            },

            BlockToken: {
                Accounts: {
                    View: '/admin/blocks/accounts',
                    Edit: '/admin/blocks/accounts/edit',
                    Add: '/admin/blocks/accounts/add'
                },

                Transactions: {
                    Home: '/admin/transactions',
                    View: '/admin/transactions/view',
                    Edit: '/admin/transactions/edit',
                    Add: '/admin/transactions/add'
                },

                Claims: {
                    Home: '/admin/blocks/claims',
                    View: '/admin/blocks/claims/view',
                    Process: '/admin/blocks/claims/process'
                }
            }
        }
    },
    API: {
        Headers: {
            'Content-Type': 'application/json',
            'Key': 'JBqz}O&k)4^KKD@\'Sbyp9XC_ioYPO);Kv_%fQ!_[-sX6(xx>6{7gVH3Rt\'40B4Z'
        },
        Users: {
            Default: 'users',
            Authenticate: 'users/auth',
            Profile: 'users/profile',
            Password: 'users/password',
            Register: 'users/register',
            ForgotPassword: 'users/password/forgot',
            ResetPassword: 'users/password/reset',
            ForgotPasswordValidate: 'users/password/forgot/validate'
        },
        Logs: {
            Activity: {
                All: 'logs/activity/all',
                Single: 'logs/activity'
            },

            System: 'logs'
        },
        Tokens: 'tokens',
        Rewards: 'rewards',
        RewardClaims: 'rewards/claims',
        DailyActions: 'dailyactions',
        DailyActionsRecent: 'dailyactions/recent',
        Payouts: 'payouts',
        Block: {
            Accounts: 'blocks/accounts',
            Packages: 'blocks/packages',
            Transactions: 'blocks/transactions',
            Listings: 'blocks/listings',
            Individual: 'blocks',
            Status: '/blocks/listings/status',
            User: 'blocks/listings/user',
            Claims: 'blocks/claims'
        },
        Dashboard: {
            Admin: 'home/statistics',
            Members: 'home/members',
            Home: 'home'
        },
        CryptoRates: {
            URL: 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?start=1&limit=10&convert=USD',
            KeyValue: '2b030c47-2056-4a52-95a9-ae3aefcc2d9a',
            KeyName: 'X-CMC_PRO_API_KEY'
        },
        Votes: {
            Main: 'votes',
            CheckVote: 'votes/check'
        }
    }
};

export {Config};