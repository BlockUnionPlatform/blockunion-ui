import React from 'react';

export default function BlockHighlight(props) {
    return (
        <div className="item">
            <div className="latest-box">
                <img src={`${props.image}-bg.jpg`} alt="" />
                    <div className="latest-content">
                        <img src={`${props.image}-img.png`} alt="" />
                            <h3>{props.name}</h3>
                            <p dangerouslySetInnerHTML={{__html: props.text}}></p>
                    </div>
            </div>
        </div>
    );
}