import React, {Component} from 'react';

export default class MainBlockTabs extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <section className="tabs-section">
                <div className="container">
                    <div className="tab-wrapper">
                        <ul className="tab-menu">
                            <li className="active">ONGOING ICOS</li>
                            <li>UPCOMING ICOS</li>
                            <li>PRESS RELEASE</li>
                        </ul>

                        <div className="tab-content">
                            <div className="tabs-wrap a-center">
                                <ul className="a-left">

                                    <li>
                                        <div className="title-tab">
                                            <h4>CargoCoin (CRGO)</h4>
                                            <span>HOT 109°<i className="fa fa-fire"></i></span>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry...</p>
                                        <div className="ratings clr">
                                            <div className="star-ratings">
                                                <div className="star-ratings-top" style={{ width: "72%" }}>
                                                </div>
                                            </div>
                                            <p>3.5</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div className="img-tab">
                                            <img src="assets/main/images/tab-img-4.jpg" alt="" />
                                        </div>
                                        <div className="title-tab">
                                            <h4>Modorr (MDR)</h4>
                                            <span>HOT 189°<i className="fa fa-fire"></i></span>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry...</p>
                                        <div className="ratings clr">
                                            <div className="star-ratings">
                                                <div className="star-ratings-top" style={{ width: "46%" }}>
                                                </div>
                                            </div>
                                            <p>3.5</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div className="img-tab">
                                            <img src="assets/main/images/tab-img-5.jpg" alt="" />
                                        </div>
                                        <div className="title-tab">
                                            <h4>PeruCoin (PERU)</h4>
                                            <span>HOT 129°<i className="fa fa-fire"></i></span>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry...</p>
                                        <div className="ratings clr">
                                            <div className="star-ratings">
                                                <div className="star-ratings-top" style={{ width: "67%" }}>
                                                </div>
                                            </div>
                                            <p>3.5</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div className="img-tab">
                                            <img src="assets/main/images/tab-img-1.jpg" alt="" />
                                        </div>
                                        <div className="title-tab">
                                            <h4>Trippki (TRIP)</h4>
                                            <span>HOT 109°<i className="fa fa-fire"></i></span>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry...</p>
                                        <div className="ratings clr">
                                            <div className="star-ratings">
                                                <div className="star-ratings-top" style={{ width: "72%" }}>
                                                </div>
                                            </div>
                                            <p>3.5</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div className="img-tab">
                                            <img src="assets/main/images/tab-img-2.jpg" alt="" />
                                        </div>
                                        <div className="title-tab">
                                            <h4>Epigencare (EPIC)</h4>
                                            <span>HOT 189°<i className="fa fa-fire"></i></span>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry...</p>
                                        <div className="ratings clr">
                                            <div className="star-ratings">
                                                <div className="star-ratings-top" style={{ width: "46%" }}>
                                                </div>
                                            </div>
                                            <p>3.5</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div className="img-tab">
                                            <img src="assets/main/images/tab-img-3.jpg" alt="" />
                                        </div>
                                        <div className="title-tab">
                                            <h4>MUXE (MUXE)</h4>
                                            <span>HOT 129°<i className="fa fa-fire"></i></span>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry...</p>
                                        <div className="ratings clr">
                                            <div className="star-ratings">
                                                <div className="star-ratings-top" style={{ width: "67%" }}>
                                                </div>
                                            </div>
                                            <p>3.5</p>
                                        </div>
                                    </li>

                                </ul>
                                <a href="#" className="btn-regular">VIEW ALL ONGOING ICOS <span>>></span></a>
                            </div>
                            <div className="tabs-wrap a-center">
                                <ul className="a-left">
                                    <li>
                                        <div className="img-tab">
                                            <img src="assets/main/images/tab-img-1.jpg" alt="" />
                                        </div>
                                        <div className="title-tab">
                                            <h4>Trippki (TRIP)</h4>
                                            <span>HOT 109°<i className="fa fa-fire"></i></span>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry...</p>
                                        <div className="ratings clr">
                                            <div className="star-ratings">
                                                <div className="star-ratings-top" style={{ width: "74%%" }}>
                                                </div>
                                            </div>
                                            <p>3.7</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div className="img-tab">
                                            <img src="assets/main/images/tab-img-2.jpg" alt="" />
                                        </div>
                                        <div className="title-tab">
                                            <h4>Epigencare (EPIC)</h4>
                                            <span>HOT 189°<i className="fa fa-fire"></i></span>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry...</p>
                                        <div className="ratings clr">
                                            <div className="star-ratings">
                                                <div className="star-ratings-top" style={{ width: "46%" }}>
                                                </div>
                                            </div>
                                            <p>2.3</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div className="img-tab">
                                            <img src="assets/main/images/tab-img-3.jpg" alt="" />
                                        </div>
                                        <div className="title-tab">
                                            <h4>MUXE (MUXE)</h4>
                                            <span>HOT 129°<i className="fa fa-fire"></i></span>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry...</p>
                                        <div className="ratings clr">
                                            <div className="star-ratings">
                                                <div className="star-ratings-top"  style={{ width: "6%" }}>
                                                </div>
                                            </div>
                                            <p>3.3</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div className="title-tab">
                                            <h4>CargoCoin (CRGO)</h4>
                                            <span>HOT 109°<i className="fa fa-fire"></i></span>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry...</p>
                                        <div className="ratings clr">
                                            <div className="star-ratings">
                                                <div className="star-ratings-top" style={{ width: "74%" }}>
                                                </div>
                                            </div>
                                            <p>3.7</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div className="img-tab">
                                            <img src="assets/main/images/tab-img-4.jpg" alt="" />
                                        </div>
                                        <div className="title-tab">
                                            <h4>Modorr (MDR)</h4>
                                            <span>HOT 189°<i className="fa fa-fire"></i></span>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry...</p>
                                        <div className="ratings clr">
                                            <div className="star-ratings">
                                                <div className="star-ratings-top" style={{ width: "46%" }}>
                                                </div>
                                            </div>
                                            <p>2.3</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div className="img-tab">
                                            <img src="assets/main/images/tab-img-5.jpg" alt="" />
                                        </div>
                                        <div className="title-tab">
                                            <h4>PeruCoin (PERU)</h4>
                                            <span>HOT 129°<i className="fa fa-fire"></i></span>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry...</p>
                                        <div className="ratings clr">
                                            <div className="star-ratings">
                                                <div className="star-ratings-top" style={{ width: "66%" }}>
                                                </div>
                                            </div>
                                            <p>3.3</p>
                                        </div>
                                    </li>
                                </ul>
                                <a href="#" className="btn-regular">VIEW ALL UPCOMING ICOS <span>>></span></a>
                            </div>
                            <div className="tabs-wrap a-center">
                                <ul className="a-left">
                                    <li>
                                        <div className="title-tab">
                                            <h4>CargoCoin (CRGO)</h4>
                                            <span>HOT 109°<i className="fa fa-fire"></i></span>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry...</p>
                                        <div className="ratings clr">
                                            <div className="star-ratings">
                                                <div className="star-ratings-top" style={{ width: "72%" }}>
                                                </div>
                                            </div>
                                            <p>3.5</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div className="img-tab">
                                            <img src="assets/main/images/tab-img-4.jpg" alt="" />
                                        </div>
                                        <div className="title-tab">
                                            <h4>Modorr (MDR)</h4>
                                            <span>HOT 189°<i className="fa fa-fire"></i></span>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry...</p>
                                        <div className="ratings clr">
                                            <div className="star-ratings">
                                                <div className="star-ratings-top" style={{ width: "46%" }}>
                                                </div>
                                            </div>
                                            <p>3.5</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div className="img-tab">
                                            <img src="assets/main/images/tab-img-5.jpg" alt="" />
                                        </div>
                                        <div className="title-tab">
                                            <h4>PeruCoin (PERU)</h4>
                                            <span>HOT 129°<i className="fa fa-fire"></i></span>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry...</p>
                                        <div className="ratings clr">
                                            <div className="star-ratings">
                                                <div className="star-ratings-top" style={{ width: "67%" }}>
                                                </div>
                                            </div>
                                            <p>3.5</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div className="img-tab">
                                            <img src="assets/main/images/tab-img-1.jpg" alt="" />
                                        </div>
                                        <div className="title-tab">
                                            <h4>Trippki (TRIP)</h4>
                                            <span>HOT 109°<i className="fa fa-fire"></i></span>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry...</p>
                                        <div className="ratings clr">
                                            <div className="star-ratings">
                                                <div className="star-ratings-top" style={{ width: "72%" }}>
                                                </div>
                                            </div>
                                            <p>3.5</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div className="img-tab">
                                            <img src="assets/main/images/tab-img-2.jpg" alt="" />
                                        </div>
                                        <div className="title-tab">
                                            <h4>Epigencare (EPIC)</h4>
                                            <span>HOT 189°<i className="fa fa-fire"></i></span>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry...</p>
                                        <div className="ratings clr">
                                            <div className="star-ratings">
                                                <div className="star-ratings-top" style={{ width: "46%" }}>
                                                </div>
                                            </div>
                                            <p>3.5</p>
                                        </div>
                                    </li>

                                    <li>
                                        <div className="img-tab">
                                            <img src="assets/main/images/tab-img-3.jpg" alt="" />
                                        </div>
                                        <div className="title-tab">
                                            <h4>MUXE (MUXE)</h4>
                                            <span>HOT 129°<i className="fa fa-fire"></i></span>
                                        </div>
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry...</p>
                                        <div className="ratings clr">
                                            <div className="star-ratings">
                                                <div className="star-ratings-top" style={{ width: "67%" }}>
                                                </div>
                                            </div>
                                            <p>3.5</p>
                                        </div>
                                    </li>

                                </ul>
                                <a href="#" className="btn-regular">VIEW ALL PRESS RELEASES ICOS <span>>></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}