import React, {Component} from 'react';
import {Config} from "../../Config";

export default class MainFooter extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <footer>
                <div className="main-footer">
                    <div className="container">
                        <div className="bottom-footer clr">
                            <div className="privacy-condition">
                                <ul>
                                    <li><a href={Config.RouteUrls.Member.Misc.Privacy}>Privacy Policy</a></li>
                                    <li><a href={Config.RouteUrls.Member.Misc.Terms}>Terms & Conditions</a></li>
                                </ul>
                            </div>
                            <div className="copyright">
                                <p>© <a href="/">BlockUnion</a> - 2018. All Rights Reserved.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}