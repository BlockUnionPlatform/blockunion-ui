import React, {Component} from 'react';
import {Helmet} from "react-helmet";

export default class MainAssets extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loaded: false
        };
    }

    componentDidMount() {
        this.setState({ loaded: true });
    }

    render() {
        return (
            <div>
                <Helmet>
                    <link type="text/css" rel="stylesheet" href="assets/main/css/styles.css" />
                    <link type="text/css" rel="stylesheet" href="assets/main/css/owl.carousel.min.css" />
                    <link type="text/css" rel="stylesheet" href="assets/main/css/responsive.css" />
                    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500" rel="stylesheet" />
                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
                    <script type="text/javascript" src="assets/main/js/main.js"></script>
                </Helmet>

                {
                    this.state.loaded &&
                    <div>{this.props.children}</div>
                }
            </div>
        );
    }
}