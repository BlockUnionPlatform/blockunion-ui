import React, {Component} from 'react';
import {Repository} from "../../Data/DataStore";
import BlockHighlight from "./BlockHighlight";

export default class MainSectionGapping extends Component {
    constructor(props) {
        super(props);

        this.state = {
            path: 'assets/main/images',
            blocks: []
        };
    }

    componentWillMount() {
        let blocks = Repository.GappingBlocks;
        this.setState({ blocks });
    }

    render() {
        return (
            <section className="latest-section section-gapping">
                <div className="container">
                    <div className="latest-carousel owl-carousel owl-theme owl-carousel-main">
                        {/*
                            
                            this.state.blocks.map((highlight) =>
                                <BlockHighlight
                                    image={`${this.state.path}/${highlight.logo}`}
                                    name={highlight.name}
                                    text={highlight.text}
                                />
                            )
                        
                        */}
                        <img src="assets/main/images/banner-1.png" alt=""/>
                        <img src="assets/main/images/banner-2.png" alt=""/>
                        <img src="assets/main/images/banner-3.png" alt=""/>
                        <img src="assets/main/images/banner-4.png" alt=""/>
                    </div>
                </div>
            </section>
        );
    }
}