import React, {Component} from 'react';

export default class MainHeaderMenu extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="header-menu header-menu-main">
                <div className="container">
                    <div className="logo">
                        <a href="/"><img src="assets/login/images/bu-banner.png" alt="BlockUnion" style={{ height: '70px' }}/></a>
                    </div>
                </div>
            </div>
        );
    }
}