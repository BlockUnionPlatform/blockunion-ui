import React, {Component} from 'react';
import {Auth} from "../../common/AuthHelpers";
import {Config} from "../../../Config";

export default function MainHeaderTop(props) {
    return (
        <div>
            <div className="header-top">
                <div className="container">
                    <div className="header-center header-center-main">
                        <p>Claim rewards upto $200 for minimal tasks.</p>
                    </div>

                    {
                        !Auth.isAuthenticated() ?
                            <span>
                                <a href={Config.RouteUrls.Auth.Register} className="btn-regular">REGISTER</a>
                                <a href={Config.RouteUrls.Auth.Login} className="btn-regular">LOGIN</a>
                            </span>
                            :
                            <span>
                                <a href="/member" className="btn-regular">MEMBERS AREA</a>
                            </span>
                    }
                </div>
            </div>
        </div>
    );
}
