import React, {Component} from 'react';
import MainHeaderTop from "./MainHeaderTop";
import MainHeaderMenu from "./MainHeaderMenu";
import {Helmet} from "react-helmet";

export default class MainHeaderContainer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
           <header id="header">
               <MainHeaderTop/>
               <MainHeaderMenu/>
           </header>
        );
    }
}