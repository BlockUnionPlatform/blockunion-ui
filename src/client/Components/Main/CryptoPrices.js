import React from 'react';

export default function CryptoPrices(props) {
    return (
        <div className="marquee">
            {
                props.crypto != null &&
                <ul className="detail_list">
                    {
                        props.crypto.map(item =>
                            <li>
                                <h4>{item.symbol}<span>${item.quote.USD.price.toLocaleString('en-US', {minimumFractionDigits: 2})}</span></h4>
                                {
                                    item.quote.USD.percent_change_24h.toString().indexOf('-') == 0 ?
                                        <p>
                                            {item.quote.USD.percent_change_24h.toLocaleString({minimumFractionDigits: 2 })}
                                        </p>
                                        :
                                        <p style={{ color: 'green' }}>
                                            +{item.quote.USD.percent_change_24h.toLocaleString({minimumFractionDigits: 2 })}
                                        </p>

                                }

                            </li>
                        )
                    }
                </ul>
            }
        </div>
    );
}