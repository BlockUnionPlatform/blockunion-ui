import React, {Component} from 'react';
import MainSectionGapping from "./MainSectionGapping";
import CryptoPrices from "./CryptoPrices";
import ServicesContainer from "../member/ServicesContainer";
import {getApi} from "../../ConfigStore";
import {Config} from "../../Config";
import axios from "axios";
import {LocalStore} from "../common/LocalStore";
import LoadingScreen from "../common/LoadingScreen";

export default class MainHomePage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dailyActions: [],
            blocks: [],
            icos: [],
            trending: [],
            isLoading: true,
            crypto: null,
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
        this.setLoading = this.setLoading.bind(this);
        this.loadCryptoPrices = this.loadCryptoPrices.bind(this);
    }

    showAlert(show, status, message) {
        this.setState({ alert: { show, status, message } });
    }

    setLoading(isLoading) {
        this.setState({ isLoading });
    }

    loadCryptoPrices() {
        let url = `${getApi(Config.API.Dashboard.Home)}/crypto`;

        axios.get(url)
            .then(res => {
                this.setState({ crypto: res.data });
                this.setLoading(false);
            })
            .catch(error => {
                this.setLoading(false);
            });

    }

    componentDidMount() {
        let url = getApi(Config.API.Dashboard.Members);

        axios.get(url)
            .then(res => {
                this.setState({
                    dailyActions: res.data.dailyActions,
                    blocks: res.data.blocks,
                    icos: res.data.icos,
                    trending: res.data.trending
                });

                this.loadCryptoPrices();
            })
            .catch(error => {
                console.log(error);
                this.setLoading(false);
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <MainSectionGapping />

                {
                    this.state.crypto != null &&
                    <CryptoPrices crypto={this.state.crypto.data} />
                }

                <section className="trending-section trending-section-main">
                    <div className="container">
                        <div className="wrap-trending clr">
                            <ServicesContainer
                                blocks={this.state.blocks}
                                icos={this.state.icos}
                                trending={this.state.trending} />
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}