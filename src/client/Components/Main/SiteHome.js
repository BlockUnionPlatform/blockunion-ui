import React from 'react';
import MainAssets from "./MainAssets";
import MainHeaderContainer from "./Header/MainHeaderContainer";
import MainHomePage from "./MainHomePage";
import MainFooter from "./MainFooter";
import SiteTitle from "../common/SiteTitle";

export default function SiteHome(props) {
    return (
        <div className="wrapper">
            <SiteTitle title = "Home" />
            <MainAssets>
                <MainHeaderContainer />
                <MainHomePage />
                <MainFooter />
            </MainAssets>
        </div>
    );
}