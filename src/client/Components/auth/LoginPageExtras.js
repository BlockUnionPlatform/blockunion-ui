import React from 'react';
import {Link} from "react-router-dom";
import {Config} from "../../Config";

export default function LoginPageExtras() {
    return (
        <div className="signup-section">
            <div className="signup text_center">
                <p>Don’t Have An Account Yet? <Link to={Config.RouteUrls.Auth.Register}>Register</Link> Now</p>
            </div>
            <div className="forget_pwd text_center">
                <Link to={Config.RouteUrls.Auth.ForgotPassword}>Forgot your password?</Link>
            </div>
        </div>
    );
}