import VerifyEmail from "../common/VerifyEmail";

export default function VerifyAuth(props) {
    let errors = [];

    if (props.emailAddress.length < 1)
        errors.push('Please enter an email address');

    if (props.emailAddress.length > 0 && !VerifyEmail(props.emailAddress))
        errors.push('Please provide a valid email address');

    if (props.password.length < 1)
        errors.push('Please enter a password');

    return errors;
}