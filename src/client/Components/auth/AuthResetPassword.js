import React, {Component} from 'react';
import SiteTitle from "../common/SiteTitle";
import LoadingScreen from "../common/LoadingScreen";
import {getApi} from "../../ConfigStore";
import axios from 'axios';
import ComposeErrorMessageFromJson from "../admin/pages/Common/ComposeErrorMessageFromJson";
import {Config} from '../../Config';
import AlertBox from "../common/AlertBox";
import VerifyResetPassword from "./VerifyResetPassword";
import ErrorMessage from "../common/ErrorMessage";
import {Link} from "react-router-dom";
import Redirect from "react-router/es/Redirect";
import {Auth} from "../common/AuthHelpers";

export default class AuthResetPassword extends Component {
    constructor(props) {
        super(props);

        this.state = {
            password: '',
            repeatPassword: '',
            isLoading: true,
            isValid: false,
            isLoggedIn: false,
            alert: {
                show: false,
                isSmall: true,
                status: '',
                message: ''
            }
        };

        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.showAlert = this.showAlert.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
    }

    showAlert(show, status, message) {
        this.setState({ alert: { show, status, message, isSmall: true } });
    }

    onInputChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    onFormSubmit(event) {
        event.preventDefault();

        let errors = VerifyResetPassword(this.state);
        if (errors.length > 0) {
            let errorMessage = <ErrorMessage errors={errors} />;
            this.showAlert(true, 'error', errorMessage);
        }
        else {
            let url = getApi(Config.API.Users.ResetPassword);
            let payload = {
                password: this.state.password,
                repeatPassword: this.state.repeatPassword,
                code: this.props.match.params.code
            };

            this.setState({ isLoading: true });

            axios.post(url, payload)
                .then(res => {
                    this.showAlert(true, 'success', 'Your password has been updated successfully!');
                    this.setState({ isLoading: false });
                })
                .catch(error => {
                    if (error.response) {
                        if (error.response.status == 422)
                            this.showAlert(true, 'error', ComposeErrorMessageFromJson(error.response.data), true);
                        else
                            this.showAlert(true, 'error', JSON.stringify(error.response.data), true);
                    }
                    else if (error.request)
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);
                    else
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                    this.setState({ isLoading: false });
                })
        }
    }

    componentDidMount() {
        this.setState({ isLoggedIn: Auth.isAuthenticated() });

        let url = `${getApi(Config.API.Users.ForgotPasswordValidate)}/${this.props.match.params.code}`;

        axios.get(url)
            .then(res => {
                this.setState({
                    isLoading: false,
                    isValid: true
                });
            })
            .catch(error => {
                if (error.response) {
                    if (error.response.status == 422)
                        this.showAlert(true, 'error', ComposeErrorMessageFromJson(error.response.data), true);
                    else
                        this.showAlert(true, 'error', JSON.stringify(error.response.data), true);
                }
                else if (error.request)
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false, isValid: false });
            });
    }

    render() {
        return (
            <div className="login_form">
                {
                    this.state.isLoggedIn ?
                        <Redirect to={Config.RouteUrls.Member.Home} />
                        :
                        <span></span>
                }
                <SiteTitle title = "Reset Password" />
                <LoadingScreen show={this.state.isLoading} />
                <div className="login-logo">
                    <AlertBox {...this.state.alert} />
                </div>
                {
                    !this.state.isValid ?
                        <div></div>
                        :
                        <form method="POST" className="contact-form" id="quick-enquiry-form" onSubmit={this.onFormSubmit}>
                            <div className="field">
                                <input type="password"
                                       placeholder="Enter Password *"
                                       className="login-placeholder"
                                       value={this.state.password}
                                       name = "password"
                                       onChange={this.onInputChange} />
                            </div>

                            <div className="field">
                                <input type="password"
                                       placeholder="Repeat Password *"
                                       className="login-placeholder"
                                       value={this.state.repeatPassword}
                                       name = "repeatPassword"
                                       onChange={this.onInputChange} />
                            </div>

                            <div className="login-check">
                                <button className="btn btn-theme btn-block" type = "submit">Update Password</button>
                            </div>
                        </form>
                }

                <div className="signup-section">
                    <div className="signup text_center">
                        <p>Don’t Have An Account Yet? <a href="#">Sign Up</a> Now</p>
                    </div>
                    <div className="forget_pwd text_center">
                        <Link to={Config.RouteUrls.Auth.Login}>Back to login</Link>
                    </div>
                </div>
            </div>
        );
    }
}