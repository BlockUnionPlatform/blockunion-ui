import React, {Component} from 'react';

export default class AuthFooter extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <footer id="footer">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="footer_menu">
                                <div className="bottom_footer">
                                    <p>Copyright © BlockUnion 2018 - All Rights Reserved.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}