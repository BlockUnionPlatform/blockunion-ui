import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Config} from '../../Config';
import SiteTitle from "../common/SiteTitle";
import AlertBox from "../common/AlertBox";
import VerifyEmail from "../common/VerifyEmail";
import {getApi} from "../../ConfigStore";
import axios from 'axios';
import ComposeErrorMessageFromJson from "../admin/pages/Common/ComposeErrorMessageFromJson";
import LoadingScreen from "../common/LoadingScreen";
import Redirect from "react-router/es/Redirect";
import {Auth} from "../common/AuthHelpers";

export default class AuthForgotPassword extends Component {
    constructor(props) {
        super(props);

        this.state = {
            emailAddress: '',
            isLoading: false,
            alert: {
                show: false,
                isSmall: true,
                status: '',
                message: ''
            },
            isLoggedIn: false
        };

        this.onEmailChange = this.onEmailChange.bind(this);
        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status, message) {
        this.setState({ alert: { show, status, message, isSmall: true } });
    }

    onEmailChange(event) {
        this.setState({emailAddress: event.target.value});
    }

    onFormSubmit(e) {
        e.preventDefault();

        if (!VerifyEmail(this.state.emailAddress)) {
            this.showAlert(true, 'error', 'Please enter a valid email address');
        }
        else {
            let url = `${getApi(Config.API.Users.ForgotPassword)}`;
            let payload = {
                emailAddress: this.state.emailAddress
            };

            this.setState({ isLoading: true });

            axios.post(url, payload)
                .then(res => {
                    this.showAlert(true, 'success', 'If an account exists with the provided email address, we have sent a forgot password link to it.');
                    this.setState({ isLoading: false });
                })
                .catch(error => {
                    if (error.response) {
                        if (error.response.status == 422)
                            this.showAlert(true, 'error', ComposeErrorMessageFromJson(error.response.data), true);
                        else
                            this.showAlert(true, 'error', JSON.stringify(error.response.data), true);
                    }
                    else if (error.request)
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);
                    else
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                    this.setState({ isLoading: false });
                });
        }
    }

    componentDidMount() {
        this.setState({ isLoggedIn: Auth.isAuthenticated() });
    }

    render() {
        return(
            <div>
                {
                    this.state.isLoggedIn ?
                        <Redirect to={Config.RouteUrls.Member.Home} />
                        :
                        <span></span>
                }
                <SiteTitle title = "Forgot Password" />
                <LoadingScreen show={this.state.isLoading} />
                <div className="login_form">
                    <form action="#" method = "POST" className = "contact-form" id = "quick-enquiry-form" onSubmit={this.onFormSubmit.bind(this)}>
                        <AlertBox {...this.state.alert} />
                        <div className="field">
                            <input type="text"
                                   placeholder="Email *"
                                   className="login-placeholder"
                                   value={this.state.emailAddress}
                                   onChange={this.onEmailChange}/>
                        </div>

                        <div className="login-check">
                            <button className="btn btn-theme btn-block" type = "submit">Reset Password</button>
                        </div>
                    </form>
                </div>
                <div className="signup-section">
                    <div className="signup text_center">
                        <p>Don’t Have An Account Yet? <a href="#">Sign Up</a> Now</p>
                    </div>
                    <div className="forget_pwd text_center">
                        <Link to={Config.RouteUrls.Auth.Login}>Back to login</Link>
                    </div>
                </div>
            </div>
        );
    }
}