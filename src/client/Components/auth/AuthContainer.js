import React, {Component} from 'react';
import AuthFooter from './AuthFooter';

export default class AuthContainer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <div className="main-content">
                    <section className="static-banner">
                        <div className="container">
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="login-logo">
                                        <img src="assets/login/images/bu-banner.png" alt=""/>
                                    </div>

                                    {this.props.children}
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <AuthFooter/>
            </div>
        );
    }
}