import VerifyEmail from "../common/VerifyEmail";

export default function VerifyRegister(profile) {
    let errors = [];

    if (profile.firstName.length < 2 || profile.firstName.length > 50)
        errors.push('First name should be between 2 and 50 characters long.');

    if (profile.lastName.length < 2 || profile.lastName.length > 50)
        errors.push('Last name should be between 2 and 50 characters long.');

    if (!VerifyEmail(profile.emailAddress))
        errors.push('Please provide a valid email address');

    if (profile.password.length < 5)
        errors.push('Password must be at least 5 characters long.');

    if (profile.password != profile.repeatPassword)
        errors.push('Password do not match.');

    return errors;
}