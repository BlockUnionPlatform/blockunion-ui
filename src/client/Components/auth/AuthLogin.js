import React, {Component} from 'react';
import axios from 'axios';
import AlertBox from "../common/AlertBox";
import LoadingScreen from "../common/LoadingScreen";
import LoginPageExtras from "./LoginPageExtras";
import VerifyAuth from "./VerifyAuth";
import ErrorMessage from "../common/ErrorMessage";
import {getApi} from "../../ConfigStore";
import {Config} from '../../Config';
import SiteTitle from "../common/SiteTitle";
import {Auth} from '../common/AuthHelpers';
import queryString from 'query-string';
import Redirect from "react-router/es/Redirect";

export default class AuthLogin extends Component {
    constructor(props) {
        super(props);

        this.onInputChange = this.onInputChange.bind(this);
        this.onLoginSubmit = this.onLoginSubmit.bind(this);
        this.verifyLogin = this.verifyLogin.bind(this);
        this.showAlert = this.showAlert.bind(this);

        this.state = {
            emailAddress: '',
            password: '',
            isLoading: false,
            rememberMe: false,
            alert: {
                show: false,
                status: '',
                message: '',
                isSmall: false
            },
            isLoggedIn: false,
            redirectUrl: ''
        };

        this.redirect = this.redirect.bind(this);
        this.handleRemember = this.handleRemember.bind(this);
    }

    handleRemember(event) {
        this.setState({ rememberMe: event.target.checked });
    }

    onInputChange(event) {
        this.setState({ [event.target.name] : event.target.value });
    }

    showAlert(show, status = 'error', message = '', isSmall = false) {
        this.setState({
            alert: { show, status, message, isSmall }
        });
    }

    redirect() {
        window.location = this.state.redirectUrl;
    }

    verifyLogin() {
        let url = getApi(Config.API.Users.Authenticate);
        let payload = {
            email: this.state.emailAddress,
            password: this.state.password
        };

        axios.post(url, payload)
            .then(res => {
                if (res.data.status == 'success') {
                    Auth.signIn(res.data.user, this.state.rememberMe);
                    this.setState({ isLoggedIn: Auth.isAuthenticated() });
                    this.redirect();

                }
                else {
                    this.showAlert(true, 'error', res.data.message, true);
                }

                this.setState({ isLoading: false });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data, true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });
    }

    onLoginSubmit(event) {
        event.preventDefault();

        let errors = VerifyAuth(this.state);

        if (errors.length > 0) {
            let message = <ErrorMessage errors={errors} isSmall={true}/>
            this.showAlert(true, 'error', message);
        }
        else {
            this.setState({ isLoading: true });
            this.showAlert(false);
            this.verifyLogin();
        }
    }

    componentDidMount() {
        let redirectUrl = queryString.parse(location.search);

        if (redirectUrl.redirect !== undefined)
            this.setState({ redirectUrl: `${location.origin.toString()}${redirectUrl.redirect.startsWith('/') ? redirectUrl.redirect : `/${redirectUrl.redirect}`}` });
        else
            this.setState({ redirectUrl: `${location.origin.toString()}${Config.RouteUrls.Member.Home}` });

        this.setState({ isLoggedIn: Auth.isAuthenticated() });
    }

    render() {
        return (
            <div>
                <SiteTitle title="Login"/>
                <LoadingScreen show={this.state.isLoading} />

                {
                    this.state.isLoggedIn ?
                        <Redirect to={Config.RouteUrls.Member.Home} />
                        :
                        <span></span>
                }

                <div className="login_form">
                    <form action="#" method = "POST" className = "contact-form" id = "quick-enquiry-form" onSubmit={this.onLoginSubmit}>
                        <AlertBox {...this.state.alert} />
                        <div className="field">
                            <input type="text"
                                   name = "emailAddress"
                                   id = "emailAddress"
                                   placeholder="Email *"
                                   className="login-placeholder"
                                   value={this.state.emailAddress}
                                   onChange={this.onInputChange}/>
                        </div>

                        <div className="field">
                            <input type="password"
                                   name = "password"
                                   id = "password"
                                   placeholder="Password *"
                                   className="login-placeholder"
                                   value={this.state.password}
                                   onChange={this.onInputChange}/>
                        </div>

                        <div className="login-check">
                            <div className="remember-box">
                                <label className="cheack-box">
                                    <p>Remember Me</p>
                                    <input type="checkbox" checked={this.state.rememberMe} onChange={this.handleRemember} />
                                    <span className="checkmark"></span>
                                </label>
                            </div>

                            <div className="submit-btn-wrap btn_read login-btn right">
                                <button className="btn btn-theme" type="submit">LOGIN</button>
                            </div>
                        </div>
                    </form>
                </div>

                <LoginPageExtras />
            </div>
        );
    }
}