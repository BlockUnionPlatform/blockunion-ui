export default function VerifyResetPassword(props) {
    let errors = [];

    if (props.password.length < 5)
        errors.push('Password must be at least 5 characters long.');

    if (props.password != props.repeatPassword)
        errors.push('Password do not match.');

    return errors;
}