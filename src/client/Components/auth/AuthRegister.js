import React, {Component} from 'react';
import SiteTitle from "../common/SiteTitle";
import LoadingScreen from "../common/LoadingScreen";
import Redirect from "react-router/es/Redirect";
import {Auth} from "../common/AuthHelpers";
import {Config} from "../../Config";
import AlertBox from "../common/AlertBox";
import {Link} from "react-router-dom";
import VerifyRegister from "./VerifyRegister";
import ErrorMessage from "../common/ErrorMessage";
import {getApi} from "../../ConfigStore";
import axios from 'axios';
import ComposeErrorMessageFromJson from "../admin/pages/Common/ComposeErrorMessageFromJson";

export default class AuthRegister extends Component {
    constructor(props) {
        super(props);

        this.state = {
            firstName: '',
            lastName: '',
            phoneNumber: '',
            emailAddress: '',
            password: '',
            repeatPassword: '',
            isLoading: false,
            isLoggedIn: false,
            alert: {
                show: false,
                status: '',
                message: '',
                isSmall: true
            }
        };

        this.showAlert = this.showAlert.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    onInputChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    showAlert(show, status, message) {
        this.setState({ alert: { show, status, message, isSmall: true } });
        window.scrollTo(0,0);
    }

    componentDidMount() {
        this.setState({ isLoggedIn: Auth.isAuthenticated() });
    }

    onFormSubmit(e) {
        e.preventDefault();

        let errors = VerifyRegister(this.state);
        if (errors.length > 0) {
            let errorMessage = <ErrorMessage errors={errors} />;
            this.showAlert(true, 'error', errorMessage);
        }
        else {
            let url = getApi(Config.API.Users.Register);
            let payload = {
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                emailAddress: this.state.emailAddress,
                phoneNumber: this.state.phoneNumber,
                password: this.state.password,
                repeatPassword: this.state.repeatPassword
            };

            this.setState({ isLoading: true });

            axios.post(url, payload)
                .then(res => {
                    this.setState({ isLoading: false });
                    this.showAlert(true, 'success', "You have been successfully registered! Redirecting in 3 seconds...");
                    setTimeout(function() {
                        Auth.signIn(res.data, true);
                        window.location = Config.RouteUrls.Member.Home;
                    }.bind(this), 3000);
                })
                .catch(error => {
                    if (error.response) {
                        if (error.response.status == 422)
                            this.showAlert(true, 'error', ComposeErrorMessageFromJson(error.response.data), true);
                        else
                            this.showAlert(true, 'error', JSON.stringify(error.response.data), true);
                    }
                    else if (error.request)
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);
                    else
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                    this.setState({ isLoading: false });
                });
        }
    }

    render() {
        return (
            <div>
                <SiteTitle title = "Register" />
                <LoadingScreen show={this.state.isLoading} />

                {
                    this.state.isLoggedIn ?
                        <Redirect to={Config.RouteUrls.Member.Home} />
                        :
                        <span></span>
                }

                <div className="login_form">
                    <form action="#" className = "contact-form form-auth" id = "quick-enquiry-form" onSubmit={this.onFormSubmit}>
                        <div>
                            <AlertBox {...this.state.alert} />
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label className="control-label">First Name</label>
                                        <div className="field">
                                            <input type="text" name="firstName" id="firstName" className="login-placeholder" value={this.state.firstName} onChange={this.onInputChange} />
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label className="control-label">Last Name</label>
                                        <div className="field">
                                            <input type="text" name="lastName" id="lastName" className="login-placeholder" value={this.state.lastName} onChange={this.onInputChange}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div>
                            <label className="control-label">Phone Number</label>
                            <div className="field">
                                <input type="text" name="phoneNumber" id="phoneNumber" className="login-placeholder" value={this.state.phoneNumber} onChange={this.onInputChange} />
                            </div>
                        </div>

                        <div>
                            <label className="control-label">Email Address</label>
                            <div className="field">
                                <input type="text" name="emailAddress" id="emailAddress" className="login-placeholder" value={this.state.emailAddress} onChange={this.onInputChange} />
                            </div>
                        </div>

                        <div>
                            <label className="control-label">Password</label>
                            <div className="field">
                                <input type="password" name="password" id="password" className="login-placeholder" value={this.state.password} onChange={this.onInputChange} />
                            </div>
                        </div>

                        <div>
                            <label className="control-label">Repeat Password</label>
                            <div className="field">
                                <input type="password" name="repeatPassword" id="repeatPassword" className="login-placeholder" value={this.state.repeatPassword} onChange={this.onInputChange} />
                            </div>
                        </div>

                        <div>
                            <button className="btn btn-theme btn-block" type="submit">Register</button>
                        </div>
                    </form>
                </div>

                <div className="signup-section">
                    <div className="signup text_center">
                        <p>Already registered? <Link to={Config.RouteUrls.Auth.Login}>Login</Link></p>
                    </div>
                    <div className="forget_pwd text_center">
                        <Link to={Config.RouteUrls.Auth.ForgotPassword}>Forgot your password?</Link>
                    </div>
                </div>
            </div>
        );
    }
}