import React, {Component} from 'react';
import {Redirect} from "react-router";
import {Config} from "../../Config";
import {Auth} from "../common/AuthHelpers";

export default class AuthLogout extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoggedOut: false
        };
    }

    componentDidMount() {
        Auth.signOut();
        this.setState({ isLoggedOut: true });
    }

    render() {
        return (
            <div>
                {
                    this.state.isLoggedOut ?
                        <Redirect to={Config.RouteUrls.Auth.Login} />
                        :
                        <div></div>
                }
            </div>
        );
    }
}