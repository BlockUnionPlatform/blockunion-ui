import React, {Component} from 'react';
import {Redirect} from "react-router-dom";
import {Auth} from "./common/AuthHelpers";
import { withRouter } from 'react-router-dom';

class EnsureLoggedInContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoggedIn: false
        };
    }

    render() {
        if (Auth.isAuthenticated()) {
            return this.props.children;
        }
        else return (<Redirect to={`/auth/login?redirect=${this.props.location.pathname}`} />);
    }
}

export default withRouter(EnsureLoggedInContainer);