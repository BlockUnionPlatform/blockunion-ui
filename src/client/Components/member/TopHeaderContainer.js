import React, { Component } from 'react';
import TopHeader from './TopHeader';
import TopHeaderLogoBar from './TopHeaderLogoBar';

class TopHeaderContainer  extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <header id="header">
                <TopHeader />
                <TopHeaderLogoBar />
            </header>
        );
    }
};

export default TopHeaderContainer;