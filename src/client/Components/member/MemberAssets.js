import React, { Component } from 'react';

class MemberAssets extends Component {
    render() {
        return (
            <div>
                <link type="text/css" rel="stylesheet" href="assets/member/css/custom-styles.css" />
                <link type="text/css" rel="stylesheet" href="assets/member/css/custom-responsive.css" />
            </div>
        );
    }
}

export default MemberAssets;

