import React, { Component } from 'react';
import {Link} from "react-router-dom";

class BlockServiceItem extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let label = (<div></div>);

        if (this.props.label == "hot") {
            label = (
                <div className="pin">
                Hot <i className="fa fa-fire"></i>
                </div>
            );
        }
        else {
            let labelClass = `pin pin-${this.props.label}`;
            let tag = this.props.label.charAt(0).toUpperCase() + this.props.label.substr(1);
            label = (
                <div className={labelClass}>{tag}</div>
            );
        }

        return (
            <li>
                <Link to={this.props.url}>
                    {label}
                    <div className="left-side">
                        <div className="img-trending">
                            <img src={this.props.logo} alt=""/>
                        </div>

                        <div className="end-time">
                            Ends in <b>{this.props.endTime}</b>
                        </div>
                    </div>

                    <div className="right-side">
                        <h4>{this.props.title}</h4>
                        <p>{this.props.description}</p>
                    </div>
                </Link>
            </li>
        );
    }
}

export default BlockServiceItem;