import React, { Component } from 'react';
import {Link} from "react-router-dom";

class IcoItem extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let label = (<div></div>);

        if (this.props.label == "hot") {
            label = (
                <div className="pin">
                Hot <i className="fa fa-fire"></i>
                </div>
            );
        }
        else {
            let labelClass = `pin pin-${this.props.label}`;
            let tag = this.props.label.charAt(0).toUpperCase() + this.props.label.substr(1);
            label = (
                <div className={labelClass}>{tag}</div>
            );
        }
        return (
            <li>
                <Link to={this.props.url}>
                    {label}
                    <div className="left-side">
                        <div className="img-trending">
                            <img src={this.props.logo} alt=""/>
                        </div>

                        <div className="social-icon">
                            <a href={this.props.telegram}><i className="fa fa-paper-plane"></i></a>
                            <a href={this.props.gmail}><i className="fa fa-envelope-open-o"></i></a>
                            <a href={this.props.facebook}><i className="fa fa-facebook"></i></a>
                            <a href={this.props.twitter}><i className="fa fa-twitter"></i></a>
                        </div>
                    </div>

                    <div className="right-side">
                        <h4>{this.props.title}</h4>
                        <p>Value: {this.props.value}<span className="price-icon"></span></p>
                    </div>
                </Link>
            </li>
        );
    }
}

export default IcoItem;