import React, { Component } from 'react';

class ServicesList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="trending-box">
                <div className="trending-box-wrap">
                    <h3>{this.props.title}</h3>
                    <div className="trending-inner">
                        <ul className="mCustomScrollbar">
                            {this.props.children}
                        </ul>

                        <a href="#" className="btn-regular">
                            {this.props.actionButtonText} &nbsp;
                            <span>
                                <i aria-hidden="true" className="fa fa-angle-double-right"></i>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        );
    }
}

export default ServicesList;