import React, { Component } from 'react';

class DailyActionContainer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <div className="trending-heading">
                    <h3>{this.props.title}</h3>
                </div>

                <div className="daily-part">
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default DailyActionContainer;