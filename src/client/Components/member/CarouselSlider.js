import React, { Component } from 'react';

class CarouselSlider extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ul className="member-slider">
                {this.props.children}
            </ul>
        );
    }
}

export default CarouselSlider;