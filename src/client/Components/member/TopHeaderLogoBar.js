import React, { Component } from 'react';
import Button from '../common/others/Button';
import {Config} from "../../Config";
import {Link} from "react-router-dom";

class TopHeaderLogoBar extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <div className="header-menu">
                    <div className="container">
                        <div className="logo">
                            <a href="/" style={{ marginTop: "9px" }}>
                                <img src="assets/login/images/bu-banner.png" alt="BlockUnion"/>
                            </a>
                        </div>
                        <div className="top-right">
                            <div className="goal">
                                <span>DAILY GOAL</span>
                                <span className="green">GET YOUR DAILY GOAL</span>
                                <span className="green">& EARN BONUS BKU</span>
                            </div>
                            <div className="go-pro">
                                <Link to={Config.RouteUrls.Member.Pro} className = "btn-regular">Go Pro</Link>
                            </div>
					    </div>
                    </div>
                </div>
            </div>
        );
    }
};

export default TopHeaderLogoBar;