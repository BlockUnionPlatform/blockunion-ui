import React from 'react';
import ServicesList from './ServicesList';
import BlockCardItem from "../admin/pages/Blocks/BlockCardItem";
import {Config} from "../../Config";

export default function ServicesContainer(props) {
    return (
        <div>
            <div className="trending-part">
                <ServicesList title="BLOCK SERVICES" actionButtonText="FIND BLOCK SERVICES">
                    { props.blocks.map(block => <BlockCardItem url={`${Config.RouteUrls.Member.Blocks.View}/${block.id}`} block={block} isListItem={true} key={block.id} />) }
                </ServicesList>

                <ServicesList title="FIND THE ACTIVE ICOs" actionButtonText="FIND THE ACTIVE AIRDROPS">
                    { props.icos.map(block => <BlockCardItem url={`${Config.RouteUrls.Member.Blocks.View}/${block.id}`} block={block} isListItem={true} key={block.id} />) }
                </ServicesList>

                <ServicesList title="TRENDING" actionButtonText="TRENDING NEW RELEASES">
                    { props.trending.map(block => <BlockCardItem url={`${Config.RouteUrls.Member.Blocks.View}/${block.id}`} block={block} isListItem={true} key={block.id} />) }
                </ServicesList>
            </div>
        </div>
    );
}