import React, { Component } from 'react';
import { timingSafeEqual } from 'crypto';

class SidebarItemsContainer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="left-menu-wrap">
                <h4 className="menu-title">{this.props.title}</h4>
                <ul>
                    {this.props.children}
                </ul>
            </div>
        );
    }
}

export default SidebarItemsContainer;