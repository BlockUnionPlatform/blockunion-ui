import React, { Component } from 'react';
import SidebarItemsContainer from './SidebarItemsContainer';
import { Link } from 'react-router-dom';

import Home from './pages/Home';
import {Config} from "../../Config";
import BlocksMemberHome from "./pages/Blocks/BlocksMemberHome";
import BlocksPayouts from "./pages/Blocks/BlocksPayouts";
import BlocksClaims from "./pages/Blocks/BlockClaims";
import DailyActionPayouts from "./pages/DailyActions/DailyActionPayouts";
import DailyActionClaims from "./pages/DailyActions/DailyActionClaims";
import EnsureIsAdminContainer from "../admin/EnsureIsAdminContainer";

class SidebarContainer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="left-panel">
                {/*
                            <CarouselSlider>
                                <li><img src="assets/member/images/member-slider1.jpg" alt=""/></li>
                                <li><img src="assets/member/images/member-slider1.jpg" alt=""/></li>
                                <li><img src="assets/member/images/member-slider1.jpg" alt=""/></li>
                            </CarouselSlider>
                        */}

                <SidebarItemsContainer title="Actions">
                    <li className="money">
                        <Link to={Config.RouteUrls.Member.Home} component={Home}>Dashboard</Link>
                    </li>
                    <li className="money">
                        <Link to={Config.RouteUrls.Member.Blocks.Home} component={BlocksMemberHome}>Blocks</Link>
                    </li>
                    <li className="money">
                        <Link to={Config.RouteUrls.Member.Blocks.Payouts} component={BlocksPayouts}>Block Payouts</Link>
                    </li>
                    <li className="money">
                        <Link to={Config.RouteUrls.Member.Blocks.Claims} component={BlocksClaims}>Block Claims</Link>
                    </li>
                    <li className="money">
                        <Link to={Config.RouteUrls.Member.DailyActionPayouts} component={DailyActionPayouts}>Daily Action Payouts</Link>
                    </li>
                    <li className="money">
                        <Link to={Config.RouteUrls.Member.DailyActionClaims} component={DailyActionClaims}>Daily Action Claims</Link>
                    </li>

                    <EnsureIsAdminContainer redirect={false}>
                        <li className="money">
                            <Link to="/admin">Admin</Link>
                        </li>
                    </EnsureIsAdminContainer>
                </SidebarItemsContainer>
            </div>
        );
    }
}

export default SidebarContainer;