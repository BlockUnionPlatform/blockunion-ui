import React, { Component } from 'react';
import {Link} from "react-router-dom";
import {Config} from "../../Config";
import {Auth} from "../common/AuthHelpers";

class TopHeader extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <div className="header-top">
                    <div className="container">
                        <div className="header-center">
                            <ul>
                                <li className="ref">Refer &amp; Earn</li>
                                <li className="dropdown">
                                    <span className="topuser-img">
                                        <img src="assets/member/images/user-img.png" alt="" title="" />
                                    </span>
                                    {
                                        Auth.isAuthenticated() ?
                                            <span>

                                                <span>Welcome, {Auth.getUser().firstName}.</span>
                                                <div className="dropdown">
                                                    <a className="dropbtn">
                                                        <i className="fa fa-angle-down" aria-hidden="true"></i>
                                                    </a>

                                                    <div className="dropdown-content">
                                                        <Link to={Config.RouteUrls.Profile.Member}>My Profile</Link>
                                                        <a href={Config.RouteUrls.Member.Home}>Members Area</a>
                                                        <a href={Config.RouteUrls.Auth.Logout}>Logout</a>
                                                    </div>
                                                </div>
                                            </span> :
                                            <span>
                                                <a href="/auth/login" style={{ color: "#fff" }}>Login / Register</a>
                                            </span>
                                    }
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};

export default TopHeader;