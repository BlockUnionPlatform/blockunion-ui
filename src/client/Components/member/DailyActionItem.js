import React, { Component } from 'react';
import {Redirect} from "react-router";
import {Config} from "../../Config";

class DailyActionItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            redirect: false
        };

        this.onItemClick = this.onItemClick.bind(this);
    }

    onItemClick() {
        this.setState({ redirect: true });
    }

    render() {
        let reward = '', price = '', logo = '', title = '', id = null;

        if (this.props.dailyAction !== undefined && this.props.dailyAction != null) {
            reward = `Earn ${this.props.dailyAction.rewardValue}`;
            price = this.props.dailyAction.rewardValue;
            logo = this.props.dailyAction.logo;
            title = this.props.dailyAction.shortTitle;
            id = this.props.dailyAction.id;
        }
        else {
            reward = `Earn ${this.props.reward}`;
            price = this.props.reward;
            logo = this.props.image;
            title = this.props.title;
        }

        return (
            <div className={`wblock ${this.props.containerClass}`} onClick={this.onItemClick}>
                <p className="weapon-title">{title}</p>

                {
                    id != null &&
                        <span>
                            {
                                this.state.redirect ?
                                    <Redirect to={`${Config.RouteUrls.Member.DailyActions.Home}/${this.props.dailyAction.id}`} />
                                    :
                                    <span></span>
                            }
                        </span>
                }

                <div className="w_img">
                    {
                        (logo === '' || logo === null) ?
                            <img src="assets/member/images/earn-default.svg" alt=""/>
                            :
                            <img src={logo} alt="Earn" />
                    }
                </div>

                <div className="w_info">
                    <div className="left-info">
                        <p>{reward}</p>
                    </div>

                    <div className="price">
                        <p>+ {price}</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default DailyActionItem;