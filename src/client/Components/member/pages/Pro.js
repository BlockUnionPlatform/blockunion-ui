import React, { Component } from 'react';
import PageHeaderWithLink from "../../admin/pages/PageHeaderWithLink";

export default class Pro extends Component {
    render() {
        return (
            <div>
                <PageHeaderWithLink title = "Go Pro" />
                <p>We are currently working to bring Pro features for our members, stay tuned for more updates!</p>
            </div>
        );
    }
}