import React, {Component} from 'react';
import SiteTitle from "../../../common/SiteTitle";
import PageHeaderWithLink from "../../../admin/Pages/PageHeaderWithLink";
import {Config} from "../../../../Config";
import LoadingScreen from "../../../common/LoadingScreen";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';
import RewardClaimRow from "../../../admin/Pages/RewardClaims/RewardClaimRow";
import {Auth} from "../../../common/AuthHelpers";

export default class DailyActionClaims extends Component {
    constructor(props) {
        super(props);

        this.state = {
            rewardClaims: [],
            isLoading: true,
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });
    }

    componentDidMount() {
        let url = `${getApi(Config.API.RewardClaims)}/user/${Auth.getUser().id}`;

        axios.get(url)
            .then(res => {
                this.setState({
                    isLoading: false,
                    rewardClaims: res.data
                });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title="Daily Action Claims" />
                <PageHeaderWithLink title="Daily Action Claims" />
                <p className="form-group">
                    A list of all the daily actions claimed by you.
                </p>

                <div className="row">
                    <div className="col-md-12">
                        <div className="table-responsive">
                            <table className="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Claimed By</th>
                                    <th>Reward</th>
                                    <th>Daily Action</th>
                                    <th>Date Created</th>
                                    <th>Is Processed?</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>

                                <tbody>
                                { this.state.rewardClaims.map((claim) => <RewardClaimRow claim={claim} key={claim.id} isAdmin={false} />)}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}