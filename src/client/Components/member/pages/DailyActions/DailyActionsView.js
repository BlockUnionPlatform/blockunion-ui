import React, {Component} from 'react';
import {getApi} from "../../../../ConfigStore";
import {Config} from "../../../../Config";
import axios from 'axios';
import SiteTitle from "../../../common/SiteTitle";
import LoadingScreen from "../../../common/LoadingScreen";
import AlertBox from "../../../common/AlertBox";
import DailyActionItem from "../../DailyActionItem";
import moment from "moment";
import {Auth} from "../../../common/AuthHelpers";

export default class DailyActionsView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dailyAction: null,
            isLoading: true,
            isClaimed: false,
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
        this.onClaimClick = this.onClaimClick.bind(this);
    }

    showAlert(show, status, message) {
        this.setState({ alert: { show, status, message } });
    }

    onClaimClick(e) {
        e.preventDefault();

        if (confirm("Are you sure you want to submit your claim for this daily action?")) {
            let url = `${getApi(Config.API.DailyActions)}/claim`;
            let payload = {
                userId: Auth.getUser().id,
                rewardId: this.state.dailyAction.reward.id,
                entityId: this.state.dailyAction.id,
                comments: ''
            };

            axios.post(url, payload)
                .then(res => {
                    this.showAlert(true, 'success', 'Your claim has been submitted successfully!');
                    this.setState({ isLoading: false });
                })
                .catch(error => {
                    if (error.response)
                        this.showAlert(true, 'error', error.response.data.toString(), true);
                    else if (error.request)
                        this.showAlert(true, 'error', 'Unable to connect to server', true);
                    else
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                    this.setState({ isLoading: false });
                });
        }
    }

    componentDidMount() {
        let url = `${getApi(Config.API.DailyActions)}/${this.props.match.params.id}`;

        axios.get(url)
            .then(res => {
                this.setState({
                   dailyAction: res.data
                });

                let claimUrl = `${getApi(Config.API.DailyActions)}/claim/check`;
                let payload = {
                    userId: Auth.getUser().id,
                    entityId: this.state.dailyAction.id
                };

                axios.post(claimUrl, payload)
                    .then(res => {
                        this.setState({ isClaimed: res.data.isClaimed, isLoading: false });
                    })
                    .catch(error => {
                        if (error.response)
                            this.showAlert(true, 'error', error.response.data.toString(), true);
                        else if (error.request)
                            this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);
                        else
                            this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                       this.setState({ isLoading: false });
                    });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });
    }

    render() {
        return (
            <div>
                <SiteTitle title = "View Daily Action" />
                <LoadingScreen show={this.state.isLoading} />
                <AlertBox {...this.state.alert} />
                {
                    !this.state.isLoading && this.state.dailyAction != null &&
                    <div>
                        <h1 className="text-center mb-30">{this.state.dailyAction.longTitle}</h1>

                        <div className="row">
                            <div className="col-md-6">
                                <h2>Description</h2>
                                <div className="daily-action-text">
                                    <div dangerouslySetInnerHTML={{ __html: this.state.dailyAction.description }}></div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <h2 className="text-center">Valid Till</h2>
                                    <p className="d-block text-center">
                                        <span style={{ fontSize: '24px' }}>
                                            {moment(this.state.dailyAction.validTillUtc).format(Config.Formats.DateTime)}
                                        </span>
                                    </p>
                                </div>

                                <div className="form-group">
                                    <h2 className="text-center">Reward</h2>
                                    <p className="d-block text-center">
                                        <span style={{ fontSize: '24px' }}>
                                            {this.state.dailyAction.rewardValue}
                                        </span>
                                    </p>
                                </div>

                                <div className="daily-action mt-30 mx-auto" style={{ width: '80%' }}>
                                    <DailyActionItem dailyAction={this.state.dailyAction} />
                                </div>

                                <div className="form-group mt-30">
                                    <button className = "btn btn-block btn-theme" onClick={this.onClaimClick} disabled={this.state.isClaimed}>CLAIM DAILY ACTION</button>
                                    {
                                        this.state.isClaimed &&
                                        <div className="text-center mt-5">
                                            <span className="small">You have already claimed this Daily Action.</span>
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </div>
        );
    }
}