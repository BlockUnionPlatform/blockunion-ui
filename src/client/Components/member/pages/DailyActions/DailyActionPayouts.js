import React, {Component} from 'react';
import SiteTitle from "../../../common/SiteTitle";
import PageHeaderWithLink from "../../../admin/pages/PageHeaderWithLink";
import PayoutRowItem from "../../../admin/pages/Payouts/PayoutRowItem";
import {getApi} from "../../../../ConfigStore";
import {Config} from "../../../../Config";
import axios from 'axios';
import AlertBox from "../../../common/AlertBox";
import LoadingScreen from "../../../common/LoadingScreen";
import UserLink from "../../../admin/pages/Common/UserLink";
import {Auth} from "../../../common/AuthHelpers";

export default class DailyActionPayouts extends Component {
    constructor(props) {
        super(props);

        this.state = {
            payouts: [],
            isLoading: true,
            alert: {
                show: false,
                status: '',
                message: '',
            }
        }

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });
    }

    componentWillMount() {
        let url = `${getApi(Config.API.Payouts)}/user/${Auth.getUser().id}`;

        axios.get(url)
            .then(res => {
                this.setState({
                    payouts: res.data,
                    isLoading: false
                })
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title="Daily Action Payouts"/>
                <PageHeaderWithLink title="Daily Action Payouts"/>
                <p>
                    A list of all the payouts made to you for your daily action claims.
                </p>
                <AlertBox {...this.state.alert} />

                <div className="table-responsive">
                    {
                        this.state.isLoading ?
                            <div>Loading</div>
                            :
                            <div>
                                <table className="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Amount</th>
                                        <th>Paid To</th>
                                        {
                                            this.props.isAdmin != undefined && this.props.isAdmin &&
                                            <th>Processed By</th>
                                        }
                                        <th>Reward Claim</th>
                                        <th>Date Processed</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    {
                                        this.state.payouts.map((payout) =>
                                            <PayoutRowItem payout={payout} />
                                        )
                                    }
                                    </tbody>
                                </table>
                            </div>
                    }
                </div>
            </div>
        );
    }
}
