import React, {Component} from 'react';
import PageHeaderWithLink from "../../../admin/pages/PageHeaderWithLink";
import {Config} from "../../../../Config";
import SiteTitle from "../../../common/SiteTitle";
import PayoutCard from "../../../admin/pages/Payouts/PayoutCard";
import AlertBox from "../../../common/AlertBox";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';
import LoadingScreen from "../../../common/LoadingScreen";

export default class DailyActionPayoutsView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            payout: {},
            isLoading: true,
            isFound: true,
            alert: {
                show: false,
                status: '',
                message: '',
            }
        };

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });
    }

    componentDidMount() {
        let url = `${getApi(Config.API.Payouts)}/${this.props.match.params.id}`;


        axios.get(url)
            .then(res => {
                this.setState({
                    payout: res.data,
                    isFound: true,
                    isLoading: false
                });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false, isFound: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title="View Daily Action Payout"/>
                <PageHeaderWithLink
                    title="View Daily Action Payout"
                    btnText="Back to Payouts"
                    btnIcon="fa fa-arrow-left"
                    btnUrl={Config.RouteUrls.Member.DailyActionPayouts} />

                <AlertBox {...this.state.alert} />

                {
                    this.state.isLoading ?
                        <div>Loading</div>
                        :
                        <div>
                            {
                                !this.state.isFound ?
                                    <div>Not Found</div>
                                    :
                                    <PayoutCard payout={this.state.payout} isAdmin={false} />
                            }
                        </div>
                }
            </div>
        );
    }
}