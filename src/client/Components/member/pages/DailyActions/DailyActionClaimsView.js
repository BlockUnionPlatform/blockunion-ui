import React, {Component} from 'react';
import PageHeaderWithLink from "../../../admin/Pages/PageHeaderWithLink";
import SiteTitle from "../../../common/SiteTitle";
import LoadingScreen from "../../../common/LoadingScreen";
import RewardClaimCard from "../../../admin/Pages/RewardClaims/RewardClaimCard";
import {Config} from '../../../../Config';
import axios from 'axios';
import {getApi} from "../../../../ConfigStore";

export default class DailyActionClaimsView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            rewardClaim: {},
            isLoading: true,
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });
    }

    componentDidMount() {
        let url = `${getApi(Config.API.RewardClaims)}/${this.props.match.params.id}`;

        axios.get(url)
            .then(res => {
                this.setState({
                    isLoading: false,
                    rewardClaim: res.data
                });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title = "View Daily Action Claim" />
                <PageHeaderWithLink title = "View Daily Action Claim" btnText = "Back to Daily Action Claims" btnIcon = "fa fa-arrow-left" btnUrl={Config.RouteUrls.Member.DailyActionClaims} />

                {
                    this.state.isLoading ?
                        <div>Loading</div>
                        :
                        <RewardClaimCard claim={this.state.rewardClaim} isAdmin={false} />
                }
            </div>
        );
    }
}