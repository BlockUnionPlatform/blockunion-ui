import React, { Component } from 'react';

export default class MemberPaymentsHome extends Component {
    render() {
        return (
            <div>
                All the payments made to you will appear here.
            </div>
        );
    }
}