import React, { Component } from 'react';

export default class MemberReferralsHome extends Component {
    render() {
        return (
            <div>
                Your pending / approved referrals will appear here.
            </div>
        );
    }
}