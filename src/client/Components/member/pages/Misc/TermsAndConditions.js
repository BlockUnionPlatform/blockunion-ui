import React from 'react';
import SiteTitle from "../../../common/SiteTitle";

export default function TermsAndConditions() {
    return (
        <div>
            <SiteTitle title = "Terms Of Use" />
            <h2>BlockUnion Terms of Use</h2>
            <p>
                These Terms of Use were last updated on: November 1, 2018
            </p>

            <h3 className="text-box mt-20">NOTICE OF ARBITRATION PROVISIONS:</h3>
            <p className="text-semibold">
                Your use of the Block Union Sites and Features and our Services (both as defined below) is subject to binding individual arbitration of any disputes which may arise, as provided in Paragraph 11 of these Terms of Use. Please read the arbitration provisions carefully and do not use any of the Block Union Sites and Features or our Services if you are unwilling to arbitrate any disputes you may have with us (including without limitation any disputes relating to these Terms of Use, our Privacy Policy, and any Additional Terms) as provided herein.
            </p>

            <p className="mt-20">
                Block Union, LTD (together with any affiliates, the “Company”) owns and operates a number of different websites, mobile apps, and interactive services, including without limitation blockunion.io, and others (collectively, the “Block Union Sites”). These Terms of Use (“Terms”) apply to the Block Union Sites and to all of the features, mobile applications, emails, online services and other functionalities (collectively, the “Features”) available via or related to the Block Union Sites, whether accessed via a computer, mobile device, or otherwise (collectively, the “Block Union Sites and Features”).
            </p>

            <p className="mt-20">
                These Terms are a legal agreement between you and the Company. By using any of the Block Union Sites and Features or our Services, and/or clicking to “Accept” or otherwise agreeing to these Terms where that option is made available to you, you agree to be bound by these Terms as well as our Privacy Policy (http://blockunion.io/member/privacy). If you do not agree to these Terms or our Privacy Policy, please do not register with or use any Block Union Sites or Features or our Services.
            </p>

            <p className="mt-20">
                We may post additional terms, official rules, or agreements that apply to certain services, applications, activities, and features we offer or provide at or through certain Block Union Sites and Features (“Additional Terms”), and you may be subject to such Additional Terms when you access those services, applications, activities and/or features. In the event of any conflict between the terms of the Additional Terms (on the one hand) and these Terms (on the other hand), these Terms shall prevail unless expressly otherwise stated in the Additional Terms, which are intended to supplement, but not replace, these Terms.
            </p>

            <p className="mt-20">
                Please don’t hesitate to contact us with any questions regarding these Terms or any Additional Terms. You can reach us by using the “Contact Us”, “Contact Member Services” or similar contact link in the footer of any of the Block Union Sites, or by mail to:, 30 N Gould St, Suite R Sheridan WY 82801; Attention: Customer Service.
            </p>

            <h2 className="mt-20 text-semibold">TABLE OF CONTENTS:</h2>

            <ol>
                <li>Use of Block Union Sites and Features</li>
                <li>User Representations and Warranties</li>
                <li>Sweepstakes, Contests and Promotions</li>
                <li>Rewards Programs</li>
                <li>Intellectual Property</li>
                <li>Reporting Copyright Infringement – DMCA Policy</li>
                <li>User Conduct</li>
                <li>Communications Channels</li>
                <li>Disclaimer of Warranties; Limitation of Liability; Indemnification</li>
                <li>Compliance with FTC Guidelines on Endorsements/Testimonials</li>
                <li>Binding Arbitration of all Disputes</li>
                <li>Tax Matters</li>
                <li>Notification of Changes</li>
                <li>International Users</li>
                <li>Excluded Users and Territories</li>
                <li>Miscellaneous</li>
                <li>Contact Us</li>
            </ol>

            <h2 className="mt-20">1. Use of Block Union Sites and Features</h2>
            <p className="mt-20">
                You agree to use the Block Union Sites and Features and the services available on or through the Block Union Sites and Features (the “Services”) only for purposes that are permitted by these Terms, any Additional Terms, and any applicable law, regulation or generally accepted practices in the relevant jurisdictions. Subject to all of the provisions of these Terms, the Company hereby grants you a limited, terminable, non-transferable, personal, non-exclusive license to access and use the Block union Sites and Features and our Services solely as provided herein. You may download material displayed on the Block union Sites and Features for non-commercial, personal use only, provided you do not remove any copyright and other proprietary notices contained on the materials. You may not, however, distribute, modify, broadcast, publicly perform, transmit, reuse, re-post, or use the content of the Block union Sites and Features, including any text, images, audio, and video, for public or commercial purposes without the Company’s prior written permission. Notwithstanding anything to the contrary herein, all rights not specifically granted in the license set forth above shall be reserved and remain always with the Company. Your right to use the Block union Sites and Features and our Services is not transferable. You acquire no rights or licenses in or to the Block union Sites and Features and materials contained therein other than the limited right to access and utilize the Block union Sites and Features and our Services in accordance with these Terms.
            </p>
            <p className="mt-20">
                If you are accessing the Block union Sites and Features via any of our applications available via third parties (collectively “Third Party Outlets”) including, without limitation, Apple, Inc.’s “App Store” or Google, Inc.’s “Google Play” store, you acknowledge and agree that these Terms are entered into by and between you and the Company only, and that none of the Third Party Outlets are party to these Terms. The Third Party Outlets are not sponsors to, nor in any way affiliated with, any of the Company’s Promotions (defined below), nor any of our Services or the Block union Sites and Features.
            </p>

            <h2 className="mt-20">2. User Representations and Warranties</h2>
            <p className="mt-20">
                By using the Block union Sites and Features or our Services, you represent, warrant and covenant that you: (i) have the power and authority to enter into and be bound by these Terms; (ii) shall use the Block union Sites and Features and our Services only as permitted by these Terms, and any applicable Additional Terms, and not for any unlawful purpose; and (iii) are thirteen (13) years of age or older. If you are under the age of 13, you are not allowed to use the Block union Sites and Features nor our Services. Some offerings on the Block union Sites and Features or our Services may be subject to additional age restrictions.
            </p>

            <h2 className="mt-20">3. Bounties, Sweepstakes, Contests and Promotions</h2>
            <p className="mt-20">
                Any sweepstakes, contests or promotions (collectively, “Promotions”) that may be offered via any of the Block union Sites and Features or our Services may be governed by Additional Terms, which may set out eligibility requirements, such as certain age or geographic area restrictions, terms and conditions, and details governing how your personal information may be used. It is your responsibility to read all Additional Terms to determine whether or not you want to or are eligible to participate, enter or register in or for the Promotions. By participating in a Promotion, you will be subject to the Additional Terms and you agree to comply with and abide by such Additional Terms and the decisions of the sponsor(s) thereof. The Third Party Outlets are in no way associated with the Company’s Promotions.
            </p>

            <h2 className="mt-20">4. Rewards Programs</h2>
            <p className="mt-20">
                <h3>Overview</h3>
                <p className="mt-20">
                    The Company may offer one or more rewards programs (“Rewards Programs”) under which you may have the opportunity to earn tokens (in the blockunion.io program, such tokens are called “BKU, and in the blockunion.io program, they are called “Tokens”), which are redeemable for rewards. Not all of the Block union Sites and Features offer Rewards Programs, however, and Rewards Programs may include Additional Terms that apply to your participation in activities allowing you to earn tokens (collectively, “Activities”). The Company may limit, suspend or terminate your ability to participate in a Rewards Program in its sole and absolute discretion, and may void any tokens, rewards, or potential rewards you may have earned or accumulated in a Rewards Program, if we determine in our sole discretion that you have not complied with these Terms or any Additional Terms applicable to such participation. You agree to abide by the final and binding decisions of the Company regarding any Rewards Program and your participation in it. We reserve the right to change, suspend, or cancel all or a portion of a Rewards Program, including any tokens you may have accrued, at any time without prior notice to you.
                </p>

                <h3 className="mt-20">Earning Tokens</h3>
                <p className="mt-20">
                    Tokens can be earned in a Rewards Program by participating in certain Activities, as described in the applicable Block union Sites and Features. If you choose to participate and follow the instructions associated with an Activity, upon satisfying all of the requirements of the Activity, you will be awarded the tokens associated with completing that Activity so long as the Company and/or its third-party Rewards Program affiliates are able to properly track your valid and completed point-earning Activities. For avoidance of doubt, Company shall not be responsible for, nor shall Company be obligated to award tokens or rewards to Rewards Program participants for, any Activity that is not properly recorded, tracked and/or deemed approved under Company’s or its third-party Rewards Program affiliates’ policies, procedures and systems. There may be limitations on Activities and rewards, so please be sure to review all applicable Additional Terms before deciding whether or not you would like to participate. For example, we reserve the right to request and verify receipts of completed purchases prior to or in connection with the awarding of tokens for shopping Activities in order to verify with the applicable merchant that such purchases are valid. Some of the limitations on Activities and rewards include (without limitation), our right to change or limit your ability to participate in certain Activities or the Rewards Program itself; our right to change or limit the allowable frequency of Activities; our right to change or limit the number of tokens you can earn for a given Activity or during a given time period; and our right to change the Activities or rewards available, or the number of tokens required for a particular reward. Tokens awarded have no cash value.
                </p>

                <h3 className="mt-20">Redeeming Tokens</h3>
                <p className="mt-20">
                    You may redeem tokens you have earned for rewards offered in a Rewards Program pursuant to these Terms and any applicable Additional Terms. Rewards may be awarded on a first-come, first-serve and while-supplies-last basis. If you attempt to redeem tokens for a reward and the Company determines that the reward is unavailable, out of stock, or for whatever reason cannot be provided to you, the Company may, at its sole and absolute discretion, provide you with a reward of equal or greater value. No credit, reversal, or refund of tokens will be issued for any reason after tokens have been redeemed; in other words, once you order a reward, you may not cancel the reward or return the reward for a refund of tokens. Some rewards may have eligibility requirements and in that case the Company reserves the right to verify your identity (by requesting photocopy of your driver’s license or state ID card, or other proof as we may require) and eligibility qualifications to our complete satisfaction prior to crediting tokens or fulfilling any reward in any Rewards Program, or otherwise providing you with any benefit.
                </p>

                <h3 className="mt-20">Delivery of Rewards</h3>
                <p className="mt-20">
                    Rewards may be emailed to your email address or mailed to the postal address, as applicable, that you provided when you registered and created an account (“Account”) for the applicable Rewards Program, or to the email or postal address that our records show your Account was last updated to reflect. Processing times may vary. Rewards that are undeliverable or unclaimed for whatever reason (including, without limitation, because your Account information is incorrect or outdated) may be forfeited, and the tokens will not be refunded.
                </p>

                <h3 className="mt-20">Rewards Program Tokens Nontransferable</h3>
                <p className="mt-20">
                    Rewards Program tokens are nontransferable, may not be bartered or sold, and are void if a transfer is attempted, and such tokens and the associated Account are not transferable upon death, as part of a domestic relations matter or otherwise by operation of law.
                </p>

                <h3 className="mt-20">Inactive Accounts</h3>
                <p className="mt-20">
                    Any Account that has not been logged into and tokens either earned or redeemed for one year or more may be deemed inactive and the Account closed. In such instances, to request Account reactivation (subject to such terms, limitations and requirements as we may impose from time to time) you may reach us by using the “Contact Us”, “Contact Member Services” or similar contact link in the footer of any of the Block union Sites. We may modify our inactive Account rules and policies in our Rewards Programs from time to time, and if your Account becomes inactive pursuant to such then-current rules or policies, we may close your Account, without any compensation or further obligation to you.
                </p>
            </p>


            <h2 className="mt-20">5. Intellectual Property</h2>
            <p className="mt-20">
                You acknowledge that the Block union Sites and Features have been developed, compiled, prepared, revised, selected and arranged by the Company and others through the expenditure of substantial time, effort and money and constitutes valuable intellectual property and trade secrets of the Company and others. It is our policy to enforce these intellectual property rights to the fullest extent permitted under law. The trademarks, logos and service marks (“Marks”) displayed on the Block union Sites and Features are the property of the Company or third parties and cannot be used without the written permission of the Company or the third party that owns the Marks. The Block union Sites and Features are also protected as a collective work or compilation under U.S. copyright and other foreign and domestic laws and treaties. Users are prohibited from using (except as expressly set forth herein), transferring, disposing of, modifying, copying, distributing, transmitting, broadcasting, publicly performing, displaying, publishing, selling, licensing, or creating derivative works of any content on the Block union Sites and Features or our Services for commercial or public purposes. Nothing contained herein shall be construed by implication, estoppel or otherwise as granting to the user an ownership interest in any copyright, trademark, patent or other intellectual property right of the Company or any third party. The Company exclusively owns all worldwide right, title and interest in and to all documentation, software, contents, graphics, designs, data, computer codes, ideas, know-how, “look and feel,” compilations, magnetic translations, digital conversions and other materials included within the Block union Sites and Features and related to our Services, and all modifications and derivative works thereof, and all intellectual property rights related thereto.
            </p>

            <h2 className="mt-20">6. Reporting Copyright Infringement - DMCA Policy</h2>
            <p className="mt-20">
                If you believe that any content, user-posted materials, or any other material found on or through the Block union Sites and Features or our Services, including through a hyperlink, infringes your copyright, you should notify us. To be effective, the notification to us must be in writing and must comply with the following instructions:
            </p>

            <ol>
                <li>
                    Written notices must be sent either:
                    <ul>
                        <li>Electronically to dmca@Blockunionblockunion.io with subject line “DMCA Takedown Request”. Emails sent to dmca@blockunion.io for purposes other than communication about copyright infringement may not be answered; or</li>
                        <li>Via certified mail (with a confirmed receipt requested) to: DMCA Notice, Block Union, LLC, 30 N Gould St, Suite R Sheridan WY 82801; Attention: Copyright Agent.</li>
                    </ul>
                </li>

                <li>
                    Each written notification must contain the following information to be deemed a valid notice:

                    <ul>
                        <li>An electronic or physical signature of the person authorized to act on behalf of the owner of the exclusive copyright interest;</li>
                        <li>Description of the copyrighted work that you claim has been infringed;</li>
                        <li>Description of where the material that you claim is infringing is located on the Block Union Sites and Features that is reasonably sufficient to enable us to identify and locate the material (for example, a complete list of specific URLs);</li>
                        <li>Our physical mailing address, telephone number and email address;</li>
                        <li>Statement by you affirming that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law; and</li>
                        <li>A statement by you that the information provided in your notice is accurate and, under penalty of perjury, that you are the owner of an exclusive right in the material or that you are authorized to act on behalf of the copyright owner.</li>
                    </ul>
                </li>
            </ol>
            <p className="mt-20">
                We will process each written notice of alleged infringement that we receive and will take appropriate action in accordance with applicable intellectual property laws.
                We have a policy of terminating and/or blocking repeat infringers in appropriate circumstances, in our sole discretion, subject to reasonable limitations.
            </p>

            <h2 className="mt-20">7. User Conduct</h2>
            <p className="mt-20">
                You agree that you will not engage in any activity that interferes with or disrupts the Block union Sites and Features or our Services (or the servers and networks which are connected to our Services) or use any service to manipulate your computer or other device to gain any advantage on any of our programs. Unless you have been specifically permitted to do so in a separate written agreement with us, you agree that you will not reproduce, duplicate, copy, sell, trade or resell our Services for any purpose. You further agree that your use of the Block union Sites and Features and our Services shall not be fraudulent or deceptive, or unlawful, as determined in our sole and absolute discretion. You shall also comply with all usage rules found throughout the Block union Sites and Features and/or our Services, including, without limitation, or other guidelines posted on any of the Block union Sites and Features. You agree to comply with the instructions set out in any page present on the Block union Sites and Features and our Services. Without limiting the generality of the foregoing, you agree not to use the Block union Sites and Features or our Services in order to:
                <ul className="mt-20">
                    <li>access (or attempt to access) any of our Services by any means other than through the interface that we provide;</li>
                    <li>share a single Account with any person other than the registered Account holder;</li>
                    <li>create and/or use multiple Accounts (i.e. only one Account is permitted per person);</li>
                    <li>maintain or use any false identity or multiple identities, or otherwise fail to participate in our </li>
                    <li>Services using your real identity and accurate contact, demographic and other information;</li>
                    <li>submit any personal information (name, email, zip code, etc.), payment information (credit card number and expiration date, etc.), or other information which we determine in our sole discretion to have been false, inaccurate or otherwise invalid in connection with any Activities or any other use of the Block union Sites and Features or our Services;</li>
                    <li>post, upload, transmit or otherwise disseminate information that (in our sole discretion) is obscene, indecent, vulgar, pornographic, sexual, hateful or otherwise objectionable;</li>
                    <li>post spam links, and/or personal referral links in an aggressive, wanton, or otherwise inappropriate fashion, whether on any Block union Sites and Features or on any other web site or application;</li>
                    <li>defame, libel, ridicule, mock, stalk, threaten, harass, intimidate or abuse anyone, hatefully, racially, ethnically or, in a reasonable person’s view, otherwise act in an offensive or objectionable manner;</li>
                    <li>upload or transmit (or attempt to upload or transmit) files that contain viruses, Trojan horses, worms, time bombs, cancelbots, corrupted files or data, or any other similar software or programs that may damage the operation of the Service, other users’ computers, or access to or functionality of the Block union Sites and Features;</li>
                    <li>violate the contractual, personal, intellectual property or other rights of any party, including using, uploading, transmitting, distributing, or otherwise making available any information made available through the Block union Sites and Features or our Services in any manner that infringes any copyright, trademark, patent, trade secret, or other right of any party (including rights of privacy or publicity);</li>
                    <li>attempt to obtain account information, passwords or other private information from other members;</li>
                    <li>improperly use support channels or complaint buttons to make false or frivolous reports to the Company or to communicate with our customer support representatives in a disrespectful, belligerent or inappropriate manner;</li>
                    <li>develop, distribute, make use of, or publicly inform other members of: “auto” software programs, “macro” software programs, web crawlers or other script or “cheat utility” software programs or applications;</li>
                    <li>abuse any of our Services in a manner that does not reflect normal or appropriate human usage, such as conducting excessive searches or other Activities in our Rewards Programs for the sole or primary purpose of receiving tokens, as we may determine in our sole discretion; or</li>
                    <li>exploit, distribute or publicly inform other members of any error, miscue or bug (“Error”) that gives an unintended advantage, violate any applicable laws or regulations, or promote or encourage any illegal or unauthorized activity including, but not limited to, hacking, cracking or distribution of counterfeit software, or cheats or hacks for our Services. If you find an BUG, we kindly request that you report it to our BUG BOUNTY support team by using the “Contact Us”, “Contact Member Services” or similar contact link in the footer of any of the Block union Sites, and a BTC or ETH bounty reward will be due to you if the BUG is unknown. </li>
                    <li>If we determine in our sole discretion that you have violated these Terms, the Company may in its sole discretion issue you a warning regarding the violation prior to terminating or suspending any or all Accounts you have created (or which are associated with you) using our Services. However, you acknowledge and agree that the Company need not provide you with any warning or notice before terminating or suspending your Account(s) and/or your access to the Block union Sites and Features and our Services for any reason, at its sole and absolute discretion.</li>
                </ul>
            </p>

            <h2 className="mt-20">8. Communication Channels</h2>
            <p className="mt-20">
                The Block union Sites and Features and our Services may include communication channels such as forums, communities, or chat areas (“Communication Channels”) designed to enable you to communicate with other Services users. The Company has no obligation to monitor these communication channels but it may do so in its sole discretion and reserves the right to review materials posted to the Communication Channels and to remove any materials, at any time, with or without notice for any reason, at its sole and absolute discretion. The Company may also terminate or suspend your access to any Communication Channels at any time, without notice, for any reason. You acknowledge that chats, postings, or materials posted by users on the Communication Channels are neither endorsed nor controlled by the Company, and these communications should not be considered reviewed or approved by the Company. The Company will not under any circumstances be liable for any activity within Communication Channels. You agree that all your communications with the Communication Channels are public, and thus you have no expectation of privacy regarding your use of the Communication Channels. The Company is not responsible for information that you choose to share on the Communication Channels, or for the actions of other users
            </p>

            <h2 className="mt-20">9. Disclaimer of Warranties; Limitation of Liability; Indemnification</h2>
            <p className="mt-20">
                You agree that your use of the Block union Sites and Features and our Services shall be at your own risk. To the maximum extent permitted by applicable law, the Company, and its affiliates, partners, employees, and agents, disclaim any and all guarantees, warranties and representations, express or implied, in connection with our Services, the Block union Sites and Features and your use thereof, including implied guarantees or warranties of title, merchantability or acceptable quality, fitness for a particular purpose or non-infringement, accuracy, authority, completeness, usefulness, and timeliness. To the maximum extent permitted by applicable law, the Company makes no guarantees, warranties, or representations about the accuracy or completeness of the content of the Block union Sites and Features or our Services, or the content of any sites linked to our Services, and assumes no liability or responsibility for any (i) errors, mistakes, nor inaccuracies of content, (ii) personal injury (including death) or property damage, of any nature whatsoever, resulting from your access to and use of the Block union Sites and Features or our Services, (iii) unauthorized access to or use of our secure servers and/or any and all personal information and/or financial information stored therein, (iv) interruption or cessation of transmission to or from our Block union Sites and Features or our Services, (v) bugs, viruses, Trojan horses, or the like which may be transmitted to or through our Services by any third party, (vi) errors or omissions in any content or for any loss or damage of any kind incurred as a result of the use of any content posted, emailed, transmitted, or otherwise made available via the Block union Sites and Features or our Services, and/or (vii) tax liability imposed against you by any taxing authority.
            </p>

            <p className="mt-20">
                To the maximum extent permitted by applicable law, in no event will the Company, or its affiliates, partners, employees, and agents, be liable to you or any third person for any special, direct, indirect, incidental, special, punitive, or consequential damages whatsoever, including any lost profits or lost data arising from your use of the Block union Sites and Features, our Services or other materials or content on, accessed through or downloaded from our Services, whether based on warranty, contract, tort (including without limitation negligence), or any other legal theory, and whether or not the Company has been advised of the possibility of these damages. The foregoing limitation of liability shall apply to the fullest extent permitted by law in the applicable jurisdiction. You specifically acknowledge that the Company shall not be liable for user submissions or the defamatory, offensive, or illegal conduct of any third party. You agree to indemnify and hold the Company, and each of its affiliates, partners, employees, and agents, harmless from and against any claim, cause of action, loss, liability, damages, costs and expenses, including reasonable attorney’s fees, arising out of or in connection with (i) your use of and access to the Block union Sites and Features or our Services; (ii) your violation of any term of these Terms; (iii) your violation of any third party right, including without limitation any copyright, property, or privacy right, or damage to a third party; (iv) any tax obligations arising from or related to your use of the Block union Sites and Features or our Services; and/or (v) any content you post or share on or through the Service.
            </p>

            <p className="mt-20">
                You agree that the Company will not be liable for, or be required to provide any compensation to you with respect to, the termination of any Rewards Program or any associated Account(s), including without limitation any tokens, rewards, prizes, or credits in your Account(s) or otherwise existing in your favor at the time of termination.
            </p>

            <p className="mt-20">
                Sometimes when you use our Services, you may use a service or download a piece of software, or purchase goods, provided by another person or company. Your use of these other services, software or goods may be subject to separate terms between you and the other company or person. If so, these Terms do not affect your legal relationship with these other companies or individuals.
            </p>

            <p className="mt-20">
                You agree that we are not responsible for the loss or impairment of any tokens, rewards, prizes, or credits, regardless of monetary value, in the event there is any: change in the value of each point (as determined in Company’s sole and absolute discretion), data or server error, computer and/or network system error or failure, criminal act, vandalism, cyber-attack or other events which make it commercially unreasonable for us to determine the tokens balance or value of any Account(s).
            </p>

            <p className="mt-20">
                All guarantees, warranties, and representations, whether express or implied, as to the condition, suitability, quality, fitness or safety of any goods and services supplied under our Rewards Programs or other Services are excluded to the fullest extent permitted by applicable law
            </p>

            <p className="mt-20">
                Any liability the Company may have to a member under any such guarantees, warranties or representations implied or imposed by law which cannot be excluded is hereby limited, to the extent legally permissible, to supplying or paying the cost of supplying the goods or services (or equivalent goods or services) or repairing or paying the cost of repairing the goods or re-performing the services, at the Company’s sole option.
            </p>

            <p className="mt-20">
                Please note that at any time, we may, in our sole discretion, terminate our legal agreement with you and deny you continued use of the Block union Sites and Features and our Services, and, without limiting the foregoing, may do so if (i) we are required to do so by law (for example, where the provision of our services to you is, or may become, unlawful); (ii) the partner with whom we offered our Services to you has terminated its relationship with us or ceased to offer their services to you; (ii) we are no longer providing all or any portion of our Services to users in the jurisdiction in which you are resident or from which you use our Services; or (iv) the provision of our Services to you is, in our opinion, no longer commercially viable.
            </p>

            <h2 className="mt-20">10. Compliance with FTC Guidelines on Endorsements/Testimonials</h2>
            <p className="mt-20">
                If you choose to promote our services to the public, including your own personal social networks, you agree that you will comply with the FTC’s Guidelines Concerning the Use of Testimonials and Endorsements in Advertising (available at https://www.ftc.gov/sites/default/files/attachments/press-releases/ftc-publishes-final-guides-governing-endorsements-testimonials/091005revisedendorsementguides.pdf) (“Guidelines”).
            </p>

            <h2 className="mt-20">11. Binding Arbitration of all Disputes</h2>

            <p className="mt-20">
                For example, if you have been paid or provided with free products in exchange for discussing or promoting a Block union product or service, or if you are an employee of a company and you decide to discuss or promote that company’s products or services through the Block union services, you agree to comply with the Guidelines’ requirements for disclosing such relationships. You, and not Block union, are solely responsible for any endorsements or testimonials you make regarding any product or service made on or through Block union’s services.
            </p>

            <p className="mt-20">
                <b>A. Providing Notice of a Claim.</b> The parties agree to arbitrate all disputes and claims between them (“Disputes or Claims”). This agreement to arbitrate is intended to be broadly interpreted. It includes, but is not limited to: (1) Disputes or Claims related in any way to any Block union Sites and Features or our Service, privacy, data security, collection, use and sharing, advertising, purchase transactions, tokens earnings, awards, balances, expiration, or transactions, sweepstakes, promotions, or any emails, texts, or other communications with you; (2) Disputes or Claims arising out of or relating to any aspect of the transactions or relationship between us, whether based in contract, tort, statute, fraud, misrepresentation or any other legal theory; (3) Disputes or Claims that arose before your agreement to these Terms of Use or any prior arbitration agreement; (4) Disputes or Claims that are currently the subject of purported class action litigation in which you are not a member of a certified class; and (5) Disputes or Claims that may arise after the termination of your use of any Block union Sites and Features or our Services.
            </p>

            <p className="mt-20">
                A party who intends to seek arbitration must first send to the other, by certified mail, a written Notice of Dispute (“Notice”). Notice to us must be sent to our customer service address at: Block union Dispute Resolution, Block union, LLC, 30 N Gould St, Suite R Sheridan WY 82801, Attention: Legal Department. The Notice must include: (1) the nature and basis of your Dispute or Claim; (2) identification or enclosure of all relevant documents and information; and (3) a description of the specific relief that you seek from us.
            </p>

            <p className="mt-20">
                <b>B. Providing Us an Opportunity to Informally Resolve Your Dispute.</b> Before you may pursue or participate in any Dispute or Claim (or raise such Dispute or Claim as a defense) in small claims court or in arbitration against us, you must first send the Notice described above, and you must allow us a reasonable opportunity (not less than thirty (30) days) to resolve your Dispute or Claim. After we receive your Notice, the parties agree to negotiate in good faith with each other to try to resolve your Dispute or Claim.
            </p>

            <p className="mt-20">
                <b>C. Agreement to Participate in Binding Arbitration.</b> If the parties do not reach an informal resolution of your Dispute or Claim within thirty (30) days after we receive your written Notice, you may pursue your Dispute or Claim in arbitration or, solely to the extent specifically provided below, in small claims court. If the parties cannot reach an informal resolution to the Dispute or Claim within thirty (30) days after our receipt of your Notice, you may commence an arbitration proceeding by sending an arbitration demand (“Arbitration Demand”) to the following address: Block union Dispute Resolution, Block union 30 N Gould St, Suite R Sheridan WY 82801, Attention: Legal Department. The parties agree to arbitrate any Dispute or Claim between them, except to the extent either party chooses to instead pursue the Dispute or Claim in small claims court as provided below. Arbitration is more informal than a lawsuit in court. Arbitration uses a neutral arbitrator instead of a judge or jury, allows for more limited discovery than in court, and is subject to very limited review by courts. Arbitrators generally can award the same damages and relief that a court can award.
            </p>

            <p className="mt-20">
                Except as otherwise provided herein, upon either party filing an Arbitration Demand, we will pay all filing, administration, and arbitrator fees, unless your Dispute or Claim exceeds $75,000 (exclusive of any filing, administration, arbitrator, or attorneys’ fees or other fees or expenses). If you initiate an arbitration in which you seek more than $75,000 (exclusive of any filing, administration, arbitrator, or attorneys’ fees or other fees or expenses) in damages, the American Arbitration Association’s (“AAA”) Commercial Arbitration Rules and the Supplementary Procedures for Consumer-Related Disputes (collectively, the “AAA Rules”) will govern the payment of these fees. The AAA Rules, as modified by these Rules, will govern the arbitration. The AAA Rules are available online at www.adr.org, or by calling the AAA at 1-800-778-7879. If your Dispute or Claim is for $10,000 or less (exclusive of any filing, administration, arbitrator, or attorneys’ fees or other fees or expenses), we agree that you may choose whether the arbitration will be conducted solely on the basis of documents submitted to the arbitrator, through a telephonic hearing, or by an in-person hearing under the AAA Rules. If your Dispute or Claim exceeds $10,000 (exclusive of any filing, administration, arbitrator, or attorneys’ fees or other fees or expenses), the right to a hearing will be determined by the AAA Rules. Furthermore, if AAA at the time the arbitration is filed has Minimum Standards of Procedural Fairness for Consumer Arbitrations in effect which would be applicable to the matter in dispute, the Company agrees to provide the benefit of such Minimum Standards to you to the extent they are more favorable than the comparable arbitration provisions set forth in this arbitration provision, provided, however, that in no event may such Minimum Standards contravene or restrict the application of language in bold type below requiring individual arbitration and prohibiting class, representative or consolidated arbitration proceedings.
            </p>

            <p className="mt-20">
                Unless the parties agree otherwise in writing, any arbitration hearings will take place in the county (or parish) in which you reside (except that, if you reside outside of the United States, any such hearings will take place in Los Angeles County, California). One arbitrator, who is selected under the AAA Rules and who has expertise in consumer disputes in the Internet industry, will conduct the arbitration. If no arbitrator possessing such expertise is available, then the arbitration will be conducted by a single arbitrator who is selected by the mutual written approval of the parties. Except as allowed under applicable law and the AAA Rules, the decisions of the arbitrator will be binding and conclusive on all parties. Judgment upon any award of the arbitrator may be entered by any court of competent jurisdiction. This provision will be specifically enforceable in any court. THE ARBITRATOR MUST FOLLOW THESE RULES AND CAN AWARD THE SAME DAMAGES AND RELIEF AS A COURT (INCLUDING ATTORNEYS’ FEES).
            </p>

            <p className="mt-20">
                All issues are for the arbitrator to decide, except that issues relating to the scope and enforceability of the arbitration provision are for a court to decide. Regardless of the manner in which the arbitration is conducted, the arbitrator shall issue a reasoned written decision sufficient to explain the essential findings and conclusions on which the award is based. Except as otherwise provided herein, we will pay all filing, administration, and arbitrator fees for any arbitration initiated in accordance with the notice requirements above. If, however, the arbitrator finds that either the substance of your Dispute or Claim or the relief sought is frivolous or brought for an improper purpose (as measured by the standards set forth in Federal Rule of Civil Procedure 11(b)), then the payment of all such fees will be governed by the AAA Rules. Also, if you initiate an arbitration in which you seek more than $75,000 in damages, the payment of fees will be governed by the AAA Rules.
            </p>

            <p className="mt-20">
                We may make a written settlement offer to you before the arbitrator issues an award. If, after the arbitrator finds in your favor on the merits of the claim, and the arbitrator issues you an award that is greater than our written offer, we will: (1) pay you the greater of the amount of the award or $7,500 (“Alternative Payment”); and (2) pay your attorney, if you use an attorney, twice the amount of any reasonable attorneys’ fees awarded by the arbitrator, and reimburse any expenses that your attorney reasonably accrues for investigating, preparing, and pursuing your arbitration claim (“Attorney Fee Premium”). The Attorney Fee Premium does not supplant any right you may have to reasonable attorneys’ fees under applicable law. Thus, if you would be entitled to a greater amount under applicable law, the Attorney Fee Premium does not preclude the arbitrator from awarding you that amount. You may not, however, recover duplicative attorneys’ fees or costs. If we do not make a written settlement offer to you before the arbitrator issues an award, you and your attorney will be entitled to the Alternative Payment and the Attorney Fee Premium, respectively, if the arbitrator decides in your favor on the merits. We agree that we will not seek any award of attorneys’ fees, even if we are entitled to such fees. The arbitrator may make any determinations and resolve any Dispute or Claim as to the payment and reimbursement of fees, the Alternative Payment, or the Attorney Fee Premium at any time during the proceeding and within fourteen (14) days after the arbitrator’s final ruling on the merits. The arbitrator may award declaratory or injunctive relief only in favor of the individual party seeking relief and only to the extent necessary to provide relief warranted by that party’s individual claim. Furthermore, these arbitration provisions shall not prevent any party from seeking provisional remedies in aid of arbitration from a court of appropriate jurisdiction.
            </p>

            <p className="mt-20 text-semibold">
                YOU AND THE COMPANY AGREE THAT:

                <ul>
                    <li>ANY DISPUTE RESOLUTION PROCEEDINGS WILL BE CONDUCTED ONLY ON AN INDIVIDUAL BASIS AND NOT IN A CLASS OR REPRESENTATIVE ACTION. NEITHER YOU NOR COMPANY SHALL BE A MEMBER IN A CLASS, CONSOLIDATED, OR REPRESENTATIVE ACTION OR PROCEEDING, AND THE ARBITRATOR MAY AWARD RELIEF ONLY IN FAVOR OF THE INDIVIDUAL PARTY SEEKING RELIEF AND ONLY TO THE EXTENT NECESSARY TO PROVIDE RELIEF WARRANTED BY THAT PARTY’S INDIVIDUAL DISPUTE OR CLAIM.</li>
                    <li>UNLESS BOTH PARTIES AGREE OTHERWISE, THE ARBITRATOR MAY NOT CONSOLIDATE MORE THAN ONE PERSON’S DISPUTES OR CLAIMS, AND MAY NOT OTHERWISE PRESIDE OVER ANY FORM OF A REPRESENTATIVE OR CLASS PROCEEDING.</li>
                    <li>COMPANY DOES NOT CONSENT TO CLASS ARBITRATION. ACCORDINGLY, IF A COURT REFUSES TO ENFORCE THE ABOVE PROVISIONS REGARDING CLASS OR REPRESENTATIVE ACTIONS, THEN THIS AGREEMENT TO ARBITRATE SHALL BE UNENFORCEABLE AS TO YOU. WHETHER A DISPUTE OR CLAIM PROCEEDS IN COURT OR IN ARBITRATION, YOU AND COMPANY HEREBY WAIVE ANY RIGHT TO A JURY TRIAL.</li>
                </ul>
            </p>

            <p className="mt-20">
                This arbitration agreement covers any Dispute or Claim arising out of or relating to any aspect of the relationship between the parties, whether based in contract, tort, statute, fraud, misrepresentation or any other legal theory, even if the Dispute or Claim arises or may arise before or after the period(s) during which you are using the Block union Sites and Features or our Services. For purposes of this arbitration provision, references to “the Company”, “we” and “us” include Block union, LLC, blockunion.io, LLC, and each of their affiliates, and each such entity’s respective directors, officers, employees, shareholders, agents, suppliers and assignees. The AAA Rules evidence a transaction in interstate commerce and the Federal Arbitration Act governs the interpretation and enforcement of this section.
            </p>

            <p className="mt-20">
                <b>The parties must bring any Dispute or Claim hereunder (including any Dispute or Claim arising out of or related to the AAA Rules), within two (2) years after the Dispute or Claim arises, or the Dispute or Claim will be permanently barred.</b> To the extent the law applicable under the Governing Law section below makes this limitations period unenforceable with respect to any Dispute(s) or Claim(s), then the statutes of limitations of the state whose laws govern the AAA Rules under the Governing Law section below shall apply.
            </p>

            <p className="mt-20">
                We may make changes to this arbitration provision from time to time. You may reject any material changes by sending us written objection within thirty (30) days of the change to Block union Dispute Resolution, Block union, LLC, 30 N Gould St, Suite R Sheridan WY 82801, Attention: Legal Department. By rejecting any future material change, you are agreeing to arbitrate in accordance with the language of this provision. If you do not send written objection to any change as provided above, you are agreeing to arbitration in accordance with the changed language of this provision. To the extent that an arbitrator or court of applicable jurisdiction determines that applying any changes to this arbitration provision to any Disputes or Claims relating to prior events or circumstances would render this an illusory or unenforceable contract or otherwise violate your legal rights, such changes shall be applicable on a prospective basis only, with respect to events or circumstances occurring after the effective date of such changes, and in that case any Disputes or Claims relating to such prior events or circumstances shall be arbitrated in accordance with the language of this provision without such changes to the extent necessary to avoid these Terms being deemed illusory or unenforceable.
            </p>

            <p className="mt-20">
                <b>D. Small Claims Court.</b> You may choose to pursue your Dispute or Claim in small claims court rather than by arbitration if your Dispute or Claim qualifies for small claims court in a location where jurisdiction and venue over you and us is proper.
            </p>

            <p className="mt-20">
                <b>E. Governing Law.</b> The AAA Rules, and any Dispute or Claim arising between you and the Company related in any way to the AAA Rules or any Block union Sites and Features or our Service, privacy, data security, collection, use and sharing, advertising, purchase transactions, tokens earnings, awards, balances, expiration, or transactions, sweepstakes, promotions, or any emails, texts, or other communications with you, or arising out of or relating to any aspect of the transactions or relationship between us, whether based on contract, tort, statute, or common law, will be governed by the internal laws of the state in which you reside (except that, if you reside outside of the United States, the internal laws of the State of California will govern), without regard to choice of law principles.
            </p>

            <h2 className="mt-20 text-semibold">Tax Matters</h2>
            <p className="mt-20">
                You acknowledge and agree that we do not have the ability, in every instance, to determine whether or not the tokens, rewards, prizes, or credits you earned or redeemed in any Rewards Program are considered reportable taxable earnings in your jurisdiction. You are therefore responsible for any and all tax liability arising from or associated with your use of the Block union Sites and Features or our Services, including liability arising from your accrual of Rewards Program tokens or your redemption of such tokens for cash or other value. As a condition of your continued use of the Block union Sites and Features and our Services, we reserve the right to require you to provide necessary tax reporting information if our records show that you are or may be required to report the value of your tokens, rewards, prizes, or credits to an appropriate tax authority. We encourage you, and it is your responsibility, to seek advice of a tax expert in order to determine the tax consequences of your use of the Block union Sites and Features and our Services, and any associated tokens, rewards, prizes, or credits earned or redeemed.
            </p>

            <h2 className="mt-20">12. Notification of Changes</h2>
            <p className="mt-20">
                We reserve the right to make changes to these Terms from time to time in our sole discretion. If we decide to change these Terms, we will provide notice of such changes by sending you an administrative email and/or posting those changes in places on the Block union Sites and Features deemed appropriate by us so our users are always aware of the terms of their use of the Block union Sites and Features and our Services. Your continued use of any of the Block union Sites and Features or our Services after delivery of the administrative email to you or after the changes are posted constitutes your agreement to the changes. If you do not agree to the changes, please discontinue your use of such Block union Sites and Features and our Services. To the extent that an arbitrator or court of applicable jurisdiction determines that applying any changes to these Terms to any prior events or circumstances would render this an illusory or unenforceable contract, such changes shall be applicable on a prospective basis only, with respect to events or circumstances occurring after the date of such changes, to the extent necessary to avoid these Terms being deemed illusory or unenforceable. In any event, if you used any of the Block union Sites and Features or our Services pursuant to a prior version of these Terms that required a certain notice period to you prior to any changes being effective, any changes under these Terms will not be effective as to you until the previously-specified notice period has passed after the date of these Terms.
            </p>

            <h2 className="mt-20">13. International Users</h2>
            <p className="mt-20">
                The Block union Sites and Features are controlled, operated, and administered by the Company from its offices within the United States of America. The Company makes no representation that materials on the Block union Sites and Features are appropriate or available for use at other locations outside of the United States and access to them from territories where the contents or products available through the Block union Sites and Features are illegal is prohibited. You may not use the Block union Sites and Features or export the content or products in violation of U.S. export laws and regulations. If you access Block union Sites and Features from a location outside of the United States, you are responsible for compliance with all local laws.
            </p>

            <h2 className="mt-20">14. Excluded Users and Territories</h2>
            <p className="mt-20">
                You are not permitted to download or use any of the Block union Sites and Features or our Services, including making any purchases of products or services from the Company, if you are (i) located in, under the control of, or a national or resident of any country to which the United States has embargoed goods or services; (ii) identified as a “Specially Designated National”; or (iii) placed on the U.S. Department of Commerce’s “Denied Persons List or Entity List” or any other U.S. export control list, or if the transaction would otherwise be illegal under any applicable law or regulation.
            </p>

            <h2 className="mt-20">15. Miscellaneous</h2>
            <p className="mt-20">
                The Company’s failure to exercise or enforce any right or provision of these Terms will not be deemed to be a waiver of such right or provision. If any provision of these Terms is found by an arbitrator or court of competent jurisdiction to be invalid, the parties nevertheless agree that (except as otherwise provided in Paragraph 11) the arbitrator or court should endeavor to give effect to the parties’ intentions as reflected in the provision or the provision shall be deemed severable, and the other provisions of these Terms remain in full force and effect. The paragraph or section titles in these Terms are for convenience only and have no legal or contractual effect. These Terms represent the entire understanding of the parties regarding its subject matter, and supersede all prior and contemporaneous agreements and understandings between the parties regarding its subject matter, and may not be amended, altered or waived except in a writing signed by the party to be charged or as otherwise expressly provided herein. These Terms are binding upon and shall inure to the benefit of parties and their respective successors, heirs, executor, administrators, personal representatives and permitted assigns. You may not assign your rights or obligations hereunder without the Company’s prior written consent, and any such unauthorized assignment shall be null and void.
            </p>

            <h2 className="mt-20">16. Contact us</h2>
            <p className="mt-20">
                If you have any questions or concerns regarding these Terms or your use of any Block union Sites and Features or our Services, please contact us by using the “Contact Us”, “Contact Member Services” or similar contact link in the footer of any of the Block Union Sites, or by mail to: Block Union, LLC, 30 N Gould St, Suite R Sheridan WY 82801; Attention: Customer Service.
            </p>
        </div>
    );
}