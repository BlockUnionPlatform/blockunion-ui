import React, {Component} from 'react';
import SiteTitle from "../../../common/SiteTitle";
import PageHeaderWithLink from "../../../admin/pages/PageHeaderWithLink";
import {Config} from "../../../../Config";
import {Editor} from 'react-draft-wysiwyg';
import BlockBasicInformationForm from "../../../admin/pages/Blocks/BlockBasicInformationForm";
import BlockPackageDropdown from "../../../admin/pages/Common/BlockPackageDropdown";
import BlocksSocialForm from "../../../admin/pages/Blocks/BlocksSocialForm";
import BlockIcoForm from "../../../admin/pages/Blocks/BlockIcoForm";
import AlertBox from "../../../common/AlertBox";
import VerifyBlock from "../../../admin/pages/Blocks/VerifyBlock";
import ErrorMessage from "../../../common/ErrorMessage";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';
import LoadingScreen from "../../../common/LoadingScreen";
import ComposeErrorMessageFromJson from "../../../admin/pages/Common/ComposeErrorMessageFromJson";
import {convertFromRaw, EditorState} from "draft-js";
import {stateToHTML} from "draft-js-export-html";
import {Auth} from "../../../common/AuthHelpers";

export default class BlocksAdd extends Component {
    constructor(props) {
        super(props);

        this.state = {
            businessName: '',
            websiteUrl: '',
            emailAddress: '',
            authorName: '',
            isAuthorInvolved: true,
            category: null,
            hasToken: false,
            tokenSymbol: '',
            editorState: EditorState.createEmpty(),
            legalName: '',
            value: '',
            totalValue: '',
            tokensPerClaim: '',
            isActive: true,
            legalAddress: '',
            originCountry: '',
            whitePaper: '',
            logo: '',
            tokenLogo: '',
            package: null,
            facebook: '',
            twitter: '',
            medium: '',
            telegram: '',
            bitcoinTalkForum: '',
            instagram: '',
            privateStartDate: 'January 01, 1970 12:00 AM',
            privateEndDate: 'January 01, 1970 12:00 AM',
            preSaleIcoStart: '',
            preSaleIcoEnd: '',
            publicIcoStart: '',
            publicIcoEnd: '',
            platformTokenType: '',
            pricePerToken: '',
            aboutBusiness: '',
            alert: {
                isVisible: false,
                status: '',
                message: ''
            },
            isLoading: true,
            packageId: '',
            showHelp: false
        };

        this.onInputChange = this.onInputChange.bind(this);
        this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
        this.onCategoryChange = this.onCategoryChange.bind(this);
        this.onEditorChange = this.onEditorChange.bind(this);
        this.convertToBase64 = this.convertToBase64.bind(this);
        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { isVisible: show, status, message}
        });
        window.scrollTo(0,0);
    }

    onInputChange(event) {
        this.setState({
            [event.target.name] : event.target.value
        });
    }

    onCategoryChange(selectedValue) {
        this.setState({
            category: selectedValue
        });
    }

    onEditorChange(newState) {
        this.setState({ editorState: newState });

        let html = stateToHTML(newState.getCurrentContent());
        this.setState({ aboutBusiness: html });
    }

    handleCheckboxChange(event) {
        this.setState({
            [event.target.name]: event.target.checked
        });
    }

    convertToBase64(name, event) {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (e) => {
                this.setState({ [name]: e.target.result });
            };
            reader.readAsDataURL(event.target.files[0]);
        }
    }

    onLogoChange(event) {
        this.convertToBase64('logo', event);
    }

    onTokenLogoChange(event) {
        this.convertToBase64('tokenLogo', event);
    }

    onPackageChange(selectedValue) {
        this.setState({ package: selectedValue });
    }

    onDateChange(name, selectedDate) {
        this.setState({ [name] : selectedDate.format(Config.Formats.DateTime) });
    }

    onActiveChange(event) {
        this.setState({ isActive: event.target.checked });
    }

    onSubmit(event) {
        event.preventDefault();
        let block = {...this.state};
        let errors = VerifyBlock(block, false);

        if (errors.length > 0) {
            let message = <ErrorMessage errors={errors} />
            this.showAlert(true, 'error', message);
            window.scrollTo(0, 0);
        }
        else {
            let url = getApi(Config.API.Block.Listings);
            let payload = {
                businessName: this.state.businessName,
                websiteUrl: this.state.websiteUrl,
                emailAddress: this.state.emailAddress,
                authorName: this.state.authorName,
                isAuthorInvolved: this.state.isAuthorInvolved,
                serviceCategory: this.state.category.value,
                hasToken: this.state.hasToken,
                tokenSymbol: this.state.tokenSymbol,
                aboutBusiness: this.state.aboutBusiness,
                legalName: this.state.legalName,
                legalAddress: this.state.legalAddress,
                originCountry: this.state.originCountry,
                whitepaper: this.state.whitePaper,
                logo: this.state.logo,
                logoToken: this.state.tokenLogo,
                packageId: this.state.packageId,
                value: this.state.value,
                totalValue: this.state.totalValue,
                tokensPerClaim: this.state.tokensPerClaim,
                isActive: this.state.isActive,
                createdById: Auth.getUser().id,
                social: {
                    facebook: this.state.facebook,
                    twitter: this.state.twitter,
                    telegram: this.state.telegram,
                    medium: this.state.medium,
                    bitcoinTalkForum: this.state.bitcoinTalkForum,
                    instagram: this.state.instagram
                }
            };

            if (this.state.category.value == 5) {
                payload["ico"] = {
                    privateSaleStart: this.state.privateStartDate,
                    privateSaleEnd: this.state.privateEndDate,
                    preSaleIcoStart: this.state.preSaleIcoStart,
                    preSaleIcoEnd: this.state.preSaleIcoEnd,
                    publicIcoStart: this.state.publicIcoStart,
                    publicIcoEnd: this.state.publicIcoEnd,
                    platformTokenType: this.state.platformTokenType,
                    pricePerToken: this.state.pricePerToken
                };
            }

            console.log(payload);

            this.setState({ isLoading: true });

            axios.post(url, payload)
                .then(res => {
                    this.showAlert(true, 'success', 'New block created successfully!');
                    this.setState({ isLoading: false });
                })
                .catch(error => {
                    if (error.response) {
                        if (error.response.status == 422)
                            this.showAlert(true, 'error', ComposeErrorMessageFromJson(error.response.data), true);
                        else
                            this.showAlert(true, 'error', JSON.stringify(error.response.data), true);
                    }
                    else if (error.request)
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);
                    else
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                    this.setState({ isLoading: false });
                });
        }

    }

    componentDidMount() {
        let url = `${getApi(Config.API.Block.Packages)}/title/default`;

        axios.get(url)
            .then(res => {
                this.setState({ packageId: res.data.id, isLoading: false });
            })
            .catch(error => {
                this.setState({ isLoading: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title = "Add Block" />
                <PageHeaderWithLink title = "Add Block" btnText = "Back to Blocks" btnIcon = "fa fa-arrow-left" btnUrl={Config.RouteUrls.Member.Blocks.Home} />
                <p className="form-group">Create blocks using this form.</p>

                <div className="text-center">
                    <a href="#" onClick={(e) => { e.preventDefault(); this.setState({ showHelp: !this.state.showHelp }); }} className = "link">
                        {
                            this.state.showHelp ?
                                <span>Hide information</span>
                                :
                                <span>What are blocks and how I can use them to earn rewards?</span>
                        }
                    </a>

                    {
                        this.state.showHelp &&
                        <div className = "alert alert-info mt-10">
                            <p>
                                <b>Blocks</b> are basically the listings you see on the main homepage and member's dashboard. These listings represent business, mostly build on Blockchain, which are submitted here allowing the BlockChain community to rank them and provide a reputation database for everyone else.
                            </p>

                            <p>
                                By posting listings, or <b>Blocks</b> as we call it, you are eligible for more rewards. You can also vote the block, perform the number of steps mentioned by the blocks (if any) and further claim those rewards too!
                            </p>
                        </div>
                    }
                </div>

                <div className="text-center">
                    {
                        Auth.getUser().accessLevel >= Config.MinAdminLevel &&
                            <div className="alert alert-warning mt-20">
                                Looks like you have admin access! You should use <a className = "link" href={Config.RouteUrls.Admin.Blocks.Add}>this</a> page instead to create blocks.
                            </div>
                    }
                </div>

                <div className="form-group">
                    <AlertBox {...this.state.alert} />
                </div>

                <div className="row mt-40">
                    <div className="col-md-7 offset-md-2">
                        <form action="#" onSubmit={this.onSubmit.bind(this)}>
                            <div className="mb-30">
                                <h3 className="text-center">Basic Information</h3>
                                <BlockBasicInformationForm
                                    {...this.state}
                                    onInputChange={this.onInputChange}
                                    onEditorChange={this.onEditorChange}
                                    handleCheckboxChange={this.handleCheckboxChange}
                                    onCategoryChange={this.onCategoryChange}
                                    onLogoChange={this.onLogoChange.bind(this)}
                                    onTokenLogoChange={this.onTokenLogoChange.bind(this)}
                                    onActiveChange={this.onActiveChange.bind(this)}
                                    isAdmin={false}
                                />
                            </div>

                            <div className="mb-30">
                                <h3 className="text-center">Social</h3>
                                <BlocksSocialForm
                                    {...this.state}
                                    onInputChange={this.onInputChange}
                                />
                            </div>

                            {
                                this.state.category != null && this.state.category.value == 5 ?
                                    <div className="mb-30">
                                        <h3 className="text-center">ICO</h3>
                                        <BlockIcoForm
                                            {...this.state}
                                            onInputChange={this.onInputChange}
                                            onDateChange={this.onDateChange.bind(this)}
                                        />
                                    </div>
                                    :
                                    <span></span>
                            }

                            <div className="form-group">
                                <button className="btn btn-theme" type="submit">
                                    Add Block
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}