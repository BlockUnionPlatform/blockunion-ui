import React, { Component } from 'react';
import {getApi} from "../../../../ConfigStore";
import {Auth} from "../../../common/AuthHelpers";
import {Config} from "../../../../Config";
import axios from 'axios';
import SiteTitle from "../../../common/SiteTitle";
import LoadingScreen from "../../../common/LoadingScreen";
import BlocksTable from "../../../common/blocks/BlocksTable";
import PageHeaderWithLink from "../../../admin/pages/PageHeaderWithLink";

export default class BlocksMemberHome extends Component {
    constructor(props) {
        super(props);

        this.state = {
            blocks: [],
            isLoading: false,
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
        this.setLoading = this.setLoading.bind(this);
    }

    showAlert(show, status, message) {
        this.setState({ alert: { show, status, message } });
    }

    setLoading(isLoading) {
        this.setState({ isLoading });
    }

    componentDidMount() {
        let url = getApi(`${Config.API.Block.User}/${Auth.getUser().id}`);

        this.setLoading(true);

        axios.get(url)
            .then(res => {
                this.setState({ blocks: res.data, isLoading: false });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setLoading(false);
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title = "My Blocks" />
                <PageHeaderWithLink title="View Blocks" btnIcon="fa fa-plus" btnText="Add Block" btnUrl={Config.RouteUrls.Member.Blocks.Add} />
                <p>A list of all the blocks you have created.</p>

                <BlocksTable {...this.state} isAdmin={false} />
            </div>
        );
    }
}