import React, {Component} from 'react';
import SiteTitle from "../../../common/SiteTitle";
import PageHeaderWithLink from "../../../admin/Pages/PageHeaderWithLink";
import {Config} from "../../../../Config";
import BlockTransactionRow from "../../../admin/Pages/BlockTransactions/BlockTransactionRow";
import LoadingScreen from "../../../common/LoadingScreen";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';
import AlertBox from "../../../common/AlertBox";
import {Auth} from "../../../common/AuthHelpers";

export default class BlocksPayouts extends Component {
    constructor(props) {
        super(props);

        this.state = {
            transactions: [],
            isLoading: true,
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });
    }

    componentDidMount() {
        let url = `${getApi(Config.API.Block.Transactions)}/user/${Auth.getUser().id}`;

        this.setState({ isLoading: true });

        axios.get(url)
            .then(res => {
                this.setState({
                    transactions: res.data,
                    isLoading: false
                });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title="Block Payouts" />
                <PageHeaderWithLink title="Block Payouts" />
                <p>
                    A list of all the payouts made to you.
                </p>
                <AlertBox  {...this.state.alert} />
                {
                    this.state.isLoading ?
                        <div>Loading</div>
                        :
                        <div className="row">
                            <div className="col-md-12">
                                <div className="table-responsive">
                                    <table className="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Title</th>
                                            <th>Amount</th>
                                            <th>Paid To</th>
                                            <th>Date</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        {
                                            this.state.transactions.map((transaction) =>
                                                <BlockTransactionRow transaction={transaction} isAdmin={false} />
                                            )
                                        }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                }
            </div>
        );
    }
}