import React from 'react';

export default function BlockHeader(props) {
    return (
        <section className="member_rating" style={{ paddingTop: '80px' }}>
            <div className="container">
                <div className="member_text left">
                    <h2>{props.title}</h2>
                </div>

                {
                    props.showRating ?
                        <div className="rating right all-btn">
                            <a href="javascript:void(0)" className="button">SUBMIT A RATING</a>
                        </div>
                        :
                        <span></span>
                }
            </div>
        </section>
    );
};