import React, {Component} from 'react';
import DailyActionItem from '../DailyActionItem';
import ServicesContainer from '../ServicesContainer';
import {Config} from "../../../Config";
import {getApi} from "../../../ConfigStore";
import axios from "axios";
import LoadingScreen from "../../common/LoadingScreen";
import SiteTitle from "../../common/SiteTitle";
import MetaMaskAccountAlert from "../../common/MetaMask/MetaMaskAccountAlert";

class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dailyActions: [],
            blocks: [],
            icos: [],
            trending: [],
            isLoading: true,
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
        this.setLoading = this.setLoading.bind(this);
    }

    showAlert(show, status, message) {
        this.setState({ alert: { show, status, message } });
    }

    setLoading(isLoading) {
        this.setState({ isLoading });
    }

    componentDidMount() {

        let recentCount = 3;
        let url = getApi(Config.API.Dashboard.Members);

        axios.get(url)
            .then(res => {
                this.setState({
                    dailyActions: res.data.dailyActions,
                    blocks: res.data.blocks,
                    icos: res.data.icos,
                    trending: res.data.trending
                });

                this.setLoading(false);
            })
            .catch(error => {
                console.log(error);
                this.setLoading(false);
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title = "Dashboard" />
                <div className="trending-heading">
                    <h3>Daily Action and Earn</h3>
                </div>

                {
                    !this.state.isLoading &&
                    <div className = "daily-part">
                        {
                            this.state.dailyActions.map((dailyAction) => <DailyActionItem key={dailyAction.id} dailyAction={dailyAction} />)
                        }
                    </div>
                }

                <ServicesContainer
                    blocks={this.state.blocks}
                    icos={this.state.icos}
                    trending={this.state.trending} />
            </div>
        );
    }
}

export default Home;