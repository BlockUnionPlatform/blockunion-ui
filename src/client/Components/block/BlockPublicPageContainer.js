import React from 'react';
import BlockAssets from "./BlockAssets";
import EnsureLoggedInContainer from "../EnsureLoggedInContainer";
import TopHeaderContainer from "../member/TopHeaderContainer";
import FooterContainer from "../common/FooterContainer";

export default function BlockPublicPageContainer(props) {
    return (
        <div>
            <div id="wrapper">
                <TopHeaderContainer />
                {props.children}
                <FooterContainer />
            </div>

            <BlockAssets />
        </div>
    );
}