import React from 'react';
import SidebarDivider from "./SidebarDivider";
import SidebarText from "./SidebarText";
import BlockSocialLinks from "./BlockSocialLinks";
import {Auth} from "../../common/AuthHelpers";

export default function BlockSidebar(props) {
    let whitepaper = <a href={props.block.whitepaper} target="_blank" className="link-whitepaper">See Whitepaper</a>;
    let claimButtonDisabled  = !props.hasVoted ? true : (props.isClaimed ? true: false);
    return (
        <div className="left_sidebar">
            <div className="block-image mb-20">
                <div className="text-center">
                    <img src={props.block.logo} alt="" style={{ width: '125px', height: 'auto' }}/>
                </div>
            </div>

            <h4 className="phone_title text-center mb-20">{props.block.businessName}</h4>

            <div className="contact-box">
                <div className="contact_button">
                    <a href={`mailto:${props.block.emailAddress}`}>Email</a>
                    <a href={props.block.websiteUrl}>Website</a>
                </div>
            </div>

            <SidebarDivider />

            <SidebarText text="Claim Value" value={props.block.value} />

            <SidebarDivider />

            <SidebarText text="Tokens Per Claim" value={props.block.tokensPerClaim} />

            <SidebarDivider />

            <SidebarText text="Total Value" value={props.block.totalValue} />

            <SidebarDivider/>

            {
                props.block.whitepaper !== '' &&
                    <div>
                        <SidebarText text="Whitepaper" value={whitepaper} />
                        <SidebarDivider/>
                    </div>
            }

            <SidebarText text="Country" value={props.block.originCountry} />

            <SidebarDivider/>

            <BlockSocialLinks social={props.block.social} />

            {
                Auth.isAuthenticated() &&
                <div>
                    <SidebarDivider/>

                    <button className="btn btn-block btn-theme" disabled={claimButtonDisabled} onClick={props.onClaimSubmit}>CLAIM BLOCK</button>
                    {
                        props.isClaimed  &&
                        <div className="text-center small mt-10">You have already claimed this block.</div>
                    }

                    {
                        !props.hasVoted  &&
                        <div className="text-center small mt-10">You need to vote for the block before you can claim it.</div>
                    }
                </div>
            }
        </div>
    );
}