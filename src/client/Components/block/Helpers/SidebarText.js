import React from 'react';
import SidebarDivider from "./SidebarDivider";

export default function SidebarText(props) {
    return (
        <div className="sidebar-text">
            <h3 className="phone_title text-center mb-10">{props.text}</h3>
            <p className="text-center">{props.value}</p>
        </div>
    );
}