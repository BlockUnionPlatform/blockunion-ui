import React from 'react';

export default function SidebarDivider() {
    return (
        <div className="sidebar_line mt-20 mb-20"></div>
    );
}