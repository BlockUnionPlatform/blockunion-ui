import React from 'react';
import {VoteState} from "../common/VoteConstants";

export default function BlockVoteWidget(props) {
    let styles = {
        pointerEvents: 'none',
        opacity: '0.5'
    };

    return (
        <div className="card card-block card-body mt-30">
            <div className="voting main_box box" style={props.disabled ? styles : null}>
                <div className="voting_like"><a href="javascript:void(0)"  onClick={e => props.processVote(e, VoteState.Up)}>+01</a></div>
                <div className="voting_unlike"><a href="javascript:void(0)"  onClick={e => props.processVote(e, VoteState.Down)}>-0.5</a></div>
            </div>

            {
                props.disabled &&
                <div className="text-center small">
                    You have voted for this block.
                </div>
            }
        </div>
    );
}