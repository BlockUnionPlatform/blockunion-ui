import React from 'react';

export default function BlockVotesOverview(props) {
    let percentage = 0, positives = 0, negatives = 0;

    if (props.reputation !== undefined && props.reputation !== null) {
        percentage = props.reputation.percentage;
        positives = props.reputation.positiveVoteCount;
        negatives = props.reputation.negativeVoteCount;
    }

    return (
        <div className="card card-body card-block mt-30 rating_box">
            <h4 className="block-title">Reputation</h4>
            <div className="loader_box mt-5" style={{ backgroundColor: '#fff', border: 'solid 1px #1f6565' }}>
                <div style={{ backgroundColor: '#33908e', width: `${percentage > 19 ? percentage : '30'}%`, letterSpace: '2px' }}>
                    {parseFloat(percentage).toFixed(1)}%
                </div>
            </div>

            <ul className="mt-5">
                <li className="dark">[{positives}] Positive Ratings</li>
                <li className="pink">[{negatives}] Negative Ratings</li>
            </ul>

            <div className="mt-5">
                <a className="link-block" href="javascript:void(0)">[{positives + negatives}] Total Customers Ratings</a>
            </div>
        </div>
    );
}