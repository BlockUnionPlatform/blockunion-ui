import React from 'react';

export default function BlockRating(props) {
    let rating = props.reputation.rating;
    let stars = [];

    if (rating.toString().includes('.')) {
        let halfIndex = parseInt(rating) + 1;

        let i;
        for(i = 1; i <= halfIndex; i++) {
            if (i < halfIndex)
                stars.push(<i className="fas fa-star"></i>);
            else
                stars.push(<i className="fas fa-star-half-alt"></i>);
        }

        for(i = 0; i < 5 - halfIndex; i++) {
            stars.push(<i className="far fa-star"></i>);
        }
    }
    else {
        let i;
        for(i = 1; i <= 5; i++) {
            if (i <= rating)
                stars.push(<i className="fas fa-star"></i>);
            else
                stars.push(<i className="far fa-star"></i>);
        }
    }

    return (
        <div className="card card-block card-body">
            <div className="rating-stars">
                {stars}
            </div>
        </div>
    );
}