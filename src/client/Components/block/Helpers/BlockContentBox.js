import React from 'react';

export default function BlockContentBox(props) {
    return (
        <div className="card card-body card-block">
            <h2 className="block-title">{props.title}</h2>
            <div dangerouslySetInnerHTML={{ __html: props.content }}></div>
        </div>
    );
}