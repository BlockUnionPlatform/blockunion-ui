import React from 'react';
import BlockContentGroup from "./BlockContentGroup";

export default function BlockOverviewSection(props) {
    let websiteUrl = <a href={props.block.websiteUrl} className="link-block" target="_target">{props.block.websiteUrl}</a>
    return (
        <div className="card card-body card-block mt-30">
            <h2 className="block-title">Overview</h2>

            <div className="mt-20">
                <BlockContentGroup title="Legal Name:" value={props.block.legalName} />

                <BlockContentGroup title="Legal Address:" value={props.block.legalAddress} />

                {
                    props.block.hasToken &&
                        <BlockContentGroup title="Token Symbol:" value={props.block.tokenSymbol} />
                }

                <BlockContentGroup title="Category:" value={props.block.category} />

                <BlockContentGroup title="Website:" value={websiteUrl} />
            </div>
        </div>
    );
}