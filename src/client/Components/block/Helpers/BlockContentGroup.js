import React from 'react';
import moment from "moment";
import {Config} from "../../../Config";

export default function BlockContentGroup(props) {
    return (
        <div className="mb-5 row">
            <label className="control-label text-semibold col-md-4">{props.title}</label>
            <div className="col-md-8">
                {
                    props.isDate !== undefined && props.isDate ?
                        moment(props.value).format(Config.Formats.Date)
                        :
                        props.value
                }
            </div>
        </div>
    );
}