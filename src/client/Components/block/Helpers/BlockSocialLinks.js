import React from 'react';

export default function BlockSocialLinks(props) {
    return (
        <div className="sidebar-text">
            <h3 className="phone_title text-center mb-10">Social</h3>
            <div className="d-flex">
                <ul className="list-inline mx-auto justify-content-center list-social">
                    <li>
                        <a href={props.social.facebook} target="_blank" className="link-social"><i className="fa fa-facebook"></i></a>
                    </li>

                    <li>
                        <a href={props.social.twitter} target="_blank" className="link-social"><i className="fa fa-twitter"></i></a>
                    </li>

                    <li>
                        <a href={props.social.instagram} target="_blank" className="link-social"><i className="fa fa-instagram"></i></a>
                    </li>

                    <li>
                        <a href={props.social.telegram} target="_blank" className="link-social"><i className="fa fa-telegram"></i></a>
                    </li>

                    <li>
                        <a href={props.social.medium} target="_blank" className="link-social"><i className="fa fa-medium"></i></a>
                    </li>

                    <li>
                        <a href={props.social.bitcoinTalkForum} target="_blank" className="link-social"><i className="fa fa-bitcoin"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    );
}