import React from 'react';
import BlockContentGroup from "./BlockContentGroup";

export default function BlockIcoSection(props) {
    return (
        <div className="card card-body card-block mt-30">
            <h2 className="block-title">ICO</h2>

            <div className="mt-20">
                <BlockContentGroup title="Platform Token Type:" value={props.block.ico.platformTokenType} />

                <BlockContentGroup title="Price Per Token:" value={props.block.ico.pricePerToken} />

                <BlockContentGroup title="Private Sale Start:" value={props.block.ico.privateSaleStart} isDate={true} />

                <BlockContentGroup title="Private Sale End:" value={props.block.ico.privateSaleEnd} isDate={true} />

                <BlockContentGroup title="Pre Sale Ico Start:" value={props.block.ico.preSaleIcoStart} isDate={true} />

                <BlockContentGroup title="Pre Sale Ico End:" value={props.block.ico.preSaleIcoEnd} isDate={true} />

                <BlockContentGroup title="Public ICO Start:" value={props.block.ico.publicIcoStart} isDate={true} />

                <BlockContentGroup title="Public ICO End:" value={props.block.ico.publicIcoEnd} isDate={true} />
            </div>
        </div>
    );
}