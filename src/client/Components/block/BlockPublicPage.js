import React, {Component} from 'react';
import SiteTitle from "../common/SiteTitle";
import {getApi} from "../../ConfigStore";
import axios from "axios";
import LoadingScreen from "../common/LoadingScreen";
import AlertBox from "../common/AlertBox";
import BlockHeader from "../member/pages/Blocks/Helpers/BlockHeader";
import {Config} from "../../Config";
import BlockSidebar from "./Helpers/BlockSidebar";
import BlockContentBox from "./Helpers/BlockContentBox";
import BlockOverviewSection from "./Helpers/BlockOverviewSection";
import BlockIcoSection from "./Helpers/BlockIcoSection";
import BlockVoteWidget from "./Helpers/BlockVoteWidget";
import BlockVotesOverview from "./Helpers/BlockVotesOverview";
import BlockCardItem from "../admin/pages/Blocks/BlockCardItem";
import {Auth} from "../common/AuthHelpers";
import {MetaMask} from "../common/MetaMask/MetaMaskCheck";
import {VoteTypes} from "./common/VoteConstants";
import {Helmet} from "react-helmet";
import BlockRating from "./Helpers/BlockRating";

export default class BlockPublicPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            block: null,
            reputation: null,
            isLoading: true,
            isClaimed: false,
            hasVoted: false,
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
        this.onClaimSubmit = this.onClaimSubmit.bind(this);
        this.processVote = this.processVote.bind(this);
        this.checkVote = this.checkVote.bind(this);
        this.checkClaim = this.checkClaim.bind(this);
        this.setReputation = this.setReputation.bind(this);
    }

    showAlert(show, status, message) {
        this.setState({ alert: { show, status, message } });
    }

    setReputation() {
        let url = `${getApi(Config.API.Block.Listings)}/reputation/${this.state.block.id}`;

        axios.get(url)
            .then(res => {
                this.setState({ reputation: res.data });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to update block reputation. Please contact the site administrators', true);
                else
                    this.showAlert(true, 'error', 'Unable to update block reputation. Please contact the site administrators', true);
            });
    }

    checkVote() {
        let payload = {
            blockId: this.state.block.id,
            userId: Auth.getUser().id
        };

        let url = getApi(Config.API.Votes.CheckVote);

        axios.post(url, payload)
            .then(res => {
                this.setState({ hasVoted: res.data.status });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to verify voting status. Please contact the site administrators', true);
                else
                    this.showAlert(true, 'error', 'Unable to verify voting status. Please contact the site administrators', true);
            });
    }

    processVote(e, voteState) {
        e.preventDefault();

        let url = getApi(Config.API.Votes.Main);
        let voteType = 0;

        if (Auth.isAuthenticated()) {
            if (MetaMask.isInstalledAndAuthenticated())
                voteType = VoteTypes.Blockchain;
            else
                voteType = VoteTypes.BlockUnionMember;
        }
        else {
            voteType = VoteTypes.Anonymous;
        }

        let payload = {
            userId: Auth.isAuthenticated() ? Auth.getUser().id : '',
            blockId: this.state.block.id,
            voteType: voteType,
            voteState: voteState,
            ehtereumTransactionId: ''
        };

        this.setState({ isLoading: true });

        axios.post(url, payload)
            .then(res => {
                this.setState({ isLoading: false, hasVoted: true });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to create block claim. Please contact the site administrators', true);
                else
                    this.showAlert(true, 'error', 'Unable to create block claim. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });
    }

    onClaimSubmit(e) {
        e.preventDefault();

        this.setState({ isLoading: true })

        let url = getApi(Config.API.Block.Claims);

        let payload = {
            userId: Auth.getUser().id,
            blockId: this.state.block.id
        };

        axios.post(url, payload)
            .then(res => {
                this.showAlert(true, 'success', 'Your block claim has been submitted! Please be patient while our staff verifies the details.')
                this.setState({ isLoading: false });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to create block claim. Please contact the site administrators', true);
                else
                    this.showAlert(true, 'error', 'Unable to create block claim. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });


    }

    checkClaim() {
        this.setState({ isLoading: true });

        let payload = {
            userId: Auth.getUser().id,
            blockId: this.state.block.id
        };

        let checkUrl = `${getApi(Config.API.Block.Claims)}/check`;

        axios.post(checkUrl, payload)
            .then(res => {
                this.setState({
                    isClaimed: res.data.isClaimed,
                    isLoading: false
                });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to verify block claim. Please contact the site administrators', true);
                else
                    this.showAlert(true, 'error', 'Unable to verify block claim Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });
    }

    componentDidMount() {
        let url = `${getApi(Config.API.Block.Listings)}/${this.props.match.params.id}`;

        axios.get(url)
            .then(res => {
                this.setState({
                    block: res.data,
                    isLoading: false
                });

                this.setReputation();

                if (Auth.isAuthenticated()) {
                    this.checkVote();
                    this.checkClaim();
                }
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            })
    }

    render() {
        return (
           <div>
               <Helmet>
                   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
                         integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU"
                         crossOrigin="anonymous" />
               </Helmet>
               <div className="wrapper">
                   <LoadingScreen show={this.state.isLoading} />

                   {
                       !this.state.isLoading && this.state.block != null &&
                       <div>
                           <SiteTitle title={this.state.block.businessName} />
                           <BlockHeader title={this.state.block.businessName} showRating={false} />

                           <section className="main_section">
                               <div className="container">
                                   <AlertBox {...this.state.alert} />
                                   <div className="row">
                                       <div className="col-md-3">
                                           <BlockSidebar block={this.state.block} isClaimed={this.state.isClaimed} hasVoted={this.state.hasVoted} onClaimSubmit={this.onClaimSubmit} />
                                       </div>
                                       <div className="col-md-6">
                                           <BlockContentBox title = "About" content={this.state.block.aboutBusiness} />
                                           <BlockOverviewSection block={this.state.block} />

                                           {
                                               this.state.block.ico != null &&
                                               <BlockIcoSection block={this.state.block} />
                                           }
                                       </div>
                                       <div className="col-md-3">
                                           {
                                               this.state.reputation != null &&
                                               <BlockRating reputation={this.state.reputation} />
                                           }

                                           <BlockVoteWidget processVote={this.processVote} disabled={this.state.hasVoted} />

                                           <BlockVotesOverview reputation={this.state.reputation} />

                                           <div className="mt-30" style={{ backgroundColor: "white" }}>
                                               <BlockCardItem block={this.state.block} url={`${Config.RouteUrls.Member.Blocks.View}/${this.state.block.id}`} />
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           </section>
                       </div>
                   }
               </div>
           </div>
        );
    }
}