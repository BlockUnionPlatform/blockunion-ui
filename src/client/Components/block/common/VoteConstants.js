let VoteState = {
    Up: 1,
    Down: 0
};

let VoteTypes = {
    BlockUnionMember: 1,
    Anonymous: 2,
    Blockchain: 3
};

export {VoteState, VoteTypes};