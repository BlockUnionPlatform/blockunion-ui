import React, {Component} from 'react';

export default class BlockFooterContainer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <section className="join-block-union">
                    <div className="container">
                        <div className="block-union-logo">
                            <div className="blk-logo-wrap">
                                <div className="jbu-logo">
                                    <img src="assets/rating/images/favicon.png" alt="Logo" />
                                </div>
                                <div className="jbu-logo-content">
                                    <h3>BLOCK UNION</h3>
                                    <p>50,000 members</p>
                                </div>
                            </div>
                        </div>
                        <div className="block-union-form">
                            <h3>Join the Official BLOCK UNION group</h3>
                            <div className="form-wrap">
                                <form method="post" id="newsletter-form" name="newsletter">
                                    <div className="form-wrap">
                                        <input type="email" placeholder="https://t.me/joinchat/EeNzU05Wi7dOoJwTxGrLoQ*"
                                               name="email" id="email" />
                                            <div className="join-wrap btn-regular">
                                                <input type="submit" value="JOIN GROUP" name="email-submit"
                                                       className="email-submit" />
                                                    <div style={{ display: 'none' }} id="ajaxLoader">
                                                        <img src="images/ajax-loader.gif" alt="load" title="loader" />
                                                    </div>
                                            </div>
                                    </div>

                                    <div id="result"></div>
                                    <div style={{ display: 'none' }} id="error1">Please enter valid website url.</div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>


                <footer id="footer">
                    <div className="container">
                        <div className="footer-copyright">
                            <p>© <a href="index.html">BLOCK UNION</a> - 2018. All Rights Reserved.</p>
                        </div>
                        <div className="footer_menu">
                            <ul>
                                <li><a href="javascript:void(0)">Privacy Policy</a></li>
                                <li><a href="javascript:void(0)">Terms & Conditions</a></li>
                            </ul>
                        </div>
                    </div>
                </footer>
            </div>
        );
    }
}