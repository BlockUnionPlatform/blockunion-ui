import React, { Component } from 'react';

class BlockAssets extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <link type="text/css" rel="stylesheet" href="assets/rating/css/jquery.fancybox.min.css" />
                <link type="text/css" rel="stylesheet" href="assets/rating/css/font-awesome.min.css" />
                <link type="text/css" rel="stylesheet" href="assets/rating/css/main_styles.css" />
                <link type="text/css" rel="stylesheet" href="assets/rating/css/responsive.css" />
            </div>
        );
    }
}

export default BlockAssets;