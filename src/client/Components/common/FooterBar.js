import React, {Component} from 'react';
import {Link} from "react-router-dom";
import {Config} from "../../Config";

export default class FooterBar extends Component {
    render() {
        return (
            <footer id="footer">
                <div className="container">
                    <div className="footer-copyright" style={{ width: '100%', float: 'none' }}>
                        <ul className="right">
                            <li><Link to={Config.RouteUrls.Member.Misc.Privacy}>Privacy Policy</Link></li>
                            <li><Link to={Config.RouteUrls.Member.Misc.Terms}>Terms &amp; Condition</Link></li>
                        </ul>

                        <p className="left">
                            &copy; <a href="#">BLOCK UNION </a> - All Rights Reserved 2018
                        </p>
                    </div>
                </div>
            </footer>
        );
    }
}