import React, { Component } from 'react';
import JoinBlockUnion from './JoinBlockUnion';
import FooterBar from './FooterBar';

class FooterContainer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <JoinBlockUnion />
                <FooterBar/>
            </div>
        );
    }
};

export default FooterContainer;