import React, {Component} from 'react';
import {Helmet} from "react-helmet";
import {Config} from "../../Config";

export default class SiteTitle extends Component {
    constructor(props) {
        super(props);

        this.handler = this.handler.bind(this);
    }

    handler() {
        //this.props.action();
    }

    render() {
        return (
            <Helmet>
                <title>{this.props.title} | {Config.SiteName}</title>
            </Helmet>
        );
    }
}