import React from 'react';

export default function StatusBadge(props) {
    if (props.isActive) {
        return (
            <span className="badge badge-success">Active</span>
        );
    }
    else {
        return (
            <span className="badge badge-danger">{props.text != undefined ? props.text : 'Inactive'}</span>
        );
    }
}