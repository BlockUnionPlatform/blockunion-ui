import React, {Component} from 'react';

export default class AlertBox extends Component {
    constructor(props) {
        super(props);

        this.handler = this.handler.bind(this);
    }

    handler() {
        //this.props.action();
    }

    render () {
        return (
            <div className={this.props.isVisible || this.props.show ? 'd-block' : 'd-none'}>
                <div className={`alert alert-${this.props.status == 'success' ? 'success' : 'danger'}`}>
                    <div className={this.props.isSmall ? 'alert-text-small' : ''}>
                        {this.props.message}
                    </div>
                </div>
            </div>
        );
    }
}