import React, { Component } from 'react';

export default class AdBanner extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="google_ads">
                <h2>{this.props.googleAd}</h2>
            </div>
        );
    }
}