import React from 'react';

export default function ErrorMessage(props) {
    if (!props.isSmall) {
        return (
            <div>
                <p className="form-group" style={{ color: '#721c24' }}>Please correct the following errors to continue:</p>
                {props.errors.map(error => <div className="d-block" key={error}>{error}</div>)}
            </div>
        );
    }
    else {
        return (
            <div className="alert-text-small">
                <span style={{ color: '#721c24' }}>Please correct the following errors to continue:</span>
                <ul className="errors">
                    {props.errors.map(error => <li key={error}>{error}</li>)}
                </ul>
            </div>
        );
    }
}