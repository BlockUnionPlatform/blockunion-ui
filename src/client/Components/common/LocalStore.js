import React from 'react';
import sjcl from 'sjcl';

const SALT = 'myTotallyR@nD0mP@aSS';
const KEY_NAME = 'crypto';

const LocalStore = {
    setItem: function(key, data) {
        let encryptedData = sjcl.encrypt(SALT, JSON.stringify(data));
        localStorage.setItem(key, encryptedData);
    },

    getItem: function(key) {
        let encryptedData = localStorage.get(key);
        return JSON.parse(sjcl.decrypt(SALT, JSON.stringify(encryptedData)));
    }
};

export {LocalStore};