import React, { Component } from 'react';

class JoinBlockUnion extends Component {
    render() {
        return (
                <section className="join-block-union">
                    <div className="container">
                        <div className="block-union-logo">
                            <div className="blk-logo-wrap">
                                <div className="jbu-logo">
                                    <img src="assets/member/images/block-union.png" alt="Block Union"/>
                                </div>
                                <div className="jbu-logo-content">
                                    <h3 style={{ fontWeight: 'bold' }}>BLOCK UNION</h3>
                                    <p style={{ fontWeight: 'bold' }}>Discover. Rate. Earn.</p>
                                </div>
                            </div>
                        </div>

                        <div className="block-union-form">
                            <h3 style={{ fontWeight: 'bold' }}>Join the Official Block Union Group</h3>
                            <div className="form-wrap">
                                <form action="#" method="post" name="newsletter" id="form-newsletter">
                                    <div className="form-wrap">
                                        <input type="email" placeholder="Coming Soon, Stay Tuned!"
                                               name="email" id="email"/>
                                            <div className="join-wrap btn-regular">
                                                <input type="submit" value="JOIN GROUP" id="email-submit"
                                                       name="email-submit"/>
                                            </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
        );
    }
};

export default JoinBlockUnion;