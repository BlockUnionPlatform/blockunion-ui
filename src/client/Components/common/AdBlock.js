import React, { Component } from 'react';

export default class AdBlock extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="video">
                <a className="fancybox" href={this.props.videoUrl}>
                    <img src={this.props.videoThumbnail} title="Star" alt=""/>
                </a>
            </div>
        );
    }
}