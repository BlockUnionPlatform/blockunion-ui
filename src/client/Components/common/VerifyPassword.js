export default function VerifyPassword(props) {
    let errors = [];

    if (props.newPassword.length < 5)
        errors.push('Password must be at least 5 characters long.');

    if (props.newPassword != props.repeatPassword)
        errors.push('Password do not match.');

    return errors;
}