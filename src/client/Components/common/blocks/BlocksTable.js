import React from 'react';
import AlertBox from "../AlertBox";
import BlocksRow from "../../admin/pages/Blocks/BlocksRow";

export default function BlocksTable(props) {
    return (
        <div className="row mt-20">
            <div className="col-md-12">
                <AlertBox {...props.alert} />

                {
                    props.isLoading ?
                        <div>Loading</div>
                        :
                        <div className="table-responsive">
                            <table className="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Business Name</th>
                                    <th>Category</th>
                                    <th>Token</th>
                                    <th>Country</th>
                                    <th>Package</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>

                                <tbody>
                                {
                                    props.blocks.map((block) => <BlocksRow block={block} key={block.id} isAdmin={props.isAdmin} />)
                                }
                                </tbody>
                            </table>
                        </div>
                }
            </div>
        </div>
    );
}