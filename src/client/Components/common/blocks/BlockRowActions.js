import React from 'react';
import {Link} from "react-router-dom";
import {Config} from "../../../Config";

export default function BlockRowActions(props) {
    if (props.isAdmin != undefined && props.isAdmin) {
        return (
            <span>
                <Link className = "link" to={`${Config.RouteUrls.Admin.Blocks.View}/${props.id}`}>
                        View
                    </Link>

                &nbsp; | &nbsp;

                <Link className = "link" to={`${Config.RouteUrls.Admin.Blocks.Edit}/${props.id}`}>
                        Edit
                    </Link>

                &nbsp; | &nbsp;

                <Link className = "link" to={`${Config.RouteUrls.Admin.Blocks.Status}/${props.id}`}>
                        Status
                    </Link>
            </span>
        );
    }
    else {
        return (
            <span>
                <Link className = "link" to={`${Config.RouteUrls.Member.Blocks.View}/${props.id}`}>
                    View
                </Link>
            </span>
        );
    }
}