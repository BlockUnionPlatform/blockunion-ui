import React from 'react';

export default function LoadingScreen(props) {
    return (
        <div style={{ display: props.show ? 'block' : 'none' }}>
            <div className="loading">
                Loading
            </div>
        </div>
    );
}