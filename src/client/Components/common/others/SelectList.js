import React, {Component} from 'react';

export default class SelectList extends Component {
    constructor(props) {
        super(props);

        this.handler = this.handler.bind(this);
    }

    handler() {
        //this.props.action();
    }

    render() {
        let name = this.props.controlName !== null || this.props.controlName !== undefined ? this.props.controlName : 'defaultSelect';

        return (
            <select value={this.props.selectedValue || this.props.value } name={name} id={name} onChange={this.props.onSelectChange} className="form-control">
                {this.props.items.map( (item) =>
                    <option value={item.key} key={item.key}>{item.value}</option>
                )}
            </select>
        );
    }
}