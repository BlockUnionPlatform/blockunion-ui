import React, { Component } from 'react';

class Button extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <a href={this.props.Url} className={this.props.btnClass}>{this.props.btnText}</a>
            </div>
        );
    }
};

export default Button;