import React from 'react';
import Cookies from 'universal-cookie';
import sjcl from 'sjcl';
import {Config} from "../../Config";

const COOKIE_PASS = 'myTotallyR@nD0mP@aSS';
const cookies = new Cookies();

const Auth = {
    loggedIn: false,
    user: null,
    isAuthenticated: function() {
        if (cookies.get('user') !== undefined)
            this.loggedIn = true;

        return this.loggedIn;
    },

    signIn: function(user, rememberMe) {
        if (cookies.get('user') !== undefined)
            cookies.remove('user', { path: '/' });

        const current = new Date();
        const nextYear = new Date();
        nextYear.setFullYear(current.getFullYear() + 1);

        let options = {
            path: '/'
        };

        if (rememberMe)
            options["expires"] = nextYear;

        let userCookie = sjcl.encrypt(COOKIE_PASS, JSON.stringify(user));
        cookies.set('user', userCookie, options);
    },

    signOut: function() {
        cookies.remove('user', { path: '/' });
    },

    getUser: function() {
        return parseUser();
    },

    isAdmin: function() {
        this.user = parseUser();

        if (this.user != null)
            return this.user.accessLevel > Config.MinAdminLevel;
        else
            return false;
    }
};

function parseUser() {
    let userCookie = cookies.get('user');
    return JSON.parse(sjcl.decrypt(COOKIE_PASS, JSON.stringify(userCookie)));
}

export {Auth};