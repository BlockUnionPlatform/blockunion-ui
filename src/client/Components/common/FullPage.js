import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Routes from '../../Routes';

import TopHeaderContainer from '../member/TopHeaderContainer';
import MemberAssets from '../member/MemberAssets';
import FooterContainer from './FooterContainer';
import MainPage from './MainPage';

import AuthContainer from '../auth/AuthContainer';
import AuthAssets from '../auth/AuthAssets';

import AdminContainer from '../admin/AdminContainer';

import EnsureLoggedInContainer from '../EnsureLoggedInContainer';
import SiteHome from "../Main/SiteHome";
import BlockPublicPageContainer from "../block/BlockPublicPageContainer";
import EnsureIsAdminContainer from "../admin/EnsureIsAdminContainer";

class FullPage extends Component {
    constructor(props){
        super(props);
    }

    render() {
        let path = this.props.location.pathname;

        if (path.includes('/block') && !path.includes('/admin') && !path.includes('/member')) {
            return (
                <BlockPublicPageContainer>
                    <Routes action={this.handler} />
                    <MemberAssets />
                </BlockPublicPageContainer>
            );
        }
        else if (path.includes('/auth')) {
            document.body.classList.add('login-page');

            return (
                <div>
                    <div id="wrapper">
                        <AuthContainer>
                            {
                                <Routes action={this.handler} />
                            }
                        </AuthContainer>
                    </div>

                    <AuthAssets />
                </div>
            );
        }

        else if (path.includes('/admin')) {
            return (
                <EnsureLoggedInContainer>
                    <EnsureIsAdminContainer redirect={true}>
                        <AdminContainer>
                            <Routes action={this.handler}  />
                        </AdminContainer>
                    </EnsureIsAdminContainer>
                </EnsureLoggedInContainer>
            );
        }
        else if (path.includes('/member')) {
            return (
                <EnsureLoggedInContainer>
                    <MemberAssets />
                    <div className="wrapper">
                        <TopHeaderContainer />
                        <MainPage>
                            <Routes action={this.handler}  />
                        </MainPage>
                        <FooterContainer />
                    </div>
                </EnsureLoggedInContainer>
            );
        }
        else {
            return (
                <SiteHome />
            );
        }
    }
}

FullPage.defaultProps = {};

export default withRouter(FullPage);
