import {Auth} from "../AuthHelpers";

const MetaMask = {
    isInstalled: function() {
        return typeof web3 != 'undefined';
    },

    isInstalledAndAuthenticated: function() {
        return typeof web3 != 'undefined' && web3.eth.coinbase != null;
    }
};

export {MetaMask};