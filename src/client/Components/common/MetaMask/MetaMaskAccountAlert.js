import React from 'react';
import {Auth} from "../AuthHelpers";
import {Link} from "react-router-dom";
import {Config} from "../../../Config";

export default function MetaMaskAccountAlert(props) {

    if (typeof web3 === 'undefined') return (<span></span>);

    if (Auth.isAuthenticated() && (Auth.getUser().wallet === '' || Auth.getUser().wallet === undefined)) {
        return (
            <div className="alert alert-warning">
                <b>Attention!</b> It seems like you have the extension <b>MetaMask</b> installed but not linked to your account. Go to <Link to={Config.RouteUrls.Profile.Member} className = "link">My Profile</Link> to link your MetaMask account.
            </div>
        );
    }
    else return (<span></span>);
}