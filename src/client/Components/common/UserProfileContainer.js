import React, {Component} from 'react';
import AlertBox from "./AlertBox";
import LoadingScreen from "./LoadingScreen";
import SiteTitle from "./SiteTitle";
import VerifyProfile from "./VerifyProfile";
import ErrorMessage from "./ErrorMessage";
import axios from 'axios';
import {Auth} from "./AuthHelpers";
import ComposeErrorMessageFromJson from "../admin/pages/Common/ComposeErrorMessageFromJson";
import {getApi} from "../../ConfigStore";
import {Config} from '../../Config';
import VerifyPassword from "./VerifyPassword";

export default class UserProfileContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            firstName: '',
            lastName: '',
            emailAddress: '',
            phoneNumber: '',
            wallet: '',
            currentPassword: '',
            newPassword: '',
            repeatPassword: '',
            isLoading: true,
            hasMetaMask: false,
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.onInputChange = this.onInputChange.bind(this);
        this.onProfileSubmit = this.onProfileSubmit.bind(this);
        this.onPasswordSubmit = this.onPasswordSubmit.bind(this);
        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = '', message = '') {
        this.setState({ alert: { show, status, message } });

        if (show) window.scrollTo(0,0);
    }

    onInputChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    onProfileSubmit(e) {
        e.preventDefault();

        let errors = VerifyProfile(this.state);
        if (errors.length > 0) {
            let errorMessage = <ErrorMessage errors={errors} />
            this.showAlert(true, 'error', errorMessage);
        }
        else {
            let url = getApi(Config.API.Users.Profile);
            let payload = {
                userId: Auth.getUser().id,
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                emailAddress: this.state.emailAddress,
                phoneNumber: this.state.phoneNumber,
                wallet: this.state.wallet
            };

            this.setState({ isLoading: true });

            axios.post(url, payload)
                .then(res => {
                    this.setState({ isLoading: false });
                    Auth.signIn(res.data, true);
                    this.showAlert(true, 'success', 'Your profile has been updated successfully.');
                })
                .catch(error => {
                    if (error.response) {
                        if (error.response.status == 422)
                            this.showAlert(true, 'error', ComposeErrorMessageFromJson(error.response.data), true);
                        else
                            this.showAlert(true, 'error', JSON.stringify(error.response.data), true);
                    }
                    else if (error.request)
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);
                    else
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                    this.setState({ isLoading: false });
                });
        }
    }

    onPasswordSubmit(e) {
        e.preventDefault();

        let errors = VerifyPassword(this.state);
        if (errors.length > 0) {
            let errorMessage = <ErrorMessage errors={errors} />
            this.showAlert(true, 'error', errorMessage);
        }
        else {
            let url = getApi(Config.API.Users.Password);
            let payload = {
                password: this.state.newPassword,
                repeatPassword: this.state.repeatPassword,
                currentPassword: this.state.currentPassword,
                userId: Auth.getUser().id
            };

            this.setState({ isLoading: true });

            axios.post(url, payload)
                .then(res => {
                    this.showAlert(true, 'success', 'Your password was updated successfully.');
                    this.setState({ isLoading: false });
                })
                .catch(error => {
                    if (error.response) {
                        if (error.response.status === 422)
                            this.showAlert(true, 'error', ComposeErrorMessageFromJson(error.response.data), true);
                        else
                            this.showAlert(true, 'error', JSON.stringify(error.response.data), true);
                    }
                    else if (error.request)
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);
                    else
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                    this.setState({ isLoading: false });
                });
        }
    }

    getMetaMaskAccount(e) {
        e.preventDefault();
        this.showAlert(false);

        let publicAddress = web3.eth.coinbase;

        if (publicAddress == null) {
            this.showAlert(true, 'error', 'Unable to connect your MetaMask account. Could not read your active account address. Try logging in to Meta Mask again.');
        }
        else {
            this.setState({
                wallet: web3.eth.coinbase
            });
        }
    }

    componentDidMount() {
        let user = Auth.getUser();
        this.setState({
            firstName: user.firstName,
            lastName: user.lastName,
            phoneNumber: user.phoneNumber,
            emailAddress: user.emailAddress,
            wallet: user.wallet,
            isLoading: false,
            hasMetaMask: typeof web3 !== 'undefined'
        });
    }

    render() {
        return (
            <div>
                <SiteTitle title = "Manage Profile" />
                <LoadingScreen show={this.state.isLoading} />
                <form action="#" method="POST" className="controls-group-lg" onSubmit={this.onProfileSubmit}>
                    <div className="row">
                        <div className="col-md-7">
                            <h1 className="form-group">Manage your profile</h1>
                            <AlertBox {...this.state.alert} />
                            <div className="form-group">
                                <label className="control-label">First Name:</label>
                                <input name="firstName" type="text" placeholder="Enter your first name here."
                                       className="form-control" value={this.state.firstName}
                                       onChange={this.onInputChange}/>
                            </div>

                            <div className="form-group">
                                <label className="control-label">Last Name:</label>
                                <input name="lastName" type="text" placeholder="Enter your last name here."
                                       className="form-control" value={this.state.lastName}
                                       onChange={this.onInputChange}/>
                            </div>

                            <div className="form-group">
                                <label className="control-label">Email Address:</label>
                                <input name="emailAddress" type="text" placeholder="Enter your email address here."
                                       className="form-control" value={this.state.emailAddress}
                                       onChange={this.onInputChange}/>
                            </div>

                            <div className="form-group">
                                <label className="control-label">Phone Number:</label>
                                <input name="phoneNumber" type="text" placeholder="Enter your phone number here."
                                       className="form-control" value={this.state.phoneNumber}
                                       onChange={this.onInputChange}/>
                            </div>

                            <div className="form-group">
                                <label className="control-label">Wallet:</label>
                                <input name="wallet" type="text" placeholder="Enter your vault address here."
                                       className="form-control" value={this.state.wallet}
                                       onChange={this.onInputChange}/>

                                {
                                    this.state.hasMetaMask && this.state.wallet == web3.eth.coinbase &&
                                    <span className="small" style={{ color: 'green' }}>MetaMask account connected.</span>
                                }
                            </div>

                            <div className="form-group">
                                <div className="pull-right">
                                    {
                                        this.state.hasMetaMask && (this.state.wallet !== web3.eth.coinbase) &&
                                        <button className="btn btn-theme" type="submit" style={{ marginRight: '5px' }} onClick={this.getMetaMaskAccount.bind(this)}>Connect MetaMask</button>
                                    }
                                    <button className="btn btn-theme" type="submit">Save Changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <form action="#" method="POST" onSubmit={this.onPasswordSubmit}>
                    <div className="row">
                        <div className="col-md-7">
                            <h1 className="form-group">Change Password</h1>
                            <div className="form-group">
                                <label className="control-label">Current Password:</label>
                                <input name="currentPassword" type="password" placeholder="Enter your current password."
                                       className="form-control" value={this.state.currentPassword}
                                       onChange={this.onInputChange}/>
                            </div>

                            <div className="form-group">
                                <label className="control-label">New Password:</label>
                                <input name="newPassword" type="password" placeholder="Enter your new password."
                                       className="form-control" value={this.state.newPassword}
                                       onChange={this.onInputChange}/>
                            </div>

                            <div className="form-group">
                                <label className="control-label">Repeat Password:</label>
                                <input name="repeatPassword" type="password" placeholder="Repeat your newpassword."
                                       className="form-control" value={this.state.repeatPassword}
                                       onChange={this.onInputChange}/>
                            </div>

                            <div className="form-group">
                                <div className="pull-right">
                                    <button className="btn btn-theme" type="submit">Change Password</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}