import React, { Component } from 'react';
import SidebarContainer from '../member/SidebarContainer';
import BlockAssets from "../block/BlockAssets";
import SiteTitle from "./SiteTitle";
import MetaMaskAccountAlert from "./MetaMask/MetaMaskAccountAlert";

class MainPage extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <section className="trending-section">
                    <div className="container">
                        <SidebarContainer />
                        <div className="wrap-trending clr right-panel">
                            <div className="wrapper-sm">
                                <MetaMaskAccountAlert/>
                                {this.props.children}
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default MainPage;