import VerifyEmail from "./VerifyEmail";

export default function VerifyProfile(profile) {
    let errors = [];

    if (profile.firstName.length < 2 || profile.firstName.length > 50)
        errors.push('First name should be between 2 and 50 characters long.');

    if (profile.lastName.length < 2 || profile.lastName.length > 50)
        errors.push('Last name should be between 2 and 50 characters long.');

    if (!VerifyEmail(profile.emailAddress))
        errors.push('Please provide a valid email address');

    return errors;
};