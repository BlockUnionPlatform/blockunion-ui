import React, {Component} from 'react';
import {Auth} from "../common/AuthHelpers";
import {Config} from "../../Config";
import {Redirect} from "react-router";

export default class EnsureIsAdminContainer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (Auth.isAuthenticated() && Auth.getUser().accessLevel >= Config.MinAdminLevel) {
            return this.props.children;
        }
        else {
            if (this.props.redirect != undefined && this.props.redirect) {
                return (<Redirect to="/"/>);
            }
            else return (<span></span>);
        }
    }
}