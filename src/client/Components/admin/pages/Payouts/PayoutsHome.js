import React, {Component} from 'react';
import SiteTitle from "../../../common/SiteTitle";
import PageHeaderWithLink from "../PageHeaderWithLink";
import PayoutRowItem from "./PayoutRowItem";
import {getApi} from "../../../../ConfigStore";
import {Config} from "../../../../Config";
import axios from 'axios';
import AlertBox from "../../../common/AlertBox";
import LoadingScreen from "../../../common/LoadingScreen";

export default class PayoutsHome extends Component {
    constructor(props) {
        super(props);

        this.state = {
            payouts: [],
            isLoading: true,
            alert: {
                show: false,
                status: '',
                message: '',
            }
        }

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });
    }

    componentWillMount() {
        let url = getApi(Config.API.Payouts);

        axios.get(url)
            .then(res => {
                this.setState({
                    payouts: res.data,
                    isLoading: false
                })
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title="Payouts"/>
                <PageHeaderWithLink title="Payouts"/>
                <AlertBox {...this.state.alert} />

                <div className="table-responsive">
                    {
                        this.state.isLoading ?
                            <div>Loading</div>
                            :
                            <div>
                                <table className="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Amount</th>
                                        <th>Paid To</th>
                                        <th>Processed By</th>
                                        <th>Reward Claim</th>
                                        <th>Date Processed</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                        {
                                            this.state.payouts.map((payout) =>
                                                <PayoutRowItem payout={payout} isAdmin={true} />
                                            )
                                        }
                                    </tbody>
                                </table>
                            </div>
                    }
                </div>
            </div>
        );
    }
}
