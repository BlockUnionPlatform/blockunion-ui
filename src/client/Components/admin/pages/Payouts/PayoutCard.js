import React from 'react';
import Link from "react-router-dom/es/Link";
import {Config} from "../../../../Config";
import UserLink from "../Common/UserLink";
import moment from "moment";

export default function PayoutCard(props) {
    let isAdmin = props.isAdmin != undefined && props.isAdmin;

    return (
        <form action="#">
            <div className="form-group row">
                <label className="control-label text-semibold col-md-2">ID:</label>
                <div className="col-md-10">
                    {props.payout.id}
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label text-semibold col-md-2">Amount:</label>
                <div className="col-md-10">
                    {
                        props.isAdmin ?
                            <Link className = "link" to={`${Config.RouteUrls.Admin.Rewards.View}/${props.payout.rewardId}`}>
                                {props.payout.amount}
                            </Link>
                            :
                            <span>
                                {props.payout.amount}
                            </span>
                    }
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label text-semibold col-md-2">Paid To:</label>
                <div className="col-md-10">
                    {
                        props.isAdmin ?
                            <UserLink user={props.payout.user} />
                            :
                            <span>{props.payout.user.name}</span>
                    }
                </div>
            </div>

            {
                props.isAdmin &&
                <div className="form-group row">
                    <label className="control-label text-semibold col-md-2">Processed By:</label>
                    <div className="col-md-10">
                        <UserLink user={props.payout.admin} />
                    </div>
                </div>
            }

            <div className="form-group row">
                <label className="control-label text-semibold col-md-2">Reward:</label>
                <div className="col-md-10">
                    {
                        props.isAdmin ?
                            <Link className = "link" to={`${Config.RouteUrls.Admin.Rewards.Claims.View}/${props.payout.rewardClaimId}`}>
                                View Reward Claim
                            </Link>
                            :
                            <Link className = "link" to={`${Config.RouteUrls.Member.DailyActionClaims}/${props.payout.rewardClaimId}`}>
                                View Reward Claim
                            </Link>
                    }
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label text-semibold col-md-2">Date Processed:</label>
                <div className="col-md-10">
                    {moment(props.payout.dateCreatedUtc).format(Config.Formats.DateTime)}
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label text-semibold col-md-2">Comments:</label>
                <div className="col-md-6">{props.payout.comments}</div>
            </div>
        </form>
    );
}