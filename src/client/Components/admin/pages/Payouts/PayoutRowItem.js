import React, {Component} from 'react';
import Link from "react-router-dom/es/Link";
import {Config} from "../../../../Config";
import UserLink from "../Common/UserLink";
import GuidLink from "../Common/GuidLink";
import moment from "moment";

export default class PayoutRowItem extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <tr>
                <td>
                    {
                        this.props.isAdmin != undefined && this.props.isAdmin ?
                            <GuidLink id={this.props.payout.id} url={Config.RouteUrls.Admin.Payouts.View} />
                            :
                            <GuidLink id={this.props.payout.id} url={Config.RouteUrls.Member.DailyActionPayouts} />
                    }
                </td>

                <td>
                    {this.props.payout.amount}
                </td>

                <td>
                    {
                        this.props.isAdmin ?
                            <UserLink user={this.props.payout.user}/>
                            :
                            <span>{this.props.payout.user.name}</span>
                    }
                </td>

                {
                    this.props.isAdmin != undefined && this.props.isAdmin &&
                    <td>
                        <UserLink user={this.props.payout.admin}/>
                    </td>
                }

                <td>
                    {
                        this.props.isAdmin != undefined && this.props.isAdmin ?
                            <Link className = "link" to={`${Config.RouteUrls.Admin.Rewards.Claims.View}/${this.props.payout.rewardClaimId}`}>
                                View Reward Claim
                            </Link>
                            :
                            <Link className = "link" to={`${Config.RouteUrls.Member.DailyActionClaims}/${this.props.payout.rewardClaimId}`}>
                                View Reward Claim
                            </Link>
                    }
                </td>

                <td>
                    {moment(this.props.payout.dateCreatedUtc).format(Config.Formats.DateTime)}
                </td>

                <td>
                    {
                        this.props.isAdmin != undefined && this.props.isAdmin ?
                            <Link className="link" to={`${Config.RouteUrls.Admin.Payouts.View}/${this.props.payout.id}`}>
                                View
                            </Link>
                            :
                            <Link className="link" to={`${Config.RouteUrls.Member.DailyActionPayouts}/${this.props.payout.id}`}>
                                View
                            </Link>

                    }
                </td>
            </tr>
        );
    }
}