import React from 'react';
import {Link} from "react-router-dom";

export default function WidgetOverview(props) {
    return (
        <div className="widget-overview widget-blocks">
            <div className="widget-wrapper">
                <div className="text-figure">{props.entity.value}</div>
                <div className="text-desc">{props.entity.text}</div>
            </div>
            <div className="url">
                <Link to={props.entity.url}>
                    details
                </Link>
            </div>
        </div>
    );
};