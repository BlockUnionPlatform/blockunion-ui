import React from 'react';

export default function ContentCard(props) {
    return (
        <div className="card">
            <div className="card-body">
                <div className="card-title">
                    <h3 className="text-center">{props.title}</h3>
                </div>

                {props.children}
            </div>
        </div>
    );
}