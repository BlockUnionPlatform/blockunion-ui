import React, {Component} from 'react';
import SiteTitle from "../../common/SiteTitle";

export default class AdminBlocks extends Component {
    render() {
        return (
            <div>
                <SiteTitle title="Blocks"/>
                All the blocks will be listed here.
            </div>
        );
    }
}