import React, {Component} from 'react';
import PageHeaderWithLink from "../PageHeaderWithLink";
import {Link} from 'react-router-dom';
import {Config} from "../../../../Config";
import {Repository} from "../../../../Data/DataStore";
import axios from 'axios';
import SiteTitle from "../../../common/SiteTitle";
import LoadingScreen from "../../../common/LoadingScreen";
import {getApi} from "../../../../ConfigStore";
import GuidLink from "../Common/GuidLink";
import moment from "moment";
import SystemLogRow from "./SystemLogRow";
import AlertBox from "../../../common/AlertBox";

export default class SystemLogsHome extends Component {
    constructor(props) {
        super(props);

        this.state = {
            logs: [],
            isLoading: true,
            alert: {
                show: false,
                status: '',
                message: '',
            }
        };

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });
    }

    componentDidMount() {
        let url = getApi(Config.API.Logs.System);
        axios.get(url)
            .then(res => {
                this.setState({
                    logs: res.data,
                    isLoading: false
                });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });
    }

    render() {
        return (
            <div>
                <SiteTitle title = "System Logs" />
                <LoadingScreen show={this.state.isLoading} />
                <PageHeaderWithLink title="System Logs" />
                <div className="form-group">
                    <p>This page displays a list of all the system logs.</p>
                </div>

                <AlertBox {...this.state.alert} />

                {
                    this.state.isLoading ?
                        <div></div>
                        :
                        <div className="row">
                            <div className="col-md-12">
                                <div className="table-responsive">
                                    <table className="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Level</th>
                                            <th>Short Message</th>
                                            <th>Long Message</th>
                                            <th>Logger</th>
                                            <th>Date & Time</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                            {
                                                this.state.logs.map(log => <SystemLogRow log={log} />)
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                }
            </div>
        );
    }
}