import React from 'react';
import GuidLink from "../Common/GuidLink";
import {Config} from "../../../../Config";
import moment from "moment";
import {Link} from "react-router-dom";

export default function SystemLogRow(props) {
    return (
        <tr>
            <td>
                <GuidLink url={Config.RouteUrls.Admin.Logs.System.View} id={props.log.id} />
            </td>
            <td>{props.log.level}</td>
            <td>{props.log.shortMessage.substr(0, 20)}...</td>
            <td>{props.log.longMessage.substr(0, 30)}...</td>
            <td>
                {
                    props.log.logger == null ?
                        <span>-</span>
                        :
                        <span>{props.log.logger}</span>
                }
            </td>
            <td>{moment(props.log.dateCreatedUtc).format(Config.Formats.DateTime)}</td>
            <td>
                <Link className="link" to={`${Config.RouteUrls.Admin.Logs.System.View}/${props.log.id}`}>View</Link>
            </td>
        </tr>
    );
}