import React, {Component} from 'react';
import {Config} from "../../../../Config";
import PageHeaderWithLink from "../PageHeaderWithLink";
import SiteTitle from "../../../common/SiteTitle";
import LoadingScreen from "../../../common/LoadingScreen";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';
import moment from "moment";
import AlertBox from "../../../common/AlertBox";

export default class SystemsLogView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            log: {},
            isLoading: true,
            isFound: true,
            alert: {
                show: false,
                status: '',
                message: '',
            }
        };

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });
    }

    componentWillMount() {
        let id = this.props.match.params.id;
        let url = `${getApi(Config.API.Logs.System)}/${id}`;

        axios.get(url)
            .then(res => {
                this.setState({
                    log: res.data,
                    isLoading: false
                });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false, isFound: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title = "View System Log" />
                <PageHeaderWithLink title="View System Log"  btnText="Back to System Logs" btnIcon="fa fa-arrow-left" btnUrl={Config.RouteUrls.Admin.Logs.System.Home}/>
                <div className="row">
                    <div className="col-md-6">
                        <AlertBox {...this.state.alert} />
                    </div>
                </div>

                {
                    this.state.isLoading ?
                        <div></div>
                        :
                        <div>
                            {
                                !this.state.isFound ?
                                    <div></div>
                                    :
                                    <form action="#" className="form-horizontal">
                                        <div className="form-group row">
                                            <label className="control-label col-md-2">Log ID:</label>
                                            <div className="col-md-6">{this.state.log.id}</div>
                                        </div>

                                        <div className="form-group row">
                                            <label className="control-label col-md-2">Log Level:</label>
                                            <div className="col-md-6">{this.state.log.level}</div>
                                        </div>

                                        <div className="form-group row">
                                            <label className="control-label col-md-2">Short Message:</label>
                                            <div className="col-md-6">{this.state.log.shortMessage}</div>
                                        </div>

                                        <div className="form-group row">
                                            <label className="control-label col-md-2">Long Message:</label>
                                            <div className="col-md-6">
                                                <textarea cols="50" rows="10" readOnly="readonly"
                                                          className="form-control">{this.state.log.longMessage}</textarea>
                                            </div>
                                        </div>

                                        <div className="form-group row">
                                            <label className="control-label col-md-2">Logger:</label>
                                            <div className="col-md-6">{this.state.log.logger}</div>
                                        </div>

                                        <div className="form-group row">
                                            <label className="control-label col-md-2">Date & Time:</label>
                                            <div
                                                className="col-md-6">{moment(this.state.log.dateCreatedUtc).format(Config.Formats.DateTime)}</div>
                                        </div>
                                    </form>
                            }
                        </div>
                }
            </div>
        );
    }
}