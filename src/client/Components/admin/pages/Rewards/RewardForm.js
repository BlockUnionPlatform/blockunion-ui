import React from 'react';
import TokenDropdown from "../Common/TokenDropdown";

export default function RewardForm(props) {
    return (
        <form action="#" onSubmit={props.onFormSubmit}>
            <div className="form-group">
                <label className="control-label text-semibold">Name:</label>
                <input type="text" name="name" id="name" value={props.name} onChange={props.onInputChange} className="form-control" />
            </div>
            <div className="form-group">
                <label className="control-label text-semibold">Value:</label>
                <input type="number" name="value" id="value" value={props.value} onChange={props.onInputChange} className="form-control" />
            </div>
            <div className="form-group">
                <label className="control-label text-semibold">Token:</label>
                <TokenDropdown onChange={props.onTokenChange} value={props.token} />
            </div>
            <div className="form-group">
                <label className="control-label text-semibold">Is Active?:</label>
                <input type="checkbox" name="isActive" id="isActive" checked={props.isActive} onChange={props.onIsActiveChange} className="position-right" />
            </div>
            <div className="form-group">
                <div className="pull-right">
                    <button type="submit" className="btn btn-theme">{props.btnText}</button>
                </div>
            </div>
        </form>
    );
}