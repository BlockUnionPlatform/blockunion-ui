export default function VerifyRewards(name, value) {
    let errors = [];

    if (name.length < 0)
        errors.push('Please enter reward name');

    if (value == 0)
        errors.push('Please enter a non-zero value for reward');

    return errors;
}