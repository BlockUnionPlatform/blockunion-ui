import React, {Component} from 'react';
import PageHeaderWithLink from "../PageHeaderWithLink";
import {Config} from "../../../../Config";
import RewardForm from "./RewardForm";
import SiteTitle from "../../../common/SiteTitle";
import ErrorMessage from "../../../common/ErrorMessage";
import AlertBox from "../../../common/AlertBox";
import {getApi} from "../../../../ConfigStore";
import LoadingScreen from "../../../common/LoadingScreen";
import axios from 'axios';

export default class RewardsAddPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            value: 0.00,
            token: null,
            isActive: true,
            isLoading: false,
            alert: {
                show: false,
                status: '',
                message: '',
            }
        }

        this.showAlert = this.showAlert.bind(this);
        this.onTokenChange = this.onTokenChange.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.onIsActiveChange = this.onIsActiveChange.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });

        window.scrollTo(0,0);
    }

    onTokenChange(selectedValue) {
        this.setState({ token: selectedValue });
    }

    onInputChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    onFormSubmit(event) {
        event.preventDefault();

        if (this.state.value <= 0) {
            let errors = [];
            errors.push('Reward value should be greater than 0.')

            let errorMessage = <ErrorMessage errors={errors}/>
            this.showAlert(true, 'error', errorMessage);

        }
        else {
            let url = getApi(Config.API.Rewards);
            let payload = {
                name: this.state.name,
                value: parseFloat(this.state.value),
                tokenId: this.state.token.value,
                isActive: this.state.isActive
            };

            axios.post(url, payload)
                .then(res => {
                    this.showAlert(true, 'success', 'New reward created successfully.');
                    this.setState({ isLoading: false });
                })
                .catch(error => {
                    if (error.response)
                        this.showAlert(true, 'error', error.response.data.toString(), true);
                    else if (error.request)
                        this.showAlert(true, 'error', 'Unable to connect to server', true);
                    else
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                    this.setState({ isLoading: false });
                });
        }
    }

    onIsActiveChange(event) {
        this.setState({ isActive: event.target.checked  });
    }

    render() {
        return (
            <div>
                <SiteTitle title="Add Reward"/>
                <LoadingScreen show={this.state.isLoading} />
                <PageHeaderWithLink title="Add Reward" btnText="Back to Rewards" btnIcon="fa fa-arrow-left" btnUrl={Config.RouteUrls.Admin.Rewards.Home} />
                <div className="row mt-20">
                    <div className="col-md-7">
                        <AlertBox {...this.state.alert} />
                        <RewardForm
                            btnText="Add Reward"
                            name={this.state.name}
                            value={this.state.value}
                            token={this.state.token}
                            isActive={this.state.isActive}
                            onInputChange={this.onInputChange}
                            onTokenChange={this.onTokenChange}
                            onFormSubmit={this.onFormSubmit}
                            onIsActiveChange={this.onIsActiveChange} />
                    </div>
                </div>
            </div>
        );
    }
}