import React, {Component} from 'react';
import StatusBadge from "../../../common/StatusBadge";

export default function RewardViewForm(props) {
    return (
        <form action="#">
            <div className="form-group row">
                <label className="control-label text-semibold">ID:</label>
                <div className="col-md-6">
                    {props.reward.id}
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label text-semibold">Name:</label>
                <div className="col-md-6">
                    {props.reward.name}
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label text-semibold">Value:</label>
                <div className="col-md-6">
                    {props.reward.value}
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label text-semibold">Token:</label>
                <label className="control-label text-semibold">Token:</label>
                <div className="col-md-6">
                    {props.reward.token.name} ({props.reward.token.symbol})
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label text-semibold">Is Active?</label>
                <div className="col-md-6">
                    <StatusBadge isActive={props.reward.isActive} />
                </div>
            </div>
        </form>
    );
}