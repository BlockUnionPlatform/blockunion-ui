import React, {Component} from 'react';
import SiteTitle from "../../../common/SiteTitle";
import PageHeaderWithLink from "../PageHeaderWithLink";
import {Config} from "../../../../Config";
import AlertBox from "../../../common/AlertBox";
import RewardForm from "./RewardForm";
import ErrorMessage from "../../../common/ErrorMessage";
import LoadingScreen from "../../../common/LoadingScreen";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';
import {GetTokenDropdownItem} from "../Common/TokenFunctions";
import VerifyRewards from "./VerifyRewards";

export default class RewardsEditPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            value: 0.00,
            token: null,
            isActive: true,
            isFound: true,
            isLoading: true,
            alert: {
                show: false,
                status: '',
                message: '',
            }
        }

        this.showAlert = this.showAlert.bind(this);
        this.onTokenChange = this.onTokenChange.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.onIsActiveChange = this.onIsActiveChange.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });

        window.scrollTo(0,0);
    }

    onTokenChange(value) {
        this.setState({ token: value });
    }

    onInputChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    onFormSubmit(event) {
        event.preventDefault();

        let errors = VerifyRewards(this.state.name, this.state.value);
        if (errors.length > 0) {
            let errorMessage = <ErrorMessage errors={errors}/>
            this.showAlert(true, 'error', errorMessage);
        }
        else {
            let id = this.props.match.params.id;
            let url = `${getApi(Config.API.Rewards)}/${id}`;
            let payload = {
                name: this.state.name,
                value: parseFloat(this.state.value),
                tokenId: this.state.token.value,
                isActive: this.state.isActive
            };
            console.log(payload);
            this.setState({ isLoading: true });

            axios.put(url, payload)
                .then(res => {
                    this.showAlert(true, 'success', 'Reward updated successfully.');
                    this.setState({ isLoading: false });
                })
                .catch(error => {
                    console.log(error);
                    if (error.response)
                        this.showAlert(true, 'error', error.response.data.toString(), true);
                    else if (error.request)
                        this.showAlert(true, 'error', 'Unable to connect to server', true);
                    else
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                    this.setState({ isLoading: false });
                });
        }
    }

    onIsActiveChange(event) {
        this.setState({ isActive: event.target.checked  });
    }

    componentDidMount() {
        let id = this.props.match.params.id;
        let url = `${getApi(Config.API.Rewards)}/${id}`;

        axios.get(url)
            .then(res => {
                this.setState({
                    name: res.data.name,
                    value: res.data.value,
                    isActive: res.data.isActive,
                    token: GetTokenDropdownItem(res.data.token),
                    isFound: true,
                    isLoading: false
                });

            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false, isFound: false });
            });
    }

    render() {
        return (
            <div>
                <SiteTitle title="Edit Reward"/>
                <LoadingScreen show={this.state.isLoading} />
                <PageHeaderWithLink title="Edit Reward" btnText="Back to Rewards" btnIcon="fa fa-arrow-left" btnUrl={Config.RouteUrls.Admin.Rewards.Home} />
                <div className="row mt-20">
                    <div className="col-md-7">
                        <AlertBox {...this.state.alert} />
                        {
                            this.state.isFound &&
                            <RewardForm
                                btnText="Edit Reward"
                                name={this.state.name}
                                value={this.state.value}
                                token={this.state.token}
                                isActive={this.state.isActive}
                                onInputChange={this.onInputChange}
                                onTokenChange={this.onTokenChange}
                                onFormSubmit={this.onFormSubmit}
                                onIsActiveChange={this.onIsActiveChange} />
                        }
                    </div>
                </div>
            </div>
        );
    }
}