import React, {Component} from 'react';
import PageHeaderWithLink from "../PageHeaderWithLink";
import {Config} from "../../../../Config";
import RewardViewForm from "./RewardViewForm";
import AlertBox from "../../../common/AlertBox";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';

export default class RewardsViewPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            reward: {},
            isFound: true,
            isLoading: true,
            alert: {
                show: false,
                status: '',
                message: '',
            }
        };

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });
    }

    componentWillMount() {
        let id = this.props.match.params.id;
        let url = `${getApi(Config.API.Rewards)}/${id}`;

        axios.get(url)
            .then(res => {
                this.setState({
                    reward: res.data,
                    isFound: true,
                    isLoading: false
                })
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false, isFound: false });
            });
    }

    render() {
        return (
            <div>
                <PageHeaderWithLink title="View Reward" btnText="Edit Reward" btnIcon="fa fa-pencil" btnUrl={`${Config.RouteUrls.Admin.Rewards.Edit}/${this.props.match.params.id}`} />
                <div className="row">
                    <div className="col-md-6">
                        <AlertBox {...this.state.alert} />
                    </div>
                </div>
                {
                    this.state.isLoading ?
                        <div>Loading</div>
                        :
                        <div>
                            {
                                !this.state.isFound ?
                                    <div>Not Found</div>
                                    :
                                    <div>
                                        <RewardViewForm reward={this.state.reward} />
                                    </div>
                            }
                        </div>
                }
            </div>
        );
    }
}