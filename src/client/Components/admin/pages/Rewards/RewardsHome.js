import React, {Component} from 'react';
import {Config} from "../../../../Config";
import PageHeaderWithLink from "../PageHeaderWithLink";
import StatusBadge from "../../../common/StatusBadge";
import {Link} from "react-router-dom";
import SiteTitle from "../../../common/SiteTitle";
import axios from 'axios';
import {getApi} from "../../../../ConfigStore";
import LoadingScreen from "../../../common/LoadingScreen";
import AlertBox from "../../../common/AlertBox";
import GuidLink from "../Common/GuidLink";

export default class RewardsHome extends Component {
    constructor(props) {
        super(props);

        this.state = {
            rewards: [],
            isLoading: true,
            alert: {
                show: false,
                status: '',
                message: '',
            }
        };

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });
    }

    componentDidMount() {
        let url = getApi(Config.API.Rewards);

        axios.get(url)
            .then(res => {
                this.setState({
                    rewards: res.data,
                    isLoading: false
                });
            })
            .catch(error => {
                    if (error.response)
                        this.showAlert(true, 'error', 'Unable to load data. Please try again.', true);
                    else if (error.request)
                        this.showAlert(true, 'error', 'Unable to connect to server', true);
                    else
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });
    }

    render() {
        return (
            <div>
                <SiteTitle title="Rewards"/>
                <LoadingScreen show={this.state.isLoading} />

                <PageHeaderWithLink title="Rewards" btnText="Add Reward" btnIcon="fa fa-plus" btnUrl={Config.RouteUrls.Admin.Rewards.Add} />
                <div className="form-group">
                    <p>A list of all the existing rewards.</p>
                </div>

                <div className="row">
                    <div className="col-md-12">
                        <AlertBox {...this.state.alert} />
                        <div className="table-responsive">
                            <table className="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Value</th>
                                        <th>Token</th>
                                        <th>Is Active?</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>

                                <tbody>
                                {this.state.rewards.map( (reward) =>
                                    <tr key={reward.id}>
                                        <td>
                                            <GuidLink url={Config.RouteUrls.Admin.Rewards.View} id={reward.id} />
                                        </td>
                                        <td>{reward.name}</td>
                                        <td>{reward.value}</td>
                                        <td>{reward.token.symbol} ({reward.token.name})</td>
                                        <td>
                                            <StatusBadge isActive={reward.isActive}/>
                                        </td>
                                        <td>
                                            <Link to={`${Config.RouteUrls.Admin.Rewards.Edit}/${reward.id}`} className="link">Edit</Link> &nbsp;
                                            <Link to={`${Config.RouteUrls.Admin.Rewards.View}/${reward.id}`} className="link">View</Link>
                                        </td>
                                    </tr>
                                )}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}