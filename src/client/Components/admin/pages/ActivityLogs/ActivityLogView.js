import React, {Component} from 'react';
import PageHeaderWithLink from "../PageHeaderWithLink";
import {Config} from "../../../../Config";
import AlertBox from "../../../common/AlertBox";
import TokenForm from "../Tokens/TokenForm";
import {Repository} from "../../../../Data/DataStore";
import SiteTitle from "../../../common/SiteTitle";
import LoadingScreen from "../../../common/LoadingScreen";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';
import UserLink from "../Common/UserLink";
import moment from "moment";
import ActivityLogCard from "./ActivityLogCard";

export default class ActivityLogView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            log: {},
            isLoading: true,
            isFound: true,
            alert: {
                show: false,
                status: '',
                message: '',
            }
        };

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });
    }

    componentDidMount() {
        let id = this.props.match.params.id;
        let url = `${getApi(Config.API.Logs.Activity.Single)}/${id}`;

        axios.get(url)
            .then(res => {
                this.setState({
                    log: res.data,
                    isLoading: false,
                    isFound: true
                });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false, isFound: false });
            });
    }

    render() {
        return (
            <div>
                <SiteTitle title = "View Activity Log" />
                <LoadingScreen show={this.state.isLoading} />
                <PageHeaderWithLink title="View Activity Log"  btnText="Back to Activity Logs" btnIcon="fa fa-arrow-left" btnUrl={Config.RouteUrls.Admin.Logs.Activity.Home}/>
                <div className="row">
                    <div className="col-md-6">
                        <AlertBox {...this.state.alert} />
                    </div>
                </div>

                {
                    this.state.isLoading ?
                        <div></div>
                        :
                        <div>
                            {
                                this.state.isFound ?
                                    <ActivityLogCard {...this.state.log} />
                                    :
                                    <div></div>
                            }
                        </div>
                }
            </div>
        );
    }
}

