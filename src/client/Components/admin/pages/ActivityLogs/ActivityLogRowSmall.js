import React from 'react';
import GuidLink from "../Common/GuidLink";
import {Config} from "../../../../Config";
import UserLink from "../Common/UserLink";
import moment from "moment";

export default function ActivityLogRowSmall(props) {
    return (
        <tr className="mb-5">
            <td>
                <GuidLink url={Config.RouteUrls.Admin.Logs.Activity.View} id={props.activity.id} />
            </td>
            <td>{props.activity.type}</td>
            <td>
                {props.activity.user.name}
            </td>
            <td>{moment(props.activity.datetime).format(Config.Formats.DateNumeric)}</td>
        </tr>
    );
}