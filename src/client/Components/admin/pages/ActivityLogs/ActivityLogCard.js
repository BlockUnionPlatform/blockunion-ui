import React from 'react';
import UserLink from "../Common/UserLink";
import moment from "moment";
import {Config} from "../../../../Config";

export default function ActivityLogCard(props) {
    return (
        <form action="#" className="form-horizontal">
            <div className="form-group text-bold row">
                <label className="control-label col-md-2">Entry ID:</label>
                <div className="col-md-6">{props.id}</div>
            </div>

            <div className="form-group text-bold row">
                <label className="control-label col-md-2">Type:</label>
                <div className="col-md-6">{props.type}</div>
            </div>

            <div className="form-group text-bold row">
                <label className="control-label col-md-2">User:</label>
                <div className="col-md-6">
                    <UserLink id={props.user.id} name={`${props.user.firstName} ${props.user.lastName}`} />
                </div>
            </div>

            <div className="form-group text-bold row">
                <label className="control-label col-md-2">Comment(s):</label>
                <div className="col-md-6">{props.comment}</div>
            </div>

            <div className="form-group text-bold row">
                <label className="control-label col-md-2">Date & Time:</label>
                <div className="col-md-6">{moment(props.dateCreatedUtc).format(Config.Formats.DateTime)}</div>
            </div>
        </form>
    );
}