import React, {Component} from 'react';
import PageHeaderWithLink from "../PageHeaderWithLink";
import {Config} from "../../../../Config";
import {Link} from 'react-router-dom';
import GuidLink from "../Common/GuidLink";
import SiteTitle from "../../../common/SiteTitle";
import LoadingScreen from "../../../common/LoadingScreen";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';
import UserLink from "../Common/UserLink";
import moment from "moment";

export default class ActivityLogsHome extends Component {
    constructor(props) {
        super(props);

        this.state = {
            activityLogs: [],
            isLoading: true,
            alert: {
                show: false,
                status: '',
                message: '',
            }
        };

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });
    }

    componentDidMount() {
        let url = getApi(Config.API.Logs.Activity.All);
        console.log(url);
        axios.get(url)
            .then(res => {
                this.setState({ activityLogs: res.data, isLoading: false });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false, isFound: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title = "Activity Logs" />
                <PageHeaderWithLink title="Activity Logs" />
                <div className="form-group">
                    <p>
                        You can view activity logs for all users here.
                    </p>
                </div>

                <div className="row">
                    <div className="col-md-12">
                        <div className="table-responsive">
                            <table className="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Type</th>
                                    <th>User</th>
                                    <th>Comments</th>
                                    <th>Date</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>

                                <tbody>
                                {this.state.activityLogs.map((log) =>
                                    <tr>
                                        <td>
                                            <GuidLink url={Config.RouteUrls.Admin.Logs.Activity.View} id={log.id}/>
                                        </td>
                                        <td>{log.type}</td>
                                        <td>
                                            <UserLink id={log.user.id} name={`${log.user.firstName} ${log.user.lastName}`} />
                                        </td>
                                        <td>{log.comment}</td>
                                        <td>{moment(log.dateCreatedUtc).format(Config.Formats.DateTime)}</td>
                                        <td>
                                            <Link to={`${Config.RouteUrls.Admin.Logs.Activity.View}/${log.id}`} className="link">View</Link>
                                        </td>
                                    </tr>)
                                }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}