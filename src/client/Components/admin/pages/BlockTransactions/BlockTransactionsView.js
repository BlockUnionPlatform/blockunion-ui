import React, {Component} from 'react';
import SiteTitle from "../../../common/SiteTitle";
import PageHeaderWithLink from "../PageHeaderWithLink";
import {Config} from "../../../../Config";
import BlockTransactionsCard from "./BlockTransactionsCard";
import AlertBox from "../../../common/AlertBox";
import LoadingScreen from "../../../common/LoadingScreen";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';

export default class BlockTransactionsView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            transaction: {},
            isFound: true,
            isLoading: true,
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });
    }

    componentDidMount() {
        let id = this.props.match.params.id;
        let url = `${getApi(Config.API.Block.Transactions)}/${id}`;

        axios.get(url)
            .then(res => {
                this.setState({
                    transaction: res.data,
                    isLoading: false
                });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false, isFound: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title="View Block Transaction" />
                <PageHeaderWithLink title="View Block Transaction" btnText="Back to Block Transactions" btnIcon="fa fa-arrow-left" btnUrl={Config.RouteUrls.Admin.BlockToken.Transactions.Home} />

                <div className="row">
                    <div className="col-md-12">
                        <AlertBox {...this.state.alert} />
                        {
                            this.state.isLoading ?
                                <div></div>
                                :
                                <div>
                                    {
                                        !this.state.isFound ?
                                            <div></div>
                                            :
                                            <BlockTransactionsCard transaction={this.state.transaction} isAdmin={true} />
                                    }
                                </div>
                        }
                    </div>
                </div>
            </div>
        );
    }
}