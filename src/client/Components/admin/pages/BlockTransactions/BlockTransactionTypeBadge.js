import React, {Component} from 'react';

export default class BlockTransactionTypeBadge extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let type = '';

        switch(this.props.type) {
            case 'Withdraw':
                type = "warning";
                break;

            case 'Deposit':
                type = "success";
                break;

            case 'Payout':
                type = "primary";
                break;
        }

        return (
            <span className={`badge badge-${type}`}>{this.props.type}</span>
        );
    }
}