import React, {Component} from 'react';
import SiteTitle from "../../../common/SiteTitle";
import PageHeaderWithLink from "../PageHeaderWithLink";
import {Config} from "../../../../Config";
import AlertBox from "../../../common/AlertBox";
import BlockTransactionForm from "./BlockTransactionForm";
import UsersCard from "../Users/UsersCard";
import VerifyBlockTransaction from "./VerifyBlockTransaction";
import ErrorMessage from "../../../common/ErrorMessage";
import LoadingScreen from "../../../common/LoadingScreen";
import {getApi} from "../../../../ConfigStore";
import {Auth} from "../../../common/AuthHelpers";
import axios from 'axios';

export default class BlockTransactionsAdd extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: '',
            amount: 0.00,
            userItem: null,
            user: null,
            notes: '',
            transactionType: null,
            blockAccount: null,
            alert: {
                show: false,
                status: '',
                message: ''
            },
            setLoading: this.setLoading.bind(this)
        };

        this.showAlert = this.showAlert.bind(this);
    }

    setLoading(value) {
        this.setState({ isLoading: value });
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });

        window.scrollTo(0,0);
    }

    onInputChange(event) {
        this.setState({ [event.target.name] : event.target.value });
    }

    onTransactionTypeChange(selectedValue) {
        this.setState({ transactionType: selectedValue });

        if (selectedValue.value !== "Payout")
            this.setState({ user: null, userItem: null });
    }

    onSelectedAccountChange(selectedValue) {
        this.setState({ blockAccount: selectedValue });
    }

    onSelectedUserChange(selectedValue) {
        this.setState({ userItem: selectedValue });

        let url = `${getApi(Config.API.Users.Default)}/${selectedValue.value}`;

        this.setState({ isLoading: true });

        axios.get(url)
            .then(res => {
                this.setState({ user: res.data, isLoading: false });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false, user: null });
            });
    }

    onSubmit(event) {
        event.preventDefault();

        let errors = VerifyBlockTransaction(
            this.state.title,
            this.state.amount,
            this.state.user,
            this.state.transactionType,
            this.state.blockAccount
        );

        if (errors.length > 0) {
            let message = <ErrorMessage errors={errors}/>
            this.showAlert(true, 'error', message);
        }
        else {
            let url = getApi(Config.API.Block.Transactions);
            let payload = {
                title: this.state.title,
                notes: this.state.notes,
                amount: this.state.amount,
                type: this.state.transactionType.value,
                accountId: this.state.blockAccount.value,
                adminId: Auth.getUser().id
            };

            if (this.state.user != null)
                payload["userId"] = this.state.user.id;

            console.log(payload);

            this.setState({ isLoading: true });

            axios.post(url, payload)
                .then(res => {
                    this.showAlert(true, 'success', 'New transaction recorded successfully');
                    this.setState({ isLoading: false });
                })
                .catch(error => {
                    if (error.response)
                        this.showAlert(true, 'error', error.response.data.toString(), true);
                    else if (error.request)
                        this.showAlert(true, 'error', 'Unable to connect to server', true);
                    else
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                    this.setState({ isLoading: false });
                });
        }
    }

    componentDidMount() {
        console.log(this.state.blockAccount);
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title="Add Block Transaction" />
                <PageHeaderWithLink title="Add Block Transaction" btnText="Back to Block Transactions" btnIcon="fa fa-arrow-left" btnUrl={Config.RouteUrls.Admin.BlockToken.Transactions.Home}/>
                <p className="form-group">You can use this form to record a block token transaction.</p>

                <div className="row">
                    <div className="col-md-6">
                        <AlertBox {...this.state.alert} />
                        <BlockTransactionForm
                            {...this.state}
                            onInputChange={this.onInputChange.bind(this)}
                            onTransactionTypeChange={this.onTransactionTypeChange.bind(this)}
                            onSelectedUserChange={this.onSelectedUserChange.bind(this)}
                            onSelectedAccountChange={this.onSelectedAccountChange.bind(this)}
                            onSubmit={this.onSubmit.bind(this)}
                            btnText="Add Block Transaction"
                        />
                    </div>

                    <div className="col-md-6">
                        <div className="form-group">
                            {
                                this.state.user !== null ?
                                    <div>
                                        <h3 className="text-center">Paid To</h3>
                                        <UsersCard user={this.state.user} />
                                    </div>
                                    :
                                    <div></div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}