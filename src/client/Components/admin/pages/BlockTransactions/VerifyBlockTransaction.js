export default function VerifyBlockTransaction(title, amount, user, transactionType, blockAccount) {
    let errors = [];

    if (title.length < 3 || title.length > 100)
        errors.push('Title must be between 3 to 100 characters.');

    if (amount == 0)
        errors.push('Please enter an amount greater than zero')

    if (transactionType == null || transactionType == undefined)
        errors.push('Please select a transaction type.');

    if (blockAccount == null || blockAccount == undefined)
        errors.push('Please select a Block Account');

    if (transactionType != null) {
        if (transactionType.value.toLowerCase() === 'payout' && (user == null || user == undefined))
            errors.push('Please select a user to which the payment is made to.');
    }

    return errors;
}