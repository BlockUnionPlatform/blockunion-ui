import React from 'react';
import {Config} from "../../../../Config";
import GuidLink from "../Common/GuidLink";
import BlockTransactionTypeBadge from "./BlockTransactionTypeBadge";
import moment from "moment";

export default function BlockTransactionRowSmall(props) {
    return (
        <tr>
            <td>
                <GuidLink url={Config.RouteUrls.Admin.BlockToken.Transactions.View} id={props.transaction.id} />
            </td>

            <td>
                {props.transaction.amount.toLocaleString()} {props.transaction.account.token.symbol}
            </td>

            <td>
                <BlockTransactionTypeBadge type={props.transaction.type} />
            </td>

            <td>
                {moment(Date.parse(props.transaction.dateCreatedUtc)).format(Config.Formats.DateNumeric)}
            </td>
        </tr>
    );
}