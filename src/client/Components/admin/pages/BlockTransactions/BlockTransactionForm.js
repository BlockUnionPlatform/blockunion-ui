import React, {Component} from 'react';
import TransactionTypeDropdown from "../Common/TransactionTypeDropdown";
import UserDropDown from "../Common/UserDropDown";
import BlockAccountDropdown from "../Common/BlockAccountDropdown";

export default class BlockTransactionForm extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <form action="#" onSubmit={this.props.onSubmit}>
                <div className="form-group">
                    <label className="control-label text-semibold">Amount</label>
                    <div className="row">
                        <div className="col-md-6">
                            <input type="number" name="amount" id="amount" className="form-control" value={this.props.amount} onChange={this.props.onInputChange} />
                        </div>
                        <div className="col-md-6">

                        </div>
                    </div>
                </div>

                <div className="form-group">
                    <label className="control-label text-semibold">Title</label>
                    <input type="text" name="title" id="title" className="form-control" value={this.props.title} onChange={this.props.onInputChange} />
                </div>

                <div className="form-group">
                    <label className="control-label text-semibold">Transaction Type</label>
                    <TransactionTypeDropdown onTransactionTypeChange={this.props.onTransactionTypeChange} value={this.props.transactionType}/>
                </div>

                {
                    this.props.transactionType !== null ?
                        <div className={this.props.transactionType.value.toLowerCase() === 'payout' ? 'd-block' : 'd-none'}>
                            <div className="form-group">
                                <label className="control-label text-semibold">Paid To</label>
                                <UserDropDown onSelectedUserChange={this.props.onSelectedUserChange} user={this.props.userItem} setLoading={this.props.setLoading}/>
                            </div>
                        </div>
                        :
                        <div></div>
                }

                <div className="form-group">
                    <label className="control-label text-semibold">Block Account</label>
                    <BlockAccountDropdown onSelectedAccountChange={this.props.onSelectedAccountChange} account={this.props.blockAccount} />
                </div>

                <div className="form-group">
                    <label className="control-label text-semibold">Notes</label>
                    <textarea name="notes" id="notes" rows="7" className="form-control" value={this.props.notes} onChange={this.props.onInputChange}></textarea>
                </div>

                <div className="form-group pull-right">
                    <button className="btn btn-theme" type="submit">{this.props.btnText}</button>
                </div>
            </form>
        );
    }
}