import React, {Component} from 'react';
import GuidLink from "../Common/GuidLink";
import {Config} from "../../../../Config";
import UserLink from "../Common/UserLink";
import BlockTransactionTypeBadge from "./BlockTransactionTypeBadge";
import {Link} from "react-router-dom";
import moment from "moment";

export default class BlockTransactionRow extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let isAdmin = this.props.isAdmin !== undefined && this.props.isAdmin;
        let url = isAdmin ? Config.RouteUrls.Admin.BlockToken.Transactions.View : Config.RouteUrls.Member.Blocks.Payouts;

        return (
            <tr key={this.props.transaction.id}>
                <td>
                    <GuidLink url={url} id={this.props.transaction.id} />
                </td>

                <td>
                    {this.props.transaction.title}
                </td>

                <td>
                    {this.props.transaction.amount.toLocaleString()} {this.props.transaction.account.token.symbol}
                </td>

                {
                    isAdmin &&
                    <td>
                        <UserLink user={this.props.transaction.admin}/>
                    </td>
                }

                <td>
                    {
                        this.props.transaction.user !== null  ?
                            <span>
                                {
                                    isAdmin ?
                                        <UserLink user={this.props.transaction.user} />
                                        :
                                        <span>
                                            {this.props.transaction.user.name}
                                        </span>
                                }
                            </span>
                            :
                            <span>-</span>
                    }
                </td>

                {
                    isAdmin &&
                    <td>
                        <BlockTransactionTypeBadge type={this.props.transaction.type} />
                    </td>
                }

                <td>
                    {moment(this.props.transaction.dateCreatedUtc).format(Config.Formats.DateTime)}
                </td>

                <td>
                    <Link className="link" to={`${url}/${this.props.transaction.id}`}>
                        View
                    </Link>
                </td>
            </tr>
        );
    }
}