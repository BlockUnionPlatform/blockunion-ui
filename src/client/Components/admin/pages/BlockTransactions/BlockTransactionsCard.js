import React, {Component} from 'react';
import UserLink from "../Common/UserLink";
import BlockTransactionTypeBadge from "./BlockTransactionTypeBadge";
import BlockAccountLink from "../Common/BlockAccountLink";
import moment from "moment";
import {Config} from "../../../../Config";

export default class BlockTransactionsCard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let isAdmin = this.props.isAdmin !== undefined && this.props.isAdmin;

        return (
            <form>
                <div className="form-group row">
                    <label className="control-label text-semibold col-md-2">ID</label>
                    <div className="col-md-10">
                        {this.props.transaction.id}
                    </div>
                </div>

                <div className="form-group row">
                    <label className="control-label text-semibold col-md-2">Title</label>
                    <div className="col-md-10">
                        {this.props.transaction.title}
                    </div>
                </div>

                <div className="form-group row">
                    <label className="control-label text-semibold col-md-2">Amount</label>
                    <div className="col-md-10">
                        {this.props.transaction.amount.toLocaleString()} {this.props.transaction.account.token.symbol}
                    </div>
                </div>

                {
                    isAdmin &&
                    <div>
                        <div className="form-group row">
                            <label className="control-label text-semibold col-md-2">Type</label>
                            <div className="col-md-10">
                                <BlockTransactionTypeBadge type={this.props.transaction.type} />
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="control-label text-semibold col-md-2">Block Account</label>
                            <div className="col-md-10">
                                <BlockAccountLink account={this.props.transaction.account} />
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="control-label text-semibold col-md-2">Processed By</label>
                            <div className="col-md-10">
                                <UserLink user={this.props.transaction.admin} />
                            </div>
                        </div>
                    </div>
                }

                {
                    this.props.transaction.user != null ?
                        <div className="form-group row">
                            <label className="control-label text-semibold col-md-2">Paid To</label>
                            <div className="col-md-10">
                                {
                                    isAdmin ?
                                        <UserLink user={this.props.transaction.user} />
                                        :
                                        <span>
                                            {this.props.transaction.user.name}
                                        </span>
                                }
                            </div>
                        </div>
                        :
                        <div></div>
                }

                <div className="form-group row">
                    <label className="control-label text-semibold col-md-2">Date</label>
                    <div className="col-md-10">
                        {moment(this.props.transaction.dateCreatedUtc).format(Config.Formats.DateTime)}
                    </div>
                </div>

                {
                    isAdmin &&
                    <div className="form-group row">
                        <label className="control-label text-semibold col-md-2">Notes</label>
                        <div className="col-md-10">
                            {this.props.transaction.notes}
                        </div>
                    </div>
                }
            </form>
        );
    }

}