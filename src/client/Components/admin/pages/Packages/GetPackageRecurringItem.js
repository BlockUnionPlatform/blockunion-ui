import BlockPackageRecurring from "../Common/PackageRenewals";

function GetPackageRecurringItem(id) {
    return BlockPackageRecurring.find(p => p.label === id);
}

export {GetPackageRecurringItem};