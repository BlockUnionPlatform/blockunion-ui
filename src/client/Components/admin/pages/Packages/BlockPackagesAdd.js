import React, {Component} from 'react';
import SiteTitle from "../../../common/SiteTitle";
import PageHeaderWithLink from "../PageHeaderWithLink";
import {Config} from "../../../../Config";
import AlertBox from "../../../common/AlertBox";
import BlockPackagesForm from "./BlockPackagesForm";
import PackageBadge from "../Common/PackageBadge";
import VerifyPackage from "./VerifyPackage";
import ErrorMessage from "../../../common/ErrorMessage";
import PackageCard from "./PackageCard";
import LoadingScreen from "../../../common/LoadingScreen";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';
import BlockPackageRecurring from "../Common/PackageRenewals";

export default class BlockPackagesHome extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: 'sample',
            shortDescription: '',
            token: '',
            value: 1,
            recurring: 1,
            color: '',
            isActive: true,
            isLoading: false,
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.onSelectChange = this.onSelectChange.bind(this);
        this.onRecurringChange = this.onRecurringChange.bind(this);
        this.onActiveChange = this.onActiveChange.bind(this);
        this.onColorChange = this.onColorChange.bind(this);
        this.onEditorChange = this.onEditorChange.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });

        window.scrollTo(0,0);
    }

    onSelectChange(selectedValue) {
        this.setState({ token: selectedValue });
    }

    onEditorChange(newState) {
        this.setState({ description: newState.blocks[0].text });
    }

    onRecurringChange(selectedValue) {
        this.setState({ recurring: selectedValue });
    }

    onInputChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    componentDidMount() {
        this.setState({
            recurring: BlockPackageRecurring[0],
            color: '#000000'
        });
    }

    onFormSubmit(event) {
        event.preventDefault();

        let errors = VerifyPackage(this.state);

        if (errors.length > 0) {
            let errorMessage = <ErrorMessage errors={errors} />
            this.showAlert(true, 'error', errorMessage);
        }
        else {
            let url = getApi(Config.API.Block.Packages);
            let payload = {
                title: this.state.title,
                description: this.state.shortDescription,
                value: this.state.value,
                tokenId: this.state.token.value,
                color: this.state.color,
                isActive: this.state.isActive,
                recurring: this.state.recurring.value
            };

            this.setState({ isLoading: true });

            axios.post(url, payload)
                .then(res => {
                    this.showAlert(true, 'success', 'Package created successfully!');
                    this.setState({ isLoading: false });
                })
                .catch(error => {
                    if (error.response)
                        this.showAlert(true, 'error', error.response.data.toString(), true);
                    else if (error.request)
                        this.showAlert(true, 'error', 'Unable to connect to server', true);
                    else
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                    this.setState({ isLoading: false });
                });
        }
    }

    onActiveChange(event) {
        this.setState({ isActive: event.target.checked });
    }

    onColorChange(color) {
        this.setState({ color: color.hex });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title="Add Package" />
                <PageHeaderWithLink title="Add Package" btnText="Back to Packages" btnIcon="fa fa-arrow-left" btnUrl={Config.RouteUrls.Admin.Packages.Home} />

                <div className="row">
                    <div className="col-md-6">
                        <AlertBox {...this.state.alert} />
                        {
                            this.state.isLoading ?
                                <div></div>
                                :
                                <BlockPackagesForm
                                    {...this.state}
                                    onInputChange={this.onInputChange}
                                    onSelectChange={this.onSelectChange}
                                    onRecurringChange={this.onRecurringChange}
                                    onActiveChange={this.onActiveChange}
                                    onColorChange={this.onColorChange}
                                    onEditorChange={this.onEditorChange}
                                    onSubmit={this.onFormSubmit}
                                    btnText="Add Package"
                                />
                        }
                    </div>

                    <div className="col-md-6">
                        <h3>Preview</h3>
                        <div className="plan-wrapper">
                            <div className="text-center mb-20">
                                <PackageBadge color={this.state.color} name={this.state.title} />
                            </div>
                            <PackageCard
                                title={this.state.title}
                                price={`${this.state.value} ${this.state.token.label} / ${this.state.recurring.label}`}
                                description={this.state.shortDescription} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}