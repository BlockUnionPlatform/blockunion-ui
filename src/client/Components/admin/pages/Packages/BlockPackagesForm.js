import React, {Component} from 'react';
import {Repository} from "../../../../Data/DataStore";
import Select from "react-select";
import {ChromePicker} from 'react-color';
import {Editor} from 'react-draft-wysiwyg';
import TokenDropdown from "../Common/TokenDropdown";
import PackageRecurringDropdown from "../Common/PackageRecurringDropdown";

export default class BlockPackagesForm extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <form action="#" onSubmit={this.props.onSubmit}>
                <div className="form-group">
                    <label className="control-label text-semibold">Title</label>
                    <input type="text" name="title" id="title" className="form-control" value={this.props.title} onChange={this.props.onInputChange} />
                </div>

                <div className="form-group">
                    <label className="control-label text-semibold">Token</label>
                    <TokenDropdown onChange={this.props.onSelectChange} value={this.props.token} />
                </div>

                <div className="form-group">
                    <label className="control-label text-semibold">Recurring/Renews</label>
                    <PackageRecurringDropdown onChange={this.props.onRecurringChange} value={this.props.recurring} />
                </div>

                <div className="form-group">
                    <label className="control-label text-semibold">Value</label>
                    <div className="row">
                        <div className="col-md-6">
                            <input type="number" name="value" id="value" className="form-control" value={this.props.value} onChange={this.props.onInputChange} />
                        </div>
                        <div className="col-md-6">
                            {
                                this.props.token !== undefined ?
                                    <div>
                                        {this.props.value} {this.props.token.label}
                                    </div>
                                    :
                                    <div className="small">
                                        Select a token to preview.
                                    </div>
                            }
                        </div>
                    </div>
                </div>

                <div className="form-group">
                    <label className="control-label text-semibold">Is Active?</label>
                    <input type="checkbox" name="isActive" id="isActive" className="form-control" checked={this.props.isActive} onChange={this.props.onActiveChange} className="position-right" />
                </div>

                <div className="form-group">
                    <label className="control-label text-semibold">Color</label>
                    <ChromePicker color={ this.props.color } onChangeComplete={ this.props.onColorChange } />
                </div>

                <div className="form-group">
                    <label className="control-label text-semibold">Description</label>
                    <textarea name="shortDescription" id="shortDescription" rows="10" className="form-control" onChange={this.props.onInputChange} value={this.props.shortDescription}></textarea>
                </div>

                <div className="form-group">
                    <button className="btn btn-theme" type="submit">{this.props.btnText}</button>
                </div>
            </form>
        );
    }
}