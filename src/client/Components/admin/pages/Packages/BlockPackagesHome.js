import React, {Component} from 'react';
import GuidLink from "../Common/GuidLink";
import {Config} from "../../../../Config";
import SiteTitle from "../../../common/SiteTitle";
import PageHeaderWithLink from "../PageHeaderWithLink";
import TokenLink from "../Common/TokenLink";
import PackageBadge from "../Common/PackageBadge";
import StatusBadge from "../../../common/StatusBadge";
import {Link} from "react-router-dom";
import LoadingScreen from "../../../common/LoadingScreen";
import AlertBox from "../../../common/AlertBox";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';

export default class BlockPackagesHome extends Component {
    constructor(props) {
        super(props);

        this.state = {
            packages: [],
            isLoading: true,
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });
    }

    componentDidMount() {
        let url = getApi(Config.API.Block.Packages);

        axios.get(url)
            .then(res => {
                this.setState({
                    packages: res.data,
                    isLoading: false
                });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title="Packages" />
                <PageHeaderWithLink title="View Packages" btnIcon="fa fa-plus" btnText="Add Package" btnUrl={Config.RouteUrls.Admin.Packages.Add}/>
                <AlertBox {...this.state.alert} />
                {
                    this.state.isLoading ?
                        <div></div>
                        :
                        <div className="row">
                            <div className="col-md-12">
                                <div className="table-responsive">
                                    <table className="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Title</th>
                                                <th>Value</th>
                                                <th>Recurring</th>
                                                <th>Badge</th>
                                                <th>Is Active</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            {
                                                this.state.packages.map((p) =>
                                                    <tr>
                                                        <td>
                                                            <GuidLink url={Config.RouteUrls.Admin.Packages.Edit} id={p.id} />
                                                        </td>
                                                        <td>{p.title}</td>
                                                        <td>
                                                            {p.value} {p.token.symbol}
                                                        </td>
                                                        <td>{p.recurring}</td>
                                                        <td>
                                                            <PackageBadge color={p.color} name={p.title} />
                                                        </td>
                                                        <td>
                                                            <StatusBadge isActive={p.isActive}/>
                                                        </td>
                                                        <td>
                                                            <Link to={`${Config.RouteUrls.Admin.Packages.Edit}/${p.id}`} className="link">View / Edit</Link>
                                                        </td>
                                                    </tr>
                                                )
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                }
            </div>
        );
    }
}