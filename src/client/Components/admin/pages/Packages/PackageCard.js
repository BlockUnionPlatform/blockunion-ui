import React, {Component} from 'react';

export default class PackageCard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="plan">
                <div className="plan-title">
                    {this.props.title}
                </div>
                <div className="plan-price">
                    {this.props.price}
                </div>
                <div className="plan-description">
                    {this.props.description}
                </div>
                <div className="plan-order">
                    <button className="btn btn-theme">Order</button>
                </div>
            </div>
        );
    }
}