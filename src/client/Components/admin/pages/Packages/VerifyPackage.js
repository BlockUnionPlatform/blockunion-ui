export default function VerifyPackage(props) {
    let errors = [];

    if (props.title.length < 3)
        errors.push('Title should be at least 3 characters long.');

    if (props.token === '' || props.token === null || props.token === undefined)
        errors.push('Please select a token');

    if (props.recurring === '' || props.recurring === null || props.recurring === undefined)
        errors.push('Please select recurring');

    if (props.value === '' || props.value < 1 || props.value === undefined)
        errors.push('Please enter a valid value');

    if (props.shortDescription == '')
        errors.push('Please specify a description.');

    return errors;
};