import React, {Component} from 'react';
import PageHeaderWithLink from "../PageHeaderWithLink";
import {Config} from "../../../../Config";
import DailyActionRow from "./DailyActionRow";
import SiteTitle from "../../../common/SiteTitle";
import LoadingScreen from "../../../common/LoadingScreen";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';

export default class DailyActionsHome extends Component {
    constructor(props) {
        super(props);

        this.state = {
            items: [],
            isLoading: true,
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });
    }

    componentDidMount() {
        let url = getApi(Config.API.DailyActions);

        axios.get(url)
            .then(res => {
                this.setState({
                    items: res.data,
                    isLoading: false
                });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title="Daily Actions" />
                <PageHeaderWithLink title="Daily Actions" btnText="Add Daily Action" btnIcon="fa fa-plus" btnUrl={Config.RouteUrls.Admin.DailyActions.Add} />
                <p className="form-group">
                    A list of all the daily actions.
                </p>
                {
                    this.isLoading ?
                        <div>Loading</div>
                        :
                        <div>
                            <div className="table-responsive">
                                <table className="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Title</th>
                                            <th>Reward</th>
                                            <th>Valid Till</th>
                                            <th>Status</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        { this.state.items.map((item) => <DailyActionRow item={item} key={item.id} />) }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                }
            </div>
        );
    }
}