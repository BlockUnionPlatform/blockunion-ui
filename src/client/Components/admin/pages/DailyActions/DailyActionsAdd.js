import React, {Component} from 'react';
import SiteTitle from "../../../common/SiteTitle";
import PageHeaderWithLink from "../PageHeaderWithLink";
import {Config} from "../../../../Config";
import DailyActionForm from "./DailyActionForm";
import DailyActionItem from "../../../member/DailyActionItem";
import AlertBox from "../../../common/AlertBox";
import VerifyDailyActionForm from "./VerifyDailyActionForm";
import ErrorMessage from "../../../common/ErrorMessage";
import LoadingScreen from "../../../common/LoadingScreen";
import { EditorState, convertFromRaw } from 'draft-js';
import {stateToHTML} from 'draft-js-export-html';
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';
import ComposeErrorMessageFromJson from "../Common/ComposeErrorMessageFromJson";

export default class DailyActionsAdd extends Component {
    constructor(props) {
        super(props);

        this.state = {
            shortTitle: 'Enter short title',
            longTitle: 'Enter long title',
            logo: '',
            logoImage: null,
            reward: null,
            validTill: '',
            description: EditorState.createEmpty(),
            isLoading: false,
            alert: {
                show: false,
                status: '',
                message: ''
            },
            html: ''
        };

        this.showAlert = this.showAlert.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onSelectChange = this.onSelectChange.bind(this);
        this.onDateChange = this.onDateChange.bind(this);
        this.onImageChange = this.onImageChange.bind(this);
        this.onEditorChange = this.onEditorChange.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });
        window.scrollTo(0,0);
    }

    onInputChange(event) {
        this.setState({ [event.target.name] : event.target.value });
    }

    onEditorChange(newState) {
        this.setState({ description: newState });

        let html = stateToHTML(newState.getCurrentContent());
        this.setState({ html });
    }

    onSelectChange(selectedValue) {
        this.setState({ reward: selectedValue });
    }

    onDateChange(target, selectedDate) {
        this.setState({ [target]: selectedDate.format(Config.Formats.DateTime) });
    }

    onImageChange(event) {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (e) => {
                this.setState({ logo: e.target.result });
            };
            reader.readAsDataURL(event.target.files[0]);
        }
    }

    onSubmit(event) {
        event.preventDefault();

        let errors = VerifyDailyActionForm({...this.state});
        console.log(this.state);
        if (errors.length > 0) {
            let message = <ErrorMessage errors={errors}/>
            this.showAlert(true, 'error', message);
        }
        else {
            let url = getApi(Config.API.DailyActions);
            let payload = {
                shortTitle: this.state.shortTitle,
                longTitle: this.state.longTitle,
                description: this.state.html,
                logo: this.state.logo,
                validTillUtc: this.state.validTill,
                rewardId: this.state.reward.value
            };

            this.setState({ isLoading: true });

            axios.post(url, payload)
                .then(res => {
                    this.showAlert(true, 'success', 'Daily Action created successfully.');
                    this.setState({ isLoading: false });
                })
                .catch(error => {
                    if (error.response) {
                        if (error.response.status == 422)
                            this.showAlert(true, 'error', ComposeErrorMessageFromJson(error.response.data), true);
                        else
                            this.showAlert(true, 'error', JSON.stringify(error.response.data), true);
                    }
                    else if (error.request)
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);
                    else
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                    this.setState({ isLoading: false });
                });
        }
    }

    render() {
        let rewardLabel = this.state.reward != null ? this.state.reward.label : '0 BKU';
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title="Add Daily Action" />
                <PageHeaderWithLink title="Add Daily Action" btnText="Back to Daily Actions" btnIcon="fa fa-arrow-left" btnUrl={Config.RouteUrls.Admin.DailyActions.Home} />
                <p className="form-group">
                    You can use this form to create Daily Actions.
                </p>

                <div className="row">
                    <div className="col-md-6">
                        <AlertBox {...this.state.alert} />
                        <DailyActionForm
                            {...this.state}
                            onInputChange={this.onInputChange}
                            onSelectChange={this.onSelectChange}
                            onDateChange={this.onDateChange}
                            onSubmit={this.onSubmit}
                            onLogoChange={this.onImageChange}
                            btnText = "Add Daily Action"
                            onEditorChange={this.onEditorChange} />
                    </div>

                    <div className="col-md-6">
                        <h3 className="text-center">Preview</h3>
                        <div className="p-20">
                            <DailyActionItem
                                title={this.state.shortTitle}
                                reward={rewardLabel}
                                image={this.state.logo}
                                price={rewardLabel}
                                containerClass="daily-action"
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}