import React, {Component} from 'react';
import DatePicker from "../Common/DatePicker";
import {Editor} from 'react-draft-wysiwyg';
import RewardsDropdown from "../Common/RewardsDropdown";

export default class DailyActionForm extends Component {
    constructor(props) {
        super(props);
    }
    
    render() {
        return (
            <form action="#" method="POST" onSubmit={this.props.onSubmit}>
                <div className="form-group">
                    <label className="control-label text-semibold">Short Title</label>
                    <input type="text" name="shortTitle" id="shortTitle" className="form-control" value={this.props.shortTitle} onChange={this.props.onInputChange} />
                </div>

                <div className="form-group">
                    <label  className="control-label text-semibold">Long Title</label>
                    <input type="text" name="longTitle" id="longTitle" className="form-control" value={this.props.longTitle} onChange={this.props.onInputChange} />
                </div>

                <div className="form-group">
                    <label  className="control-label text-semibold">Logo</label>
                    <input type="file" name="logo" id="logo" className="form-control" onChange={this.props.onLogoChange} />
                </div>

                <div className="form-group">
                    <label className="control-label text-semibold">Reward</label>
                    <RewardsDropdown onChange={this.props.onSelectChange} value={this.props.reward} />
                </div>

                <div className="form-group">
                    <label className="control-label text-semibold">Valid Till</label>
                    <DatePicker name="validTill" value={this.props.validTill} onChange={this.props.onDateChange} />
                </div>

                <div className="form-group">
                    <label className="control-label text-semibold">Description</label>
                    <Editor editorClassName="main-editor" name="description" id="description" editorState={this.props.description} onEditorStateChange={this.props.onEditorChange} />
                </div>

                <div className="form-group">
                    <div className="pull-right">
                        <button className="btn btn-theme" type="submit">{this.props.btnText}</button>
                    </div>
                </div>
            </form>
        );
    }
}