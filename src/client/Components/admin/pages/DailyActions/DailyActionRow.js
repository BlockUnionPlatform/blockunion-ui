import React, {Component} from 'react';
import RewardLink from "../Common/RewardLink";
import StatusBadge from "../../../common/StatusBadge";
import Link from "react-router-dom/es/Link";
import {Config} from "../../../../Config";
import GuidLink from "../Common/GuidLink";
import moment from "moment";

export default function DailyActionRow(props) {
    return (
        <tr key={props.item.id}>
            <td>
                <GuidLink id={props.item.id} url={Config.RouteUrls.Admin.DailyActions.Edit}/>
            </td>
            <td>{props.item.shortTitle}</td>
            <td>
                <RewardLink reward={props.item.reward}/>
            </td>
            <td>{moment(props.item.validTillUtc).format(Config.Formats.DateTime)}</td>
            <td>
                <StatusBadge isActive={props.item.isActive} text="Expired"/>
            </td>
            <td>
                <Link className="link" to={`${Config.RouteUrls.Admin.DailyActions.Edit}/${props.item.id}`}>
                    Edit
                </Link>
            </td>
        </tr>
    );
}