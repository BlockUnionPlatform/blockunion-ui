export default function VerifyDailyActionForm(form) {
    let errors = [];

    if (form.shortTitle.length < 5) {
        errors.push('Short title should be at least 5 characters long.');
    }

    if (form.longTitle.length < 5) {
        errors.push('Long title should be at least 5 characters long.');
    }

    if (form.reward === null || form.reward === undefined) {
        errors.push('Please select a reward');
    }

    if (form.validTill.length < 1) {
        errors.push('Please select a valid till date & time.');
    }

    if (form.description.length < 5) {
        errors.push('Description should be at least 5 characters long.');
    }

    if (!form.isEditForm) {
        if (form.logo === null || form.logo === '') {
            errors.push('Please select a logo');
        }
    }

    return errors;
}