import React from 'react';
import DatePicker from "../Common/DatePicker";

export default function BlockIcoForm(props) {
    return (
        <div>
            <div className="alert alert-warning">
                Please enter the date &amp; time as UTC +0:00
            </div>
            <div className="form-group">
                <label className="control-label text-semibold">Private Start Date</label>
                <DatePicker name="privateStartDate" value={props.privateStartDate} onChange={props.onDateChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Private End Date</label>
                <DatePicker name="privateEndDate" value={props.privateEndDate} onChange={props.onDateChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Pre Sale Ico Start</label>
                <DatePicker name="preSaleIcoStart" value={props.preSaleIcoStart} onChange={props.onDateChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Pre Sale Ico End</label>
                <DatePicker name="preSaleIcoEnd" value={props.preSaleIcoEnd} onChange={props.onDateChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Public Ico Start</label>
                <DatePicker name="publicIcoStart" value={props.publicIcoStart} onChange={props.onDateChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Public Ico End</label>
                <DatePicker name="publicIcoEnd" value={props.publicIcoEnd} onChange={props.onDateChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Platform Token Type</label>
                <input type="text" name="platformTokenType" id="platformTokenType" className="form-control" value={props.platformTokenType} onChange={props.onInputChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Price Per Token</label>
                <input type="text" name="pricePerToken" id="pricePerToken" className="form-control" value={props.pricePerToken} onChange={props.onInputChange} />
            </div>
        </div>
    );
}