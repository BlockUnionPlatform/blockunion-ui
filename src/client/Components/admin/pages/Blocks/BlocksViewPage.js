import React, {Component} from 'react';
import SiteTitle from "../../../common/SiteTitle";
import LoadingScreen from "../../../common/LoadingScreen";
import PageHeaderWithLink from "../PageHeaderWithLink";
import AlertBox from "../../../common/AlertBox";
import {getApi} from "../../../../ConfigStore";
import {Config} from '../../../../Config';
import axios from 'axios';
import BlockViewCard from "./BlockViewCard";
import BlockServiceItem from "../../../member/BlockServiceItem";
import IcoItem from "../../../member/IcoItem";
import BlockCardItem from "./BlockCardItem";

export default class BlocksViewPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            block: {},
            isLoading: true,
            isFound: true,
            alert: {
                show: false,
                message: '',
                status: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status, message) {
        this.setState({ show, status, message });
    }

    componentDidMount() {
        let url = `${getApi(Config.API.Block.Listings)}/${this.props.match.params.id}?adminAccess=true`;
        axios.get(url)
            .then(res => {
                this.setState({
                    block: res.data,
                    isLoading: false,
                    isFound: true
                });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false, isFound: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title = "View Block" />
                <PageHeaderWithLink title = "View Block" btnIcon = "fa fa-arrow-left" btnUrl={Config.RouteUrls.Admin.Blocks.Home} btnText = "Back to Blocks" />

                <AlertBox {...this.state.alert} />

                {
                    this.state.isLoading ?
                        <div></div>
                        :
                        <div>
                            {
                                !this.state.isFound ?
                                    <div></div>
                                    :
                                    <div className="row">
                                        <div className="col-md-7">
                                            <BlockViewCard block={this.state.block} />
                                        </div>
                                        <div className="col-md-5">
                                            <BlockCardItem url = "/block" block={this.state.block} />
                                        </div>
                                    </div>
                            }
                        </div>
                }
            </div>
        );
    }
}