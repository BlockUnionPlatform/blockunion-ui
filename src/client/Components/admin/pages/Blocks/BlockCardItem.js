import React from 'react';
import {Link} from "react-router-dom";
import moment from "moment";
import {Config} from "../../../../Config";

export default function BlockCardItem(props) {
    let block =
        <Link to={props.url}>
            <div style={{ minHeight: "100px" }}>
                <div className="left-side">
                    <div className="img-trending">
                        <img src={props.block.logo} alt={props.block.businessName}/>
                    </div>

                    {
                        props.block.category.toLowerCase() == "icorankbusiness" ?
                            <span></span>
                            :
                            <div>
                                {
                                    props.block.social != null &&
                                    <div className="social-icon">
                                        <a target="_blank" href={props.block.social.telegram}><i className="fa fa-paper-plane"></i></a>
                                        <a target="_blank" href={props.block.social.gmail}><i className="fa fa-envelope-open-o"></i></a>
                                        <a target="_blank" href={props.block.social.facebook}><i className="fa fa-facebook"></i></a>
                                        <a target="_blank" href={props.block.social.twitter}><i className="fa fa-twitter"></i></a>
                                    </div>
                                }
                            </div>
                    }
                </div>

                {
                    props.block.category.toLowerCase() == "icorankbusiness" ?
                        <div className = "small" style={{ position: 'absolute', bottom: '12px' }}>
                            Ends on {moment(props.block.ico.publicIcoEnd).format("MMM DD, YYYY")}
                        </div>
                        :
                        <span></span>
                }

                <div className="right-side">
                    <h4>{props.block.businessName}</h4>
                    <p>Value: {props.block.value}<span className="price-icon"></span></p>
                </div>

                <div className="block-label" style={{ background: props.block.package != null ? props.block.package.color : 'blue' }}>{props.block.package != null ? props.block.package.title : 'Default title'}</div>
            </div>
        </Link>;

    if (props.isListItem !== undefined && props.isListItem == true) {
        return (
            <li>{block}</li>
        );
    }
    else return (
        <div className="block-container">
            {block}
        </div>
    );
}