import React, {Component} from 'react';
import SiteTitle from "../../../common/SiteTitle";
import PageHeaderWithLink from "../PageHeaderWithLink";
import {Config} from "../../../../Config";
import {Editor} from 'react-draft-wysiwyg';
import BlockBasicInformationForm from "./BlockBasicInformationForm";
import BlockPackageDropdown from "../Common/BlockPackageDropdown";
import BlocksSocialForm from "./BlocksSocialForm";
import BlockIcoForm from "./BlockIcoForm";
import AlertBox from "../../../common/AlertBox";
import VerifyBlock from "./VerifyBlock";
import ErrorMessage from "../../../common/ErrorMessage";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';
import LoadingScreen from "../../../common/LoadingScreen";
import ComposeErrorMessageFromJson from "../Common/ComposeErrorMessageFromJson";
import {ContentState, convertFromHTML, convertFromRaw, EditorState} from "draft-js";
import {stateToHTML} from "draft-js-export-html";
import GetPackageListItem from "../Common/GetPackageListItem";
import {BlockServiceCategories} from "../Common/BlockServiceCateogires";
import {Auth} from "../../../common/AuthHelpers";
import moment from "moment";

export default class BlocksAdminAdd extends Component {
    constructor(props) {
        super(props);

        this.state = {
            businessName: '',
            websiteUrl: '',
            emailAddress: '',
            authorName: '',
            isAuthorInvolved: true,
            category: '',
            hasToken: false,
            tokenSymbol: '',
            legalName: '',
            totalValue: '',
            tokensPerClaim: '',
            isActive: true,
            legalAddress: '',
            originCountry: '',
            whitePaper: '',
            logo: '',
            tokenLogo: '',
            package: '',
            facebook: '',
            twitter: '',
            medium: '',
            telegram: '',
            editorState: EditorState.createEmpty(),
            bitcoinTalkForum: '',
            instagram: '',
            privateStartDate: '',
            privateEndDate: '',
            preSaleIcoStart: '',
            preSaleIcoEnd: '',
            publicIcoStart: '',
            publicIcoEnd: '',
            platformTokenType: '',
            pricePerToken: '',
            aboutBusiness: '',
            createdById: '',
            alert: {
                isVisible: false,
                status: '',
                message: ''
            },
            isLoading: true,
            isFound: true
        };

        this.onInputChange = this.onInputChange.bind(this);
        this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
        this.onCategoryChange = this.onCategoryChange.bind(this);
        this.onEditorChange = this.onEditorChange.bind(this);
        this.convertToBase64 = this.convertToBase64.bind(this);
        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { isVisible: show, status, message}
        });

        window.scrollTo(0,0);
    }

    onInputChange(event) {
        this.setState({
            [event.target.name] : event.target.value
        });
    }

    onCategoryChange(selectedValue) {
        this.setState({
            category: selectedValue
        });
    }

    onEditorChange(newState) {
        this.setState({ editorState: newState });
        this.setState({ aboutBusiness: stateToHTML(newState.getCurrentContent()) });
    }

    handleCheckboxChange(event) {
        this.setState({
            [event.target.name]: event.target.checked
        });
    }

    convertToBase64(name, event) {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (e) => {
                this.setState({ [name]: e.target.result });
            };
            reader.readAsDataURL(event.target.files[0]);
        }
    }

    onLogoChange(event) {
        this.convertToBase64('logo', event);
    }

    onTokenLogoChange(event) {
        this.convertToBase64('tokenLogo', event);
    }

    onPackageChange(selectedValue) {
        this.setState({ package: selectedValue });
    }

    onDateChange(name, selectedDate) {
        if (typeof selectedDate.format !== 'undefined') {
            this.setState({ [name] : selectedDate.format(Config.Formats.DateTime) });
        }
    }

    onSubmit(event) {
        event.preventDefault();
        let block = {...this.state};
        let errors = VerifyBlock(block);

        if (errors.length > 0) {
            let message = <ErrorMessage errors={errors} />
            this.showAlert(true, 'error', message);
            window.scrollTo(0, 0);
        }
        else {
            let url = `${getApi(Config.API.Block.Listings)}/${this.props.match.params.id}`;
            let payload = {
                businessName: this.state.businessName,
                websiteUrl: this.state.websiteUrl,
                emailAddress: this.state.emailAddress,
                authorName: this.state.authorName,
                isAuthorInvolved: this.state.isAuthorInvolved,
                serviceCategory: this.state.category.value,
                hasToken: this.state.hasToken,
                tokenSymbol: this.state.tokenSymbol,
                aboutBusiness: this.state.aboutBusiness,
                legalName: this.state.legalName,
                legalAddress: this.state.legalAddress,
                originCountry: this.state.originCountry,
                whitepaper: this.state.whitePaper,
                logo: this.state.logo,
                logoToken: this.state.tokenLogo,
                packageId: this.state.package.value,
                value: this.state.value,
                totalValue: this.state.totalValue,
                tokensPerClaim: this.state.tokensPerClaim,
                isActive: this.state.isActive,
                createdById: this.state.createdById,
                social: {
                    facebook: this.state.facebook,
                    twitter: this.state.twitter,
                    telegram: this.state.telegram,
                    medium: this.state.medium,
                    bitcoinTalkForum: this.state.bitcoinTalkForum,
                    instagram: this.state.instagram
                }
            };

            if (this.state.category.value == 5) {
                payload["ico"] = {
                    privateSaleStart: this.state.privateStartDate,
                    privateSaleEnd: this.state.privateEndDate,
                    preSaleIcoStart: this.state.preSaleIcoStart,
                    preSaleIcoEnd: this.state.preSaleIcoEnd,
                    publicIcoStart: this.state.publicIcoStart,
                    publicIcoEnd: this.state.publicIcoEnd,
                    platformTokenType: this.state.platformTokenType,
                    pricePerToken: this.state.pricePerToken
                };
            }

            this.setState({ isLoading: true });

            axios.put(url, payload)
                .then(res => {
                    this.showAlert(true, 'success', 'Block updated successfully!');
                    this.setState({ isLoading: false });
                })
                .catch(error => {
                    console.log(error);
                    if (error.response) {
                        if (error.response.status == 422)
                            this.showAlert(true, 'error', ComposeErrorMessageFromJson(error.response.data), true);
                        else
                            this.showAlert(true, 'error', JSON.stringify(error.response.data), true);
                    }
                    else if (error.request)
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);
                    else
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                    this.setState({ isLoading: false });
                });
        }

    }

    onActiveChange(event) {
        this.setState({ isActive: event.target.checked });
    }

    componentDidMount() {
        let id = this.props.match.params.id;
        let url = `${getApi(Config.API.Block.Listings)}/${id}?adminAccess=true`;

        axios.get(url)
            .then(res => {
                console.log(res.data);
                this.setState({
                    businessName: res.data.businessName,
                    websiteUrl: res.data.websiteUrl,
                    whitePaper: res.data.whitepaper,
                    emailAddress: res.data.emailAddress,
                    authorName: res.data.authorName,
                    isAuthorInvolved: res.data.isAuthorInvolved,
                    hasToken: res.data.hasToken,
                    tokenSymbol: res.data.tokenSymbol,
                    value: res.data.value,
                    totalValue: res.data.totalValue,
                    tokensPerClaim: res.data.tokensPerClaim,
                    isActive: res.data.isActive,
                    legalName: res.data.legalName,
                    legalAddress: res.data.legalAddress,
                    originCountry: res.data.originCountry,
                    facebook: res.data.social.facebook,
                    twitter: res.data.social.twitter,
                    instagram: res.data.social.instagram,
                    medium: res.data.social.medium,
                    bitcoinTalkForum: res.data.social.bitcoinTalkForum,
                    telegram: res.data.social.telegram,
                    aboutBusiness: res.data.aboutBusiness,
                    package: GetPackageListItem(res.data.package),
                    logo: res.data.logo,
                    category: { value: BlockServiceCategories[res.data.category], label: res.data.category },
                    createdById: res.data.createdBy.id
                });

                const blocksFromHTML = convertFromHTML(res.data.aboutBusiness);
                const state = ContentState.createFromBlockArray(
                    blocksFromHTML.contentBlocks,
                    blocksFromHTML.entityMap
                );
                const editorState = EditorState.createWithContent(state);

                this.setState({
                    editorState: editorState
                });

                if (res.data.ico !== null) {
                    this.setState({
                        privateStartDate: moment(res.data.ico.privateSaleStart).format(Config.Formats.DateTime),
                        privateEndDate: moment(res.data.ico.privateSaleEnd).format(Config.Formats.DateTime),
                        preSaleIcoStart: moment(res.data.ico.preSaleIcoStart).format(Config.Formats.DateTime),
                        preSaleIcoEnd: moment(res.data.ico.preSaleIcoEnd).format(Config.Formats.DateTime),
                        publicIcoStart: moment(res.data.ico.publicIcoStart).format(Config.Formats.DateTime),
                        publicIcoEnd: moment(res.data.ico.publicIcoEnd).format(Config.Formats.DateTime),
                        platformTokenType: res.data.ico.platformTokenType,
                        pricePerToken: res.data.ico.pricePerToken,
                    });
                }

                console.log(this.state);
                this.setState({ isLoading: false });
            })
            .catch(error => {
                if (error.response) {
                    if (error.response.status == 422)
                        this.showAlert(true, 'error', ComposeErrorMessageFromJson(error.response.data), true);
                    else
                        this.showAlert(true, 'error', JSON.stringify(error.response.data), true);
                }
                else if (error.request)
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false, isFound: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title = "Edit Block" />
                <PageHeaderWithLink title = "Edit Block" btnText = "Back to Blocks" btnIcon = "fa fa-arrow-left" btnUrl={Config.RouteUrls.Admin.Blocks.Home} />
                <p className="form-group">You can use this form to update/edit blocks.</p>

                <div className="form-group">
                    <AlertBox {...this.state.alert} />
                </div>

                {
                    !this.state.isLoading &&
                    <div className="row mt-40">
                        <div className="col-md-7 offset-md-2">
                            <form action="#" onSubmit={this.onSubmit.bind(this)}>
                                <div className="mb-30">
                                    <h3 className="text-center">Package</h3>
                                    <BlockPackageDropdown onPackageChange={this.onPackageChange.bind(this)} package={this.state.package} />
                                </div>

                                <div className="mb-30">
                                    <h3 className="text-center">Basic Information</h3>
                                    <BlockBasicInformationForm
                                        {...this.state}
                                        onInputChange={this.onInputChange}
                                        onEditorChange={this.onEditorChange}
                                        handleCheckboxChange={this.handleCheckboxChange}
                                        onCategoryChange={this.onCategoryChange}
                                        onLogoChange={this.onLogoChange.bind(this)}
                                        onTokenLogoChange={this.onTokenLogoChange.bind(this)}
                                        onActiveChange={this.onActiveChange.bind(this)}
                                        isAdmin={true}
                                    />
                                </div>

                                <div className="mb-30">
                                    <h3 className="text-center">Social</h3>
                                    <BlocksSocialForm
                                        {...this.state}
                                        onInputChange={this.onInputChange}
                                    />
                                </div>

                                {
                                    this.state.category.value == 5 ?
                                        <div className="mb-30">
                                            <h3 className="text-center">ICO</h3>
                                            <BlockIcoForm
                                                {...this.state}
                                                onInputChange={this.onInputChange}
                                                onDateChange={this.onDateChange.bind(this)}
                                            />
                                        </div>
                                        :
                                        <span></span>
                                }

                                <div className="form-group">
                                    <button className="btn btn-theme" type="submit">
                                        Edit Block
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                }
            </div>
        );
    }
}