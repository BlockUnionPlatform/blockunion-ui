import React, {Component} from 'react';
import GuidLink from "../Common/GuidLink";
import {Config} from "../../../../Config";
import PackageBadge from "../Common/PackageBadge";
import {Link} from "react-router-dom";
import BlockStatusBadge from "./BlockStatusBadge";
import BlockRowActions from "../../../common/blocks/BlockRowActions";

export default class BlockRow extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let actionDropdown = {
            name: 'Actions',
            options: [
                {
                    url: Config.RouteUrls.Admin.Blocks.Edit,
                    name: 'Edit'
                },
                {
                    url: Config.RouteUrls.Admin.Blocks.View,
                    name: 'View'
                },
                {
                    url: Config.RouteUrls.Admin.Blocks.Approve,
                    name: 'Approve'
                }
            ]
        };

        return (
            <tr key={this.props.block.id}>
                <td>
                    <GuidLink url={this.props.isAdmin ? Config.RouteUrls.Admin.Blocks.View : Config.RouteUrls.Member.Blocks.View} id={this.props.block.id} />
                </td>

                <td>
                    {this.props.block.businessName}
                </td>

                <td>
                    {this.props.block.category}
                </td>

                <td>
                    {
                        this.props.block.hasToken ?
                            <span>
                                {this.props.block.tokenSymbol}
                            </span>
                            :
                            <span>-</span>
                    }
                </td>

                <td>
                    {this.props.block.originCountry}
                </td>

                <td>
                    <PackageBadge color={this.props.block.package.color} name={this.props.block.package.title} />
                </td>

                <td>
                    <BlockStatusBadge isApproved={this.props.block.isApproved} />
                </td>

                <td>
                    <BlockRowActions id={this.props.block.id} isAdmin={this.props.isAdmin} />
                </td>
            </tr>
        );
    }
}