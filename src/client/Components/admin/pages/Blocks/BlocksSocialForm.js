import React from 'react';

export default function BlocksSocialForm(props) {
    return (
        <div>
            <div className="form-group">
                <label className="control-label text-semibold">Facebook</label>
                <input type="text" name="facebook" id="facebook" className="form-control" value={props.facebook} onChange={props.onInputChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Twitter</label>
                <input type="text" name="twitter" id="twitter" className="form-control" value={props.twitter} onChange={props.onInputChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Instagram</label>
                <input type="text" name="instagram" id="instagram" className="form-control" value={props.instagram} onChange={props.onInputChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Telegram</label>
                <input type="text" name="telegram" id="telegram" className="form-control" value={props.telegram} onChange={props.onInputChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Medium</label>
                <input type="text" name="medium" id="medium" className="form-control" value={props.medium} onChange={props.onInputChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Bitcoin Talk Forum</label>
                <input type="text" name="bitcoinTalkForum" id="bitcoinTalkForum" className="form-control" value={props.bitcoinTalkForum} onChange={props.onInputChange} />
            </div>
        </div>
    );
}