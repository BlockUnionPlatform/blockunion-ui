import React from 'react';
import BlockServiceCategoryDropdown from "../Common/BlockServiceCategoryDropdown";
import {Editor} from "react-draft-wysiwyg";

export default function BlockBasicInformationForm(props) {
    let isAdmin = props.isAdmin !== undefined && props.isAdmin;

    return (
        <div>
            <div className="form-group">
                <label className="control-label text-semibold">Service Category</label>
                <BlockServiceCategoryDropdown value={props.category} onCategoryChange={props.onCategoryChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Business Name</label>
                <input type="text" name="businessName" id="businessName" className="form-control" value={props.businessName} onChange={props.onInputChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Website URL</label>
                <input type="text" name="websiteUrl" id="websiteUrl" className="form-control" value={props.websiteUrl} onChange={props.onInputChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Whitepaper URL</label>
                <input type="text" name="whitePaper" id="whitePaper" className="form-control" value={props.whitePaper} onChange={props.onInputChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Email Address</label>
                <input type="text" name="emailAddress" id="emailAddress" className="form-control" value={props.emailAddress} onChange={props.onInputChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Author Name</label>
                <input type="text" name="authorName" id="authorName" className="form-control" value={props.authorName} onChange={props.onInputChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Is Author Involved?</label>
                <input type="checkbox" className = "ml-15 mt-15" name="isAuthorInvolved" id="isAuthorInvolved" checked={props.isAuthorInvolved} onChange={props.handleCheckboxChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Logo</label>
                <input type="file" name="logo" id="logo" className="form-control" onChange={props.onLogoChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Token Logo (if any)</label>
                <input type="file" name="tokenLogo" id="tokenLogo" className="form-control" onChange={props.onTokenLogoChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Has Token?</label>
                <input type="checkbox" className = "ml-15 mt-15" name="hasToken" id="hasToken" checked={props.hasToken} onChange={props.handleCheckboxChange} />
            </div>

            {
                props.hasToken ?
                    <div className="form-group">
                        <label className="control-label text-semibold">Token Symbol</label>
                        <input type="text" name="tokenSymbol" id="tokenSymbol" className="form-control" value={props.tokenSymbol} onChange={props.onInputChange} />
                    </div>
                    :
                    <span></span>
            }

            {
                props.isAdmin &&
                    <div>
                        <div className="form-group">
                            <label className="control-label text-semibold">Estimated Value Per Claim</label>
                            <input type="text" name="value" id="value" className="form-control" value={props.value} onChange={props.onInputChange} />
                        </div>

                        <div className="form-group">
                            <label className="control-label text-semibold">Total Value</label>
                            <input type="text" name="totalValue" id="totalValue" className="form-control" value={props.totalValue} onChange={props.onInputChange} />
                        </div>

                        <div className="form-group">
                            <label className="control-label text-semibold">Tokens Per Claim</label>
                            <input type="text" name="tokensPerClaim" id="tokensPerClaim" className="form-control" value={props.tokensPerClaim} onChange={props.onInputChange} />
                        </div>
                    </div>
            }

            {
                isAdmin &&
                <div className="form-group">
                    <label className="control-label text-semibold">Is Active?</label>
                    <input type="checkbox" name="isActive" id="isActive" className="mt-5 ml-10" checked={props.isActive} onChange={props.onActiveChange} />
                </div>
            }

            <div className="form-group">
                <label className="control-label text-semibold">Legal Name</label>
                <input type="text" name="legalName" id="legalName" className="form-control" value={props.legalName} onChange={props.onInputChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Legal Address</label>
                <input type="text" name="legalAddress" id="legalAddress" className="form-control" value={props.legalAddress} onChange={props.onInputChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Origin Country</label>
                <input type="text" name="originCountry" id="originCountry" className="form-control" value={props.originCountry} onChange={props.onInputChange} />
            </div>

            <div className="form-group">
                <label className="control-label text-semibold">Description</label>
                <Editor editorClassName="main-editor" name="description" id="description" editorState={props.editorState} onEditorStateChange={props.onEditorChange} />
            </div>
        </div>
    );
}