import VerifyEmail from "../../../common/VerifyEmail";
import moment from "moment";


export default function VerifyBlock(block, checkPackage = true) {
    let errors = [];

    if (block === undefined || block === null) {
        errors.push('Please send a valid block object for verifications.');
        return errors;
    }

    if (block.businessName.length < 5 && block.businessName.length > 100)
        errors.push('Business name should be at least 5 and maximum 100 characters long');

    if (block.websiteUrl.length < 3)
        errors.push('Please specify a website url');

    if (block.authorName.length < 3)
        errors.push('Please enter an author name');

    if (block.category === null || block.category === undefined || block.category === '')
        errors.push('Please select a category for the block');

    if (block.hasToken && block.tokenSymbol === '' )
        errors.push('Please specify a token symbol, as you have selected that your block has a token');

    if (block.hasToken && block.tokenSymbol.length > 5)
        errors.push('Token Symbol can have maximum 5 characters, not more than that.');

    if (block.aboutBusiness.length < 5)
        errors.push('Please enter a description for your block');

    if (block.legalName.length < 3)
        errors.push('Please specify the legal name for the business');

    if (block.legalAddress.length < 3)
        errors.push('Please specify a legal address for the business');

    if (block.originCountry.length < 3)
        errors.push('Please specify the origin country of the business');

    if (block.logo === '')
        errors.push('Please specify a logo for your block');

    if (checkPackage) {
        if (block.package === undefined || block.package === null || block.package === '')
            errors.push('Please select a package for the block');
    }

    if (block.category != undefined || block.category != null) {
        if (block.category.value == 5) {
            if (!(moment(block.preSaleIcoStart).isValid()))
                errors.push('Please enter a valid Pre ICO Start Date');

            if (!(moment(block.preSaleIcoEnd).isValid()))
                errors.push('Please enter a valid Pre ICO End Date');

            if (!(moment(block.publicIcoStart).isValid()))
                errors.push('Please enter a valid Public ICO Start Date');

            if (!(moment(block.publicIcoEnd).isValid()))
                errors.push('Please enter a valid Public ICO End Date');

            if (block.platformTokenType === '')
                errors.push('Please enter the platform type for your token');

            if (block.pricePerToken === '')
                errors.push('Please enter a valid price per token');
        }
    }

    return errors;
}