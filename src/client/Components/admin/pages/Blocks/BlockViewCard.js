import React from 'react';
import {Link} from "react-router-dom";
import BlockPackageItem from "../Common/BlockPackageItem";
import {Config} from '../../../../Config';
import BlockStatusBadge from "./BlockStatusBadge";
import UserLink from "../Common/UserLink";
import moment from "moment";

export default function BlockViewCard(props) {
    return (
        <div>
            <div className="form-group row">
                <label className="control-label col-md-3 text-semibold">ID:</label>
                <div className="col-md-9">
                    {props.block.id}
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label col-md-3 text-semibold">Package:</label>
                <div className="col-md-9">
                    <Link to={`${Config.RouteUrls.Admin.Packages.View}/${props.block.package.id}`}>
                        <BlockPackageItem package={props.block.package} />
                    </Link>
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label col-md-3 text-semibold">Block Status:</label>
                <div className="col-md-9">
                    <BlockStatusBadge isApproved={props.block.isApproved} />
                </div>
            </div>

            {
                !props.block.isApproved ?
                    <div></div>
                    :
                    <div className="form-group row">
                        <label className="control-label col-md-3 text-semibold">Approved By:</label>
                        <div className="col-md-9">
                            <UserLink user={props.block.admin} />
                        </div>
                    </div>
            }

            <div className="form-group row">
                <label className="control-label col-md-3 text-semibold">Created By:</label>
                <div className="col-md-9">
                    <UserLink user={props.block.createdBy} />
                </div>
            </div>


            <div className="form-group row">
                <label className="control-label col-md-3 text-semibold">Business Name:</label>
                <div className="col-md-9">
                    {props.block.businessName}
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label col-md-3 text-semibold">Estimated Value Per Claim:</label>
                <div className="col-md-9">
                    {props.block.value}
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label col-md-3 text-semibold">Total Value:</label>
                <div className="col-md-9">
                    {props.block.totalValue}
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label col-md-3 text-semibold">Tokens Per Claim:</label>
                <div className="col-md-9">
                    {props.block.tokensPerClaim}
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label col-md-3 text-semibold">Is Active?</label>
                <div className="col-md-9">
                    {
                        props.block.isActive ?
                            <span className="badge badge-success">Active</span>
                            :
                            <span className="badge badge-danger">Inactive</span>
                    }
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label col-md-3 text-semibold">Logo:</label>
                <div className="col-md-9">
                    <img src={props.block.logo} alt={props.block.businessName} style={{ height: '100px', width: 'auto' }} />
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label col-md-3 text-semibold">Website URL:</label>
                <div className="col-md-9">
                    <a href={props.block.websiteUrl} target = "_blank" className = "link">{props.block.websiteUrl}</a>
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label col-md-3 text-semibold">Author Name:</label>
                <div className="col-md-9">
                    {props.block.authorName}
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label col-md-3 text-semibold">Is Author Involved?</label>
                <div className="col-md-9">
                    {props.block.isAuthorInvolved ? "Yes" : "No"}
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label col-md-3 text-semibold">Category:</label>
                <div className="col-md-9">
                    {props.block.category}
                </div>
            </div>

            {
                props.block.hasToken ?
                    <div className="form-group row">
                        <label className="control-label col-md-3 text-semibold">Token Symbol:</label>
                        <div className="col-md-9">
                            {props.block.tokenSymbol}
                        </div>
                    </div>
                    :
                    <div></div>
            }

            <div className="form-group row">
                <label className="control-label col-md-3 text-semibold">Legal Name:</label>
                <div className="col-md-9">
                    {props.block.legalName}
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label col-md-3 text-semibold">Legal Address:</label>
                <div className="col-md-9">
                    {props.block.legalAddress}
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label col-md-3 text-semibold">Origin Country:</label>
                <div className="col-md-9">
                    {props.block.originCountry}
                </div>
            </div>

            {
                props.block.whitepaper !== '' &&
                <div className="form-group row">
                    <label className="control-label col-md-3 text-semibold">Whitepaper:</label>
                    <div className="col-md-9">
                        <a href={props.block.whitepaper} target = "_blank" className = "link">Click to view</a>
                    </div>
                </div>
            }

            <h3 className="mt-20 mb-25">Social</h3>

            <div className="form-group row">
                <label className="control-label col-md-3 text-semibold">Facebook:</label>
                <div className="col-md-9">
                    <Link to={props.block.social.facebook} target = "_blank" className = "link">{props.block.social.facebook}</Link>
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label col-md-3 text-semibold">Twitter:</label>
                <div className="col-md-9">
                    <Link to={props.block.social.twitter} target = "_blank" className = "link">{props.block.social.twitter}</Link>
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label col-md-3 text-semibold">Instagram:</label>
                <div className="col-md-9">
                    <Link to={props.block.social.instagram} target = "_blank" className = "link">{props.block.social.instagram}</Link>
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label col-md-3 text-semibold">Telegram:</label>
                <div className="col-md-9">
                    <Link to={props.block.social.telegram} target = "_blank" className = "link">{props.block.social.telegram}</Link>
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label col-md-3 text-semibold">Bitcoin Talk Forum:</label>
                <div className="col-md-9">
                    <Link to={props.block.social.bitcoinTalkForum} target = "_blank" className = "link">{props.block.social.bitcoinTalkForum}</Link>
                </div>
            </div>

            <div className="form-group row">
                <label className="control-label col-md-3 text-semibold">Medium:</label>
                <div className="col-md-9">
                    <Link to={props.block.social.medium} target = "_blank" className = "link">{props.block.social.medium}</Link>
                </div>
            </div>

            {
                props.block.category.toLowerCase() == 'icorankbusiness' ?
                    <div>
                        <h3 className="mt-20 mb-25">ICO Details</h3>

                        <div className="form-group row">
                            <label className="control-label col-md-3 text-semibold">Platform Token Type:</label>
                            <div className="col-md-9">
                                {props.block.ico.platformTokenType}
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="control-label col-md-3 text-semibold">Price Per Token:</label>
                            <div className="col-md-9">
                                {props.block.ico.pricePerToken}
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="control-label col-md-3 text-semibold">Private Sale Start:</label>
                            <div className="col-md-9">
                                {moment(props.block.ico.privateSaleStart).format(Config.Formats.DateTime)}
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="control-label col-md-3 text-semibold">Private Sale End:</label>
                            <div className="col-md-9">
                                {moment(props.block.ico.privateSaleEnd).format(Config.Formats.DateTime)}
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="control-label col-md-3 text-semibold">Pre Sale ICO Start:</label>
                            <div className="col-md-9">
                                {moment(props.block.ico.preSaleIcoStart).format(Config.Formats.DateTime)}
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="control-label col-md-3 text-semibold">Pre Sale ICO End:</label>
                            <div className="col-md-9">
                                {moment(props.block.ico.preSaleIcoEnd).format(Config.Formats.DateTime)}
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="control-label col-md-3 text-semibold">Public ICO Start:</label>
                            <div className="col-md-9">
                                {moment(props.block.ico.publicIcoStart).format(Config.Formats.DateTime)}
                            </div>
                        </div>

                        <div className="form-group row">
                            <label className="control-label col-md-3 text-semibold">Public ICO End:</label>
                            <div className="col-md-9">
                                {moment(props.block.ico.publicIcoEnd).format(Config.Formats.DateTime)}
                            </div>
                        </div>
                    </div>
                    :
                    <div></div>
            }

            <label className="control-label text-semibold">About Business:</label>
            <div className="mt-15" >
                <span dangerouslySetInnerHTML={{ __html: props.block.aboutBusiness }}></span>
            </div>
        </div>
    );
}