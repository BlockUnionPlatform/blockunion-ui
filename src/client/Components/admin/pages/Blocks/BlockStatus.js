import React, {Component} from 'react';
import axios from 'axios';
import {Config} from '../../../../Config';
import SiteTitle from "../../../common/SiteTitle";
import LoadingScreen from "../../../common/LoadingScreen";
import PageHeaderWithLink from "../PageHeaderWithLink";
import AlertBox from "../../../common/AlertBox";
import {getApi} from "../../../../ConfigStore";
import {Link} from "react-router-dom";
import BlockPackageItem from "../Common/BlockPackageItem";
import {Auth} from "../../../common/AuthHelpers";
import BlockStatusBadge from "./BlockStatusBadge";

export default class BlockStatus extends Component {
    constructor(props) {
        super(props);

        this.state = {
            block: {},
            isApproved: false,
            isLoading: true,
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
        this.onStatusChange = this.onStatusChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    showAlert(show, status, message) {
        this.setState({ alert: { show, status, message } });

        window.scrollTo(0,0);
    }

    onStatusChange(event) {
        this.setState({ isApproved: event.target.checked });
    }

    onFormSubmit(event) {
        event.preventDefault();

        let url = getApi(Config.API.Block.Status);
        let payload = {
            id: this.state.block.id,
            isApproved: this.state.isApproved,
            adminId: Auth.getUser().id
        };

        this.setState({ isLoading: true });

        axios.post(url, payload)
            .then(res => {
                this.showAlert(true, 'success', 'Block status updated successfully.');
                this.setState({ isLoading: false });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            })
    }

    componentDidMount() {
        let id = this.props.match.params.id;
        let url = `${getApi(Config.API.Block.Listings)}/${id}?adminAccess=true`;

        axios.get(url)
            .then(res => {
                this.setState({
                    block: res.data,
                    isApproved: res.data.isApproved,
                    isLoading: false
                })
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });
    }

    render() {
        return (
            <div>
                <SiteTitle title = "Update Block Status" />
                <LoadingScreen show={this.state.isLoading} />
                <PageHeaderWithLink title = "Update Block Status" btnIcon = "fa fa-arrow-left" btnText = "Back to Blocks" btnUrl={Config.RouteUrls.Admin.Blocks.Home} />
                <AlertBox {...this.state.alert} />

                {
                    this.state.isLoading ?
                        <div>Loading</div>
                        :
                        <div className="row mt-30">
                            <div className="col-md-6">
                                <form action="#" onSubmit={this.onFormSubmit}>
                                    <div className="form-group">
                                        <label className="control-label text-semibold">Is Approved?:</label>
                                        <input type = "checkbox" name = "isApproved" id = "isApproved" checked={this.state.isApproved} className = "mt-10 ml-15" onChange={this.onStatusChange}/>
                                    </div>

                                    <div className="form-group">
                                        <label className="control-label text-semibold">Status:</label>
                                        <BlockStatusBadge isApproved={this.state.isApproved} />
                                    </div>

                                    <div className="form-group">
                                        <button type = "submit" className = "btn btn-theme">Update Status</button>
                                    </div>
                                </form>
                            </div>
                            <div className="col-md-6">
                                <h3 className="text-bold">Block Info</h3>
                                <div className="form-group">
                                    <label className="control-label text-semibold">ID:</label>
                                    <div>
                                        {this.state.block.id}
                                    </div>
                                </div>

                                <div className="form-group">
                                    <label className="control-label text-semibold">Package:</label>
                                    <div>
                                        <Link to={`${Config.RouteUrls.Admin.Packages.View}/${this.state.block.package.id}`}>
                                            <BlockPackageItem package={this.state.block.package} />
                                        </Link>
                                    </div>
                                </div>

                                <div className="form-group">
                                    <label className="control-label text-semibold">Business Name:</label>
                                    <div>
                                        {this.state.block.businessName}
                                    </div>
                                </div>

                                <div className="form-group">
                                    <label className="control-label text-semibold">Value:</label>
                                    <div>
                                        {this.state.block.value}
                                    </div>
                                </div>

                                <div className="form-group">
                                    <Link to={`${Config.RouteUrls.Admin.Blocks.View}/${this.state.block.id}`} className = "link">See full details</Link>
                                </div>
                            </div>
                        </div>
                }
            </div>
        );
    }
}
