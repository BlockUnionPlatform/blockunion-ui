import React from 'react';

export default function BlockStatusBadge(props) {
    if (props.isApproved)
        return (<span className="badge badge-primary ml-20">Approved</span>);
    else
        return (<span className="badge badge-warning ml-20">Pending</span>);
}