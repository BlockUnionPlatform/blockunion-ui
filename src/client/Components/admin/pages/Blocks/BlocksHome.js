import React, {Component} from 'react';
import SiteTitle from "../../../common/SiteTitle";
import PageHeaderWithLink from "../PageHeaderWithLink";
import {Config} from "../../../../Config";
import BlocksRow from "./BlocksRow";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';
import AlertBox from "../../../common/AlertBox";
import LoadingScreen from "../../../common/LoadingScreen";
import BlocksTable from "../../../common/blocks/BlocksTable";

export default class BlocksHome extends Component {
    constructor(props) {
        super(props);

        this.state = {
            blocks: [],
            isLoading: true,
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });
    }

    componentDidMount() {
        let url = getApi(Config.API.Block.Listings);

        axios.get(url)
            .then(res => {
                this.setState({
                    blocks: res.data,
                    isLoading: false
                });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title="Blocks"/>
                <PageHeaderWithLink title="View Blocks" btnIcon="fa fa-plus" btnText="Add Block" btnUrl={Config.RouteUrls.Admin.Blocks.Add} />

                <BlocksTable {...this.state} isAdmin={true} />
            </div>
        );
    }
}