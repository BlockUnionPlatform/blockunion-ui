import React, {Component} from 'react';
import RewardLink from "../Common/RewardLink";
import UserLink from "../Common/UserLink";
import moment from "moment";
import {Config} from '../../../../Config';

export default class RewardClaimCard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let isAdmin = this.props.isAdmin !== undefined && this.props.isAdmin;

        return (
            <form action="#">
                <div className="form-group row">
                    <label className="control-label col-md-2 text-semibold">ID:</label>
                    <div className="col-md-10">
                        {this.props.claim.id}
                    </div>
                </div>

                <div className="form-group row">
                    <label className="control-label col-md-2 text-semibold">Claimed By:</label>
                    <div className="col-md-10">
                        {
                            isAdmin ?
                                <UserLink user={this.props.claim.user}/>
                                :
                                <span>{this.props.claim.user.name}</span>
                        }
                    </div>
                </div>

                <div className="form-group row">
                    <label className="control-label col-md-2 text-semibold">Reward:</label>
                    <div className="col-md-10">
                        {
                            isAdmin ?
                                <RewardLink reward={this.props.claim.reward} />
                                :
                                <span>{this.props.claim.reward.value} {this.props.claim.reward.token.symbol}</span>
                        }
                    </div>
                </div>

                <div className="form-group row">
                    <label className="control-label col-md-2 text-semibold">Type:</label>
                    <div className="col-md-10">
                        {this.props.claim.type}
                    </div>
                </div>

                <div className="form-group row">
                    <label className="control-label col-md-2 text-semibold">Comments:</label>
                    <div className="col-md-10">{this.props.claim.comments}</div>
                </div>

                <div className="form-group row">
                    <label className="control-label col-md-2 text-semibold">Date Claimed:</label>
                    <div className="col-md-10">{moment(this.props.claim.dateCreatedUtc).format(Config.Formats.DateTime)}</div>
                </div>
            </form>
        );
    }
}