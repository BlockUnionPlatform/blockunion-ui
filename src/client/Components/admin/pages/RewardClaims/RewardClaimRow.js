import React from 'react';
import GuidLink from "../Common/GuidLink";
import UserLink from "../Common/UserLink";
import RewardLink from "../Common/RewardLink";
import {Link} from "react-router-dom";
import moment from "moment";
import {Config} from "../../../../Config";

export default function RewardClaimRow(props) {
    let isAdmin = props.isAdmin != undefined && props.isAdmin;
    let url = isAdmin ? Config.RouteUrls.Admin.Rewards.Claims.View : Config.RouteUrls.Member.DailyActionClaims;
    let dailyActionUrls = isAdmin ? Config.RouteUrls.Admin.DailyActions.Edit : Config.RouteUrls.Member.DailyActions.Home;
    return (
        <tr>
            <td>
                <GuidLink id={props.claim.id} url={url} />
            </td>

            <td>
                {
                    props.isAdmin ?
                        <UserLink user={props.claim.user}/>
                        :
                        <span>
                            {props.claim.user.name}
                        </span>
                }
            </td>

            <td>
                {
                    isAdmin ?
                        <RewardLink reward={props.claim.reward} />
                        :
                        <span>
                            {props.claim.reward.value} {props.claim.reward.token.symbol}
                        </span>
                }
            </td>

            <td>
                <Link to={`${dailyActionUrls}/${props.claim.entityId}`} className="link">View Daily Action</Link>
            </td>

            <td>
                {moment(props.claim.dateCreatedUtc).format(Config.Formats.DateTime)}
            </td>

            <td>
                {
                    !props.claim.isProcessed ?
                        <span className="badge badge-warning">Pending</span>
                        :
                        <span className="badge badge-success">Processed</span>
                }
            </td>

            <td>
                <Link to={`${url}/${props.claim.id}`} className="link">View</Link> &nbsp;
                {
                    isAdmin ?
                        <span>
                            {
                                !props.claim.isProcessed ?
                                    <span>| &nbsp; <Link to={`${Config.RouteUrls.Admin.Rewards.Claims.Process}/${props.claim.id}`} className="link">Process</Link></span>
                                    :
                                    <span>| &nbsp; <Link to={`${Config.RouteUrls.Admin.Payouts.View}/${props.claim.payout.id}`} className="link">See Payout</Link></span>
                            }
                        </span>
                        :
                        <span>
                            {
                                props.claim.payout != null &&
                                    <span>
                                        | &nbsp; <Link to={`${Config.RouteUrls.Member.DailyActionPayouts}/${props.claim.payout.id}`} className="link">See Payout</Link>
                                    </span>

                            }
                        </span>
                }
            </td>
        </tr>
    );
}