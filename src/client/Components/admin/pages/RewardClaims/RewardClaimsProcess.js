import React, {Component} from 'react';
import AlertBox from "../../../common/AlertBox";
import SiteTitle from "../../../common/SiteTitle";
import PageHeaderWithLink from "../PageHeaderWithLink";
import RewardClaimCard from './RewardClaimCard';
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';
import LoadingScreen from "../../../common/LoadingScreen";
import {Config} from '../../../../Config';
import {Auth} from "../../../common/AuthHelpers";

export default class RewardsClaimProcess extends Component {
    constructor(props) {
        super(props);

        this.state = {
            rewardClaim: {},
            isLoading: true,
            isFound: true,
            rewardProcessed: false,
            comments: '',
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
        this.handleCheckbox = this.handleCheckbox.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });

        window.scrollTo(0,0);
    }

    handleCheckbox(event) {
        this.setState({ rewardProcessed: event.target.checked });
    }

    onInputChange(event) {
        this.setState({ [event.target.name] : event.target.value });
    }

    onFormSubmit(event) {
        event.preventDefault();

        if (!this.state.rewardProcessed) {
            this.showAlert(true, 'error', 'Please check the undertaking checkbox.');
        }
        else {
            let url = getApi(Config.API.Payouts);
            let payload = {
                adminId: Auth.getUser().id,
                userId: this.state.rewardClaim.user.id,
                rewardClaimId: this.state.rewardClaim.id,
                comments: this.state.comments
            };

            this.setState({ isLoading: true });

            axios.post(url, payload)
                .then(res => {
                    this.showAlert(true, 'success', 'Reward claim processed successfully.');
                    this.setState({ isLoading: false });
                })
                .catch(error => {
                    console.log(error);
                    if (error.response)
                        this.showAlert(true, 'error', error.response.data.toString(), true);
                    else if (error.request)
                        this.showAlert(true, 'error', 'Unable to connect to server', true);
                    else
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                    this.setState({ isLoading: false });
                });
        }
    }

    componentDidMount() {
        let url = `${getApi(Config.API.RewardClaims)}/${this.props.match.params.id}`;

        axios.get(url)
            .then(res => {
                if (res.data.isProcessed) {
                    this.setState({
                        isLoading: false,
                        isFound: false
                    });

                    this.showAlert(true, 'error', 'This claim has already been processed.')
                }
                else {
                    this.setState({
                        rewardClaim: res.data,
                        isLoading: false,
                        isFound: true
                    })
                };
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title="Process Reward Claim"/>
                <PageHeaderWithLink title = "Process Reward Claim" btnText = "Back to Reward Claims" btnIcon = "fa fa-arrow-left" btnUrl={Config.RouteUrls.Admin.Rewards.Claims.Home} />
                <div className="row">
                    <div className="col-md-7">
                        <p className="form-group">
                            If you have made the transaction mentioned above to the user, please use the following form to mark this <b>reward claim</b> as <b>processed.</b>
                        </p>
                    </div>
                </div>

                <div className="mt-15">
                    <AlertBox  {...this.state.alert} />
                </div>

                {
                    this.state.isLoading ?
                        <div>Loading</div>
                        :
                        <div>
                            {
                                !this.state.isFound ?
                                    <div></div>
                                    :
                                    <div>
                                        <div className="form-group mt-30">
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <h3>Process Reward Claim</h3>
                                                    <AlertBox isVisible={this.state.alertVisible} status={this.state.alertStatus} message={this.state.alertMessage} />

                                                    <form action="#" method="post" onSubmit={this.onFormSubmit} className="mt-30">
                                                        <div className="form-group">
                                                            <label className="control-label text-semibold">Reward Processed?</label>
                                                            <div>
                                                                <input type="checkbox" className="position-left" onChange={this.handleCheckbox} value={this.state.rewardProcessed}/>
                                                                The reward claim has been processed by me.
                                                                <div className="small">
                                                                    <span style={{ color: "red" }}>This is an irreversible change, so only if you have completed the above transactions, mark this as complete.</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="form-group">
                                                            <label className="control-label text-semibold">Comments</label>
                                                            <p>
                                                                You can enter any notes regarding the transaction here.
                                                            </p>
                                                            <div>
                                                                <textarea name="comments" id="comments" rows="10" className="form-control" value={this.state.comments} onChange={this.onInputChange}></textarea>
                                                            </div>
                                                        </div>

                                                        <div className="form-group">
                                                            <div className="pull-right">
                                                                <button className="btn btn-theme" type="submit">Process Reward</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div className="col-md-6">
                                                    <h3>Reward Claim</h3>
                                                    <RewardClaimCard claim={this.state.rewardClaim} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            }
                        </div>
                }
            </div>
        );
    }
}