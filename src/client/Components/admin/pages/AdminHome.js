import React, {Component} from 'react';
import SiteTitle from "../../common/SiteTitle";
import WidgetOverview from "./Widgets/WidgetOverview";
import {Config} from "../../../Config";
import ContentCard from "./Widgets/ContentCard";
import {Repository} from "../../../Data/DataStore";
import BlockTransactionRow from "./BlockTransactions/BlockTransactionRow";
import BlockTransactionRowSmall from "./BlockTransactions/BlockTransactionRowSmall";
import ActivityLogRowSmall from "./ActivityLogs/ActivityLogRowSmall";
import {getApi} from "../../../ConfigStore";
import axios from 'axios';
import AlertBox from "../../common/AlertBox";
import LoadingScreen from "../../common/LoadingScreen";

export default class AdminHome extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            overview: {
                blocks: {
                    value: '',
                    text: '',
                    url: ''
                },

                users: {
                    value: '',
                    text: '',
                    url: ''
                },

                tokens: {
                    value: '',
                    text: '',
                    url: ''
                }
            },
            transactions: [],
            activity: [],
            alert: {
                show: false,
                status: '',
                message: '',
            }
        }

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });
    }

    componentDidMount() {
        let url = getApi(Config.API.Dashboard.Admin);

        axios.get(url)
            .then(res => {
                this.setState({
                    overview: {
                        blocks: {
                            value: res.data.blocksCount,
                            text: 'Listed Blocks',
                            url: Config.RouteUrls.Admin.Blocks.Home
                        },

                        users: {
                            value: res.data.usersCount,
                            text: 'Registered Users',
                            url: Config.RouteUrls.Admin.Users.Home
                        },

                        tokens: {
                            value: res.data.tokensCount,
                            text: 'Registered Tokens',
                            url: Config.RouteUrls.Admin.Tokens.Home
                        }
                    },

                    transactions: res.data.recentTransactions,
                    activity: res.data.recentActivity,
                    isLoading: false
                });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title="Dashboard"/>
                <AlertBox {...this.state.alert} />
                <div className="row">
                    <div className="col-md-4">
                        <WidgetOverview entity={this.state.overview.blocks} />
                    </div>
                    <div className="col-md-4">
                        <WidgetOverview entity={this.state.overview.users} />
                    </div>
                    <div className="col-md-4">
                        <WidgetOverview entity={this.state.overview.tokens} />
                    </div>
                </div>

                <div className="row mt-30">
                    <div className="col-md-6">
                        <ContentCard title = "Recent Transactions">
                            <table style={{ width: '100%' }}>
                                <tbody>
                                    {
                                        this.state.transactions.map((t) => <BlockTransactionRowSmall transaction={t} key={t.id} />)
                                    }
                                </tbody>
                            </table>
                        </ContentCard>
                    </div>

                    <div className="col-md-6">
                        <ContentCard title = "Recent Activity">
                            <table style={{ width: '100%' }}>
                                <tbody>
                                {
                                    this.state.activity.map((t) => <ActivityLogRowSmall activity={t} key={t.id} />)
                                }
                                </tbody>
                            </table>
                        </ContentCard>
                    </div>
                </div>
            </div>
        );
    }
}