function GetTokenDropdownItem(token) {
    return { value: token.id, label: `${token.name} (${token.symbol})`}
}

export {GetTokenDropdownItem};