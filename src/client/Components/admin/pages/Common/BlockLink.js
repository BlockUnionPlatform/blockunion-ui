import React, {Component} from 'react';
import Link from "react-router-dom/es/Link";
import {Config} from "../../../../Config";

export default class BlockLink extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let url = this.props.url !== undefined ? this.props.url : Config.RouteUrls.Admin.Blocks.View;
        return (
            <Link to={`${url}/${this.props.block.id}`} className="link">
                {this.props.block.businessName}
            </Link>
        );
    }
}