import React from 'react';
import {Link} from "react-router-dom";
import {Config} from '../../../../Config';

export default function BlockPackageItem(props) {
    return (
        <Link to={`${Config.RouteUrls.Admin.Packages.Edit}/${props.package.id}`} className = "link">
            {`${props.package.title} - ${props.package.value} ${props.package.token.symbol} / ${props.package.recurring}`}
        </Link>
    );
}