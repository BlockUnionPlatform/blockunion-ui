import React, {Component} from 'react';
import Select from "react-select";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';
import {Config} from "../../../../Config";

export default class BlockPackageDropdown extends Component {
    constructor(props) {
        super(props);

        this.state = {
            packages: []
        };
    }

    componentDidMount() {
        let url = getApi(Config.API.Block.Packages);
        let selectItems = [];

        axios.get(url)
            .then(res => {
                res.data.map(p => selectItems.push({ value: p.id, label: `${p.title} - ${p.value} ${p.token.symbol} / ${p.recurring}` }));
                this.setState({ packages: selectItems });
            })
            .catch();
    }

    render() {
        return (
            <div>
                <Select options={this.state.packages} onChange={this.props.onPackageChange} value={this.props.package}/>
            </div>
        );
    }
}