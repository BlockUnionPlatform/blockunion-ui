import React, {Component} from 'react';
import {Config} from "../../../../Config";
import Link from "react-router-dom/es/Link";

export default class UserLink extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let link = <div></div>;

        if (this.props.user === undefined) {
            link = <Link to={`${Config.RouteUrls.Admin.Users.View}/${this.props.id}`} className="link">
                        {this.props.name}
                </Link>;
        }
        else {
            link = <Link to={`${Config.RouteUrls.Admin.Users.View}/${this.props.user.guid != undefined ? this.props.user.guid : this.props.user.id}`} className="link">
                {this.props.user.name}
            </Link>;
        }
        return (
            <span>
                {link}
            </span>
        );
    }
}