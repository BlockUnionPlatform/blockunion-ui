import React, {Component} from 'react';
import DateTime from "react-datetime";
import {Config} from "../../../../Config";

export default class DatePicker extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <DateTime
                dateFormat={Config.Formats.Date}
                utc={true}
                inputProps={{ id: this.props.name, name: this.props.name }}
                value={this.props.value}
                onChange={(e) => this.props.onChange(this.props.name, e)}/>
        );
    }
}