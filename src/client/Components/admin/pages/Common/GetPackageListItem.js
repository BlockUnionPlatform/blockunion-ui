export default function GetPackageListItem(p) {
    return { value: p.id, label: `${p.title} - ${p.value} ${p.token.symbol} / ${p.recurring}` };
}