let BlockPackageRecurring = [
    { value: 1, label: 'Monthly' },
    { value: 2, label: 'Weekly' },
    { value: 3, label: 'Quarterly' },
    { value: 4, label: 'Yearly' }
];

export default BlockPackageRecurring;