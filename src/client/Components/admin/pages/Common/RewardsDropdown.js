import React, {Component} from 'react';
import Select from "react-select";
import {getApi} from "../../../../ConfigStore";
import {Config} from "../../../../Config";
import axios from 'axios';

export default class RewardsDropdown extends Component {
    constructor(props) {
        super(props);

        this.state = {
            rewards: []
        };
    }

    componentDidMount() {
        let url = getApi(Config.API.Rewards)
        let rewards = [];

        axios.get(url)
            .then(res => {
                res.data.map(reward => rewards.push({ value: reward.id, label: `${reward.value} ${reward.token.symbol}` }));
                this.setState({ rewards });
            });
    }

    render() {
        return (
            <Select options={this.state.rewards} onChange={this.props.onChange} value={this.props.value} />
        );
    }

}