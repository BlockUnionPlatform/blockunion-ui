import React from 'react';

export default function ComposeErrorMessageFromJson(errors) {
    return (
        <div>
            <p className="form-group" style={{ color: '#721c24' }}>Please correct the following errors to continue:</p>
            <div dangerouslySetInnerHTML={{ __html: showObject(errors) }}></div>
        </div>
    );
}

function showObject(obj) {
    let result = "";
    for (let p in obj) {
        if( obj.hasOwnProperty(p) ) {
            result += `<div><b>${p}:</b> ${obj[p]}</div>`;
        }
    }
    return result;
}