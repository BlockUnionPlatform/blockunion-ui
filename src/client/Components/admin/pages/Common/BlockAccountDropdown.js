import React, {Component} from 'react';
import Select from "react-select";
import {getApi} from "../../../../ConfigStore";
import {Config} from "../../../../Config";
import axios from 'axios';

export default class BlockAccountDropdown extends Component {
    constructor(props) {
        super(props);

        this.state = {
            accounts: []
        };
    }

    componentDidMount() {
        let accounts = [];
        let url = getApi(Config.API.Block.Accounts);

        axios.get(url)
            .then(res => {
                res.data.map(account => {
                    accounts.push({ value: account.id, label: `${account.title} (${account.token.symbol})` });
                });
                this.setState({ accounts });
            })
            .catch(error => {
                accounts.push({ value: 'none', label: 'Unable to load accounts' });
                this.setState({ accounts });
            });
    }

    render() {
        return (
            <Select options={this.state.accounts} onChange={this.props.onSelectedAccountChange} value={this.props.account} />
        );
    }
}