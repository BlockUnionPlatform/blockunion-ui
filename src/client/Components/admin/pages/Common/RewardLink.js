import React, {Component} from 'react';
import {Config} from "../../../../Config";
import Link from "react-router-dom/es/Link";

export default class RewardLink extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Link to={`${Config.RouteUrls.Admin.Rewards.View}/${this.props.reward.id}`} className="link">
                {this.props.reward.value} {this.props.reward.token.symbol}
            </Link>
        );
    }
}