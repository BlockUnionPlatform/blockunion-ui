import React, {Component} from 'react';
import Link from "react-router-dom/es/Link";
import {Config} from "../../../../Config";

export default class BlockAccountLink extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Link to={`${Config.RouteUrls.Admin.BlockToken.Accounts.View}/${this.props.account.id}`} className="link">
                {this.props.account.title} - {this.props.account.token.name} ({this.props.account.token.symbol})
            </Link>
        );
    }
}