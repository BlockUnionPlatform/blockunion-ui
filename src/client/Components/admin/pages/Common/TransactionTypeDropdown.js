import React, {Component} from 'react';
import {Repository} from "../../../../Data/DataStore";
import Select from "react-select";

export default class TransactionTypeDropdown extends Component {
    constructor(props) {
        super(props);

        this.state = {
            types: []
        };
    }

    componentWillMount() {
        let transactionTypes = Repository.TransactionTypes;
        let types = [];

        transactionTypes.map( (type) =>
            types.push({ value: type, label: type })
        );

        this.setState({ types });
    }

    render() {
        return (
            <Select options={this.state.types} onChange={this.props.onTransactionTypeChange} value={this.props.transactionType} />
        );
    }
}