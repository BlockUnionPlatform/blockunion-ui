import React, {Component} from 'react';
import Select from "react-select";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';
import {Config} from "../../../../Config";

export default class UserDropDown extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectItems: []
        };
    }

    componentDidMount() {
        let url = getApi(Config.API.Users.Default);
        let selectItems = [];

        this.props.setLoading(true);

        axios.get(url)
            .then(res => {
                res.data.map(user => selectItems.push({ value: user.id, label: `${user.name} (${user.emailAddress})` }));
                this.setState({ selectItems });
                this.props.setLoading(false);

            })
            .catch(error => {
                selectItems.push({ value: 'none', label: 'Unable to load users' });
                this.setState({ selectItems });
                this.props.setLoading(false);
            });
    }

    render() {
        return (
            <Select options={this.state.selectItems} onChange={this.props.onSelectedUserChange} value={this.props.user} />
        );
    }
}