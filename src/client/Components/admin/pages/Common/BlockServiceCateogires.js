let BlockServiceCategories = {
    Dapp: 1,
    Exchange: 2,
    Marijuana: 3,
    Airdrops: 4,
    ICO: 5,
    CryptoApparel: 6,
    Otc: 7,
    CryptoMixer: 8,
    Other: 9
};

export {BlockServiceCategories};