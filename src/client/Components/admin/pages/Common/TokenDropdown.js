import React, {Component} from 'react';
import Select from "react-select";
import {getApi} from "../../../../ConfigStore";
import {Config} from "../../../../Config";
import axios from 'axios';

export default class TokenDropdown extends Component {
    constructor(props) {
        super(props);

        this.state = {
            tokens: []
        };
    }

    componentDidMount() {
        let url = getApi(Config.API.Tokens)
        let tokens = [];

        axios.get(url)
            .then(res => {
                res.data.map(token => tokens.push({ value: token.id, label: `${token.name} (${token.symbol})` }));
                this.setState({ tokens });
            });
    }

    render() {
        return (
            <Select options={this.state.tokens} onChange={this.props.onChange} value={this.props.value} />
        );
    }

}