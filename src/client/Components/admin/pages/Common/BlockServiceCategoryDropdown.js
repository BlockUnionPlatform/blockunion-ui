import React, {Component} from 'react';
import {Repository} from "../../../../Data/DataStore";
import Select from "react-select";
import {BlockServiceCategories} from "./BlockServiceCateogires";

export default class BlockServiceCategoryDropdown extends Component {
    constructor(props) {
        super(props);

        this.state = {
            categories: []
        };
    }

    componentDidMount() {
        let categories = [];

        Object.entries(BlockServiceCategories).map(([key, value]) => categories.push({ value: value, label: key}));

        this.setState({ categories });
    }

    render() {
        return (
            <Select options={this.state.categories} value={this.props.value} onChange={this.props.onCategoryChange} />
        );
    }
}