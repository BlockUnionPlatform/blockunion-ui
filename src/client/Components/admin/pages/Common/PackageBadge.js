import React, {Component} from 'react';

export default class PackageBadge extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <span style={{ backgroundColor: this.props.color }} className="badge badge-light">
                {this.props.name}
            </span>
        );
    }
}