import React, {Component} from 'react';
import Link from "react-router-dom/es/Link";

export default class GuidLink extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Link to={`${this.props.url}/${this.props.id}`} className="link">
                {this.props.id.substr(0,5)}...
            </Link>
        );
    }
}