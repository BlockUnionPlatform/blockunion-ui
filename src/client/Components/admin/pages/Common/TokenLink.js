import React, {Component} from 'react';
import Link from "react-router-dom/es/Link";
import {Config} from "../../../../Config";

export default class TokenLink extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Link to={`${Config.RouteUrls.Admin.Tokens.View}/${this.props.token.guid}`} className="link">
                {this.props.value} {this.props.token.symbol}
            </Link>
        );
    }
}