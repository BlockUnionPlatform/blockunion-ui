import React from 'react';
import Select from 'react-select';
import BlockPackageRecurring from "./PackageRenewals";

export default function PackageRecurringDropdown(props) {
    return (
        <Select onChange={props.onChange} value={props.value} options={BlockPackageRecurring} />
    );
}