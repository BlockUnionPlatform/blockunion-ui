import React, {Component} from 'react';

export default class TokenForm extends Component {
    constructor(props) {
        super(props);

        this.handler = this.handler.bind(this);
    }

    handler() {
        //this.props.action();
    }

    render() {
        return (
            <form action="#" method="POST" onSubmit={this.props.onFormSubmit}>
                <div className="form-group">
                    <label className="control-label">Token Name:</label>
                    <input name="tokenName" type="text" placeholder="Enter the token name here." className="form-control" value={this.props.tokenName} onChange={this.props.onInputChange} />
                </div>

                <div className="form-group">
                    <label className="control-label">Token Symbol:</label>
                    <input name="tokenSymbol" type="text" placeholder="Enter the token symbol here." className="form-control" value={this.props.tokenSymbol} onChange={this.props.onInputChange} />
                </div>

                <div className="form-group">
                    <label className="control-label">Is Active?</label>
                    <input name="isActive" type="checkbox" className="position-right" checked={this.props.isActive} onChange={this.props.handleCheckbox} />
                </div>

                <div className="form-group">
                    <div className="pull-right">
                        <button className="btn btn-theme" type="submit">{this.props.btnText}</button>
                    </div>
                </div>
            </form>
        );
    }
}

TokenForm.defaultProps = {
    btnText: 'Add Token'
};