import React from 'react';
import {Link} from 'react-router-dom';
import {Config} from '../../../../Config';
import GuidLink from "../Common/GuidLink";

export default function TokenListItem(props) {
    let badge = props.isActive ?
        <span className="badge badge-success">yes</span> :
        <span className="badge badge-warning">no</span>;

    let editUrl = `${Config.RouteUrls.Admin.Tokens.Edit}/${props.id}`;

    return (
        <tr>
            <td>
                <GuidLink url={Config.RouteUrls.Admin.Tokens.View} id={props.id}/>
            </td>
            <td>{props.name}</td>
            <td>{props.symbol}</td>
            <td>{badge}</td>
            <td><Link to={editUrl} className="link">Edit</Link></td>
        </tr>
    );
}