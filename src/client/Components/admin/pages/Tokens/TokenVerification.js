export default function VerifyToken(name, symbol) {
    let errors = [];

    if (name.length < 5)
        errors.push('Token Name should be at least 5 characters in length.');

    if (symbol.length < 2)
        errors.push('Token Symbol should be at least 2 characters in length.');

    return errors;
}