import React, {Component} from 'react';
import TokenListItem from './TokenListItem';
import {Link} from 'react-router-dom';
import {Config} from '../../../../Config';
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';
import SiteTitle from "../../../common/SiteTitle";
import LoadingScreen from "../../../common/LoadingScreen";
import AlertBox from "../../../common/AlertBox";


export default class TokenHome extends Component {
    constructor(props) {
        super(props);

        this.state = {
            tokens: [],
            isLoading: true,
            alert: {
                show: false,
                status: '',
                message: '',
                isSmall: false
            }
        };

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '', isSmall = false) {
        this.setState({
            alert: { show, status, message, isSmall }
        });
    }

    componentDidMount() {
        let url = getApi(Config.API.Tokens);
        axios.get(url)
            .then(res => {
                this.setState({ tokens: res.data, isLoading: false });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data, true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);
            });
    }

    render() {
        return (
            <div>
                <SiteTitle title="Tokens"/>
                <LoadingScreen show={this.state.isLoading} />

                <div className="row form-group">
                    <div className="col-md-6">
                        <div className="pull-left">
                            <h1>View Tokens</h1>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="pull-right">
                            <Link to={Config.RouteUrls.Admin.Tokens.Add} className="btn btn-sm btn-theme">
                                <i className="fa fa-plus position-left"></i>
                                Add Token
                            </Link>
                        </div>
                    </div>
                </div>

                <div className="form-group">
                    <p>
                        These tokens are attached with rewards, which are paid to the user. They determine the value & kind of rewards.
                    </p>
                </div>

                <AlertBox {...this.state.alert}/>

                <div className="table-responsive">
                    <table className="table table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Token ID</th>
                            <th>Name</th>
                            <th>Symbol</th>
                            <th>Is Active</th>
                            <th>Actions</th>
                        </tr>
                        </thead>

                        <tbody>
                            {this.state.tokens.map(token => <TokenListItem {...token} />)}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}