import React, {Component} from 'react';
import TokenForm from './TokenForm';
import PageHeaderWithLink from "../PageHeaderWithLink";
import {Config} from "../../../../Config";
import AlertBox from "../../../common/AlertBox";
import VerifyToken from "./TokenVerification";
import ErrorMessage from "../../../common/ErrorMessage";
import SiteTitle from "../../../common/SiteTitle";
import LoadingScreen from "../../../common/LoadingScreen";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';

export default class TokenEditPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            tokenName: '',
            tokenSymbol: '',
            isActive: false,
            isLoading: true,
            isFound: false,
            url: '',
            alert: {
                show: false,
                status: '',
                message: ''
            },
        };

        this.onInputChange = this.onInputChange.bind(this);
        this.handleCheckbox = this.handleCheckbox.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });

        window.scrollTo(0,0);
    }

    componentDidMount() {
        let tokenId = this.props.match.params.id;
        if (tokenId == null || tokenId == undefined) {
            this.showAlert(true, 'error', 'Please specify a token id.');
            this.setState({ isLoading: false });
        }

        let url = `${getApi(Config.API.Tokens)}/${tokenId}`;

        axios.get(url)
            .then(res => {
                this.setState({
                    tokenName: res.data.name,
                    tokenSymbol: res.data.symbol,
                    isActive: res.data.isActive,
                    isLoading: false,
                    isFound: true
                });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true)
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true)

                this.setState({
                    isLoading: false
                });
            });
    }

    onInputChange(event) {
        this.setState({ [event.target.name] : event.target.value });
    }

    handleCheckbox(event) {
        this.setState({ isActive: event.target.checked });
    }

    onFormSubmit(event) {
        event.preventDefault();

        let errors = VerifyToken(this.state.tokenName, this.state.tokenSymbol);

        if (errors.length > 0) {
            let errorMessage = <ErrorMessage errors={errors}/>

            this.showAlert(true, 'error', errorMessage)
        }
        else {
            let payload = {
                name: this.state.tokenName,
                symbol: this.state.tokenSymbol,
                isActive: this.state.isActive
            };

            this.setState({ isLoading: true });

            let tokenId = this.props.match.params.id;
            let url = `${getApi(Config.API.Tokens)}/${tokenId}`;

            axios.put(url, payload)
                .then(res => {
                    this.showAlert(true, 'success', `Token ${this.state.tokenName} (${this.state.tokenSymbol}) updated successfully.`);
                })
                .catch(error => {
                    if (error.response)
                        this.showAlert(true, 'error', error.response.data.toString(), true);
                    else if (error.request)
                        this.showAlert(true, 'error', 'Unable to connect to server', true)
                    else
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true)

                });

            this.setState({ isLoading: false });
        }
    }

    render() {
        return (
            <div>
                <SiteTitle title="Edit Token" />
                <LoadingScreen show={this.state.isLoading} />

                <PageHeaderWithLink title="Edit Token" btnText="Back to Tokens list" btnIcon="fa fa-arrow-left" btnUrl={Config.RouteUrls.Admin.Tokens.Home}/>
                <div className="form-group">
                    <p>
                        You can use this form to edit existing tokens.
                    </p>
                </div>

                <div className="row">
                    <div className="col-md-7">
                        <AlertBox {...this.state.alert} />
                        {
                            this.state.isFound ?
                                <TokenForm
                                    onFormSubmit={this.onFormSubmit}
                                    tokenName={this.state.tokenName}
                                    tokenSymbol={this.state.tokenSymbol}
                                    isActive={this.state.isActive}
                                    onInputChange={this.onInputChange}
                                    handleCheckbox={this.handleCheckbox}
                                    btnText="Edit Token" />
                                :
                                <div></div>
                        }
                    </div>
                </div>
            </div>
        );
    }
}