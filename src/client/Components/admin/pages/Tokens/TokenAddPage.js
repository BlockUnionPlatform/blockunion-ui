import React, {Component} from 'react';
import PageHeaderWithLink from '../PageHeaderWithLink';
import AlertBox from '../../../common/AlertBox';
import ErrorMessage from '../../../common/ErrorMessage';
import VerifyToken from './TokenVerification';
import TokenForm from './TokenForm';
import {Config} from '../../../../Config';
import SiteTitle from "../../../common/SiteTitle";
import LoadingScreen from "../../../common/LoadingScreen";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';

export default class TokenAddPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            tokenName: '',
            tokenSymbol: '',
            isActive: true,
            alert: {
                show: false,
                status: '',
                message: '',
                isSmall: false
            },
            isLoading: false
        };

        this.onInputChange = this.onInputChange.bind(this);
        this.handleCheckbox = this.handleCheckbox.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '', isSmall = false) {
        this.setState({
            alert: { show, status, message, isSmall }
        });

        window.scrollTo(0,0);
    }

    onInputChange(event) {
        this.setState({ [event.target.name] : event.target.value });
    }

    handleCheckbox(event) {
        this.setState({ isActive: event.target.checked });
    }

    onFormSubmit(event) {
        event.preventDefault();

        let errors = VerifyToken(this.state.tokenName, this.state.tokenSymbol);

        if (errors.length > 0) {
            let message = <ErrorMessage errors={errors}/>
            this.showAlert(true, 'error', message);
        }
        else {
            let url = getApi(Config.API.Tokens);
            let payload = {
                name: this.state.tokenName,
                symbol: this.state.tokenSymbol,
                isActive: this.state.isActive
            };

            this.setState({ isLoading: true });

            axios.post(url, payload)
                .then(res => {
                    console.log(res);
                    this.showAlert(true, 'success', 'New token created successfully');
                })
                .catch(error => {
                    console.log(error)
                        if (error.response)
                            this.showAlert(true, 'error', 'There was a problem with the data you sent. Please verify it and try again.', true);
                        else if (error.request)
                            this.showAlert(true, 'error', 'Unable to connect to server', true)
                        else
                            this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true)
                });

            this.setState({ isLoading: false });
        }
    }

    render() {
        return (
            <div>
                <SiteTitle title="Add Token" />
                <LoadingScreen show={this.state.isLoading} />
                <PageHeaderWithLink title="Add Token" btnText="Back to Tokens list" btnIcon="fa fa-arrow-left" btnUrl={Config.RouteUrls.Admin.Tokens.Home}/>
                <div className="form-group">
                    <p>
                        You can use this form to create new tokens.
                    </p>
                </div>

                <div className="row">
                    <div className="col-md-7">
                        <AlertBox {...this.state.alert} />
                        <TokenForm
                            onFormSubmit={this.onFormSubmit}
                            tokenName={this.state.tokenName}
                            tokenSymbol={this.state.tokenSymbol}
                            isActive={this.state.isActive}
                            onInputChange={this.onInputChange}
                            handleCheckbox={this.handleCheckbox} />
                    </div>
                </div>
            </div>
        );
    }
}