import React, {Component} from 'react';
import {Config} from "../../../../Config";
import axios from 'axios';
import SiteTitle from "../../../common/SiteTitle";
import LoadingScreen from "../../../common/LoadingScreen";
import PageHeaderWithLink from "../PageHeaderWithLink";
import {getApi} from "../../../../ConfigStore";
import AlertBox from "../../../common/AlertBox";
import StatusBadge from "../../../common/StatusBadge";

export default class TokenView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            token: null,
            isLoading: true,
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(status, success, message) {
        this.setState({ alert: { status, success, message } });
    }

    componentDidMount() {
        let url = `${getApi(Config.API.Tokens)}/${this.props.match.params.id}`;

        axios.get(url)
            .then(res => {
                this.setState({
                    token: res.data,
                    isLoading: false
                });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data, true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title = "View Token" />
                <PageHeaderWithLink title = "View Token" btnIcon="fa fa-arrow-left" btnText="Back to Tokens" btnUrl={Config.RouteUrls.Admin.Tokens.Home} />
                <AlertBox {...this.state.alert} />
                {
                    !this.state.isLoading && this.state.token != null &&
                    <div>
                        <div className="form-group row">
                            <label className="control-label col-md-2 text-semibold">Token ID:</label>
                            <div className="col-md-10">{this.state.token.id}</div>
                        </div>

                        <div className="form-group row">
                            <label className="control-label col-md-2 text-semibold">Name:</label>
                            <div className="col-md-10">{this.state.token.name}</div>
                        </div>

                        <div className="form-group row">
                            <label className="control-label col-md-2 text-semibold">Symbol:</label>
                            <div className="col-md-10">{this.state.token.symbol}</div>
                        </div>

                        <div className="form-group row">
                            <label className="control-label col-md-2 text-semibold">Is Active?</label>
                            <div className="col-md-10">
                                <StatusBadge isActive={this.state.token.isActive} />
                            </div>
                        </div>
                    </div>
                }
            </div>
        );
    }
}