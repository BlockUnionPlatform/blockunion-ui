import React, {Component} from 'react';
import {Config} from "../../../../Config";
import Link from "react-router-dom/es/Link";

export default class ReferralCard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <form action="#" className="form-horizontal">
                <div className="form-group text-bold row">
                    <label className="control-label col-md-2">ID:</label>
                    <div className="col-md-6">{this.props.referral.id}</div>
                </div>

                <div className="form-group text-bold row">
                    <label className="control-label col-md-2">Referral By:</label>
                    <div className="col-md-6">
                        <Link to={`${Config.RouteUrls.Admin.Users.View}/${this.props.referral.referral.user.id}`} className="link">
                            {this.props.referral.referral.user.name} ({this.props.referral.referral.user.email})
                        </Link>
                    </div>
                </div>

                <div className="form-group text-bold row">
                    <label className="control-label col-md-2">Referred:</label>
                    <div className="col-md-6">
                        <Link to={`${Config.RouteUrls.Admin.Users.View}/${this.props.referral.referred.id}`} className="link">
                            {this.props.referral.referred.name} ({this.props.referral.referred.email})
                        </Link>
                    </div>
                </div>

                <div className="form-group text-bold row">
                    <label className="control-label col-md-2">Date:</label>
                    <div className="col-md-6">{this.props.referral.dateCreatedUtc}</div>
                </div>
            </form>
        );
    }
}