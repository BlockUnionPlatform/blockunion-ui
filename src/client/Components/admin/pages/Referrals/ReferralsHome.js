import React, {Component} from 'react';
import SiteTitle from "../../../common/SiteTitle";
import PageHeaderWithLink from "../PageHeaderWithLink";
import {Repository} from "../../../../Data/DataStore";
import UserLink from "../Common/UserLink";
import {Link} from "react-router-dom";
import {Config} from "../../../../Config";

export default class ReferralsHome extends Component {
    constructor(props) {
        super(props);

        this.state = {
            referrals: [],
            isLoading: true
        };
    }

    componentWillMount() {
        let referrals = Repository.Referrals;

        this.setState({ referrals, isLoading: false });
    }

    render() {
        return(
            <div>
                <SiteTitle title="Referrals"/>
                <PageHeaderWithLink title="View Referrals" />

                <div className="row">
                    <div className="col-md-12">
                        <div className="table-responsive">
                            {
                                this.state.isLoading ?
                                    <div>Loading</div>
                                    :
                                    <div>
                                        <table className="table table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Referral</th>
                                                <th>Reffered</th>
                                                <th>Date</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                                {/*
                                                    {this.state.referrals.map((referral) =>
                                                    <tr>
                                                        <td>
                                                            <Link className="link" to={`${Config.RouteUrls.Admin.Referrals}/${referral.id}`}>
                                                                {referral.id.substr(0, 5)}...
                                                            </Link>
                                                        </td>
                                                        <td>
                                                            <UserLink id={referral.referral.user.id} name={referral.referral.user.name}/>
                                                        </td>
                                                        <td>
                                                            <UserLink id={referral.referred.id} name={referral.referred.name}/>
                                                        </td>
                                                        <td>
                                                            {referral.dateCreatedUtc}
                                                        </td>
                                                        <td>
                                                            <Link className="link" to={`${Config.RouteUrls.Admin.Referrals}/${referral.id}`}>
                                                                View
                                                            </Link>
                                                        </td>
                                                    </tr>
                                                )}
                                                */}
                                            </tbody>
                                        </table>
                                        <p className="text-center">No referrals to show right now.</p>
                                    </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}