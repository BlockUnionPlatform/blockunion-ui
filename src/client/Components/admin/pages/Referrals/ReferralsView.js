import React, {Component} from 'react';
import {Repository} from "../../../../Data/DataStore";
import AlertBox from "../../../common/AlertBox";
import SiteTitle from "../../../common/SiteTitle";
import PageHeaderWithLink from "../PageHeaderWithLink";
import {Config} from "../../../../Config";
import ReferralCard from "./ReferralCard";

export default class ReferralsView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            referral: {},
            isLoading: true,
            isFound: true
        };
    }

    componentWillMount() {
        let referral = Repository.Referrals.find(r => r.id === this.props.match.params.id);

        this.setState({ isLoading: false });

        if (referral !== undefined) {
            this.setState({ referral });
        }
        else {
            this.setState({ isFound: false });
        }
    }

    render() {
        return (
            <div>
                <SiteTitle title="View Referral"/>
                <PageHeaderWithLink title="View Referral" btnText="Back to Referrals" btnIcon="fa fa-arrow-left" btnUrl={Config.RouteUrls.Admin.Referrals}/>
                {
                    this.state.isLoading ?
                        <div>Loading</div>
                        :
                        <div>
                            {
                                !this.state.isFound ?
                                    <div>
                                        <AlertBox isVisible={true} status="error" message="No referral found for the specified id." />
                                    </div>
                                    :
                                    <div>
                                        <ReferralCard referral={this.state.referral}/>
                                    </div>
                            }
                        </div>
                }
            </div>
        );
    }
}