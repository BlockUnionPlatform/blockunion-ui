import React, {Component} from 'react';
import {Repository} from "../../../../Data/DataStore";
import Select from "react-select";
import TokenDropdown from "../Common/TokenDropdown";

export default class BlockAccountsForm extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <form action="#" onSubmit={this.props.onFormSubmit}>
                <div className="form-group">
                    <label className="control-label text-semibold">Token:</label>
                    <TokenDropdown onChange={this.props.onTokenSelectChange} value={this.props.token} />
                </div>

                <div className="form-group">
                    <label className="control-label text-semilbold">Title</label>
                    <input type="text" name="title" id="title" className="form-control" value={this.props.title} onChange={this.props.onInputChange} />
                </div>

                <div className="form-group">
                    <label className="control-label text-semilbold">Description</label>
                    <textarea name="description" id="description" className="form-control" rows="6" value={this.props.description} onChange={this.props.onInputChange}></textarea>
                </div>

                <div className="form-group">
                    <label className="control-label text-semilbold">Is Active?</label>
                    <input type="checkbox" name="isActive" id="isActive" className="position-right" checked={this.props.isActive} onChange={this.props.onActiveChange} />
                </div>

                <div className="form-group pull-right">
                    <button className="btn btn-theme" type="submit">{this.props.btnText}</button>
                </div>
            </form>
        );
    }

}