import React, {Component} from 'react';
import GuidLink from "../Common/GuidLink";
import {Config} from "../../../../Config";
import {Link} from "react-router-dom";
import StatusBadge from "../../../common/StatusBadge";
import moment from "moment";

export default class BlockAccountRow extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <tr key={this.props.account.id} >
                <td>
                    <GuidLink url={Config.RouteUrls.Admin.BlockToken.Accounts.View} id={this.props.account.id} />
                </td>

                <td>
                    {this.props.account.title}
                </td>

                <td>
                    {this.props.account.description.substr(0, 10)}...
                </td>

                <td>
                    {this.props.account.balance.toLocaleString()} {this.props.account.token.symbol}
                </td>

                <td>
                    <StatusBadge isActive={this.props.account.isActive} />
                </td>

                <td>
                    {moment(this.props.account.dateCreatedUtc).format(Config.Formats.DateTime)}
                </td>

                <td>
                    <Link to={`${Config.RouteUrls.Admin.BlockToken.Accounts.View}/${this.props.account.id}`} className="link">
                        View
                    </Link>

                    &nbsp;|&nbsp;

                    <Link to={`${Config.RouteUrls.Admin.BlockToken.Accounts.Edit}/${this.props.account.id}`} className="link">
                        Edit
                    </Link>
                </td>
            </tr>
        );
    }
}