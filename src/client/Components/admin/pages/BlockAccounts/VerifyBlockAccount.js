export default function VerifyBlockAccount(title, token) {
    let errors = [];

    if (title.length < 3 || title.length > 100)
        errors.push('Title should be at between 3 and 100 characters  in length.');

    if (token === undefined || token === '')
        errors.push('Please select a token.');

    return errors;
}