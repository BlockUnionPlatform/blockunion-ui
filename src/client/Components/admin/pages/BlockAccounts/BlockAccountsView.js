import React, {Component} from 'react';
import SiteTitle from "../../../common/SiteTitle";
import PageHeaderWithLink from "../PageHeaderWithLink";
import {Config} from "../../../../Config";
import AlertBox from "../../../common/AlertBox";
import BlockAccountsCard from "./BlockAccountsCard";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';
import LoadingScreen from "../../../common/LoadingScreen";

export default class BlockAccountsView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            account: {},
            isLoading: true,
            isFound: true,
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });
    }

    componentWillMount() {
        let url = `${getApi(Config.API.Block.Accounts)}/${this.props.match.params.id}`;

        axios.get(url)
            .then(res => {
                this.setState({
                    account: res.data,
                    isLoading: false,
                    isFound: true
                });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false, isFound: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title="View Block Account" />
                {
                    this.state.isLoading ?
                        <div></div>
                        :
                        <div>
                            {
                                !this.state.isFound ?
                                    <div></div>
                                    :
                                    <div>
                                        <PageHeaderWithLink
                                            title="View Block Account"
                                            btnText="Edit Block Account"
                                            btnIcon="fa fa-pencil"
                                            btnUrl={`${Config.RouteUrls.Admin.BlockToken.Accounts.Edit}/${this.state.account.id}`}
                                        />
                                        <div className="row">
                                            <div className="col-md-7">
                                                <BlockAccountsCard account={this.state.account} />
                                            </div>
                                        </div>
                                    </div>
                            }
                        </div>
                }

                <AlertBox {...this.state.alert} />
            </div>
        );
    }
}