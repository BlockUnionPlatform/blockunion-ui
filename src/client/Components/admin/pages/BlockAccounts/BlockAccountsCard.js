import React, {Component} from 'react';
import TokenLink from "../Common/TokenLink";
import {Link} from "react-router-dom";
import {Config} from "../../../../Config";
import StatusBadge from "../../../common/StatusBadge";
import moment from "moment";

export default class BlockAccountsCard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <form className="form-horizontal">
                <div className="form-group row">
                    <label className="col-md-2 control-label text-semibold">ID:</label>
                    <div className="col-md-10">
                        {this.props.account.id}
                    </div>
                </div>

                <div className="form-group row">
                    <label className="col-md-2 control-label text-semibold">Title:</label>
                    <div className="col-md-10">
                        {this.props.account.title}
                    </div>
                </div>

                <div className="form-group row">
                    <label className="col-md-2 control-label text-semibold">Balance:</label>
                    <div className="col-md-10">
                        {this.props.account.balance.toLocaleString()} {this.props.account.token.symbol}
                    </div>
                </div>

                <div className="form-group row">
                    <label className="col-md-2 control-label text-semibold">Token:</label>
                    <div className="col-md-10">
                        <Link className="link" to={`${Config.RouteUrls.Admin.Tokens.View}/${this.props.account.token.id}`}>
                            {this.props.account.token.name} ({this.props.account.token.symbol})
                        </Link>
                    </div>
                </div>

                <div className="form-group row">
                    <label className="col-md-2 control-label text-semibold">Active?</label>
                    <div className="col-md-10">
                        <StatusBadge isActive={this.props.account.isActive} />
                    </div>
                </div>

                <div className="form-group row">
                    <label className="col-md-2 control-label text-semibold">Date Created:</label>
                    <div className="col-md-10">
                        {moment(this.props.account.dateCreatedUtc).format(Config.Formats.DateTime)}
                    </div>
                </div>

                <div className="form-group row">
                    <label className="col-md-2 control-label text-semibold">Description:</label>
                    <div className="col-md-10">
                        <p>
                            {this.props.account.description}
                        </p>
                    </div>
                </div>
            </form>
        );
    }
}