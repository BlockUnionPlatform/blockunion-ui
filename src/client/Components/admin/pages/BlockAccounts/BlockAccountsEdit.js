import React, {Component} from 'react';
import SiteTitle from "../../../common/SiteTitle";
import PageHeaderWithLink from "../PageHeaderWithLink";
import {Config} from "../../../../Config";
import BlockAccountsForm from "./BlockAccountsForm";
import AlertBox from "../../../common/AlertBox";
import VerifyBlockAccount from "./VerifyBlockAccount";
import ErrorMessage from "../../../common/ErrorMessage";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';
import {GetTokenDropdownItem} from "../Common/TokenFunctions";
import LoadingScreen from "../../../common/LoadingScreen";

export default class BlockAccountsEdit extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: '',
            description: '',
            token: null,
            isActive: true,
            isFound: true,
            isLoading: true,
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.onTokenSelectChange = this.onTokenSelectChange.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });

        window.scrollTo(0,0);
    }

    onInputChange(event) {
        this.setState({ [event.target.name] : event.target.value });
    }

    onTokenSelectChange(selectedValue) {
        this.setState({ token: selectedValue });
    }

    onActiveChange(event) {
        this.setState({ isActive: event.target.checked });
    }

    onFormSubmit(event) {
        event.preventDefault();

        let errors = VerifyBlockAccount(this.state.title, this.state.token.value);

        if (errors.length > 0) {
            let message = <ErrorMessage errors={errors}/>;
            this.showAlert(true, 'error', message);
        }
        else {
            let url = `${getApi(Config.API.Block.Accounts)}/${this.props.match.params.id}`;
            let payload = {
                title: this.state.title,
                description: this.state.description,
                tokenId: this.state.token.value,
                isActive: this.state.isActive
            };

            this.setState({ isLoading: true });

            axios.put(url, payload)
                .then(res => {
                    this.showAlert(true, 'success', 'Block account updated successfully.');
                    this.setState({ isLoading: false });
                })
                .catch(error => {
                    if (error.response)
                        this.showAlert(true, 'error', error.response.data.toString(), true);
                    else if (error.request)
                        this.showAlert(true, 'error', 'Unable to connect to server', true);
                    else
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                    this.setState({ isLoading: false });
                });
        }
    }

    componentWillMount() {
        let url = `${getApi(Config.API.Block.Accounts)}/${this.props.match.params.id}`;

        axios.get(url)
            .then(res => {
                this.setState({
                    title: res.data.title,
                    description: res.data.description,
                    token: GetTokenDropdownItem(res.data.token),
                    isActive: res.data.isActive,
                    isLoading: false,
                    isFound: true
                });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false, isFound: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title="Edit Block Account" />
                <PageHeaderWithLink title="Edit Block Account" btnText="Back to Block Accounts" btnIcon="fa fa-arrow-left" btnUrl={Config.RouteUrls.Admin.BlockToken.Accounts.View} />
                <p className="form-group">You can use this form to update a block account.</p>

                <div className="row">
                    <div className="col-md-6">
                        <AlertBox {...this.state.alert} />
                        {
                            this.state.isLoading ?
                                <div></div>
                                :
                                <div>
                                    {
                                        !this.state.isFound ?
                                            <div></div>
                                            :
                                            <BlockAccountsForm
                                                {...this.state}
                                                onFormSubmit = {this.onFormSubmit}
                                                onInputChange = {this.onInputChange}
                                                onTokenSelectChange={this.onTokenSelectChange}
                                                onActiveChange={this.onActiveChange.bind(this)}
                                                btnText="Update Block Account"
                                            />
                                    }
                                </div>
                        }
                    </div>
                </div>
            </div>
        );
    }
}