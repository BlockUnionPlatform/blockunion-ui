import React, {Component} from 'react';
import SiteTitle from "../../../common/SiteTitle";
import PageHeaderWithLink from "../PageHeaderWithLink";
import {Config} from "../../../../Config";
import BlockAccountRow from "./BlockAccountRow";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';
import AlertBox from "../../../common/AlertBox";
import LoadingScreen from "../../../common/LoadingScreen";

export default class BlockAccountsHome extends Component {
    constructor(props) {
        super(props);

        this.state = {
            blockAccounts: [],
            isLoading: true,
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });
    }

    componentWillMount() {
        let url = getApi(Config.API.Block.Accounts);

        axios.get(url)
            .then(res => {
                this.setState({
                    blockAccounts: res.data,
                    isLoading: false
                });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title="Block Accounts" />
                <PageHeaderWithLink title="View Block Accounts" btnText="Add Block Account" btnIcon="fa fa-plus" btnUrl={Config.RouteUrls.Admin.BlockToken.Accounts.Add}/>
                <p className="form-group">You can view all block token accounts here.</p>
                <AlertBox {...this.state.alert} />
                {
                    this.state.isLoading ?
                        <div></div>
                        :
                        <div className="row">
                            <div className="col-md-12">
                                <div className="table-responsive">
                                    <table className="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Title</th>
                                                <th>Description</th>
                                                <th>Balance</th>
                                                <th>Active</th>
                                                <th>Date Created</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            {
                                                this.state.blockAccounts.map((account) =>
                                                    <BlockAccountRow account={account} />
                                                )
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                }
            </div>
        );
    }
}