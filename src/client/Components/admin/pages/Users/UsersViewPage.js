import React, {Component} from 'react';
import AlertBox from "../../../common/AlertBox";
import PageHeaderWithLink from "../PageHeaderWithLink";
import {Config} from "../../../../Config";
import UsersCard from "./UsersCard";
import SiteTitle from "../../../common/SiteTitle";
import LoadingScreen from "../../../common/LoadingScreen";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';

export default class UsersViewPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {},
            isFound: true,
            isLoading: true,
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });
    }

    componentDidMount() {
        let id = this.props.match.params.id;
        let url = `${getApi(Config.API.Users.Default)}/${id}`;

        axios.get(url)
            .then(res => {
                this.setState({
                    user: res.data,
                    isLoading: false
                });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false, isFound: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title = "View User" />
                <PageHeaderWithLink title="View User" btnText="Edit User" btnIcon="fa fa-pencil" btnUrl={`${Config.RouteUrls.Admin.Users.Edit}/${this.state.user.guid}`} />

                <div className="row">
                    <div className="col-md-6">
                        <AlertBox {...this.state.alert} />
                    </div>
                </div>

                {
                    this.state.isLoading ?
                        <div></div>
                        :
                        <div>
                            {
                                !this.state.isFound ?
                                    <div></div>
                                    :
                                    <div className="row">
                                        <div className="col-md-12">
                                            <UsersCard user={this.state.user} />
                                        </div>
                                    </div>
                            }
                        </div>
                }
            </div>
        );
    }
}