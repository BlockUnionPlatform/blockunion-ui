import React from 'react';

export default function UserAccessLevelBadge(props) {
    let badge = <span></span>;

    if (props.level == 10)
        badge = <span className="badge badge-primary">Admin</span>;

    if (props.level == 7)
        badge = <span className="badge badge-warning">Management</span>;

    if (props.level == 5)
        badge = <span className="badge badge-primary">Sales</span>;

    if (props.level == 3)
        badge = <span className="badge badge-info">Moderator</span>;

    if (props.level == 1)
        badge = <span className="badge badge-default">User</span>;

        return badge;
}