import React from 'react';
import Select from "react-select";
import UserLevels from './UserAccessLevels';

export default function UserAccessLevelDropdown(props) {
    let options = [];

    UserLevels.map(level => options.push({ value: level.Level, label: `${level.Level} - ${level.Label}` }));

    return (
        <Select options={options} onChange={props.onChange} value={props.value} />
    );
}