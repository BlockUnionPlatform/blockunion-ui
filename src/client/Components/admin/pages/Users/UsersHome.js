import React, {Component} from 'react';
import {Config} from "../../../../Config";
import PageHeaderWithLink from "../PageHeaderWithLink";
import SiteTitle from "../../../common/SiteTitle";
import LoadingScreen from "../../../common/LoadingScreen";
import AlertBox from "../../../common/AlertBox";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';
import UsersRow from "./UsersRow";

export default class UsersHome extends Component {
    constructor(props) {
        super(props);

        this.state = {
            users: [],
            isLoading: true,
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });
    }

    componentDidMount() {
        let url = getApi(Config.API.Users.Default);

        axios.get(url)
            .then(res => {
                this.setState({
                    users: res.data,
                    isLoading: false
                });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title = "Users" />
                <PageHeaderWithLink title="View Users" btnText="Add User" btnIcon="fa fa-plus" btnUrl={Config.RouteUrls.Admin.Users.Add}/>
                <div className="form-group">
                    This is a list of all registered users on BlockUnion.
                </div>

                <AlertBox {...this.state.alert} />

                {
                    this.state.isLoading ?
                        <div></div>
                        :
                        <div className="row">
                            <div className="col-md-12">
                                <div className="table-responsive">
                                    <table className="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Email Address</th>
                                            <th>Phone</th>
                                            <th>Access</th>
                                            <th>Is Active?</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                            { this.state.users.map(user => <UsersRow user={user} />) }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                }
            </div>
        );
    }
}