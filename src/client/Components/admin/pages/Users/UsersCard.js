import React from 'react';
import {Config} from "../../../../Config";
import moment from "moment";
import UserAccessLevelBadge from "./UserAccessLevelBadge";

export default function UsersCard(props) {
    return (
        <form action="#" className="mt-20">
            <div className="form-group row">
                <label className="text-semibold control-label col-md-2">ID:</label>
                <div className="col-md-10">
                    {props.user.id}
                </div>
            </div>

            <div className="form-group row">
                <label className="text-semibold control-label col-md-2">Name:</label>
                <div className="col-md-10">
                    {props.user.firstName} {props.user.lastName}
                </div>
            </div>

            <div className="form-group row">
                <label className="text-semibold control-label col-md-2">Email Address:</label>
                <div className="col-md-10">
                    {props.user.emailAddress}
                </div>
            </div>

            <div className="form-group row">
                <label className="text-semibold control-label col-md-2">Phone:</label>
                <div className="col-md-10">
                    {props.user.phoneNumber}
                </div>
            </div>

            <div className="form-group row">
                <label className="text-semibold control-label col-md-2">Wallet:</label>
                <div className="col-md-10">
                    {props.user.wallet}
                </div>
            </div>

            <div className="form-group row">
                <label className="text-semibold control-label col-md-2">Account Active:</label>
                <div className="col-md-10">
                    {
                        !props.user.isBlocked ?
                            <span className="badge badge-success">Active</span>
                            :
                            <span className="badge badge-danger">Blocked</span>
                    }
                </div>
            </div>

            <div className="form-group row">
                <label className="text-semibold control-label col-md-2">Access Level:</label>
                <div className="col-md-10">
                    <UserAccessLevelBadge level={props.user.accessLevel} />
                </div>
            </div>

            <div className="form-group row">
                <label className="text-semibold control-label col-md-2">Date Created:</label>
                <div className="col-md-10">
                    {moment(props.user.dateCreatedUtc).format(Config.Formats.DateTime)}
                </div>
            </div>

            <div className="form-group row">
                <label className="text-semibold control-label col-md-2">Date Modified:</label>
                <div className="col-md-10">
                    {moment(props.user.dateModifiedUtc).format(Config.Formats.DateTime)}
                </div>
            </div>
        </form>
    );
}