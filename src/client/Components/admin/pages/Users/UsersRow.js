import React from 'react';
import GuidLink from "../Common/GuidLink";
import {Config} from "../../../../Config";
import Link from "react-router-dom/es/Link";
import UserLevels from './UserAccessLevels';

export default function UsersRow(props) {
    return (
        <tr key={props.user.id} >
            <td>
                <GuidLink url={Config.RouteUrls.Admin.Users.View} id={props.user.id} />
            </td>
            <td>
                {props.user.firstName} {props.user.lastName}
            </td>
            <td>{props.user.emailAddress}</td>
            <td>{props.user.phoneNumber}</td>
            <td>
                {UserLevels.find(u => u.Level == props.user.accessLevel).Label}
            </td>
            <td>{!props.user.isBlocked ? <span className="badge badge-success">Active</span> : <span className="badge badge-danger">Blocked</span>}</td>
            <td>
                <Link className="link" to={`${Config.RouteUrls.Admin.Users.Edit}/${props.user.id}`}>Edit</Link> &nbsp;
                <Link className="link" to={`${Config.RouteUrls.Admin.Users.View}/${props.user.id}`}>View</Link>
            </td>
        </tr>
    );
}