import React, {Component} from 'react';
import PageHeaderWithLink from "../PageHeaderWithLink";
import {Config} from "../../../../Config";
import UserForm from './UserForm';
import AlertBox from "../../../common/AlertBox";
import VerifyUser from './VerifyUser';
import ErrorMessage from "../../../common/ErrorMessage";
import SiteTitle from "../../../common/SiteTitle";
import LoadingScreen from "../../../common/LoadingScreen";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';

export default class UsersAddPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            firstName: '',
            lastName: '',
            emailAddress: '',
            phoneNumber: '',
            password: '',
            repeatPassword: '',
            accessLevel: null,
            passwordRequired: true,
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.onInputChange = this.onInputChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.showAlert = this.showAlert.bind(this);
        this.onLevelChange = this.onLevelChange.bind(this);
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });

        window.scrollTo(0,0);
    }

    onLevelChange(selectedValue) {
        this.setState({ accessLevel: selectedValue.value });
    }

    onInputChange(event) {
        this.setState({ [event.target.name] : event.target.value });
    }

    onFormSubmit(event) {
        event.preventDefault();

        let errors = VerifyUser({ ...this.state });

        if (errors.length > 0) {
            let errorMessage = <ErrorMessage errors={errors}/>
            this.showAlert(true, 'error', errorMessage);
        }
        else {
            let url = getApi(Config.API.Users.Default);
            let payload = {
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                emailAddress: this.state.emailAddress,
                password: this.state.password,
                repeatPassword: this.state.repeatPassword,
                accessLevel: this.state.accessLevel,
                phoneNumber: this.state.phoneNumber
            };

            this.setState({ isLoading: true });

            axios.post(url, payload)
                .then(res => {
                    this.setState({ isLoading: false });
                    this.showAlert(true, 'success', `User successfully created with id: ${res.data.id}`);
                })
                .catch(error => {
                    if (error.response)
                        this.showAlert(true, 'error', error.response.data.toString(), true);
                    else if (error.request)
                        this.showAlert(true, 'error', 'Unable to connect to server', true);
                    else
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                    this.setState({ isLoading: false });
                });
        }
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title = "Add User" />
                <PageHeaderWithLink title="Add User" btnText="Back to Users" btnIcon="fa fa-arrow-left" btnUrl={Config.RouteUrls.Admin.Users.Home}/>
                <div className="form-group">
                    You can use this view to create a new user.
                </div>

                <div className="row">
                    <div className="col-md-7">
                        <AlertBox {...this.state.alert} />
                        <UserForm
                            {...this.state}
                            onFormSubmit={this.onFormSubmit}
                            btnText="Add User"
                            onInputChange={this.onInputChange}
                            onLevelChange={this.onLevelChange}
                            showDates={false}
                        />
                    </div>
                </div>
            </div>
        );
    }
}