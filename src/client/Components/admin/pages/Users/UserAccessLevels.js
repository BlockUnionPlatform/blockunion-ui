let UserLevels = [
    { Level: 1, Label: "User" },
    { Level: 3, Label: "Moderator" },
    { Level: 5, Label: "Sales" },
    { Level: 7, Label: "Management" },
    { Level: 10, Label: "Admin" },
];

export default UserLevels;