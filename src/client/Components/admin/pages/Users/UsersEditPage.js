import React, {Component} from 'react';
import VerifyUser from "./VerifyUser";
import ErrorMessage from "../../../common/ErrorMessage";
import {Config} from "../../../../Config";
import PageHeaderWithLink from "../PageHeaderWithLink";
import AlertBox from "../../../common/AlertBox";
import UserForm from "./UserForm";
import SiteTitle from "../../../common/SiteTitle";
import LoadingScreen from "../../../common/LoadingScreen";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';
import ActiveUserLevelItem from "./ActiveUserLevelItem";

export default class UsersEditPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            firstName: '',
            lastName: '',
            emailAddress: '',
            phoneNumber: '',
            isBlocked: false,
            password: '',
            repeatPassword: '',
            accessLevel: null,
            passwordRequired: false,
            alert: {
                show: false,
                status: '',
                message: ''
            },
            isFound: true,
            isLoading: true
        };

        this.onInputChange = this.onInputChange.bind(this);
        this.handleCheckbox = this.handleCheckbox.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.showAlert = this.showAlert.bind(this);
        this.onLevelChange = this.onLevelChange.bind(this);
    }

    onLevelChange(selectedValue) {
        this.setState({
            accessLevel: selectedValue
        });
    }

    showAlert(show, status = 'error', message = '') {
        this.setState({
            alert: { show, status, message}
        });

        window.scrollTo(0,0);
    }

    onInputChange(event) {
        this.setState({ [event.target.name] : event.target.value });
    }

    handleCheckbox(event) {
        this.setState({ isBlocked: event.target.checked });
    }

    onFormSubmit(event) {
        event.preventDefault();

        let errors = VerifyUser({ ...this.state });

        if (errors.length > 0) {
            let errorMessage = <ErrorMessage errors={errors}/>
            this.showAlert(true, 'error', errorMessage);
        }
        else {
            let url = `${getApi(Config.API.Users.Default)}/${this.props.match.params.id}`;
            let payload = {
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                emailAddress: this.state.emailAddress,
                phoneNumber: this.state.phoneNumber,
                isBlocked: this.state.isBlocked,
                accessLevel: this.state.accessLevel.value
            };

            this.setState({ isLoading: true });

            console.log(payload);

            axios.put(url, payload)
                .then(res => {
                    this.showAlert(true, 'success', 'User account updated successfully');
                    this.setState({ isLoading: false });
                })
                .catch(error => {
                    if (error.response)
                        this.showAlert(true, 'error', error.response.data.toString(), true);
                    else if (error.request)
                        this.showAlert(true, 'error', 'Unable to connect to server', true);
                    else
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                    this.setState({ isLoading: false });
                });
        }
    }

    componentDidMount() {
        let id = this.props.match.params.id;
        let url = `${getApi(Config.API.Users.Default)}/${id}`;

        axios.get(url)
            .then(res => {
                this.setState({
                    firstName: res.data.firstName,
                    lastName: res.data.lastName,
                    emailAddress: res.data.emailAddress,
                    phoneNumber: res.data.phoneNumber,
                    isBlocked: res.data.isBlocked,
                    accessLevel: ActiveUserLevelItem(res.data.accessLevel),
                    isLoading: false,
                    isFound: true
                });
            })
            .catch(error => {
                console.log(error);
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false, isFound: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading}/>
                <SiteTitle title = "Edit User" />
                <PageHeaderWithLink title="Edit User" btnText="Back to Users" btnIcon="fa fa-arrow-left" btnUrl={Config.RouteUrls.Admin.Users.Home}/>
                <div className="form-group">
                    You can use this view to create a new user.
                </div>

                <AlertBox {...this.state.alert} />

                {
                    this.state.isLoading ?
                        <div></div>
                        :
                        <div>
                            {
                                !this.state.isFound ?
                                    <div></div>
                                    :
                                    <div className="row">
                                        <div className="col-md-7">
                                            <UserForm
                                                {...this.state}
                                                onFormSubmit={this.onFormSubmit}
                                                btnText="Edit User"
                                                onInputChange={this.onInputChange}
                                                handleCheckbox={this.handleCheckbox}
                                                onLevelChange={this.onLevelChange}
                                                showDates={false}
                                                showPasswords={false}
                                                showActive={true}
                                            />
                                        </div>
                                    </div>
                            }
                        </div>
                }
            </div>
        );
    }
}