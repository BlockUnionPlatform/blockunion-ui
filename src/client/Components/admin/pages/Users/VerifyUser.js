export default function VerifyUser(user) {
    let errors = [];

    if (user.firstName.length < 2)
        errors.push('First Name must be at least 2 characters long');

    if (user.lastName.length < 2)
        errors.push('Last Name must be at least 2 characters long');

    if (!validateEmail(user.emailAddress))
        errors.push('Email Address is not valid');

    if (user.phoneNumber.length < 5)
        errors.push('Phone Number must be at least 5 characters long.');

    if (user.accessLevel == undefined, user.accessLevel == null)
        errors.push('Please select an access level');

    if (user.passwordRequired) {
        if (user.password.length < 3)
            errors.push('Password must be at least 3 characters long');

        if (user.repeatPassword.length < 3)
            errors.push('Repeat Password must be at least 3 characters long');

        if (user.password != user.repeatPassword)
            errors.push('Passwords do not match');
    }

    return errors;
}

function validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}