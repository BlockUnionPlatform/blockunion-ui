import UserLevels from './UserAccessLevels';

export default function ActiveUserLevelItem(levelId) {
    let level = UserLevels.find(l => l.Level == levelId);
    return { value: level.Level, label: `${level.Level} - ${level.Label}` };
}