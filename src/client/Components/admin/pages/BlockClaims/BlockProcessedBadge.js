import React, {Component} from 'react';

export default class BlockProcessedBadge extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let badgeClass = this.props.isProcessed ? 'success' : 'primary';

        return (
            <span className={`badge badge-${badgeClass}`}>
                {this.props.isProcessed ? 'Processed' : 'Pending'}
            </span>
        );
    }
}