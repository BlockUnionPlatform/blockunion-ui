import React, {Component} from 'react';
import BlockProcessedBadge from "./BlockProcessedBadge";
import UserLink from "../Common/UserLink";
import BlockLink from "../Common/BlockLink";
import moment from "moment";
import {Config} from "../../../../Config";

export default class BlockClaimsCard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let isAdmin = this.props.isAdmin !== undefined && this.props.isAdmin;

        return (
            <form action="#">
                <div className="form-group row">
                    <label className="control-label text-semibold col-md-2 col-md-2">ID:</label>
                    <div className = "col-md-10">
                        {this.props.claim.id}
                    </div>
                </div>

                <div className="form-group row">
                    <label className="control-label text-semibold col-md-2">Processed:</label>
                    <div className = "col-md-10">
                        <BlockProcessedBadge isProcessed={this.props.claim.isProcessed} />
                    </div>
                </div>

                <div className="form-group row">
                    <label className="control-label text-semibold col-md-2">User / Payee:</label>
                    <div className = "col-md-10">
                        {
                            isAdmin ?
                                <UserLink user={this.props.claim.user} />
                                :
                                <span>
                                    {this.props.claim.user.name}
                                </span>
                        }
                    </div>
                </div>

                {
                    isAdmin && this.props.claim.admin !== null ?
                        <div className="form-group row">
                            <label className="control-label text-semibold col-md-2">Admin / Processed By:</label>
                            <div className = "col-md-10">
                                <UserLink user={this.props.claim.admin} />
                            </div>
                        </div>
                        :
                        <div></div>
                }

                <div className="form-group row">
                    <label className="control-label text-semibold col-md-2">Block:</label>
                    <div className = "col-md-10">
                        {
                            isAdmin ?
                                <BlockLink block={this.props.claim.block} />
                                :
                                <BlockLink block={this.props.claim.block} url={Config.RouteUrls.Member.Blocks.View} />
                        }
                    </div>
                </div>

                <div className="form-group row">
                    <label className="control-label text-semibold col-md-2">Date Created:</label>
                    <div className = "col-md-10">
                        {moment(this.props.claim.dateCreatedUtc).format(Config.Formats.DateTime)}
                    </div>
                </div>

                {
                    isAdmin &&
                    <div className="form-group row">
                        <label className="control-label text-semibold col-md-2">Notes:</label>
                        <div className = "col-md-10">
                            {this.props.claim.notes}
                        </div>
                    </div>
                }
            </form>
        );
    }
}