import React from 'react';
import GuidLink from "../Common/GuidLink";
import {Config} from "../../../../Config";
import UserLink from "../Common/UserLink";
import BlockLink from "../Common/BlockLink";
import BlockProcessedBadge from "./BlockProcessedBadge";
import {Link} from "react-router-dom";
import moment from "moment";

export default function BlockClaimsRow(props) {
    let isAdmin = props.isAdmin != undefined && props.isAdmin;
    let url = isAdmin ? Config.RouteUrls.Admin.BlockToken.Claims.View : Config.RouteUrls.Member.Blocks.Claims;

    return (
        <tr>
            <td>
                <GuidLink url={url} id={props.claim.id} />
            </td>

            <td>
                {
                    props.isAdmin ?
                        <UserLink user={props.claim.user} />
                        :
                        <span>
                            {props.claim.user.name}
                        </span>
                }
            </td>

            {
                isAdmin &&
                <td>
                    {
                        props.claim.admin != null ?
                            <UserLink user={props.claim.admin} />
                            :
                            <span className="text-center">-</span>
                    }
                </td>
            }

            <td>
                <BlockLink block={props.claim.block} url={Config.RouteUrls.Member.Blocks.View} />
            </td>

            <td>
                <BlockProcessedBadge isProcessed={props.claim.isProcessed} />
            </td>

            <td>
                {moment(props.claim.dateCreatedUtc).format(Config.Formats.DateTime)}
            </td>

            <td>
                <Link className="link" to={`${url}/${props.claim.id}`}>
                    View
                </Link>

                <span>
                    {
                        isAdmin &&
                            <span>
                                {
                                    !props.claim.isProcessed &&
                                    <span>
                                        &nbsp; | &nbsp;

                                        <Link className="link" to={`${Config.RouteUrls.Admin.BlockToken.Claims.Process}/${props.claim.id}`}>
                                            Process
                                        </Link>
                                    </span>
                                }
                            </span>
                    }
                </span>
            </td>
        </tr>
    );
}