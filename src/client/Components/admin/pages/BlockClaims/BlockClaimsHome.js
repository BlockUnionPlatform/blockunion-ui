import React, {Component} from 'react';
import SiteTitle from "../../../common/SiteTitle";
import PageHeaderWithLink from "../PageHeaderWithLink";
import BlockClaimsRow from "./BlockClaimsRow";
import {Config} from "../../../../Config";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';
import LoadingScreen from "../../../common/LoadingScreen";
import AlertBox from "../../../common/AlertBox";

export default class BlockClaimsHome extends Component {
    constructor(props) {
        super(props);

        this.state = {
            claims: [],
            isLoading: true,
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status, message) {
        this.setState({ alert: { show, status, message } });
    }

    componentWillMount() {
        let url = getApi(Config.API.Block.Claims);

        axios.get(url)
            .then(res => {
                this.setState({
                    claims: res.data,
                    isLoading: false
                });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title="Block Claims" />
                <PageHeaderWithLink title="Block Claims" />
                <p>
                    A list of all block claims.
                </p>
                <AlertBox {...this.state.alert} />

                {
                    this.state.isLoading ?
                        <div>Loading</div>
                        :
                        <div className="row">
                            <div className="col-md-12">
                                <div className="table-responsive">
                                    <table className="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>User</th>
                                                <th>Admin</th>
                                                <th>Block</th>
                                                <th>Status</th>
                                                <th>Date Claimed</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                        {
                                            this.state.claims.map((claim) =>
                                                <BlockClaimsRow claim={claim} isAdmin={true} />
                                            )
                                        }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                }
            </div>
        );
    }
}