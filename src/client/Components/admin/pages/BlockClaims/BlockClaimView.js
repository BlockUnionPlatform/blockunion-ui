import React, {Component} from 'react';
import SiteTitle from "../../../common/SiteTitle";
import PageHeaderWithLink from "../PageHeaderWithLink";
import {Config} from "../../../../Config";
import AlertBox from "../../../common/AlertBox";
import BlockClaimsCard from "./BlockClaimsCard";
import axios from 'axios';
import {getApi} from "../../../../ConfigStore";
import LoadingScreen from "../../../common/LoadingScreen";

export default class BlockClaimView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            claim: {},
            isLoading: true,alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status, message) {
        this.setState({ alert: { show, status, message } });
    }

    componentDidMount() {
        let url = `${getApi(Config.API.Block.Claims)}/${this.props.match.params.id}`;

        axios.get(url)
            .then(res => {
                this.setState({
                    isLoading: false,
                    claim: res.data
                })
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });
    }


    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title = "View Block Claim" />
                <PageHeaderWithLink title = "View Block Claim" btnIcon = "fa fa-arrow-left" btnText = "Back to Block Claims" btnUrl={Config.RouteUrls.Admin.BlockToken.Claims.Home} />
                <AlertBox {...this.state.alert} />

                {
                    this.state.isLoading ?
                        <div>Loading</div>
                        :
                        <div className="row">
                            <div className="col-md-12">
                                <BlockClaimsCard claim={this.state.claim} isAdmin={true} />
                            </div>
                        </div>
                }
            </div>
        );
    }
}