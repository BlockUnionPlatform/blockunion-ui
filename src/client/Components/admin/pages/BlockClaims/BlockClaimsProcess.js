import React, {Component} from 'react';
import SiteTitle from "../../../common/SiteTitle";
import PageHeaderWithLink from "../PageHeaderWithLink";
import {Config} from "../../../../Config";
import AlertBox from "../../../common/AlertBox";
import BlockClaimsCard from "./BlockClaimsCard";
import LoadingScreen from "../../../common/LoadingScreen";
import {getApi} from "../../../../ConfigStore";
import axios from 'axios';
import {Auth} from "../../../common/AuthHelpers";

export default class BlockClaimsProcess extends Component {
    constructor(props) {
        super(props);

        this.state = {
            claim: null,
            isLoading: true,
            isProcessed: false,
            notes: '',
            alert: {
                show: false,
                status: '',
                message: ''
            }
        };

        this.onProcessedChange = this.onProcessedChange.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.showAlert = this.showAlert.bind(this);
    }

    showAlert(show, status, message) {
        this.setState({ alert: { show, status, message } });
    }

    onSubmit(event) {
        event.preventDefault();

        if (!this.state.isProcessed) {
            this.setState({
                alert: {
                    isVisible: true,
                    status: "error",
                    message: "Please check the acknowledgement that the transaction has been processed"
                }
            });
        }
        else {
            let url = `${getApi(Config.API.Block.Claims)}/process`
            let payload = {
                adminId: Auth.getUser().id,
                claimId: this.state.claim.id,
                blockId: this.state.claim.block.id,
                notes: this.state.notes
            };

            this.setState({ isLoading: true });

            axios.post(url, payload)
                .then(res => {
                    this.showAlert(true, 'success', 'The block claim has been processed successfully!');
                    this.setState({ isLoading: false });
                })
                .catch(error => {
                    if (error.response)
                        this.showAlert(true, 'error', error.response.data.toString(), true);
                    else if (error.request)
                        this.showAlert(true, 'error', 'Unable to connect to server', true);
                    else
                        this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                    this.setState({ isLoading: false });
                });
        }
    }

    onProcessedChange(event) {
        this.setState({
            isProcessed: event.target.checked
        });
    }

    onInputChange(event) {
        this.setState({
            [event.target.name] : event.target.value
        });
    }

    componentDidMount() {
        let url = `${getApi(Config.API.Block.Claims)}/${this.props.match.params.id}`;

        axios.get(url)
            .then(res => {
                this.setState({
                    isLoading: false,
                    claim: res.data
                });
            })
            .catch(error => {
                if (error.response)
                    this.showAlert(true, 'error', error.response.data.toString(), true);
                else if (error.request)
                    this.showAlert(true, 'error', 'Unable to connect to server', true);
                else
                    this.showAlert(true, 'error', 'Something went wrong. Please contact the site administrators', true);

                this.setState({ isLoading: false });
            });
    }

    render() {
        return (
            <div>
                <LoadingScreen show={this.state.isLoading} />
                <SiteTitle title="Process Block Claim" />
                <PageHeaderWithLink title="Process Block Claim" btnText="Back to Block Claims" btnIcon="fa fa-arrow-left" btnUrl={Config.RouteUrls.Admin.BlockToken.Claims.Home} />
                <AlertBox {...this.state.alert} />

                {
                    this.state.isLoading ?
                        <div>Loading</div>
                        :
                        <div>
                            {
                                this.state.claim != null &&
                                <div className="row mt-50">
                                    <div className="col-md-5">
                                        <AlertBox {...this.state.alert} />
                                        <form action="#" onSubmit={this.onSubmit.bind(this)}>
                                            <h3 className="text-center">Process Claim</h3>
                                            <div className="form-group">
                                                <input
                                                    type="checkbox"
                                                    name="isProcessed"
                                                    id="isProcessed"
                                                    selected={this.state.isProcessed}
                                                    onChange={this.onProcessedChange}
                                                    className="mt-10"
                                                />
                                                <span className="ml-10">I have processed this block claim request.</span>
                                                <div className="alert alert-warning mt-10">
                                                    This is an irreversible action. Please mark this processed only if you have made the payment(s) and processed everything.
                                                </div>
                                            </div>

                                            <div className="form-group">
                                                <label className="control-label text-semibold">Notes</label>
                                                <p>
                                                    You can add any relevant information here like the transaction id and so on.
                                                </p>
                                                <textarea className="form-control" name="notes" id="notes" rows="7" value={this.state.notes} onChange={this.onInputChange}></textarea>
                                            </div>

                                            <div className="form-group pull-right">
                                                <button className="btn btn-theme" type="submit">Process Block Claim</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div className="col-md-6 offset-md-1">
                                        <h3 className="text-center">Claim Information</h3>
                                        <BlockClaimsCard claim={this.state.claim} />
                                    </div>
                                </div>
                            }
                        </div>
                }
            </div>
        );
    }
}