import React, {Component} from 'react';
import {Link} from "react-router-dom";
import {Config} from "../../../Config";

export default class PageHeaderWithLink extends Component {
    constructor(props) {
        super(props);

        this.handler = this.handler.bind(this);
    }

    handler() {
        //this.props.action();
    }

    render() {
        let btnIcon = `${this.props.btnIcon} position-left`;
        let pageTitle = <div className="pull-left">
                            <h1>{this.props.title}</h1>
                        </div>;
        let helperButton = this.props.btnText != '' ?
            <Link to={this.props.btnUrl} className="btn btn-sm btn-theme">
                <i className={btnIcon}></i>
                {this.props.btnText}
            </Link>
            :
            <div></div>;

        return (
            <div className="row form-group">
                <div className="col-md-6">
                    {pageTitle}
                </div>
                <div className="col-md-6">
                    <div className="pull-right">
                        {helperButton}
                    </div>
                </div>
            </div>
        );
    }
}

PageHeaderWithLink.defaultProps = {
    btnText: '',
    btnUrl: '',
    btnIcon: ''
};