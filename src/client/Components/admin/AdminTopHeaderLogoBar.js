import React, {Component} from 'react';

export default class AdminTopHeaderLogoBar extends Component {
    render() {
        return (
            <div>
                <div className="header-menu">
                    <div className="container">
                        <div className="logo">
                            <a href="/">
                                <img src="assets/login/images/bu-banner.png" alt="BlockUnion" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}