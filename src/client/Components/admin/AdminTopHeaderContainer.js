import React, {Component} from 'react';
import AdminTopHeader from './AdminTopHeader';
import AdminTopHeaderLogoBar from './AdminTopHeaderLogoBar';

export default class AdminTopHeaderContainer extends Component {
    render() {
        return (
            <header id="header">
                <AdminTopHeader/>
                <AdminTopHeaderLogoBar/>
            </header>
        );
    }
}