import React, {Component} from 'react';
import AdminTopHeaderContainer from './AdminTopHeaderContainer';
import AdminMainPage from './AdminMainPage';
import MemberAssets from '../member/MemberAssets';

export default class AdminContainer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <div className="wrapper">
                    <AdminTopHeaderContainer/>
                    <AdminMainPage>
                        {this.props.children}
                    </AdminMainPage>
                </div>

                <MemberAssets/>
            </div>
        );
    }
}