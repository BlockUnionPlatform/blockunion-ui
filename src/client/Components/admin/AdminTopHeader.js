import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Config} from "../../Config";
import {Auth} from "../common/AuthHelpers";

export default class AdminTopHeader extends Component {
    render() {
        return (
            <div>
                <div className="header-top">
                    <div className="container">
                        <div className="header-center">
                            <ul>
                                <li>
                                    <a href="/" className="link-light">Main Website</a>
                                </li>
                                <li className="dropdown">
                                    <span className="topuser-img">
                                        <img src="assets/member/images/user-img.png" alt="" title="" />
                                    </span>
                                    <span>Welcome, {Auth.getUser().firstName}.</span>
                                    <div className="dropdown">
                                        <a className="dropbtn">
                                            <i className="fa fa-angle-down" aria-hidden="true"></i>
                                        </a>

                                        <div className="dropdown-content">
                                            <Link to={Config.RouteUrls.Profile.Member}>My Profile</Link>
                                            <a href={Config.RouteUrls.Member.Home}>Members Area</a>
                                            <a href={Config.RouteUrls.Auth.Logout}>Logout</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}