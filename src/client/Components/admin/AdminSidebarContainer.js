import React, {Component} from 'react';
import {Link} from "react-router-dom";
import {Config} from '../../Config';

export default class AdminSidebarContainer extends Component {

    render() {
        return (
            <div className="left-panel">
                <div className="left-menu-wrap">
                    <h4 className="menu-title">Actions</h4>
                    <ul>
                        <li className="Home">
                            <Link to={Config.RouteUrls.Admin.Home}>Dashboard</Link>
                        </li>

                        <li className="Inbox">
                            <Link to={Config.RouteUrls.Admin.Blocks.Home}>Blocks</Link>
                        </li>

                        <li className="Inbox">
                            <Link to={Config.RouteUrls.Admin.Packages.Home}>Packages</Link>
                        </li>

                        <li className="Inbox">
                            <Link to={Config.RouteUrls.Admin.Referrals}>Referrals</Link>
                        </li>

                        <li className="Inbox">
                            <Link to={Config.RouteUrls.Admin.DailyActions.Home}>Daily Actions</Link>
                        </li>

                        <li className="Inbox">
                            <Link to={Config.RouteUrls.Admin.Rewards.Claims.Home}>Daily Action Claims</Link>
                        </li>

                        <li className="Inbox">
                            <Link to={Config.RouteUrls.Admin.Payouts.Home}>Daily Action Payouts</Link>
                        </li>
                    </ul>

                    <h4 className="menu-title" style={{marginTop: "20px"}}>Financial</h4>
                    <ul>
                        <li className="Home">
                            <Link to={Config.RouteUrls.Admin.BlockToken.Accounts.View}>Block Accounts</Link>
                        </li>

                        <li className="Home">
                            <Link to={Config.RouteUrls.Admin.BlockToken.Transactions.Home}>Block Transactions</Link>
                        </li>

                        <li className="Inbox">
                            <Link to={Config.RouteUrls.Admin.BlockToken.Claims.Home}>Block Claims</Link>
                        </li>
                    </ul>

                    <h4 className="menu-title" style={{marginTop: "20px"}}>Data</h4>
                    <ul>
                        <li className="Home">
                            <Link to={Config.RouteUrls.Admin.Tokens.Home}>Tokens</Link>
                        </li>

                        <li className="Home">
                            <Link to={Config.RouteUrls.Admin.Rewards.Home}>Rewards</Link>
                        </li>

                        <li className="Inbox">
                            <Link to={Config.RouteUrls.Admin.Logs.Activity.Home}>Activity Logs</Link>
                        </li>

                        <li className="Inbox">
                            <Link to={Config.RouteUrls.Admin.Users.Home}>Users</Link>
                        </li>
                    </ul>

                    <h4 className="menu-title" style={{marginTop: "20px"}}>Settings</h4>
                    <ul>
                        <li className="Inbox">
                            <Link to={Config.RouteUrls.Admin.Logs.System.Home}>System Logs</Link>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}