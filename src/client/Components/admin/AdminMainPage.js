import React, {Component} from 'react';
import AdminSidebarContainer from './AdminSidebarContainer';

export default class AdminMainPage extends Component {
    render() {
        return (
            <section className="trending-section">
                <div className="container">
                    <AdminSidebarContainer/>
                    <div className="wrap-trending clr right-panel">
                        <div className="wrapper-sm">
                            {this.props.children}
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}