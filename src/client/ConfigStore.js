function getApi(endpoint) {
    const apiUrl = 'api';
    const activeVersion = 'v1';
    const url = "http://api.blockunion.io";
    //const url = "http://localhost:5100";

    return `${url}/${apiUrl}/${activeVersion}/${endpoint}`;
}

export {getApi};