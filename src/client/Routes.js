import React, {Component} from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';

import RouteNotFound from "./RouteNotFound";

import MemberHome from "./Components/member/pages/Home";
import Pro from "./Components/member/pages/Pro";
import Login from "./Components/auth/AuthLogin";
import ForgotPassword from "./Components/auth/AuthForgotPassword";
import ResetPassword from "./Components/auth/AuthResetPassword";
import UserProfileContainer from "./Components/common/UserProfileContainer";

import AdminHome from './Components/admin/pages/AdminHome';

import TokensHome from './Components/admin/pages/Tokens/TokenHome';
import TokensAddPage from './Components/admin/pages/Tokens/TokenAddPage';
import TokensEditPage from './Components/admin/pages/Tokens/TokenEditPage';

import ActivityLogsHome from './Components/admin/pages/ActivityLogs/ActivityLogsHome';
import ActivityLogsView from './Components/admin/pages/ActivityLogs/ActivityLogView';

import SystemLogsHome from './Components/admin/pages/SystemLogs/SystemLogsHome';
import SystemLogView from './Components/admin/pages/SystemLogs/SystemsLogView';

import UsersHome from './Components/admin/pages/Users/UsersHome';
import UsersAddPage from './Components/admin/pages/Users/UsersAddPage';
import UsersEditPage from './Components/admin/pages/Users/UsersEditPage';
import UsersViewPage from './Components/admin/pages/Users/UsersViewPage';

import RewardsHomePage from './Components/admin/pages/Rewards/RewardsHome';
import RewardsAddPage from './Components/admin/pages/Rewards/RewardsAddPage';
import RewardsEditPage from './Components/admin/pages/Rewards/RewardsEditPage';
import RewardsViewPage from './Components/admin/pages/Rewards/RewardsViewPage';

import RewardClaimsHome from './Components/admin/pages/RewardClaims/RewardClaimsHome';
import RewardClaimsProcess from './Components/admin/pages/RewardClaims/RewardClaimsProcess';
import RewardClaimsView from './Components/admin/pages/RewardClaims/RewardClaimView';

import PayoutsHome from './Components/admin/pages/Payouts/PayoutsHome';
import PayoutsView from './Components/admin/pages/Payouts/PayoutsView';

import DailyActionsHome from './Components/admin/pages/DailyActions/DailyActionsHome';
import DailyActionsAdd from './Components/admin/pages/DailyActions/DailyActionsAdd';
import DailyActionEdit from './Components/admin/pages/DailyActions/DailyActionEdit';

import ReferralsHome from './Components/admin/pages/Referrals/ReferralsHome';
import ReferralsView from './Components/admin/pages/Referrals/ReferralsView';

import BlocksHome from './Components/admin/pages/Blocks/BlocksHome';
import BlocksViewPage from './Components/admin/pages/Blocks/BlocksViewPage';

import BlockPackagesHome from './Components/admin/pages/Packages/BlockPackagesHome';
import BlockPackagesAdd from './Components/admin/pages/Packages/BlockPackagesAdd';
import BlockPackagesEdit from './Components/admin/pages/Packages/BlockPackagesEdit';

import BlockAccountsHome from './Components/admin/pages/BlockAccounts/BlockAccountsHome';
import BlockAccountsAdd from './Components/admin/pages/BlockAccounts/BlockAccountsAdd';
import BlockAccountsEdit from './Components/admin/pages/BlockAccounts/BlockAccountsEdit';
import BlockAccountsView from './Components/admin/pages/BlockAccounts/BlockAccountsView';

import BlockTransactionsHome from './Components/admin/pages/BlockTransactions/BlockTransactionsHome';
import BlockTransactionsAdd from './Components/admin/pages/BlockTransactions/BlockTransactionsAdd';
import BlockTransactionsView from './Components/admin/pages/BlockTransactions/BlockTransactionsView';

import BlockClaimsHome from './Components/admin/pages/BlockClaims/BlockClaimsHome';
import BlockClaimsProcess from './Components/admin/pages/BlockClaims/BlockClaimsProcess';
import BlockClaimView from './Components/admin/pages/BlockClaims/BlockClaimView';

import BlocksAdminAdd from './Components/admin/pages/Blocks/BlocksAdminAdd';
import BlocksAdminEdit from './Components/admin/pages/Blocks/BlocksAdminEdit';
import BlockStatusPage from './Components/admin/pages/Blocks/BlockStatus';

import BlocksMemberHome from './Components/member/pages/Blocks/BlocksMemberHome';
import MemberReferralsHome from './Components/member/pages/Referrals/MemberReferralsHome';
import MemberPaymentsHome from './Components/member/pages/Payments/MemberPaymentsHome';

import {Config} from './Config';
import AuthLogout from "./Components/auth/AuthLogout";
import AuthRegister from "./Components/auth/AuthRegister";

import BlocksAdd from "./Components/member/pages/Blocks/BlocksAdd";
import BlocksPayouts from "./Components/member/pages/Blocks/BlocksPayouts";
import BlocksClaims from "./Components/member/pages/Blocks/BlockClaims";
import DailyActionClaims from "./Components/member/pages/DailyActions/DailyActionClaims";
import DailyActionPayouts from "./Components/member/pages/DailyActions/DailyActionPayouts";
import DailyActionPayoutsView from "./Components/member/pages/DailyActions/DailyActionPayoutsView";
import DailyActionClaimsView from "./Components/member/pages/DailyActions/DailyActionClaimsView";
import BlocksPayoutView from "./Components/member/pages/Blocks/BlocksPayoutView";
import BlockClaimsView from "./Components/member/pages/Blocks/BlockClaimsView";
import DailyActionsView from "./Components/member/pages/DailyActions/DailyActionsView";
import BlockPublicPage from "./Components/block/BlockPublicPage";
import TermsAndConditions from "./Components/member/pages/Misc/TermsAndConditions";
import PrivacyPolicy from "./Components/member/pages/Misc/PrivacyPolicy";
import TokenView from "./Components/admin/pages/Tokens/TokenView";

class Routes extends Component {
    constructor(props){
        super(props);
    }

    render() {
        return (
            <div>
                <Switch>
                    <Route exact path={Config.RouteUrls.Member.Blocks.Add} component={BlocksAdd}/>
                    <Route exact path={Config.RouteUrls.Member.Misc.Terms} component={TermsAndConditions}/>
                    <Route exact path={Config.RouteUrls.Member.Misc.Privacy} component={PrivacyPolicy}/>
                    <Route exact path={`${Config.RouteUrls.Member.Blocks.View}/:id`} component={BlockPublicPage}/>
                    <Route exact path={Config.RouteUrls.Member.Blocks.Payouts} component={BlocksPayouts}/>
                    <Route exact path={`${Config.RouteUrls.Member.Blocks.Payouts}/:id`} component={BlocksPayoutView}/>
                    <Route exact path={Config.RouteUrls.Member.Blocks.Claims} component={BlocksClaims}/>
                    <Route exact path={`${Config.RouteUrls.Member.Blocks.Claims}/:id`} component={BlockClaimsView}/>
                    <Route exact path={Config.RouteUrls.Member.DailyActionClaims} component={DailyActionClaims}/>
                    <Route exact path={`${Config.RouteUrls.Member.DailyActionClaims}/:id`} component={DailyActionClaimsView}/>
                    <Route exact path={Config.RouteUrls.Member.DailyActionPayouts} component={DailyActionPayouts}/>
                    <Route exact path={`${Config.RouteUrls.Member.DailyActions.Home}/:id`} component={DailyActionsView}/>
                    <Route exact path={`${Config.RouteUrls.Member.DailyActionPayouts}/:id`} component={DailyActionPayoutsView}/>

                    <Route exact path={Config.RouteUrls.Member.Home} component={MemberHome}/>
                    <Route exact path={Config.RouteUrls.Member.Blocks.Home} component={BlocksMemberHome}/>
                    <Route exact path={Config.RouteUrls.Member.Referrals} component={MemberReferralsHome}/>
                    <Route exact path={Config.RouteUrls.Member.Payments} component={MemberPaymentsHome}/>
                    <Route exact path={Config.RouteUrls.Member.Pro} component={Pro}/>
                    <Redirect exact from="/auth" to="/auth/login"/>
                    <Route exact path={Config.RouteUrls.Auth.Login} component={Login} />
                    <Route exact path={Config.RouteUrls.Auth.Logout} component={AuthLogout} />
                    <Route exact path={Config.RouteUrls.Auth.Register} component={AuthRegister} />
                    <Route exact path={Config.RouteUrls.Auth.ForgotPassword} component={ForgotPassword} />
                    <Route exact path={`${Config.RouteUrls.Auth.ResetPassword}/:code`} component={ResetPassword} />
                    <Route exact path={Config.RouteUrls.Admin.Home} component={AdminHome} />
                    <Route exact path={Config.RouteUrls.Profile.Member} component={UserProfileContainer} />

                    <Route exact path={Config.RouteUrls.Admin.Tokens.Home} component={TokensHome} />
                    <Route exact path={Config.RouteUrls.Admin.Tokens.Add} component={TokensAddPage} />
                    <Route exact path={Config.RouteUrls.Admin.Tokens.EditWithId} component={TokensEditPage} />
                    <Route exact path={`${Config.RouteUrls.Admin.Tokens.View}/:id`} component={TokenView} />

                    <Route exact path={Config.RouteUrls.Admin.Logs.Activity.Home} component={ActivityLogsHome} />
                    <Route exact path={Config.RouteUrls.Admin.Logs.Activity.ViewWithId} component={ActivityLogsView} />

                    <Route exact path={Config.RouteUrls.Admin.Logs.System.Home} component={SystemLogsHome} />
                    <Route exact path={Config.RouteUrls.Admin.Logs.System.ViewWithId} component={SystemLogView} />

                    <Route exact path={Config.RouteUrls.Admin.Users.Home} component={UsersHome} />
                    <Route exact path={Config.RouteUrls.Admin.Users.Add} component={UsersAddPage} />
                    <Route exact path={`${Config.RouteUrls.Admin.Users.Edit}/:id`} component={UsersEditPage} />
                    <Route exact path={`${Config.RouteUrls.Admin.Users.View}/:id`} component={UsersViewPage} />

                    <Route exact path={Config.RouteUrls.Admin.Rewards.Home} component={RewardsHomePage} />
                    <Route exact path={Config.RouteUrls.Admin.Rewards.Add} component={RewardsAddPage} />
                    <Route exact path={`${Config.RouteUrls.Admin.Rewards.Edit}/:id`} component={RewardsEditPage} />
                    <Route exact path={`${Config.RouteUrls.Admin.Rewards.View}/:id`} component={RewardsViewPage} />

                    <Route exact path={Config.RouteUrls.Admin.Rewards.Claims.Home} component={RewardClaimsHome} />
                    <Route exact path={`${Config.RouteUrls.Admin.Rewards.Claims.Process}/:id`} component={RewardClaimsProcess} />
                    <Route exact path={`${Config.RouteUrls.Admin.Rewards.Claims.View}/:id`} component={RewardClaimsView} />

                    <Route exact path={Config.RouteUrls.Admin.Payouts.Home} component={PayoutsHome} />
                    <Route exact path={`${Config.RouteUrls.Admin.Payouts.View}/:id`} component={PayoutsView} />

                    <Route exact path={Config.RouteUrls.Admin.DailyActions.Home} component={DailyActionsHome} />
                    <Route exact path={Config.RouteUrls.Admin.DailyActions.Add} component={DailyActionsAdd} />
                    <Route exact path={`${Config.RouteUrls.Admin.DailyActions.Edit}/:id`} component={DailyActionEdit} />

                    <Route exact path={Config.RouteUrls.Admin.Referrals} component={ReferralsHome} />
                    <Route exact path={`${Config.RouteUrls.Admin.Referrals}/:id`} component={ReferralsView} />

                    <Route exact path={Config.RouteUrls.Admin.Blocks.Home} component={BlocksHome} />
                    <Route exact path={`${Config.RouteUrls.Admin.Blocks.View}/:id`} component={BlocksViewPage} />
                    <Route exact path={`${Config.RouteUrls.Admin.Blocks.Status}/:id`} component={BlockStatusPage} />

                    <Route exact path={Config.RouteUrls.Admin.Packages.Home} component={BlockPackagesHome} />
                    <Route exact path={Config.RouteUrls.Admin.Packages.Add} component={BlockPackagesAdd} />
                    <Route exact path={`${Config.RouteUrls.Admin.Packages.Edit}/:id`} component={BlockPackagesEdit} />

                    <Route exact path={Config.RouteUrls.Admin.BlockToken.Accounts.View} component={BlockAccountsHome} />
                    <Route exact path={Config.RouteUrls.Admin.BlockToken.Accounts.Add} component={BlockAccountsAdd} />
                    <Route exact path={`${Config.RouteUrls.Admin.BlockToken.Accounts.Edit}/:id`} component={BlockAccountsEdit} />
                    <Route exact path={`${Config.RouteUrls.Admin.BlockToken.Accounts.View}/:id`} component={BlockAccountsView} />

                    <Route exact path={Config.RouteUrls.Admin.BlockToken.Transactions.Home} component={BlockTransactionsHome} />
                    <Route exact path={Config.RouteUrls.Admin.BlockToken.Transactions.Add} component={BlockTransactionsAdd} />
                    <Route exact path={`${Config.RouteUrls.Admin.BlockToken.Transactions.View}/:id`} component={BlockTransactionsView} />

                    <Route exact path={Config.RouteUrls.Admin.BlockToken.Claims.Home} component={BlockClaimsHome} />
                    <Route exact path={`${Config.RouteUrls.Admin.BlockToken.Claims.Process}/:id`} component={BlockClaimsProcess} />
                    <Route exact path={`${Config.RouteUrls.Admin.BlockToken.Claims.View}/:id`} component={BlockClaimView} />

                    <Route exact path={Config.RouteUrls.Admin.Blocks.Add} component={BlocksAdminAdd} />
                    <Route exact path={`${Config.RouteUrls.Admin.Blocks.Edit}/:id`} component={BlocksAdminEdit} />

                    <Route component={RouteNotFound}></Route>
                </Switch>
            </div>
        );
    }
}

Routes.propTypes = {};
Routes.defaultProps = {};

export default Routes;

