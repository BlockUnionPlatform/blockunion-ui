
<a name="0.1"></a>
## 0.1

> 2018-08-24

### Add

* Add custom routing to the web app

### Added

* Added footer component to rating main page
* Added AdBlock component to display Ads
* Added HoursOfOperations and CustomerRating components
* Added RatingReason component
* Added AdBanner, BlockIntro and Voting Poll components
* Added block page's sidebar business info component
* Added Contact Box for the block page
* Added Rating and Breadcrumbs to block page
* Added pages for member's portal
* Added Top Header Components for Ratings Page
* Added ICOs and Trending sections with IcoItem Component
* Added services list on home page
* Added Daily Action Components
* Added Sidebar as the base component and adjusted routing accordingly
* Added Sidbar Components
* Added footer components
* Added Button Components
* Added Top Header Components
* Added assets for the user interface

### Dynamically

* Dynamically load assets depending on the route
* Dynamically render the block service items

### Fixed

* Fixed logo references
* Fixed styling for the main section in members portal

### Initial

* Initial Commit

### Rearranged

* Rearranged the assets rending to load them properly

### Refactor

* Refactor styling for header

### Refactorings

* Refactorings to overall look and feel

### Reloacted

* Reloacted the members components directory

