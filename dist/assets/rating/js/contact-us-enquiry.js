﻿jQuery(document).ready(function () 
{
	jQuery('.csubmit-btn').click(function()
	{
		  var validflag = true;
		//Name 
				if(jQuery('#name').val().trim()=="" )
				{
					$('#name').css({'border':'1px solid #ff0000'});
					$("#error").show();
					validflag = false;
				}
				else
				{
					if(jQuery('#name').val())
					{
						var name=$("#name").val();
						if(!(name.match( /^[a-zA-Z ]{2,30}$/)))
						{
							$('#name').css({'border':'1px solid #ff0000'});
							validflag = false;
						}
						else
						{
							$('#name').css({'color':'#ccc'});
							$("#error").hide();
						}
					}
					
				}
		//Name1
				if(jQuery('#name1').val().trim()=="" )
				{
					$('#name1').css({'border':'1px solid #ff0000'});
					$("#error").show();
					validflag = false;
				}
				else
				{
					if(jQuery('#name1').val())
					{
						var name=$("#name1").val();
						if(!(name.match( /^[a-zA-Z ]{2,30}$/)))
						{
							$('#name1').css({'border':'1px solid #ff0000'});
							validflag = false;
						}
						else
						{
							$('#name1').css({'color':'#ccc'});
							$("#error").hide();
						}
					}
					
				}

				if(!validflag)
				{	
					setTimeout(function() { jQuery("#error").hide(); }, 10000);
					return validflag;
				}
				else
				{
					jQuery('#ajaxLoader').show();
					sendquickMailFunc2();
					return false;
				}
		
	});
			 
			 // Focus and blure 
				jQuery('#name').focus(function()
				{
					$(this).css({'border-color':'#000000'});
					$("#error").hide();
				});
				jQuery('#name').blur(function()
				{
					$(this).css({'border-color':'#e2e2e2'});
				});
				
				jQuery('#name1').focus(function()
				{
					$(this).css({'border-color':'#000000'});
					$("#error").hide();
				});
				jQuery('#name1').blur(function()
				{
					$(this).css({'border-color':'#e2e2e2'});
				});
				
				
	});


				function sendquickMailFunc2()
				{
	
					var data = $("#contact-enquiry-form").serialize()+'&frmcontact=submit';
					$.post('contact-us-enquiry.php', data).done(function(response) 
					{	
						if(response == 1) 
						{
							$("#contact-enquiry-form")[0].reset();
							var oldcolor = $("#quicksubmit").css('color');
							$("#result").css({'display':'block','color':'#003300','border':'1px solid #003300','margin-top':'20px','padding':'10px 0','text-align': 'center'});
							$("#result").text('Thank you for your message. It has been sent.' );
							jQuery('#ajaxLoader').hide();
							setTimeout(function(){ $("#quicksubmit").val('Submit');
							$("#quicksubmit").css('color',oldcolor);
							$("#result").css('display','none');}, 6000);
						}
						else 
						{
							//alert('response' );
							$("#contact-enquiry-form")[0].reset();
							var oldcolor = $("#quicksubmit").css('color');
							$("#result").css({'display':'block','color':'#ff0000'});
							$("#result").text('Failed!!');
							jQuery('#ajaxLoader').hide();
							setTimeout(function(){ $("#quicksubmit").val('Submit');
							$("#quicksubmit").css('color',oldcolor);
							$("#result").css('display','none');}, 3000);		
						}
					});
				}
			
		
