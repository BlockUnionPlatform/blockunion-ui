$(document).ready(function() {
    $(".fancybox").fancybox({
        openEffect  : 'none',
        closeEffect : 'true',
        nextEffect  : 'none',
        prevEffect  : 'none',
    });



    var $wrapper = $('.tab-wrapper'),
        $allTabs = $wrapper.find('.tab-content > div'),
        $tabMenu = $wrapper.find('.tab-menu li'),
        $line = $('<div class="line"></div>').appendTo($tabMenu);
    
    $allTabs.not(':first-of-type').hide();  
    $tabMenu.filter(':first-of-type').find(':first').width('100%')
    
    $tabMenu.each(function(i) {
        $(this).attr('data-tab', 'tab'+i);
    });
    
    $allTabs.each(function(i) {
        $(this).attr('data-tab', 'tab'+i);
    });
    
    $tabMenu.on('click', function() {
        
        var dataTab = $(this).data('tab'),
            $getWrapper = $(this).closest($wrapper);
        
        $getWrapper.find($tabMenu).removeClass('active');
        $(this).addClass('active');
        
        $getWrapper.find('.line').width(0);
        $(this).find($line).animate({'width':'100%'}, 'fast');
        $getWrapper.find($allTabs).hide();
        $getWrapper.find($allTabs).filter('[data-tab='+dataTab+']').show();
    });
    
});

$(document).ready(function(){
    $('.menu').click(function(){
        $('html').toggleClass('show-menu');
    });
    $('.close-button').click(function(){
        $('html').removeClass('show-menu');
    });
    $(".menu-act ul li a").each(function(){
    var pathname1 = window.location.href.substr(window.location.href.lastIndexOf('/') + 1);
        var pathname = pathname1.replace("#/", "");
        if ( $(this).attr('href') == pathname) { 
            $(this).parent().addClass('active');
        } 
    });

    $(".menu-act li ul.dropdown-menu li a").each(function(){
        var pathname1 = window.location.href.substr(window.location.href.lastIndexOf('/') + 1);
    // alert(pathname1);
        var pathname = pathname1.replace("#/", "");
        if ( $(this).attr('href').indexOf(pathname1) > -1) { 
        $(this).parent().addClass('active');
        $(this).parent().parent().parent().addClass('active');
    } 
    });
});

$(document).ready(function(){
    $(window).scroll(function(){
    var sticky = $('#wrapper'),
        scroll = $(window).scrollTop();

    if (scroll >= 10) sticky.addClass('fixed');
    else sticky.removeClass('fixed');
    });
});

$(document).ready(function() {
    // hide #back-top first
    $("#myBtn").hide();

    // fade in #back-top
    $(function() {
        $(window).scroll(function() {
            if ($(this).scrollTop() > 100) {
                $('#myBtn').fadeIn();
            } else {
                $('#myBtn').fadeOut();
            }
        });

        // scroll body to 0px on click
        $('#myBtn').click(function() {
            $('body,html').animate({
                scrollTop: 0
            }, 1000);
            return false;
        });
    });
});