$(document).ready(function () {
    $('.close-button').click(function () {
        $('body').removeClass('show-menu');
    });

    if ($(window).width() >= 768 && $(window).width() <= 1024) {
        $(window).scroll(function () {
            var sticky = $('#header'),
                scroll = $(window).scrollTop();

            if (scroll >= 101) sticky.addClass('fixed');
            else sticky.removeClass('fixed');
        });
    } else if ($(window).width() <= 767) {
        $(window).scroll(function () {
            var sticky = $('#header'),
                scroll = $(window).scrollTop();

            if (scroll >= 67) sticky.addClass('fixed');
            else sticky.removeClass('fixed');
        });
    } else {
        $(window).scroll(function () {
            var sticky = $('#header'),
                scroll = $(window).scrollTop();

            if (scroll >= 60) sticky.addClass('fixed');
            else sticky.removeClass('fixed');
        });
    }

    $(document).ready(function () {
        // hide #back-top first
        $("#myBtn").hide();

        // fade in #back-top
        $(function () {
            $(window).scroll(function () {
                if ($(this).scrollTop() > 100) {
                    $('#myBtn').fadeIn();
                } else {
                    $('#myBtn').fadeOut();
                }
            });

            // scroll body to 0px on click
            $('#myBtn').click(function () {
                $('body,html').animate({
                    scrollTop: 0
                }, 1000);
                return false;
            });
        });
    });

});

$(document).on("click", '#menu_toggle', function () {
    $('html').toggleClass('show-menu');
});

$(document).ready(function () {
    $('.member-slider').owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 1
            },
            1025: {
                items: 1
            }
        }
    })
});

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function (event) {
    if (document.getElementsByClassName('.dropbtn').length > 0) {
        if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }  
};

/*(function ($) {
    $(window).on("load", function () {

        $(".mCustomScrollbar").mCustomScrollbar({
            theme: "minimal"
        });

    });
})(jQuery);*/