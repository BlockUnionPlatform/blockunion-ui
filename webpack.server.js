const webpack = require('webpack');
const path = require('path');

const SERVER_DIR = path.resolve(__dirname, 'src/server');
const BUILD_DIR = path.resolve(__dirname, 'dist');

const merge = require('webpack-merge');
const baseConfig = require('./webpack.base.config');

const webpackNodeExternals = require('webpack-node-externals');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const config = {
    target: 'node',
    entry: SERVER_DIR + '/index.js',
    devtool: 'eval',
    mode: 'production',
    output: {
        path: BUILD_DIR,
        filename: 'bundle.server.js'
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        })
    ]
};


module.exports = merge(baseConfig, config);